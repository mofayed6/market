#!/bin/bash

if [ ! -f ./.env ]; then
    echo "Please add the .env file"
    exit
fi

docker-compose build --no-cache web
docker-compose up -d

docker-compose exec web /bin/sh -c 'mkdir /var/www/html/storage/app/public >/dev/null 2>&1'
docker-compose exec web /bin/sh -c 'mkdir /var/www/html/storage/framework/cache/data >/dev/null 2>&1'
docker-compose exec web /bin/sh -c 'chown -R www-data:www-data /var/www/html \
    && chmod 777 -R /var/www \
    && rm -f /var/www/html/public/storage \
    && composer install -d /var/www/html \
    && composer install -d /var/www/html/data-import-tool \
    && composer dump-autoload -d /var/www/html \
    && composer dump-autoload -d /var/www/html/data-import-tool \
    && php /var/www/html/artisan key:generate \
    && php /var/www/html/artisan storage:link \
    && php /var/www/html/artisan optimize:clear \
    && php /var/www/html/artisan migrate:fresh --seed'
docker-compose stop
