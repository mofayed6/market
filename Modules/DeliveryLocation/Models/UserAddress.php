<?php

namespace Modules\DeliveryLocation\Models;

use App\Http\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;
use Modules\Locations\Models\Location;
use Modules\User\Models\User;

class UserAddress extends Model
{
   protected $fillable = [
      'user_id', 'country_id', 'city_id', 'district_id',
      'address', 'notes', 'apartment_number', 'building_number',
      'phone_number', 'is_default', 'lng', 'lat'
   ];
   protected $table = 'user_addresses';

   public function User()
   {
      return $this->belongsTo(User::class);
   }

   public function country()
   {
      return $this->belongsTo(Location::class, 'country_id');
   }

   public function city()
   {
      return $this->belongsTo(Location::class, 'city_id');
   }

   public function district()
   {
      return $this->belongsTo(Location::class, 'district_id');
   }

}
