<?php

namespace Modules\DeliveryLocation\Models;

use App\Http\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;
use Modules\Branches\Models\Branch;
use Modules\Locations\Models\Location;
use Modules\User\Models\User;

class DeliveryLocation extends Model
{
    use TranslatableTrait;

    protected $fillable = ['name'];
   // protected $translatable = ['name'];
   // protected $appends = ['name'];

    public function children()
    {
        return $this->belongsTo(DeliveryLocation::class, 'parent_id');
    }

    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
    }



    public function parent()
    {
        $this->depth++;
        return $this->belongsTo(DeliveryLocation::class, 'parent_id');
    }

    public function parentRecursive()
    {
        return $this->parent()->with('parentRecursive');
    }


    public static function getParentsFlat($parent = null)
    {
        $items = DeliveryLocation::with('parentRecursive')->where('id', $parent)->first();
        if (!$items) {
            return [];
        }
        $items = $items->toArray();
        $items = (new self)->recursiveParentToFlat($items, 'parent_recursive', 0);
        $items = array_reverse($items, true);
        unset($items[$parent]);
        //array_pop($items);

        return $items;
    }

    protected function recursiveParentToFlat($item, $recursive_key = 'children_recursive', $i)
    {
        if (count($item) == 0)
            $results = [];
        $results[$item['id']] = $item['name'];
        if (is_array($item[$recursive_key]) && count($item[$recursive_key]) > 0) {
            $i++;
            $results = $results + $this->recursiveParentToFlat($item[$recursive_key], $recursive_key, $i);
        }
        return $results;
    }



    public static function getChildrenFlat($parent = null, $maxDepth = 3)
    {
        $items = self::with('childrenRecursive')->where('parent_id', $parent)->get()->toArray();
        $items = (new self)->recursiveChildrenToFlat($items, '-', 1, $maxDepth);
        return $items;
    }

    protected function recursiveChildrenToFlat($array, $dashes = "-", $depth = 1, $maxDepth = 4, $recursive_key = 'children_recursive')
    {
        $results = [];
        if ($depth <= $maxDepth) {
            foreach ($array as $k => $item) {
                $results[$item['id']] = $dashes . ' ' . $item['name'];
                if (is_array($item[$recursive_key]) && count($item[$recursive_key]) > 0) {
                    $results = $results + $this->recursiveChildrenToFlat($item[$recursive_key], '-' . $dashes, ++$depth, $maxDepth);
                }
            }
        }
        return $results;
    }


    public static function getAll()
    {
       // $results = [];
        $countries = Location::with('children.children')->where('parent_id', 0)->get();
       //dd($countries);
        foreach ($countries as $key => $country) {
           //dd(json_decode($country['name'],true));
            $results[$key]['id'] = $country->id;
            $results[$key]['name'] = json_decode($country['name'], true)['en'];

            foreach ($country->children as $x => $city) {
                $results[$key]['cities'][$x]['id'] = $city->id;
                $results[$key]['cities'][$x]['name'] = json_decode($city['name'], true)['en'];

                foreach ($city->children as $z => $district) {
                    $results[$key]['cities'][$x]['districts'][$z]['id'] = $district->id;
                    $results[$key]['cities'][$x]['districts'][$z]['name'] = json_decode($district['name'], true)['en'];
                }
            }

        }

        return $results;
    }


//    public function getNameAttribute()
//    {
//        return $this->en['name'];
//    }



//    public static function CalcolateDistance($branch_id, $user_id, $address_id = 0)
//    {
//        $branch = Branch::find($branch_id);
//
//        $userQuery = UserAddress::where('user_id', $user_id);
//        if (!empty($address_id)) {
//            $userQuery->where('id', $address_id);
//        } else {
//            $userQuery->where('is_default', 1);
//        }
//
//        $user = $userQuery->first();
//
//        return $distance = (new self)->GetDrivingDistance($branch->latitude, $branch->longitude, $user->lat, $user->lng);
//
//    }

    public static function calcolateMony($branch, $distract )
    {

        $price = DeliveryLocation::where([['branch_id',$branch],['district_id',$distract]])->first();

        return $price['price'];
    }



//    public static function calcolateMonyBranch($branch, $distric_id)
//    {
//        $userQuery = UserAddress::where('user_id', $user);
//
//        if (!empty($address_id)) {
//            $userQuery->where('id', $address_id);
//        } else {
//            $userQuery->where('is_default', 1);
//        }
//
//        $user = $userQuery->first();
//
//        $price = PriceDelivary::where('min', '<', $distance['distance'])->where('max', '>', $distance['distance'])->first();
//        if (empty($price)) {
//            $price = PriceDelivary::orderBy('max', 'desc')->first();
//
//            if ($price->max > $distance['distance']) {
//                $price = PriceDelivary::orderBy('min', 'asc')->first();
//            }
//        }
//
//        return $price->price;
//    }




    function GetDrivingDistance($lat1, $lat2, $long1, $long2)
    {
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $lat1 . "," . $long1 . "&destinations=" . $lat2 . "," . $long2 . "&mode=driving&language=en&key=AIzaSyDvt4xYX0QycPedzqGKJ7_1sg6KH_iztDA";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);

        $returnData = [];
        if (!empty($response_a['rows'][0]['elements'][0]['distance']['text'])) {
            $returnData['distance'] = intval($response_a['rows'][0]['elements'][0]['distance']['text']);
        }

        if (!empty($response_a['rows'][0]['elements'][0]['duration']['text'])) {
            $returnData['time'] = $response_a['rows'][0]['elements'][0]['duration']['text'];
        }

        if (!empty($returnData)) {
            return $returnData;
        } else {
            return array('distance' => rand(0, 99), 'time' => rand(10, 48));
        }
    }


}
