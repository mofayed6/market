@extends('admin.layouts.master')
@section('page-title','DeliveryLocations')
@section('breadcrumb')
    <li class="breadcrumb-item {{($parent == null)?"active":""}}">@if($parent != null)<a href='{{url('admin/deliverylocations')}}'>DeliveryLocations</a>@else DeliveryLocations @endif</li>
    @php($parents = \Modules\DeliveryLocation\Models\DeliveryLocation::getParentsFlat($parent))
    @if($parent != null)
        @foreach($parents as $id=>$name)
            <li class="breadcrumb-item {{($parent == $id)?'active':''}}">
                @if($parent != $id)
                    <a href="{{url('admin/deliverylocations/'.$id)}}">{{$name}}</a>
                @else
                    {{$name}}
                @endif
            </li>
        @endforeach
    @endif
@endsection
@section('content')


    <div class="card">
        <div class="header">
            {{--@if(@$parents && count($parents)< 4 || $parent == null)--}}
                <a href="{{url('admin/deliverylocations/create/'.$parent)}}" class="btn btn-primary pull-right"><i class="fa fa-plus-square"></i> <span>Add New</span></a>
            {{--@endif--}}
            <h2>DeliveryLocation list</h2>
        </div>
        <div class="body">

            @include('admin.pratical.message')
            <div class="table-responsive">
                <table class="table table-hover m-b-0 c_list datatable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>


@endsection
@section('scripts')
    <script>
        $(function() {
            $('.datatable').DataTable({
                processing: true,
                searching: true,
                serverSide: true,
                ajax: '{!! url('admin/deliverylocations/datatable/?parent='.$parent) !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'options', name: 'options' },
                ],
            });
        });


    </script>
@stop
