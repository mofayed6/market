<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('price');
//            $table->integer('country_id')->unsigned();
//            $table->integer('city_id')->unsigned();
            $table->integer('district_id')->unsigned();
            $table->integer('branch_id')->unsigned();
//            $table->unsignedInteger('parent_id')->default(0)->nullable();
            $table->timestamps();
        });

        Schema::table('delivery_locations', function (Blueprint $table) {
            //foreign key constrains
//            $table->foreign('country_id')->references('id')->on('locations')->onDelete('cascade');
//            $table->foreign('city_id')->references('id')->on('locations')->onDelete('cascade');
            $table->foreign('district_id')->references('id')->on('locations')->onDelete('cascade');
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_locations');
    }
}
