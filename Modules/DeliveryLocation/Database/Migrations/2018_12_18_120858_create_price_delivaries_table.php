<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceDelivariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_delivaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('delivery_location_id')->unsigned();
            $table->smallInteger('max')->unsigned();
            $table->smallInteger('min')->unsigned();
            $table->decimal('price')->unsigned();
            $table->timestamps();
        });

        Schema::table('price_delivaries', function (Blueprint $table) {
            //foreign key constrains
            $table->foreign('delivery_location_id')->references('id')->on('delivery_locations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_delivaries');
    }
}
