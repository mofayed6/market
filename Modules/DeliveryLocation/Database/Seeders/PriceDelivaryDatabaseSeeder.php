<?php

namespace Modules\DeliveryLocation\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\DeliveryLocation\Models\PriceDelivary;
use Modules\DeliveryLocation\Models\DeliveryLocation;

class PriceDelivaryDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $deliveryLocations = DeliveryLocation::limit(5)->get();
        foreach ($deliveryLocations as $deliveryLocation) {
            for ($i = 1; $i <= 100; $i += 10) {
                PriceDelivary::create([
                    'delivery_location_id' => $deliveryLocation->id,
                    'min' => $i - 1,
                    'max' => $i + 8,
                    'price' => $i * 10
                ]);
            }
        }
        // $this->call("OthersTableSeeder");
    }
}
