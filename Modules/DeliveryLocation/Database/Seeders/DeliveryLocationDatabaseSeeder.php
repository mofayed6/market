<?php

namespace Modules\DeliveryLocation\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\DeliveryLocation\Models\DeliveryLocation;
use Modules\Locations\Models\Location;

class DeliveryLocationDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $countries = Location::where(['parent_id' => 0])->with('children.children')->get();

        for ($i = 1; $i <= 5; ++$i) {
            $country = $countries->shift();
            DeliveryLocation::create([
                'district_id' => $country->children()->first()->children()->first()->id,
                'branch_id' => $i,
                'price' => $i + 20,
            ]);

        }

        //  $this->call("OthersTableSeeder");
    }
}
