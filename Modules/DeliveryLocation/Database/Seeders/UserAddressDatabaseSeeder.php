<?php

namespace Modules\DeliveryLocation\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\DeliveryLocation\Models\UserAddress;
use Modules\User\Models\User;
use Modules\Locations\Models\Location;

class UserAddressDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $users = User::get();
        $countries = Location::where(['parent_id' => 0])->with('children.children')->get();
        $i = 0;
        foreach ($users as $user) {
            foreach ($countries as $country) {
                $i = $i + 0.1;
                UserAddress::create([
                    'country_id' => $country->id,
                    'city_id' => $country->children()->first()->id,
                    'district_id' => $country->children()->first()->children()->first()->id,
                    'user_id' => $user->id,
                    'lat' => 29.8206 + $i,
                    'lng' => 30.8206 + $i,
                    'address' => "Address {$country->id}",
                    'is_default' => !empty($country->id == 1) ? 1 : 0,
                ]);
            }
        }
    }
}
