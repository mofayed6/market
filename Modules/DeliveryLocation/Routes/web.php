<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::middleware('admin:admin')->prefix('admin/deliverylocations')->group(function () {
    Route::get('datatable', 'DeliveryLocationController@datatable');
    Route::get('create/{parent?}', 'DeliveryLocationController@create');
    Route::get('/{category}/delete', 'DeliveryLocationController@destroy');
    Route::get('/{category}/edit', 'DeliveryLocationController@edit');
    Route::patch('/{category}/update', 'DeliveryLocationController@update');
    Route::get('/{parent?}', 'DeliveryLocationController@index');
    Route::post('/', 'DeliveryLocationController@store')->name('store.delivery');
//    Route::get('slugify/{string}/{locale}','CategoriesController@slugify');
});

Route::middleware('auth')->prefix('{locale}/shipping-addresses')->name('user_addresses.')->group(function () {
    Route::get('change-primary-address/{address_id}', 'UserAddressesSiteController@changePrimaryAddress')->name('changePrimaryAddress');
    Route::post('address-store', 'UserAddressesSiteController@store')->name('store');
    Route::post('address-update', 'UserAddressesSiteController@update')->name('update');
    Route::post('address-remove/{address_id}', 'UserAddressesSiteController@remove')->name('remove');
    Route::get('/', 'UserAddressesSiteController@show')->name('show');
});
