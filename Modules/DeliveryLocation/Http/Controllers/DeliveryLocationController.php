<?php

namespace Modules\DeliveryLocation\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Branches\Models\Branch;
use Modules\DeliveryLocation\Models\DeliveryLocation;
use Modules\DeliveryLocation\Models\PriceDelivary;

class DeliveryLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($parent = null)
    {
        if ($parent) {
            $category = DeliveryLocation::findOrFail($parent);
        }

        return view('deliverylocation::index', compact('parent'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
//        $parentLocation = null;
//        if ($parent != null) {
//            $parentLocation = DeliveryLocation::findOrFail($parent);
//        }
       // $branches = Branch::all()->pluck('name', 'id');
       // dd($branches);

        $branches = Branch::get();
        foreach ($branches as $branch) {

            $selected_types = json_decode($branch->name, true);

            $selects_types[$branch->id] = $selected_types[app()->getLocale()];
        }

        $locations = DeliveryLocation::getAll();
        return view('deliverylocation::create', compact('locations', 'branches', 'selects_types'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'en.name' => 'required',
            'ar.name' => 'required',
            'country_id' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
            'branch_id' => 'required',
        ]);
        $name = json_encode(['ar' => $request->ar['name'], 'en' => $request->en['name']], JSON_UNESCAPED_UNICODE);

        $delivery = new DeliveryLocation();
        $delivery->name = $name;
        $delivery->country_id = $request->country_id;
        $delivery->city_id = $request->city_id;
        $delivery->district_id = $request->district_id;
        $delivery->branch_id = $request->branch_id;
        $delivery->parent_id = $request->parent_id;
        $saveData = $delivery->save();
       // $delivery->translateAttributes($request->only(['ar', 'en']));


        if ($saveData) {

            $price = new PriceDelivary();
            $price->delivery_location_id = $delivery->id;
            $price->min = $request->min;
            $price->max = $request->max;
            $price->price = $request->price;
            $price->save();

        }

        return redirect('admin/deliverylocations/' . $delivery->parent_id)->with(['success' => 'deliveryLocations created successfully']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('deliverylocation::show');
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {

        $deliveryLocation = DeliveryLocation::find($id);
       // dd($deliveryLocation);
        $name = json_decode($deliveryLocation['name'], true);
       // dd($name);

        $deliveryLocations = DeliveryLocation::getChildrenFlat(null, 3);
        $parent = $deliveryLocation->parent_id;
      //  $branches = Branch::pluck('name', 'id')->toArray();
      //  dd($branches);

        $branches = Branch::get();
        foreach ($branches as $branch) {

            $selected_types = json_decode($branch->name, true);

            $selects_types[$branch->id] = $selected_types[app()->getLocale()];
        }

        $locations = DeliveryLocation::getAll();
        $price = PriceDelivary::where('delivery_location_id', $id)->first();
        return view('deliverylocation::edit', compact('deliveryLocation', 'branches', 'selects_types', 'name', 'price', 'locations', 'deliveryLocations', 'parent'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {

        $deliveryLocation = DeliveryLocation::find($id);
        $request->validate([
            'en.name' => 'required',
            'ar.name' => 'required',

        ]);
        $name = json_encode(['ar' => $request->ar['name'], 'en' => $request->en['name']], JSON_UNESCAPED_UNICODE);

        $deliveryLocation->name = $name;

        $deliveryLocation->country_id = $request->country_id;
        $deliveryLocation->city_id = $request->city_id;
        $deliveryLocation->district_id = $request->district_id;
        $deliveryLocation->branch_id = $request->branch_id;
        $deliveryLocation->parent_id = $request->parent_id;
        $deliveryLocation->update();
        //$deliveryLocation->deleteTranslations();
       // $deliveryLocation->translateAttributes($request->only('ar', 'en'));


        $price = PriceDelivary::where('delivery_location_id', $id)->first();
        $price->min = $request->min;
        $price->max = $request->max;
        $price->price = $request->price;
        $price->update();


        return redirect(url('admin/deliverylocations/' . $deliveryLocation->parent_id))->with(['success' => 'deliveryLocation Updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $deliveryLocation = DeliveryLocation::find($id);

        $deliveryLocation->delete();
      //  $deliveryLocation->deleteTranslations();
        return redirect(url('admin/deliverylocations/' . $deliveryLocation->parent_id))->with(['success' => 'deliverylocations Deleted Successfully']);
    }

    public function datatable(Request $request)
    {
        $categories = DeliveryLocation::all();
        return \DataTables::of($categories)
            ->editColumn('name', function ($model) {
//                return "<a href='".url('admin/deliverylocations/'.$model->id)."'>".$model->name."</a>";
               // return $model->name;
                $enName = json_decode($model->name, true);
                return $enName['en'];
            })->editColumn('options', function ($model) {
                return "<a href='" . url('admin/deliverylocations/' . $model->id . '/edit') . "' class='btn btn-warning'>Edit</a>
                <a href='" . url('admin/deliverylocations/' . $model->id . '/delete') . "' class='btn btn-danger'>Delete</a>";

            })
            ->make(true);
    }


    public function calcolateMony($branch, $user, $address_id = 0)
    {
        return DeliveryLocation::calcolateMony($branch, $user, $address_id);
    }

}
