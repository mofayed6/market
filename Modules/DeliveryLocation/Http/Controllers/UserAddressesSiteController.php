<?php

namespace Modules\DeliveryLocation\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\DeliveryLocation\Models\UserAddress;
use Modules\Locations\Models\Location;
use App\Http\Controllers\BaseController;

class UserAddressesSiteController extends BaseController
{
    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($locale)
    {
        $this->viewData['user_addresses'] = UserAddress::where(['user_id' => auth()->id()])->get();
        $this->viewData['countries'] = Location::with('children.children')->where('parent_id', 0)->get();
        $this->viewData['defaultCountry'] = $this->viewData['countries']
            ->where('id', old('country_id', $this->viewData['countries']->first()->id))->first();
        $this->viewData['defaultCity'] = $this->viewData['defaultCountry']->children()
            ->where('id', old('city_id', $this->viewData['defaultCountry']->children()->first()->id))->first();
        $this->viewData['defaultDistrict'] = $this->viewData['defaultCity']->children()
            ->where('id', old('district_id', $this->viewData['defaultCity']->children()->first()->id))->first();

        return view('site.user_account.shipping_addresses', $this->viewData);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request, $locale)
    {
        $request->validate([
            'address' => 'required',
            'country_id' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
        ]);

        $address = new UserAddress();

        $address->user_id = auth()->user()->id;
        $address->country_id = $request->country_id;
        $address->city_id = $request->city_id;
        $address->district_id = $request->district_id;
        $address->lat = !empty($request->lat) ? $request->lat : '';
        $address->lng = !empty($request->lng) ? $request->lng : '';
        $address->address = $request->address;
        $address->apartment_number = !empty($request->apartment_number) ? $request->apartment_number : null;
        $address->building_number = !empty($request->building_number) ? $request->building_number : '';
        $address->phone_number = !empty($request->phone_number) ? $request->phone_number : '';
        $address->notes = !empty($request->notes) ? $request->notes : '';
        $address->is_default = !empty($request->is_default) ? 1 : 0;

        $address->save();

        return redirect()->back();
    }

    /**
     * Updates a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $locale)
    {
        $request->validate([
            'address_id' => 'required',
            'address' => 'required',
            'country_id' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
        ]);

        $address = UserAddress::where(['id' => $request->address_id, 'user_id' => auth()->id()])->firstOrFail();

        $address->country_id = $request->country_id;
        $address->city_id = $request->city_id;
        $address->district_id = $request->district_id;
        $address->lat = !empty($request->lat) ? $request->lat : '';
        $address->lng = !empty($request->lng) ? $request->lng : '';
        $address->address = $request->address;
        $address->apartment_number = !empty($request->apartment_number) ? $request->apartment_number : null;
        $address->building_number = !empty($request->building_number) ? $request->building_number : '';
        $address->phone_number = !empty($request->phone_number) ? $request->phone_number : '';
        $address->notes = !empty($request->notes) ? $request->notes : '';
        $address->is_default = !empty($request->is_default) ? 1 : 0;

        $address->save();

        return redirect()->back();
    }

    /**
     * @param  Request $request
     * @return Response
     */
    public function changePrimaryAddress(Request $request, $locale, $addressId)
    {
        $addressId = (int)$addressId;
        UserAddress::where(['user_id' => auth()->id()])->update(['is_default' => 0]);
        UserAddress::where(['id' => $addressId, 'user_id' => auth()->id()])->update(['is_default' => 1]);

        return 1;
    }

    /**
     * remove resource from storage.
     * @param  Request $request
     * @return Response
     */
    public function remove(Request $request, $locale, $addressId)
    {
        $addressId = (int)$addressId;
        UserAddress::where(['id' => $addressId, 'user_id' => auth()->id()])->delete();

        return redirect()->back();
    }
}
