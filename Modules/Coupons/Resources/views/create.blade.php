@extends('admin.layouts.master')
@section('page-title','Create Coupon')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url("admin/coupon")}}">Coupons</a></li>
    <li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')
    <div class="card">
        <div class="header">
            <div class="row">
                <div class="col-md-6">
                    <h2>Create new Coupon</h2>
                </div>
            </div>
        </div>
        <div class="body">
            @include('admin.pratical.message')
            {{Form::open(['action'=>'\Modules\Coupons\Http\Controllers\CouponsController@store'])}}
                @include('coupons::form')
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('scripts')

@stop
