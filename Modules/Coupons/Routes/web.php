<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('admin:admin')->group(function() {
    Route::get('coupons/search_products', 'CouponsController@searchProducts');
    Route::get('coupons/datatable', 'CouponsController@datatable');
    Route::get('coupons/{coupon}/delete', 'CouponsController@destroy');
    Route::resource('coupons', 'CouponsController');
});
