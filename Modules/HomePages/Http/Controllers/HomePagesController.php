<?php

namespace Modules\HomePages\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\HomePages\Models\HomePage;
use Modules\HomePages\Models\HomePageImage;

class HomePagesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $homepages = HomePage::with('images')->get();

        return view('homepages::index',compact('homepages'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('homepages::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        if($request->type === 'slider'){

           $homepage = HomePage::create([
                'type' => 'slider',
                'status' => $request->status

            ]);

            $data = [];
            foreach( $request->link1 as $k=>$t){
                $data[$k] = [ 'image'=> $request->image1[$k]  , 'link'=>$t];
            }


          foreach ($data as $k=>$v){
              HomePageImage::create([
                  'homepage_id'=> $homepage->id,
                  'image'=> $v['image']->store('homepages'),
                  'link'=>$v['link']
              ]);
          }

        }

        if($request->type === 'slidertop'){

            $homepage = HomePage::create([
                'type' => 'slidertop',
                'status' => $request->status

            ]);

            HomePageImage::create([
                'homepage_id'=> $homepage->id,
                'image'=> $request->image2->store('homepages'),
                'link'=>$request->link2
            ]);
        }

        if($request->type === 'sliderbottom'){

            $homepage = HomePage::create([
                'type' => 'sliderbottom',
                'status' => $request->status

            ]);

            HomePageImage::create([
                'homepage_id'=> $homepage->id,
                'image'=> $request->image3->store('homepages'),
                'link'=>$request->link3
            ]);
        }

        if($request->type === 'fullbanner'){

            $homepage = HomePage::create([
                'type' => 'sliderbottom',
                'status' => $request->status

            ]);

            HomePageImage::create([
                'homepage_id'=> $homepage->id,
                'image'=> $request->image5->store('homepages'),
                'link'=>$request->link5
            ]);
        }

        if($request->type === 'fullpagetrip'){

            $homepage = HomePage::create([
                'type' => 'sliderbottom',
                'status' => $request->status

            ]);

            HomePageImage::create([
                'homepage_id'=> $homepage->id,
                'image'=> $request->image6->store('homepages'),
                'link'=>$request->link6
            ]);
        }

        if($request->type === 'promotionboxwithtext'){

            $homepage = HomePage::create([
                'type' => 'promotionboxwithtext',
                'status' => $request->status

            ]);

            $data = [];
            foreach( $request->link7 as $k=>$t){
                $data[$k] = [ 'image'=> $request->image7[$k]  , 'link'=>$t ,'text'=>$request->text[$k]];
            }


            foreach ($data as $k=>$v){
                HomePageImage::create([
                    'homepage_id'=> $homepage->id,
                    'image'=> $v['image']->store('homepages'),
                    'link'=>$v['link'],
                    'text'=>$v['text']
                ]);
            }

        }


        return redirect('admin/home-pages')->with(['success'=>'homePages created successfully']);

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('homepages::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $homepages = HomePage::find($id);
        $homepageImages = HomePageImage::where('homepage_id',$id)->get();

        return view('homepages::edit',compact('homepages','homepageImages'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request ,$id )
    {
        $homepages = HomePage::find($id);


        if($request->type === 'slider'){

//            dd($request->all());
            $homepage = $homepages->update([
                'type' => 'slider',
                'status' => $request->status

            ]);



            $data = [];
            foreach( $request->link1 as $k=>$t){
                $data[$k] = [ 'image'=> $request->image1[$k]  , 'link'=>$t];
            }

          //  dd($data);

            HomePageImage::where('homepage_id',$id)->delete();

            foreach ($data as $k=>$v){
                HomePageImage::create([
                    'homepage_id'=> $id,
                    'image'=> $v['image']->store('homepages'),
                    'link'=>$v['link']
                ]);
            }

        }

        if($request->type === 'slidertop'){

            $homepage = $homepages->update([
                'type' => 'slidertop',
                'status' => $request->status

            ]);

            HomePageImage::create([
                'homepage_id'=> $homepage->id,
                'image'=> $request->image2->store('homepages'),
                'link'=>$request->link2
            ]);
        }

        if($request->type === 'sliderbottom'){

            $homepage = $homepages->update([
                'type' => 'sliderbottom',
                'status' => $request->status

            ]);

            HomePageImage::create([
                'homepage_id'=> $homepage->id,
                'image'=> $request->image3->store('homepages'),
                'link'=>$request->link3
            ]);
        }

        if($request->type === 'fullbanner'){

            $homepage = $homepages->update([
                'type' => 'sliderbottom',
                'status' => $request->status

            ]);

            HomePageImage::create([
                'homepage_id'=> $homepage->id,
                'image'=> $request->image5->store('homepages'),
                'link'=>$request->link5
            ]);
        }

        if($request->type === 'fullpagetrip'){

            $homepage = $homepages->update([
                'type' => 'sliderbottom',
                'status' => $request->status

            ]);

            HomePageImage::create([
                'homepage_id'=> $homepage->id,
                'image'=> $request->image6->store('homepages'),
                'link'=>$request->link6
            ]);
        }

        if($request->type === 'promotionboxwithtext'){

            $homepage = $homepages->update([
                'type' => 'promotionboxwithtext',
                'status' => $request->status

            ]);
            HomePageImage::where('homepage_id',$id)->delete();


            $data = [];
            foreach( $request->link7 as $k=>$t){
                $data[$k] = [ 'image'=> $request->image7[$k]  , 'link'=>$t ,'text'=>$request->text[$k]];
            }


            foreach ($data as $k=>$v){
                HomePageImage::create([
                    'homepage_id'=> $homepage->id,
                    'image'=> $v['image']->store('homepages'),
                    'link'=>$v['link'],
                    'text'=>$v['text']
                ]);
            }

        }


        return redirect('admin/home-pages')->with(['success'=>'homePages update successfully']);

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $homepages = HomePage::find($id);

        $homepages->delete();
        return redirect('admin/home-pages')->with(['success'=>'home pages deleted successfully']);
    }
}
