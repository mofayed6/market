<?php

namespace Modules\HomePages\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Products\Models\Product;
use Modules\HomePages\Models\HomePage;
use App\Http\Controllers\BaseController;

class HomePagesSiteController extends BaseController
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $this->viewData['latestProducts'] = Product::latest(15);
        $this->viewData['trendingProducts'] = Product::trending(15);

        $homePageComponents = HomePage::with('images')->where('status', 1)->get();
        $this->viewData['homePageComponent_slider'] = $homePageComponents->where('type', 'slider')->shuffle()->first();
        $this->viewData['homePageComponent_slidertop'] = $homePageComponents->where('type', 'slidertop')->shuffle()->first();
        $this->viewData['homePageComponent_sliderbottom'] = $homePageComponents->where('type', 'sliderbottom')->shuffle()->first();
        $this->viewData['homePageComponent_fullbanner'] = $homePageComponents->where('type', 'fullbanner')->shuffle()->first();
        $this->viewData['homePageComponent_fullpagetrip'] = $homePageComponents->where('type', 'fullpagetrip')->shuffle()->first();
        $this->viewData['homePageComponent_promotionboxwithtext'] = $homePageComponents->where('type', 'promotionboxwithtext')->shuffle()->first();

        return view('site.homepage.homepage', $this->viewData);
    }
}
