<?php

namespace Modules\HomePages\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class HomePageImage extends Model
{
    protected $table = 'homepage_images';
    protected $fillable = ['image','link','homepage_id','text'];

    public const PUBLIC_FILES_DIR = 'homepages';

    public function getPictureUrl()
    {
        return Storage::url(!empty($this->image) ? $this->image : self::PUBLIC_FILES_DIR . '/default.png');
    }
}
