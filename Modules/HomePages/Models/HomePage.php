<?php

namespace Modules\HomePages\Models;

use Illuminate\Database\Eloquent\Model;

class HomePage extends Model
{
    protected $fillable = ['type', 'status'];
    protected $table = 'homepages';

    public function images()
    {
        return $this->hasMany(HomePageImage::class, 'homepage_id');
    }
}
