<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomepagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homepages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type',20); // ["slider", "slidertop", "sliderbottom", "fullbanner", "fullpagetrip", "promotionboxwithtext",]
            $table->string('link',60); 
            $table->string('text',255);
            $table->tinyInteger('status'); // [0, 1]
            $table->smallInteger('sort');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homepages');
    }
}
