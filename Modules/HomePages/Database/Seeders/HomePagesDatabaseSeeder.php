<?php

namespace Modules\HomePages\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Modules\HomePages\Models\HomePage;
use Modules\HomePages\Models\HomePageImage;

class HomePagesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Storage::persistentFake(HomePageImage::PUBLIC_FILES_DIR);

        $types = [
            "slider" => [
                'width' => 960,
                'height' => 400
            ],
            "slidertop" => [
                'width' => 960,
                'height' => 400
            ],
            "sliderbottom" => [
                'width' => 960,
                'height' => 400
            ],
            "fullbanner" => [
                'width' => 1150,
                'height' => 320
            ],
            "fullpagetrip" => [
                'width' => 1280,
                'height' => 100
            ],
            "promotionboxwithtext" => [
                'width' => 600,
                'height' => 300
            ],
        ];
        foreach ($types as $type => $dimensions) {
            $homepage = HomePage::create([
                'type' => $type,
                'link' => "https://google.com?query=type+{$type}",
                'text' => 'Save 100% on 89j94thniu',
                'status' => 1,
                'sort' => 1,
            ]);
            for ($x = 1; $x < 5; ++$x) {
                HomePageImage::create([
                    'homepage_id' => $homepage->id,
                    'link' => "https://google.com?query=link+{$x}",
                    'image' => UploadedFile::fake()->image("picture_{$x}.jpg", $dimensions['width'], $dimensions['height'])->store(HomePageImage::PUBLIC_FILES_DIR, 'public'),
                    'text' => 'Save 100% on 89j94thniu',
                ]);
            }
        }
        // $this->call("OthersTableSeeder");
    }
}
