<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */



Route::middleware('admin:admin')->prefix('admin/home-pages')->group(function () {
    Route::get('/', 'HomePagesController@index');
    Route::get('/create', 'HomePagesController@create');
    Route::post('/store', 'HomePagesController@store')->name('storeHomePage');
    Route::get('/{id}/edit', 'HomePagesController@edit');
    Route::get('/{id}/edit', 'HomePagesController@edit');
    Route::patch('/{id}/update', 'HomePagesController@update');
    Route::get('/{id}/delete','HomePagesController@destroy');

});


Route::middleware('checkRoute')->prefix('{locale?}')->name('homepage.')->group(function () {

    Route::get('/', 'HomePagesSiteController@index')->name('index');

});
