@extends('admin.layouts.master')
@section('page-title','Create Home Page')

@section('breadcrumb')
    <li class="breadcrumb-item active"><a href="{{url('admin/orders')}}">Home Page</a></li>
    <li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card">
                <div class="header">
                    <h2>Home Page Details</h2>

                </div>
                <div class="body">
                    {{--<form id="wizard_with_validation" method="POST">--}}
                    {{Form::open(['route'=>'storeHomePage','files'=>true ])}}

                    <div class="form-group">
                        <label>Type</label><br>
                        <select name="type" id="type" class="form-control">
                            <option value="" selected>Choose Type ..</option>
                            <option value="slider">Slider</option>
                            <option value="slidertop">Banner Top</option>
                            <option value="sliderbottom">SliderBottom</option>
                            <option value="fullbanner">Full Banner</option>
                            <option value="fullpagetrip">Full Page Trip</option>
                            <option value="promotionboxwithtext">Promotion Box With Text</option>
                        </select>
                    </div>

                        <div class="row fieldGroup1" id="slider">

                            <div class="col-md-12">
                                <div class="form-group form-float">
                                    <label>Status</label>
                                    <select name="status" class="form-control" style="width: 100%">
                                        <option selected>Choose Status</option>
                                        <option value="1">Active</option>
                                        <option value="0">Disable</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{--<input type="text" class="form-control" placeholder="Username *" name="username" required>--}}
                                    <label>Link</label><br>
                                    {{Form::text('link1[]',null,['class'=>'form-control','style'=>'width:100%',])}}
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div class="form-group form-float">
                                    {{--<input type="password" class="form-control" placeholder="Password *" name="password" id="password" required>--}}
                                    <label>Image</label>
                                    {{Form::file('image1[]',['class'=>'form-control'])}}
                                </div>
                            </div>

                            <div class="form-group ">
                                <label></label>
                                <button type="button" class="btn btn-primary btn-labeled addMoreAttribute1">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                            </div>

                        </div>

                        <div class="row fieldGroup2" id="slidertop">

                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <label>Status</label>
                                <select name="status" class="form-control" style="width: 100%">
                                    <option selected>Choose Status</option>
                                    <option value="1">Active</option>
                                    <option value="0">Disable</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Link</label><br>
                                {{Form::text('link2',null,['class'=>'form-control','style'=>'width:100%',])}}
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group form-float">
                                <label>Image</label>
                                {{Form::file('image2',['class'=>'form-control'])}}
                            </div>
                        </div>


                    </div>

                        <div class="row fieldGroup3" id="sliderbottom">

                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <label>Status</label>
                                <select name="status" class="form-control" style="width: 100%">
                                    <option selected>Choose Status</option>
                                    <option value="1">Active</option>
                                    <option value="0">Disable</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{--<input type="text" class="form-control" placeholder="Username *" name="username" required>--}}
                                <label>Link</label><br>
                                {{Form::text('link3',null,['class'=>'form-control','style'=>'width:100%',])}}
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group form-float">
                                {{--<input type="password" class="form-control" placeholder="Password *" name="password" id="password" required>--}}
                                <label>Image</label>
                                {{Form::file('image3',['class'=>'form-control'])}}
                            </div>
                        </div>



                    </div>

                         <div class="row fieldGroup4" id="fullbanner">

                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <label>Status</label>
                                <select name="status" class="form-control" style="width: 100%">
                                    <option selected>Choose Status</option>
                                    <option value="1">Active</option>
                                    <option value="0">Disable</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{--<input type="text" class="form-control" placeholder="Username *" name="username" required>--}}
                                <label>Link</label><br>
                                {{Form::text('link5',null,['class'=>'form-control','style'=>'width:100%',])}}
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group form-float">
                                {{--<input type="password" class="form-control" placeholder="Password *" name="password" id="password" required>--}}
                                <label>Image</label>
                                {{Form::file('image5',['class'=>'form-control'])}}
                            </div>
                        </div>



                    </div>

                         <div class="row fieldGroup5" id="fullpagetrip">

                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <label>Status</label>
                                <select name="status" class="form-control" style="width: 100%">
                                    <option selected>Choose Status</option>
                                    <option value="1">Active</option>
                                    <option value="0">Disable</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{--<input type="text" class="form-control" placeholder="Username *" name="username" required>--}}
                                <label>Link</label><br>
                                {{Form::text('link6',null,['class'=>'form-control','style'=>'width:100%',])}}
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group form-float">
                                {{--<input type="password" class="form-control" placeholder="Password *" name="password" id="password" required>--}}
                                <label>Image</label>
                                {{Form::file('image6',['class'=>'form-control'])}}
                            </div>
                        </div>



                    </div>

                         <div class="row fieldGroup6" id="promotionboxwithtext">

                             <div class="col-md-12">
                                 <div class="form-group form-float">
                                     <label>Status</label>
                                     <select name="status" class="form-control" style="width: 100%">
                                         <option selected>Choose Status</option>
                                         <option value="1">Active</option>
                                         <option value="0">Disable</option>
                                     </select>
                                 </div>
                             </div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Link</label><br>
                                {{Form::text('link7[]',null,['class'=>'form-control','style'=>'width:100%',])}}
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group form-float">
                                <label>Image</label>
                                {{Form::file('image7[]',['class'=>'form-control'])}}

                            </div>
                        </div>
                        <div class="form-group ">
                            <label></label>
                            <button type="button" class="btn btn-primary btn-labeled addMoreAttribute6">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group form-float">
                                <label>Text</label>
                                {{Form::text('text[]',null,['class'=>'form-control','style'=>'width:100%',])}}
                            </div>
                        </div>




                    </div>


                    <div class="form-group">
                        <button type="submit" class="btn btn-success"> Save </button>
                    </div>

           {{Form::close()}}

            </div>
        </div>
    </div>


@endsection
@section('styles')
    <link href="{{asset('/assets/vendor/bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/vendor/daterangepicker/daterangepicker.css') }}">


    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
@endsection


@section('scripts')




    <script src="{{ asset('assets/vendor/daterangepicker/moment.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('assets/vendor/select2/js/select2.full.min.js')}}"></script>

    <script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.js')}}"></script> <!-- Jquery Validation Plugin Css -->
    <script src="{{ asset('assets/vendor/jquery-steps/jquery.steps.js')}}"></script> <!-- JQuery Steps Plugin Js -->

    <script src="{{ asset('assets/bundles/mainscripts.bundle.js')}}"></script>
    <script src="{{ asset('assets/js/pages/forms/form-wizard.js')}}"></script>




    <script>
        $(document).ready(function () {

            $('.datee').daterangepicker({
                minDate: new Date(),
                timePicker: true,
                locale: {
                    format: 'Y-M-DD HH:mm:ss'
                }
            });
        });



    </script>


    <script>
        $(document).ready(function() {
            // $('.disease').select2();
            $('.disease').select2();


            $( "#user" ).change(function() {
                var value =$(this).val();
                if(value === 'new')
                {
                    $("#button").show();
                }else{
                    $("#button").hide();
                }

            });

        });



    </script>





    <script>
        $(document).ready(function(){
            //group add limit
            var maxGroup = 20;

            //add more fields group
            $(".addMoreAttribute1").click(function(){
                if($('body').find('.fieldGroup1').length < maxGroup){
                    var fieldHTML = '<div class="row fieldGroupCopy1">'+$(".fieldGroupCopy1").html()+'</div>';
                    $('body').find('.fieldGroup1:last').after(fieldHTML);


                }else{
                    alert('Maximum '+maxGroup+' groups are allowed.');
                }
            });

            //remove fields group
            $("body").on("click",".remove",function(){
                $(this).parents(".fieldGroupCopy1").remove();
            });




        });

        $(document).ready(function(){
            //group add limit
            var maxGroup = 20;
            //add more fields group
            $(".addMoreAttribute2").click(function(){

                if($('body').find('.fieldGroup2').length < maxGroup){
                    var fieldHTML = '<div class="row fieldGroup2">'+$(".fieldGroupCopy2").html()+'</div>';
                    $('body').find('.fieldGroup2:last').after(fieldHTML);
                }else{
                    alert('Maximum '+maxGroup+' groups are allowed.');
                }
            });

            //remove fields group
            $("body").on("click",".remove",function(){
                $(this).parents(".fieldGroup2").remove();
            });




        });

        $(document).ready(function(){
            //group add limit
            var maxGroup = 20;
            //add more fields group
            $(".addMoreAttribute3").click(function(){

                if($('body').find('.fieldGroup3').length < maxGroup){
                    var fieldHTML = '<div class="row fieldGroup3">'+$(".fieldGroupCopy3").html()+'</div>';
                    $('body').find('.fieldGroup3:last').after(fieldHTML);
                }else{
                    alert('Maximum '+maxGroup+' groups are allowed.');
                }
            });

            //remove fields group
            $("body").on("click",".remove",function(){
                $(this).parents(".fieldGroup2").remove();
            });




        });

        $(document).ready(function(){
            //group add limit
            var maxGroup = 20;
            //add more fields group
            $(".addMoreAttribute6").click(function(){

                if($('body').find('.fieldGroup6').length < maxGroup){
                    var fieldHTML = '<div class="row fieldGroup6">'+$(".fieldGroupCopy6").html()+'</div>';
                    $('body').find('.fieldGroup6:last').after(fieldHTML);
                }else{
                    alert('Maximum '+maxGroup+' groups are allowed.');
                }
            });

            //remove fields group
            $("body").on("click",".remove",function(){
                $(this).parents(".fieldGroup6").remove();
            });




        });

        $(document).ready(function(){
            $('#slider').hide();
            $('#slidertop').hide().prop('required',false);
            $('#sliderbottom').hide().prop('required',false);
            $('#fullbanner').hide().prop('required',false);
            $('#promotionboxwithtext').hide().prop('required',false);
            $('#fullpagetrip').hide().prop('required',false);
            $('#type').change(function(){
                $('#slider').hide().prop('required',false);
                $('#slidertop').hide().prop('required',false);
                $('#sliderbottom').hide().prop('required',false);
                $('#fullbanner').hide().prop('required',false);
                $('#promotionboxwithtext').hide().prop('required',false);
                $('#fullpagetrip').hide().prop('required',false);
                $('.fieldGroupCopy1').hide().prop('required',false);
                $('.fieldGroup6').hide().prop('required',false);

                $('#' + $(this).val()).show();
            });
        });


    </script>

@endsection






<div class="row fieldGroupCopy1"  style="display: none;">

    <div class="col-md-6">
        <div class="form-group">
            <label>Link</label><br>
            {{Form::text('link1[]',null,['class'=>'form-control','style'=>'width:100%',])}}
        </div>
    </div>

    <div class="col-md-5">
        <div class="form-group form-float">
            <label>Image</label>
            {{Form::file('image1[]',['class'=>'form-control'])}}
        </div>
    </div>
    <div class="form-group ">
        <label></label>
        <button type="button" class="btn btn-primary btn-labeled remove">
            <i class="fa fa-minus" aria-hidden="true"></i>
        </button>
    </div>

</div>

<div class="row fieldGroupCopy6" style="display: none;">


            <div class="col-md-5">
                <div class="form-group">
                    <label>Link</label><br>
                    {{Form::text('link7[]',null,['class'=>'form-control','style'=>'width:100%',])}}
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group form-float">
                    <label>Image</label>
                    {{Form::file('image7[]',['class'=>'form-control'])}}
                </div>
            </div>
            <div class="form-group ">
            <label></label>
            <button type="button" class="btn btn-primary btn-labeled remove">
            <i class="fa fa-minus" aria-hidden="true"></i>
            </button>
            </div>

            <div class="col-md-5">
                <div class="form-group form-float">
                    <label>Text</label>
                    {{Form::text('text[]',null,['class'=>'form-control','style'=>'width:100%',])}}
                </div>
            </div>



        </div>






