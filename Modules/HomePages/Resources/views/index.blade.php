@extends('admin.layouts.master')
@section('page-title','Home Pages')
@section('breadcrumb')
    <li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')
    <div class="card">
        <div class="header">
            <div class="row">
                <div class="col-md-6">
                    <h2>Locations list</h2>
                </div>
                <div class="col-md-6">
                    <a href="{{url('/admin/products/create/')}}" class="btn btn-primary pull-right"><i class="fa fa-plus-square"></i> <span>Add New</span></a>
                </div>
            </div>
        </div>
        <div class="body">
            @include('admin.pratical.message')
            <div class="table-responsive">
                <table class="table table-hover m-b-0 c_list datatable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Image</th>
                        <th>Type</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($homepages as $homepage)
                        @if($homepage['images'] != '')
                        @endif
                    <tr>
                        <th>{{$homepage->id}}</th>
                        <th><img src="{{url(''). '/uploads/'.$homepage["images"][0]["image"]}}" width="50px" height="50x"></th>
                        <th>{{$homepage->type}}</th>
                        <th>
                            <a href="{{url('admin/home-pages/'.$homepage->id.'/edit')}}" class='btn btn-warning'>Edit</a>
                            <a href='{{url('admin/home-pages/'.$homepage->id.'/delete')}}' class='btn btn-danger'>Delete</a>
                        </th>
                    </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
