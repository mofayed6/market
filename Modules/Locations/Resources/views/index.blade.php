@extends('admin.layouts.master')
@section('page-title','Locations')
@section('breadcrumb')
    <li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')
    <div class="card">
        <div class="header">
            <div class="row">
                <div class="col-md-6">
                    <h2>Locations list</h2>
                </div>
                <div class="col-md-6">
                    <a href="{{url('/admin/locations/create/'.$parent)}}" class="btn btn-primary pull-right"><i class="fa fa-plus-square"></i> <span>Add New</span></a>
                </div>
            </div>
        </div>
        <div class="body">
            @include('admin.pratical.message')
            <div class="table-responsive">
                <table class="table table-hover m-b-0 c_list datatable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        @if($parent)
                            <th>parent</th>
                        @endif
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function() {
            $('.datatable').DataTable({
                processing: true,
                searching: true,
                serverSide: true,
                ajax: '{!! url('admin/locations/datatable/'.$parent) !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    @if($parent)
                    { data: 'parent', name: 'parent' },
                    @endif
                    { data: 'options', name: 'options' },
                ],
            });
        });
    </script>
@stop
