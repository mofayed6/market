<?php

namespace Modules\Locations\Models;

use App\Http\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use TranslatableTrait;
    protected $table = 'locations';
    protected $fillable = ['parent_id', 'code'];
  //  protected $appends = ['name'];
 //   protected $translatable = ['name'];

    public function children()
    {
        return $this->hasMany(Location::class, 'parent_id');
    }

    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
    }

    public function parent()
    {
        $this->depth++;
        return $this->belongsTo(Location::class, 'parent_id');
    }

    public function parentRecursive()
    {
        return $this->parent()->with('parentRecursive');
    }

    public static function getAll()
    {
        // $results = [];
        $countries = Location::where('parent_id', 0)->get();
        //dd($countries);
        foreach ($countries as $key => $country) {
            //dd(json_decode($country['name'],true));
            $results[$key]['id'] = $country->id;
            $results[$key]['name'] = json_decode($country['name'], true)['en'];

            foreach ($country->children as $x => $city) {
                $results[$key]['cities'][$x]['id'] = $city->id;
                $results[$key]['cities'][$x]['name'] = json_decode($city['name'], true)['en'];

                foreach ($city->children as $z => $district) {
                    $results[$key]['cities'][$x]['districts'][$z]['id'] = $district->id;
                    $results[$key]['cities'][$x]['districts'][$z]['name'] = json_decode($district['name'], true)['en'];
                }
            }

        }

        return $results;
    }



    public static function getDistrict()
    {
        $countries = Location::where('parent_id', 0)->get();
        foreach ($countries as $key => $country) {
            foreach ($country->children as $x => $city) {
                foreach ($city->children as $z => $district) {
                    $results[$key]['id'] = $district->id;
                    $results[$key]['name'] = json_decode($district['name'], true)['en'];
                }
            }

        }
        return $results;
    }


    public static function getDistrictSite()
    {
        $countries = Location::where('parent_id', 0)->get();
        foreach ($countries as $key => $country) {
            foreach ($country->children as $x => $city) {
                foreach ($city->children as $z => $district) {
                    $results[$key]['id'] = $district->id;
                    $results[$key]['name'] = $district['name'];
                }
            }

        }
        return $results;
    }


//    public function getNameAttribute()
//    {
//        return $this->en['name'];
//    }
}

