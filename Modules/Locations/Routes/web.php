<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::prefix('admin/locations')->middleware('admin:admin')->group(function () {
    Route::post('/', 'LocationsController@store');
    Route::get('/create/{parent?}', 'LocationsController@create');
    Route::get('{location}/delete', 'LocationsController@destroy');
    Route::get('{location}/edit', 'LocationsController@edit');
    Route::patch('{location}/edit', 'LocationsController@update');
    Route::get('/datatable/{parent?}', 'LocationsController@datatable');
    Route::get('/{parent?}', 'LocationsController@index');
});

Route::get('{locale}/get-cities/{country_id}', 'LocationsSiteController@getCities')->name('getCities');
Route::get('{locale}/get-districts/{city_id}', 'LocationsSiteController@getDistricts')->name('getDistricts');
