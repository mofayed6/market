<?php

namespace Modules\Locations\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Locations\Models\Location;

class LocationsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($parent = 0)
    {
        return view('locations::index', compact('parent'));
    }



    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create($parent = 0)
    {
        $parentLocation = 0;
        if ($parent != 0) {
            $parentLocation = Location::findOrFail($parent);
            $name = json_decode($parentLocation->name,true);
        }


        return view('locations::create', compact('parentLocation', 'parent','name'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'ar.name' => 'required',
            'en.name' => 'required'
        ]);
        $name = json_encode(['ar' => $request->ar['name'] , 'en' => $request->en['name']],JSON_UNESCAPED_UNICODE);

        $location = new Location();
        $location->name = $name;
        $location->parent_id = $request->parent_id;
        $location->save();
       // $location->translateAttributes($request->all());
        return redirect('admin/locations/' . $location->parent_id)->with(['success' => 'Location created Successfully']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('locations::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Location $location)
    {
        $name = json_decode($location->name,true);

        return view('locations::edit', compact('location','name'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Location $location, Request $request)
    {
        $this->validate($request, [
            'ar.name' => 'required',
            'en.name' => 'required'
        ]);

        $name = json_encode(['ar' => $request->ar['name'] , 'en' => $request->en['name']],JSON_UNESCAPED_UNICODE);
        $location->name = $name;
        $location->save();

       // $location->translateAttributes($request->all());
        return redirect('admin/locations/' . $location->parent_id)->with(['success' => 'Location updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Location $location)
    {
        $location->delete();
      //  $location->deleteTranslations();
        return redirect('admin/locations/' . $location->parent_id)->with(['success' => 'Location deleted Successfully']);
    }

    public function datatable($parent = 0)
    {

            $parent = (int)$parent;

        

       $locations = Location::where('parent_id', $parent)->get();
        return \DataTables::of($locations)
            ->editColumn('name', function ($model) use ($parent) {
                $name = json_decode($model->name);

                return "<a href='" . url('admin/locations/' . $model->id) . "'>" . $name->en . "</a>";
            })->editColumn('parent', function ($model) use ($parent) {
                if (!$parent)
                    return "";
                $nameParent = json_decode($model->parent->name);

                return "<a href='" . url('admin/locations/' . $parent) . "'>" . $nameParent->en . "</a>";
            })->editColumn('options', function ($model) {
                return "<a href='" . url('admin/locations/' . $model->id . '/edit') . "' class='btn btn-warning'>Edit</a>
                <a href='" . url('admin/locations/' . $model->id . '/delete') . "' class='btn btn-danger'>Delete</a>";

            })
            ->make(true);
    }


}
