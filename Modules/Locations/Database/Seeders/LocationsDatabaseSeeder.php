<?php

namespace Modules\Locations\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Locations\Models\Location;

class LocationsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        for ($i = 1; $i <= 90; $i += 10) {
            // Country
            $country = Location::create([
                'id' => $i,
                'name'=>'{"ar":"الدولة'.$i.'","en":"Country'.$i.'"}',

                'parent_id' => 0,
            ]);
        //    $country->translateAttributes(['en' => ['name' => "Country {$i}"], 'ar' => ['name' => "الدولة {$i}"]]);

            // City
            $city = Location::create([
                'id' => $i + 1,
                'name'=>'{"ar":"المدينة'.$i.'","en":"City'.$i.'"}',

                'parent_id' => $i, // The country
            ]);
           // $city->translateAttributes(['en' => ['name' => "City {$i}"], 'ar' => ['name' => "المدينة {$i}"]]);

            // District
            $district = Location::create([
                'id' => $i + 2,
                'name'=>'{"ar":"الحي'.$i.'","en":"District'.$i.'"}',
                'parent_id' => $i + 1, // The city
            ]);
          //  $district->translateAttributes(['en' => ['name' => "District {$i}"], 'ar' => ['name' => "الحي {$i}"]]);
        }

        // $this->call("OthersTableSeeder");
    }
}
