@extends('admin.layouts.master')
@section('page-title','Create Admin')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href='{{url('admin/user')}}'>Admins</a> </li>

    <li class="breadcrumb-item active">Create New</li>
@endsection
@section('content')
    <div class="card">
        <div class="header">
            <h2>Create New Admin</h2>
        </div>
        <div class="body">
            @include('admin.pratical.message')
            {{Form::open(['action'=>'\Modules\Admins\Http\Controllers\AdminsController@store'])}}
                @include('admins::form')
            {{Form::close()}}
        </div>
    </div>
@endsection
