<?php

namespace Modules\Attributes\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Attributes\Models\Attribute;
use Modules\Categories\Models\Category;
use Modules\Attributes\Models\CategoryAttribute;

class CategoryAttributesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $category_1 = Category::find(1);
        $childCategory_1 = Category::where('parent_id', $category_1->id)->first();

        for ($i = 1; $i <= 5; ++$i) {
            CategoryAttribute::create([
                'category_id' => $category_1->id,
                'attribute_id' => $i,
                'sort' => $i
            ]);
        }

        for ($i = 6; $i <= 7; ++$i) {
            CategoryAttribute::create([
                'category_id' => $childCategory_1->id,
                'attribute_id' => $i,
                'sort' => $i - 5
            ]);
        }

        $category_2 = Category::find(2);
        for ($i = 8; $i <= 10; ++$i) {
            CategoryAttribute::create([
                'category_id' => $category_2->id,
                'attribute_id' => $i,
                'sort' => $i - 7
            ]);
        }

        // $this->call("OthersTableSeeder");
    }
}
