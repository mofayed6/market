<?php

namespace Modules\Attributes\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Attributes\Models\Attribute;
use Modules\Categories\Models\Category;
use Modules\Attributes\Models\CategoryAttribute;

class AttributesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        for ($i = 1; $i <= 10; ++$i) {
            $attribute = Attribute::create([
                'name'=>'{"ar":"المصنع'.$i.'","en":"Brand'.$i.'"}',
                'selection_type' => 'single',
                'default_value' => '',
                'template' => 'input_text',
                'is_required' => true
            ]);
//            $attribute->translateAttributes([
//                'en' => ['name' => "Brand {$attribute->id}"],
//                'ar' => ['name' => "المصنع {$attribute->id}"]
//            ]);
        }

        $this->call(CategoryAttributesDatabaseSeeder::class);
    }
}
