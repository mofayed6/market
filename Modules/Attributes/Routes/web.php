<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('attributes')->middleware('admin:admin')->group(function() {

    Route::get('list/{parent?}', 'AttributesController@list');
    Route::get('datatable', 'AttributesController@datatable');
    Route::get('/', 'AttributesController@index');
    Route::get('create', 'AttributesController@create');
    Route::post('/', 'AttributesController@store')->name('attributes.store');
    Route::get('{attr}/edit', 'AttributesController@edit');
    Route::patch('{attr}/update', 'AttributesController@update');
    Route::get('{attr}/delete', 'AttributesController@destroy');
});
