<?php

namespace Modules\Attributes\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Products\Models\Product;
use App\Http\Traits\TranslatableTrait;

class AttributeProduct extends Model
{
    use TranslatableTrait;
    protected $table = 'attributes_product';

    public function attributes()
    {
        return $this->belongsTo(Attribute::class, 'attribute_id');
    }

    public function products()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
