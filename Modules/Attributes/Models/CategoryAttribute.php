<?php

namespace Modules\Attributes\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Categories\Models\Category;

class CategoryAttribute extends Model
{
    public function attributes()
    {
        return $this->belongsTo(Attribute::class, 'attribute_id');
    }

    public function categories()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
