<?php

namespace Modules\Attributes\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\Attributes\Models\Attribute;

class AttributesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('attributes::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('attributes::create');
    }


    public function store(Request $request)
    {

       // dd($request->all());
        $name = json_encode(['ar' => $request->ar['name'] , 'en' => $request->en['name']],JSON_UNESCAPED_UNICODE);
//        $option = json_decode($request->options,true);
//        //dd($option);
//      //  $options = json_encode($option);
//        //dd($request->options);


        $this->validate($request,[
            'en.name'=>'required',
            'ar.name'=>'required',
            'template'=>'required'
        ]);

        if($request->template =='dropdown'){
            $this->validate($request,[
                'options'=>'required',
            ]);
        }


        $attributes = new Attribute();
        $attributes->name           = $name;
        $attributes->options        = $request->options;
        $attributes->selection_type = $request->selection_type;
        $attributes->default_value  = $request->default_value;
        $attributes->template       = $request->template;
        $attributes->is_required    = ($request->filled('is_required'))?$request->is_required:0;
        $attributes->save();

       // dd($request->all());
      //  $attributes->translateAttributes($request->all());
        return redirect('admin/attributes')->with(['success'=>'Attribute Add Successfully']);
    }

    public function formattingOptionsForDB($options)
    {
        $results = [];

            foreach($options as $k => $option){

                $results[$k]['slug'] = $option['slug'];
                $results[$k]['optionAr'] = $options[$k]['optionAr'];
                $results[$k]['optionEn'] = $options[$k]['optionEn'];
            }


        return $results;

    }

    public function formattingOptionsForView($attr)
    {
           $options = json_decode($attr->options ,true);
            foreach ($options as $k=>$option) {
                $results[$k]['slug'] = $options[$k]['slug'];
                $results[$k]['optionAr'] = $options[$k]['optionAr'];
                $results[$k]['optionEn'] = $options[$k]['optionEn'];
//                $results[$k]['option'.ucfirst($locale->code)] = $attr->{$locale->code}['options'][$k]['name'];

        }
        return json_encode($results,JSON_UNESCAPED_UNICODE);
    }


    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('attributes::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Attribute $attr)
    {

        $name = $attr->name;

        $options = "";
        if($attr->template =='dropdown') {
            $options = json_decode($attr->options ,true);
            foreach ($options as $k=>$option) {
                $results[$k]['slug'] = $options[$k]['slug'];
                $results[$k]['optionAr'] = $options[$k]['optionAr'];
                $results[$k]['optionEn'] = $options[$k]['optionEn'];
//                $results[$k]['option'.ucfirst($locale->code)] = $attr->{$locale->code}['options'][$k]['name'];

            }
            $options =  json_encode($results,JSON_UNESCAPED_UNICODE);
        }

        return view('attributes::edit',compact('attr','options','name'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Attribute $attr,Request $request)
    {
        $this->validate($request,[
            'en.name'=>'required',
            'ar.name'=>'required',
            'template'=>'required'
        ]);
        if($request->template =='dropdown') {
            $this->validate($request, [
                'options' => 'required',
            ]);

        }
        $name = json_encode(['ar' => $request->ar['name'] , 'en' => $request->en['name']],JSON_UNESCAPED_UNICODE);
        $attr->name           = $name;
        $attr->options        = $this->formattingOptionsForDB(json_decode($request->options, true));
        $attr->selection_type = $request->selection_type;
        $attr->default_value  = $request->default_value;
        $attr->template       = $request->template;

        $attr->is_required    = ($request->filled('is_required'))?$request->is_required:0;
        $attr->save();

        return redirect('admin/attributes')->with(['success'=>'Attribute Add Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy( $id )
    {
        $attr = Attribute::find($id);
        $attr->delete();
        return redirect('admin/attributes')->with(['success'=>'Attribute Deleted Successfully']);
    }

    public function datatable()
    {
        $categories = Attribute::get();
        return \DataTables::of($categories)
            ->editColumn('name',function ($model){
                $enName = json_decode($model->name,true);
                return $enName['en'];
            })->editColumn('actions',function ($model){
                return "<a href='".url('admin/attributes/'.$model->id.'/edit')."' class='btn btn-warning'>Edit</a>
                        <a href='".url('admin/attributes/'.$model->id.'/delete')."' class='btn btn-danger'>Delete</a>";
            })
            ->make(true);
    }

    public function list(Request $request,$parent = null)
    {
        $attributes = Attribute::query();
        if($parent != null)
            $attributes = Attribute::query();

        if($request->filled('q')){
            $attributes->whereHas('translations',function ($query) use($request){
                $query->where('attribute','name')->where('value','like','%'.$request->q.'%');
            });
        }
        $parent_attributes = Attribute::getParentCategoriesAttributes($parent);
        $attributes = $attributes->whereNotIn('id',$parent_attributes->pluck('id'))->get();
        return \response()->json($attributes);

    }
}
