<?php

namespace Modules\Branches\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Branches\Models\Branch;
use Illuminate\Support\Facades\Hash;
use Modules\Locations\Models\Location;

class BranchesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $branchesInfo = require __DIR__ . '/branches_dump.php';
        $countries = Location::where(['parent_id' => 0])->with('children.children')->get();

        for ($i = 1; $i < 6; ++$i) {
            $country = $countries->shift();
            $branch = Branch::create([
                'id' => $i,
                'name' => json_encode([
                    'ar' => $branchesInfo[$i]['ar']['name'],
                    'en' => $branchesInfo[$i]['en']['name'],
                ], JSON_UNESCAPED_UNICODE),
                'address' => json_encode([
                    'ar' => $branchesInfo[$i]['ar']['address'],
                    'en' => $branchesInfo[$i]['en']['address'],
                ], JSON_UNESCAPED_UNICODE),
                'latitude' => $branchesInfo[$i]['latitude'],
                'longitude' => $branchesInfo[$i]['longitude'],
                'country_id' => $country->id,
                'city_id' => $country->children()->first()->id,
                'district_id' => $country->children()->first()->children()->first()->id,
                'phone' => 1010101010,
            ]);

            //$branch->translateAttributes($branchesInfo[$i]);
        }

        // $this->call("OthersTableSeeder");
    }
}
