<?php
return [
    1 => [
        'en' => [
            'name' => 'Maadi Branch',
            'address' => 'Maadi - Main st.',
        ],
        'ar' => [
            'name' => 'فرع المعادي',
            'address' => 'المعادي - الشارع الرئيسي',
        ],
        'latitude' => 29.961378,
        'longitude' => 31.2461405,
    ],
    2 => [
        'en' => [
            'name' => '5th Settlement Branch',
            'address' => '5th Settlement - Main st.',
        ],
        'ar' => [
            'name' => 'فرع التجمع الخامس',
            'address' => 'التجمع الخامس - الشارع الرئيسي',
        ],
        'latitude' => 30.5641677,
        'longitude' => 31.718651,
    ],
    3 => [
        'en' => [
            'name' => 'Hurghada Branch',
            'address' => 'Hurghada - Main st.',
        ],
        'ar' => [
            'name' => 'فرع الغردقة',
            'address' => 'الغردقة - الشارع الرئيسي',
        ],
        'latitude' => 30.1641677,
        'longitude' => 31.219651,
    ],
    4 => [
        'en' => [
            'name' => 'Alexandria Branch',
            'address' => 'Alexandria - Main st.',
        ],
        'ar' => [
            'name' => 'فرع الإسكندرية',
            'address' => 'الإسكندرية - الشارع الرئيسي',
        ],
        'latitude' => 30.0642677,
        'longitude' => 31.318651,
    ],
    5 => [
        'en' => [
            'name' => 'North Coast Branch',
            'address' => 'North Coast - Main st.',
        ],
        'ar' => [
            'name' => 'فرع الساحل الشمالي',
            'address' => 'الساحل الشمالي - الشارع الرئيسي',
        ],
        'latitude' => 30.0641647,
        'longitude' => 31.218631,
    ],
    6 => [
        'en' => [
            'name' => 'Zamalek Branch',
            'address' => 'Zamalek - Main st.',
        ],
        'ar' => [
            'name' => 'فرع الزمالك',
            'address' => 'الزمالك - الشارع الرئيسي',
        ],
        'latitude' => 30.0641677,
        'longitude' => 31.218651,
    ],
];
