<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('address', 100)->default('');
            $table->decimal('latitude', '10', '8')->default(0);
            $table->decimal('longitude', '11', '8')->default(0);
            $table->string('internal_id');
            $table->unsignedInteger('country_id')->default(0);
            $table->unsignedInteger('city_id')->default(0);
            $table->unsignedInteger('district_id')->default(0);
            $table->string('phone', 11)->default('');
            $table->timestamps();
        });

        // Schema::table('branches', function (Blueprint $table) {
        //     //foreign key constrains
        //     $table->foreign('country_id')->references('id')->on('locations')->onDelete('cascade');
        //     $table->foreign('city_id')->references('id')->on('locations')->onDelete('cascade');
        //     $table->foreign('district_id')->references('id')->on('locations')->onDelete('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}
