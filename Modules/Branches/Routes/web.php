<?php

Route::prefix('admin/branches')->middleware('admin:admin')->group(function () {
    Route::get('/', 'BranchesController@index');
    Route::get('create', 'BranchesController@create');
    Route::post('/', 'BranchesController@store');
    Route::get('{branch}/edit', 'BranchesController@edit');
    Route::get('{branch}/delete', 'BranchesController@destroy');
    Route::patch('{branch}', 'BranchesController@update');
    Route::get('datatable', 'BranchesController@datatable');
});

Route::prefix('{locale}/branches')
    ->name('branches.')
    ->group(function () {
        Route::get('/change-branch/{branchId}', 'BranchesSiteController@changeBranch')->name('change_branch');
        Route::get('/get-branch/{distractId}', 'BranchesSiteController@getBranch')->name('get_branch');
    });
