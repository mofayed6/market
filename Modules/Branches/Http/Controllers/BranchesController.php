<?php

namespace Modules\Branches\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Branches\Models\Branch;
use App\Http\Controllers\Controller;
use Modules\DeliveryLocation\Models\DeliveryLocation;
use Modules\Locations\Models\Location;

class BranchesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('branches::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $locations = Location::getAll();
        $district  = Location::getDistrict();
        return view('branches::create', compact('locations','district'));
    }


    public function store(Request $request)
    {
      //  dd($request->all());
        $this->validate($request, [
            'en.name' => 'required',
            'en.address' => 'required',
            'ar.name' => 'required',
            'ar.address' => 'required',
            'country_id' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
        ]);

        $name = json_encode(['ar' => $request->ar['name'] , 'en' => $request->en['name']],JSON_UNESCAPED_UNICODE);
        $address = json_encode(['ar' => $request->ar['address'] , 'en' => $request->en['address']],JSON_UNESCAPED_UNICODE);

        //dd($request->all());

        $branch = new Branch();
        $branch->name = $name;
        $branch->address = $address;
        $branch->phone = $request->phone;
        $branch->country_id = $request->country_id;
        $branch->city_id = $request->city_id;
        $branch->district_id = $request->district_id;
        $branch->latitude = $request->lat;
        $branch->longitude = $request->lng;
        $branch->save();


        $datae = [];
        foreach ($request->districtt_id as $k => $t) {
            $datae[$t] = ['price' => $request->stock_amount[$k]];
        }


        $branch->DeliveryLocation()->sync($datae);

        return redirect('admin/branches')->with(['success' => 'branch created successfully']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('branches::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Branch $branch)
    {
        $locations = collect(Location::getAll());
        $name = json_decode($branch->name,true);
        $address = json_decode($branch->address,true);
        $district  = Location::getDistrict();

        $devlicary  = DeliveryLocation::where('branch_id',$branch->id)->get();
        return view('branches::edit', compact('branch', 'locations','name','address','district','devlicary'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Branch $branch, Request $request)
    {
        $this->validate($request, [
            'en.name' => 'required',
            'en.address' => 'required',
            'ar.name' => 'required',
            'ar.address' => 'required',
            'country_id' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
        ]);


        $name = json_encode(['ar' => $request->ar['name'] , 'en' => $request->en['name']],JSON_UNESCAPED_UNICODE);
        $address = json_encode(['ar' => $request->ar['address'] , 'en' => $request->en['address']],JSON_UNESCAPED_UNICODE);



        $branch->name = $name;
        $branch->address = $address;
        $branch->phone = $request->phone;
        $branch->country_id = $request->country_id;
        $branch->city_id = $request->city_id;
        $branch->district_id = $request->district_id;
        $branch->latitude = $request->lat;
        $branch->longitude = $request->lng;
        $branch->save();


        $datae = [];
        foreach ($request->districtt_id as $k => $t) {
            $datae[$t] = ['price' => $request->stock_amount[$k]];
        }


        $branch->DeliveryLocation()->sync($datae);

       // $branch->deleteTranslations();
        //$branch->translateAttributes($request->only(['ar', 'en']));
        return redirect('admin/branches')->with(['success' => 'branch updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Branch $branch)
    {
        $branch->delete();
        //$branch->deleteTranslations();
        return redirect('admin/branches')->with(['success' => 'branch deleted successfully']);

    }

    public function datatable()
    {
        $locations = Branch::get();
        return \DataTables::of($locations)
            ->editColumn('name', function ($model) {
                $enName = json_decode($model->name,true);
                return $enName['en'];
            })->editColumn('options', function ($model) {
                return "<a href='" . url('admin/branches/' . $model->id . '/edit') . "' class='btn btn-warning'>Edit</a>
                <a href='" . url('admin/branches/' . $model->id . '/delete') . "' class='btn btn-danger'>Delete</a>";

            })
            ->make(true);
    }
}
