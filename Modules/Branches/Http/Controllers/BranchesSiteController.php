<?php

namespace Modules\Branches\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Modules\Branches\Models\Branch;
use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController;
use Modules\DeliveryLocation\Models\DeliveryLocation;

class BranchesSiteController extends BaseController
{
    /**
     * change the current branch
     * @return Response
     */
    public function changeBranch($locale, $branchId)
    {
        setCurrentBranch(Branch::find((int)$branchId));
        return 1;
    }

    public function getBranch($locale,$distract)
    {
        \session(['distract' => $distract]);
        
        $dist = DeliveryLocation::where('district_id',$distract)->pluck('branch_id')->toArray();
        $branch = Branch::whereIn('id',$dist)->get();
        return $branch;

    }
}
