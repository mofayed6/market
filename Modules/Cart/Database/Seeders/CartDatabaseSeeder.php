<?php

namespace Modules\Cart\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\User\Models\User;
use Modules\Cart\Models\Cart;
use Modules\Cart\Models\CartProduct;
use Modules\Products\Models\Product;

class CartDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $users = User::limit(2)->get();
        $products = Product::limit(5)->get();
        foreach ($users as $user) {
            $cart = Cart::create([
                'user_id' => $user->id,
            ]);

            foreach ($products as $product) {
                CartProduct::create([
                    'product_id' => $product->id,
                    'cart_id' => $cart->id,
                    'quantity' => $user->id,
                ]);
            }
        }
        // $this->call("OthersTableSeeder");
    }
}
