<?php

namespace Modules\Cart\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Products\Models\Product;

class CartProduct extends Model
{
    protected $fillable = [];
    protected $table = "carts_products";

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function getPriceAttribute()
    {
        return $this->product->getPrice();
    }

    public function getSubTotalAttribute()
    {
        return $this->getPriceAttribute() * $this->quantity;
    }
}
