<?php

namespace Modules\Cart\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = [];

    public function cartProducts()
    {
        return $this->hasMany(CartProduct::class, 'cart_id');
    }

    /**
     * @return float Returns the sum of cart products prices,
     * and their related taxes (later)
     */
    public function totalProductsCost()
    {
        $totalCost = 0;
        foreach ($this->cartProducts as $cartProduct) {
            $totalCost += $cartProduct->getSubTotalAttribute();
        }

        return $totalCost;
    }
}
