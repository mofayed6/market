<?php

namespace Modules\Cart\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Cart\Services\CartItemsService;
use App\Http\Controllers\BaseController;

class CartController extends BaseController
{
    protected $cart;

    public function __construct(CartItemsService $cartItems)
    {
        $this->middleware(function ($request, $next) use ($cartItems) {
            $this->user = auth("web")->user();
            $this->cart = $cartItems->setUser(auth()->id());
            return $next($request);
        });

        $this->cart = $cartItems->setUser(0);

        parent::__construct();
    }

    public function index(Request $request, $locale)
    {
        $this->viewData['items'] = $this->cart->getItems();
        if ($request->wantsJson()) {
            return count($this->viewData['items']);
        }
        $this->viewData['total'] = $this->cart->totalPrice();
        return View('site.checkout_process.cart', $this->viewData);
    }

    public function store(Request $request, $locale)
    {

        $item = $this->cart->addItem($request->product_id, $request->quantity ?? 1);
        return $this->cart->getItemsCount();
    }

    public function update(Request $request, $locale, $item)
    {
        $action = 'updateQty';
        if ($request->filled('action')) {
            $action = $request->get('action');
        }
        switch ($action) {
            case "updateQty":
                $this->cart->updateItemQuantityById($item, $request->get('quantity'));
                return back();
                break;
            case "save":
                return back();
                break;
            case "delete":
                $this->destroy($item);
                return back();
                break;
            default:
               return back();
        }
    }

    public function destroy($item)
    {
        $item = $this->cart->removeItemById($item);
        return $item;
    }

    public function clear(Request $request)
    {
        $item = $this->cart->clearItems($request->item_id);
        return $item;
    }
}
