<?php

namespace Modules\Cart\Services;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Modules\Cart\Models\CartProduct;
use Modules\Products\Models\Product;

class CartItemsService
{
    protected $dbStorage = false;
    protected $user_id = null;
    protected $cartService = null;

    public function __construct(CartService $cart)
    {
        $this->cartService = $cart;
    }

    /**
     * @param $user_id
     * @return $this
     */
    public function setUser($user_id)
    {
        if (Auth::check()) {

            $this->user_id = $user_id;
            return $this;
        }

        $this->user_id = 0;
        return $this;
    }

    /**
     * @return array CartProduct
     */
    public function getItems()
    {
        if (Auth::check()) {
            $userCart = $this->cartService->createOrReturnCartInstance($this->user_id);
            $items = CartProduct::where('cart_id', $userCart->id)->with('product')->get();
            $cart = \session('cart');

            if($items->count() == 0 && isset($cart)){
                return $cart;
            }
            return $items;
        }

        $cart = \session('cart');
        return $cart;

    }

    /**
     * @return int
     */
    public function getItemsCount()
    {
        if (Auth::check()) {
            $userCart = $this->cartService->createOrReturnCartInstance($this->user_id);
            $items = CartProduct::where('cart_id', $userCart->id)->count();
            if($items < 0 ){
                $cart = \session('cart');
                $count =   count($cart);
                return $count;
            }
            return $items;
        }

        $cart = \session('cart');
        $count =   count($cart);
       return $count;
    }
    /**
     * @param $item_id
     * @param int $quantity
     * @return CartProduct
     */
    public function addItem($item_id, $quantity = 1)
    {
        
        if ($this->itemExists($item_id)) {
            return $this->increaseItemQuantity($item_id, $quantity);
        }
        if (Auth::check()) {
            $userCart = $this->cartService->createOrReturnCartInstance($this->user_id);
            $cartItem = new CartProduct;
            $cartItem->cart_id = $userCart->id;
            $cartItem->product_id = $item_id;
            $cartItem->quantity = $quantity;
            $cartItem->save();
            return $cartItem;
        }

        setCart($item_id,$quantity);

        $cartItem['product_id']   = $item_id;
        $cartItem['quantity']   = $quantity;

        return $cartItem;


    }

    /**
     * @param array $items
     * @param int $quantity
     * @return array CartProduct
     */
    public function addItems(array $items, $quantity = 1)
    {
        if (Auth::check()) {

            $res = [];
            foreach ($items as $item) {
                $cartItem = $this->addItem($item['id'], $item['quantity'] ?? $quantity);
//            setCart($item,$quantity);
                $res[] = $cartItem;
            }

            if($res == null){
                $carts = \session('cart');

                $res = [];
                foreach ($carts as $item) {
                    $cartItem = $this->addItem($item['id'], $item['quantity'] ?? $quantity);
//            setCart($item,$quantity);
                    $res[] = $cartItem;
                }
                return $res;
            }

            return $res;

        }


        $carts = \session('cart');
        
        $res = [];
        foreach ($carts as $item) {
            $cartItem = $this->addItem($item['id'], $item['quantity'] ?? $quantity);
//            setCart($item,$quantity);
            $res[] = $cartItem;
        }
        return $res;
    }

    /**
     * @param $item_id
     * @return CartProduct
     */
    public function itemExists($item_id)
    {
        if (Auth::check()) {
            $userCart = $this->cartService->createOrReturnCartInstance($this->user_id);
            $exists = CartProduct::whereCartIdAndProductId($userCart->id, $item_id)->exists();
            return $exists;
        }


    }

    /**
     * @param $item_id
     * @param $quantity
     * @return CartProduct
     */
    public function updateItemQuantity($item_id, $quantity)
    {
        if (Auth::check()) {
            $userCart = $this->cartService->createOrReturnCartInstance($this->user_id);
            $item = CartProduct::whereCartIdAndProductId($userCart->id, $item_id)->first();
            if (!$item){
                $carts = \session('cart');
                $carts[$item_id]["quantity"] = $quantity;
                session()->put('cart', $carts);
                $cart = \session('cart');

                return $cart;
            }
//                return $item;
            $item->quantity = $quantity;
            $item->save();
            return $item;
        }


             $carts = \session('cart');
             $carts[$item_id]["quantity"] = $quantity;
              session()->put('cart', $carts);
              $cart = \session('cart');

             return $cart;
//        dd($carts);

    }

    /**
     * @param $item_id
     * @param $quantity
     * @return CartProduct
     */
    public function updateItemQuantityById($item_id, $quantity)
    {
        if (Auth::check()) {
            $userCart = $this->cartService->createOrReturnCartInstance($this->user_id);
            $productId = CartProduct::whereIdAndCartId($item_id, $userCart->id)->first();
            if(!$productId){
                return $this->updateItemQuantity($item_id, $quantity);
            }
            return $this->updateItemQuantity($productId->product_id, $quantity);
        }
        return $this->updateItemQuantity($item_id, $quantity);

    }

    /**
     * @param $item_id
     * @param int $quantity
     * @return CartProduct
     */
    public function increaseItemQuantity($item_id, $quantity = 1)
    {
        if (Auth::check()) {
            $userCart = $this->cartService->createOrReturnCartInstance($this->user_id);
            $oldItem = CartProduct::whereCartIdAndProductId($userCart->id, $item_id)->first();
            $newItem = $this->updateItemQuantity($item_id, $oldItem->quantity + $quantity);
            return $newItem;
        }else{
            $newItem = $this->updateItemQuantity($item_id, $quantity);
            return $newItem;
        }
    }

    /**
     * @param $item_id
     * @param int $quantity
     * @return CartProduct
     */
    public function decreaseItemQuantity($item_id, $quantity = 1)
    {
        if (Auth::check()) {
            $userCart = $this->cartService->createOrReturnCartInstance($this->user_id);

        $oldItem = CartProduct::whereCartIdAndProductId($userCart->id, $item_id)->first();
        if (!$oldItem)
            return false;
        if ($quantity >= $oldItem->quantity) {
            return $this->removeItem($item_id);
        }
        $this->updateItemQuantity($item_id, $oldItem->quantity - $quantity);
        return $oldItem;
        }
    }
    /**
     * @param $item_id
     * @return $mix
     */
    public function removeItem($item_id)
    {
        if (Auth::check()) {
            $userCart = $this->cartService->createOrReturnCartInstance($this->user_id);

        if ($this->getItemsCount() == 1)
            return $this->clearItems();
        $item = CartProduct::whereCartIdAndProductId($userCart->id, $item_id)->delete();
        return $item;
        }


        $carts = \session('cart');
//        $carts[$item_id]["quantity"] = $quantity;
        session()->forget('cart',$carts[$item_id]);
        session()->put('cart', $carts);
//        $cart = \session('cart');

//        return $cart;

    }

    public function removeItemById($item_id)
    {
        if (Auth::check()) {
            $userCart = $this->cartService->createOrReturnCartInstance($this->user_id);
            $product = CartProduct::whereIdAndCartId($item_id, $userCart->id)->first();
            return $this->removeItem($product->product_id);
        }

        $carts = \session('cart');
        if(isset($carts[$item_id])) {
            unset($carts[$item_id]);
            session()->put('cart', $carts);
        }
        $carts = \session('cart');
        return $carts;

    }

    /**
     * @return \Modules\Cart\Models\Cart
     */
    public function clearItems()
    {
        if (Auth::check()) {
            $userCart = $this->cartService->createOrReturnCartInstance($this->user_id);

        $this->cartService->destroyCartInstance($userCart->id);
        return $userCart;
        }



    }

    /**
     * @return int
     */
    public function totalPrice()
    {
        if (Auth::check()) {

            $items = $this->getItems();



            $total = 0;


            if(session('cart')) {
                foreach(session('cart') as $id => $details){
                    $total += $details['price'] * $details ['quantity'];

                }

                return $total;
            }

            if ($items != null) {
                foreach ($items as $item) {
                    $total += $item->getSubTotalAttribute();
                }
            }

            return $total;
        }

        $total = 0;
        if(session('cart')) {
            foreach(session('cart') as $id => $details){
                $total += $details['price'] * $details ['quantity'];

            }

            return $total;
        }
    }
}
