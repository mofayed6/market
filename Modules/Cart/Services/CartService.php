<?php

namespace Modules\Cart\Services;


use Modules\Cart\Models\Cart;
use Illuminate\Support\Facades\Auth;

class CartService
{
    /**
     * @param $user_id
     * @param $coupon_id
     * @return Cart
     */
    public function createCartInstance($user_id, $coupon_id = 0)
    {
        if (Auth::check()) {

            $cart = new Cart;
            $cart->user_id = $user_id;
            $cart->coupon_id = $coupon_id;
            $cart->save();
            return $cart;
        }
    }

    /**
     * @param $user_id
     * @param $coupon_id
     * @return Cart
     */
    public function createOrReturnCartInstance($user_id, $coupon_id = 0)
    {
        if (Auth::check()) {

            $cart = Cart::whereUserIdAndCouponId($user_id, $coupon_id)->first();
            if ($cart)
                return $cart;

            $cart = $this->createCartInstance($user_id, $coupon_id);
            return $cart;
        }
    }

    /**
     * @param $cartId
     * @return bool
     */
    public function destroyCartInstance($cartId)
    {
        if (Auth::check()) {

            $cart = Cart::find($cartId);
            $cart->delete();
            return true;
        }
    }

}
