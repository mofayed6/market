<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('classifications')->middleware('admin:admin')->group(function() {
    Route::get('create', 'ClassificationsController@create');
    Route::get('/datatable', 'ClassificationsController@datatable');
    Route::get('/', 'ClassificationsController@index');
    Route::post('/', 'ClassificationsController@store');
    Route::get('{classification}/edit', 'ClassificationsController@edit');
    Route::patch('{classification}', 'ClassificationsController@update');
    Route::get('{classification}/delete', 'ClassificationsController@destroy');
});
