<?php

namespace Modules\Classifications\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Classifications\Models\Classification;

class ClassificationsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        for ($i = 1; $i < 6; ++$i) {
            $classifications = Classification::create([
                'id' => $i,
                'title'=>'{"ar":"تجريبى'.$i.'","en":"classifications'.$i.'"}',

            ]);

            //$branch->translateAttributes($branchesInfo[$i]);
        }

        // $this->call("OthersTableSeeder");
    }
}
