@extends('admin.layouts.master')
@section('page-title','Edit '.$user->name)
@section('breadcrumb')
    <li class="breadcrumb-item"><a href='{{url('admin/user')}}'>Users</a></li>

    <li class="breadcrumb-item active">Update </li>
@endsection
@section('content')
    <div class="card">
        <div class="header">
            <h2>Edit {{$user->first_name}}</h2>
        </div>
        <div class="body">
            @include('admin.pratical.message')
            {{Form::model($user,['method'=>'PATCH','action'=>['UserController@update',$user->id]])}}
            @include('user::form')
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function() {
            $('.datatable').DataTable({
                processing: true,
                searching: true,
                serverSide: true,
                ajax: '{!! url('admin/user/datatable') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'gender', name: 'gender' },
                    { data: 'mobile', name: 'mobile' },
                    { data: 'status', name: 'status' },
                ],
            });
        });





    </script>
@stop
