@extends('admin.layouts.master')
@section('page-title','Users')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href='{{url('admin/user')}}'>Users</a></li>

    <li class="breadcrumb-item active">Create New</li>
@endsection
@section('content')
    <div class="card">
        <div class="header">
                <a href="{{url('admin/user/create/')}}" class="btn btn-primary pull-right"><i class="fa fa-plus-square"></i> <span>Add New</span></a>

            <h2>Users list</h2>
        </div>
        <div class="body">
            @include('admin.pratical.message')
            <div class="table-responsive">
                <table class="table table-hover m-b-0 c_list datatable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>name</th>
                        <th>gender</th>
                        <th>mobile</th>
                        <th>status</th>
                        <th>options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function() {
            $('.datatable').DataTable({
                processing: true,
                searching: true,
                serverSide: true,
                ajax: '{!! url('admin/user/datatable') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'gender', name: 'gender' },
                    { data: 'mobile', name: 'mobile' },
                    { data: 'status', name: 'status' },
                    { data: 'options', name: 'options' },
                ],
            });
        });

    </script>
@stop
