<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\DeliveryLocation\Models\UserAddress;
use Modules\Products\Models\Product;

class User extends Authenticatable
{
    protected $table = 'users';
    protected $fillable = ['first_name', 'last_name', 'gender', 'date_of_birth', 'mobile', 'home_number', 'email', 'status'];

    public function addresses()
    {
        return $this->hasMany(UserAddress::class);
    }

    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function favorites()
    {
        return $this->belongsToMany(Product::class, 'product_favorites', 'user_id', 'product_id');
    }
}
