<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 25);
            $table->string('last_name', 25);
            $table->enum('gender', ['male', 'female']);
            $table->date('date_of_birth');
            $table->string('mobile', 11);
            $table->string('image', 255);
            $table->string('home_number', 10)->nullable();
            $table->string('email', 60);
            $table->integer('activation_code');
            $table->boolean('status')->comment('0=>inactive | 1=>active | 2=> disabled | 3=>blocked');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
