<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\User\Models\User;
use Illuminate\Support\Facades\Hash;

class UserDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        for ($i = 1; $i < 10; $i++) {
            User::create([
                'first_name' => 'User',
                'last_name' => "{$i}",
                'gender' => 'male',
                'date_of_birth' => '2018-12-26',
                'mobile' => "1010101010",
                'home_number' => "2020202020",
                'email' => "user{$i}@alfamarket.com",
                'password' => Hash::make('secret_password'),
                'status' => 1,
            ]);
        }

        // $this->call("OthersTableSeeder");
    }
}
