<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('admin:admin')->prefix('admin/categories')->group(function() {
    Route::get('datatable','CategoriesController@datatable');
    Route::get('create/{parent?}', 'CategoriesController@create');
    Route::get('/{category}/delete', 'CategoriesController@destroy');
    Route::get('/{category}/edit', 'CategoriesController@edit');
    Route::patch('/{category}/update', 'CategoriesController@update');
    Route::get('/{parent?}', 'CategoriesController@index');
    Route::post('/','CategoriesController@store');
    Route::get('slugify/{string}/{locale}','CategoriesController@slugify');

});
