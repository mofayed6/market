<?php

namespace Modules\Categories\Http\Controllers;

use Modules\Attributes\Models\Attribute;
use Modules\Languages\Models\Local;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use \App\Http\Controllers\Controller;
use Modules\Categories\Models\Category;
use Modules\Languages\Models\Translation;

class CategoriesController extends Controller
{
    protected $results = [];
    protected $i = 0;

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($parent_id = 0)
    {
        if ($parent_id) {
            $parent = Category::findOrFail($parent_id);
           // dd($name);
        }

        return view('categories::index', compact('parent', 'parent_id'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request, $parent = 0)
    {

       // dd($request->all());

        if ($parent)
            Category::findOrFail($request->parent);
        $locales = Local::all();
      //  dd($categories);


        $categories = Category::getChildrenFlat(0, 3);

//        foreach ($categories as $category) {
//            dd($category);
//
//            $selected_categiries = json_decode($category->name, true);
//
//            $selects_category[$category->id] = $selected_categiries[app()->getLocale()];
//        }


        if ($parent = 0) {
            $depth = count(Category::getParentsFlat($parent));
            if ($depth >= 4) {
                abort(503);
            }
        }

        $parentCategories = Category::getParentsFlat($parent);
       // dd($parentCategories);
        $parentAttributes = Attribute::getAttributesByCategoriesArray(array_keys($parentCategories));
       // dd($parentAttributes);
        $parent = $request->parent;
        return view('categories::create', compact('locales', 'categories', 'parent', 'parentAttributes'));
    }

    public function store(Request $request)
    {
       // dd($request->all());

        $this->validate($request, [
            'en.name' => 'required',
            'en.slug' => 'required',
            'ar.name' => 'required',
            'ar.slug' => 'required',
        ]);




        $category = new Category();

        $name = json_encode(['ar' => $request->ar['name'] , 'en' => $request->en['name']],JSON_UNESCAPED_UNICODE);
        $description = json_encode(['ar' => $request->ar['description'] , 'en' => $request->en['description']],JSON_UNESCAPED_UNICODE);
        $slug = json_encode(['ar' => $request->ar['slug'] , 'en' => $request->en['slug']],JSON_UNESCAPED_UNICODE);



        $category->name        = $name;
        $category->description = $description;
        $category->slug        = $slug;
        $category->parent_id   = $request->parent_id;
        $category->is_active   = $request->is_active ?? 0;
        $category->show_in_navbar = $request->show_in_navbar ?? 0;
        $category->save();
       // $category->translateAttributes($request->only('ar', 'en'));
        if ($request->has('attributes')) {
            $attrList = [];
            $attributes = json_decode($request->get('attributes'));
            if (is_array($attributes) && count($attributes) > 0)
                foreach ($attributes as $attribute) {
                $attrList[] = $attribute->id;
            }
            $category->attributes()->sync($attrList);
        }
        return redirect(url('admin/categories/' . $category->parent_id))->with(['success' => 'Category Created Successfully']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('categories::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Category $category)
    {
        //$locales = Local::all();
        $categories = Category::getChildrenFlat(0, 3);
     //   dd($categories);
        $attributes = $category->attributes;
       // dd($attributes);
        if (count($attributes) > 0) {
            $attributesList = [];
            foreach ($attributes as $k => $attribute) {
               // dd($attribute);
                $attributesList[$k]['id'] = $attribute->id;
                $name = json_decode($attribute->name,true);
               // dd($name['ar']);
              //  dd($attributesList[$k]);
                $attributesList[$k]['name'] = $name['ar'] . ' - ' . $name['en'];
            }
            $category->attributes_str = json_decode(json_encode($attributesList));
            $category->attributes = json_encode($attributesList);
        }
        $parentCategories = Category::getParentsFlat($category->id);
        $parentAttributes = Attribute::getAttributesByCategoriesArray(array_keys($parentCategories));
        $parent = $category->parent_id;
        $name = json_decode($category->name,true);
        $slug = json_decode($category->slug,true);
        $description = json_decode($category->description,true);
        return view('categories::edit', compact('name','category', 'slug','description','categories', 'parent', 'parentAttributes'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Category $category, Request $request)
    {
        $this->validate($request, [
            'en.name' => 'required',
            'en.slug' => 'required',
            'ar.name' => 'required',
            'ar.slug' => 'required',
        ]);

        $name = json_encode(['ar' => $request->ar['name'] , 'en' => $request->en['name']],JSON_UNESCAPED_UNICODE);
        $description = json_encode(['ar' => $request->ar['description'] , 'en' => $request->en['description']],JSON_UNESCAPED_UNICODE);
        $slug = json_encode(['ar' => $request->ar['slug'] , 'en' => $request->en['slug']],JSON_UNESCAPED_UNICODE);

        $category->name        = $name;
        $category->description = $description;
        $category->slug        = $slug;
        $category->parent_id = $request->parent_id;
        $category->is_active = $request->is_active ?? 0;
        $category->show_in_navbar = $request->show_in_navbar ?? 0;
        $category->save();
      //  $category->deleteTranslations();
       // $category->translateAttributes($request->only('ar', 'en'));
        if ($request->has('attributes')) {
            $attrList = [];
            $attributes = json_decode($request->get('attributes'));
            if (is_array($attributes) && count($attributes) > 0)
                foreach ($attributes as $attribute) {
                $attrList[] = $attribute->id;
            }
            $category->attributes()->sync($attrList);
        }
        return redirect(url('admin/categories/' . $category->parent_id))->with(['success' => 'Category Updated Successfully']);
    }


    /**
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Category $category)
    {
        $category->delete();
      //  $category->deleteTranslations();
        return redirect(url('admin/categories/' . $category->parent_id))->with(['success' => 'Category Deleted Successfully']);

    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function datatable(Request $request)
    {
        $categories = Category::whereParentId($request->parent ?? 0)->get();
        return \DataTables::of($categories)
            ->editColumn('name', function ($model) {
                $enName = json_decode($model->name,true);
                return "<a href='" . url('admin/categories/' . $model->id) . "'>" .$enName['en']. "</a>";
            })->editColumn('options', function ($model) {
                return "<a href='" . url('admin/categories/' . $model->id . '/edit') . "' class='btn btn-warning'>Edit</a>
                <a href='" . url('admin/categories/' . $model->id . '/delete') . "' class='btn btn-danger'>Delete</a>";

            })
            ->make(true);
    }

//    public function slugify($string, $locale_code)
//    {
//        $slug = Category::makeSlug($string, $locale_code);
//        return response()->json(['slug' => $slug]);
//    }
}
