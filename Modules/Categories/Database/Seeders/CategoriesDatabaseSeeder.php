<?php

namespace Modules\Categories\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Categories\Models\Category;

class CategoriesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $category_1 = Category::create([
            'parent_id' => 0,
            'name'=> '{"ar":"التصنيف 1","en":"Category 1"}',
            'slug'=> '{"ar":"التصنيف 1","en":"Category 1"}',
            'description'=> '{"ar":"التصنيف 1","en":"Category 1"}',
            'is_active' => 1,
            'show_in_navbar' => 1
        ]);

       // $category_1->translateAttributes([
//            'en' => [
//                'name' => "Category " . $category_1->id,
//                'slug' => "category-" . $category_1->id,
//            ],
//            'ar' => [
//                'name' => "التصنيف " . $category_1->id,
//                'slug' => "التصنيف-" . $category_1->id,
//            ],
//        ]);

        $category_2 = Category::create([
            'parent_id' => 0,
            'is_active' => 1,
            'name'=> '{"ar":"التصنيف 2","en":"Category 2"}',
            'slug'=> '{"ar":"التصنيف 2","en":"Category 2"}',
            'description'=> '{"ar":"التصنيف 2","en":"Category 2"}',
            'show_in_navbar' => 1
        ]);

//        $category_2->translateAttributes([
//            'en' => [
//                'name' => "Category " . $category_2->id,
//                'slug' => "category-" . $category_2->id,
//            ],
//            'ar' => [
//                'name' => "التصنيف " . $category_2->id,
//                'slug' => "التصنيف-" . $category_2->id,
//            ],
//        ]);

        $subcategory_11 = Category::create([
            'parent_id' => $category_1->id,
            'name'=> '{"ar":"التصنيف 3","en":"Category 3"}',
            'slug'=> '{"ar":"التصنيف 3","en":"Category 3"}',
            'description'=> '{"ar":"التصنيف 3","en":"Category 3"}',
            'is_active' => 1,
            'show_in_navbar' => 1
        ]);

//        $subcategory_11->translateAttributes([
//            'en' => [
//                'name' => "Sub-Category [" . $category_1->id . "-" . $subcategory_11->id . "]",
//                'slug' => "sub-category-[" . $category_1->id . "-" . $subcategory_11->id . "]",
//            ],
//            'ar' => [
//                'name' => "التصنيف الفرعي [" . $category_1->id . "-" . $subcategory_11->id . "]",
//                'slug' => "التصنيف-الفرعي-[" . $category_1->id . "-" . $subcategory_11->id . "]",
//            ],
//        ]);

        $subcategory_12 = Category::create([
            'parent_id' => $category_1->id,
            'name'=> '{"ar":"التصنيف 4","en":"Category 4"}',
            'slug'=> '{"ar":"التصنيف 4","en":"Category 4"}',
            'description'=> '{"ar":"التصنيف 4","en":"Category 4"}',
            'is_active' => 1,
            'show_in_navbar' => 1
        ]);
//        $subcategory_11->translateAttributes([
//            'en' => [
//                'name' => "Sub-Category [" . $category_1->id . "-" . $subcategory_11->id . "]",
//                'slug' => "sub-category-[" . $category_1->id . "-" . $subcategory_11->id . "]",
//            ],
//            'ar' => [
//                'name' => "التصنيف الفرعي [" . $category_1->id . "-" . $subcategory_11->id . "]",
//                'slug' => "التصنيف-الفرعي-[" . $category_1->id . "-" . $subcategory_11->id . "]",
//            ],
//        ]);

//        $subcategory_12->translateAttributes([
//            'en' => [
//                'name' => "Sub-Category [" . $category_1->id . "-" . $subcategory_12->id . "]",
//                'slug' => "sub-category-[" . $category_1->id . "-" . $subcategory_12->id . "]",
//            ],
//            'ar' => [
//                'name' => "التصنيف الفرعي [" . $category_1->id . "-" . $subcategory_12->id . "]",
//                'slug' => "التصنيف-الفرعي-[" . $category_1->id . "-" . $subcategory_12->id . "]",
//            ],
//        ]);

        $subcategory_21 = Category::create([
            'parent_id' => $category_2->id,
            'name'=> '{"ar":"التصنيف 5","en":"Category 5"}',
            'slug'=> '{"ar":"التصنيف 5","en":"Category 5"}',
            'description'=> '{"ar":"التصنيف 5","en":"Category 5"}',
            'is_active' => 1,
            'show_in_navbar' => 1
        ]);

//        $subcategory_21->translateAttributes([
//            'en' => [
//                'name' => "Sub-Category [" . $category_2->id . "-" . $subcategory_21->id . "]",
//                'slug' => "sub-category-[" . $category_2->id . "-" . $subcategory_21->id . "]",
//            ],
//            'ar' => [
//                'name' => "التصنيف الفرعي [" . $category_2->id . "-" . $subcategory_21->id . "]",
//                'slug' => "التصنيف-الفرعي-[" . $category_2->id . "-" . $subcategory_21->id . "]",
//            ],
//        ]);
        // $this->call("OthersTableSeeder");
    }
}
