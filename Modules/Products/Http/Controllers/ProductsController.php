<?php

namespace Modules\Products\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Attributes\Models\Attribute;
use Modules\Branches\Models\Branch;
use Modules\Categories\Models\Category;
use Modules\Classifications\Models\Classification;
use Modules\Identifiers\Models\Identifier;
use Modules\Products\Models\Product;
use Modules\Products\Models\ProductImage;
use Modules\Units\Models\Unit;
use DB;
use Modules\Attributes\Models\AttributeProduct;

class ProductsController extends Controller
{

    protected $list = [];
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('products::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $branches = Branch::get();
        foreach ($branches as $branch) {

            $selected_types = json_decode($branch->name, true);

            $selects_branches[$branch->id] = $selected_types[app()->getLocale()];
        }

        $categories = Category::getChildrenFlat();

        $units = Unit::get();
        foreach ($units as $unit) {

            $selected_types = json_decode($unit->name, true);

            $selects_units[$unit->id] = $selected_types[app()->getLocale()];
        }


        $classifications = Classification::get();
       // dd($classifications);
//        foreach ($classifications as $classification) {
//
//            $selected_classifications = json_decode($classification->title, true);
//
//            $selects_classification[$unit->id] = $selected_classifications[app()->getLocale()];
//        }


       // dd($selects_classification);

       $productAttributesValues = [];

       if (old('attribute_product')) {
               $productAttributesValues = old('attribute_product');
       }

       $productAttributesValues = json_encode($productAttributesValues, JSON_UNESCAPED_UNICODE);

        $identifiers = Identifier::pluck('identifier', 'id');
        return view('products::create', compact('productAttributesValues', 'categories', 'selects_units', 'classifications', 'selects_branches', 'classifications', 'identifiers'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'en.name' => 'required',
            'ar.name' => 'required',
            'preparation_duration' => 'required',
            'unit' => 'required',
            'unit_volume' => 'required',
            'category_id' => 'required',
            'price' => 'required',
            'discount_price' => 'required',
            'featured_image' => 'required|image',
        ]);

        $product = new Product();

        $name = json_encode(['ar' => $request->ar['name'], 'en' => $request->en['name']], JSON_UNESCAPED_UNICODE);
        $description = json_encode(['ar' => $request->ar['description'], 'en' => $request->en['description']], JSON_UNESCAPED_UNICODE);
        $tags = json_encode(['ar' => $request->ar['tags'], 'en' => $request->en['tags']], JSON_UNESCAPED_UNICODE);

        $product->name = $name;
        $product->description = $description;
        $product->tags = $tags;
        $product->barcode = $request->barcode;
        $product->preparation_duration = $request->preparation_duration;
        $product->unit_id = $request->unit;
        $product->unit_volume = $request->unit_volume;
        $product->category_id = $request->category_id;
        $product->price = $request->price;
        if (isset($request->available)){
            $product->available = $request->available;
        }
        $product->discount_price = $request->discount_price;
        $product->identifier_id = $request->identifier_id;
        $product->featured_image = $request->featured_image->store('products');
        $product->save();
       // $product->translateAttributes($request->only('ar', 'en'));
        if ($request->filled('uploadedFiles')) {
            $files = json_decode($request->get('uploadedFiles'));
            ProductImage::whereIn('id', $files)->update(['product_id' => $product->id]);
        }

        $product->classifications()->sync($request->classifications);
       // $pivotData = array($request->attribute_id => array('value' => $request->attribute_product));

        if ($request->attribute_product != null) {

            $data = [];
            foreach ($request->attribute_product as $attributeId => $translations) {
                if (!empty($attributeId) && !empty($translations) && !empty($translations['en']) && !empty($translations['ar'])) {
                    $data[$attributeId] = [
                        'value' => json_encode([
                            'en' => $translations['en'],
                            'ar' => $translations['ar'],
                        ], JSON_UNESCAPED_UNICODE)
                    ];
                }
            }

            $product->attributes()->sync($data);
        }

        $datae = [];
        foreach ($request->branch_id as $k => $t) {
            $datae[$t] = ['stock_amount' => $request->stock_amount[$k] ,'price' => $request->pricee[$k] ,'discount_price' => $request->discount_pricee[$k]];
        }

        $product->branches()->sync($datae);

        return redirect('admin/products')->with(['success' => 'product created successfully']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        // return view('products::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $category_id = $product->category_id;

        $name = json_decode($product->name, true);
        $description = json_decode($product->description, true);
        $tags = json_decode($product->tags, true);

//        $branches = Branch::all()->pluck('name', 'id');
        $branches = Branch::get();
        foreach ($branches as $branch) {

            $selected_types = json_decode($branch->name, true);

            $selects_branches[$branch->id] = $selected_types[app()->getLocale()];
        }


        $brancheProducts = Product::productBranch($product->id);
        $categories = Category::getChildrenFlat();
      //$units = Unit::all()->pluck('name', 'id');

        $units = Unit::get();
        foreach ($units as $unit) {

            $selected_types = json_decode($unit->name, true);

            $selects_units[$unit->id] = $selected_types[app()->getLocale()];
        }

        $productAttributes = AttributeProduct::where('product_id', $product->id)->get();
        $productAttributesValues = [];

        if (!empty($productAttributes) && count($productAttributes)) {
            foreach ($productAttributes as $prdctAtr) {
                $productAttributesValues[$prdctAtr->attribute_id] = $prdctAtr->value;
            }
        }

        $productAttributesValues = json_encode($productAttributesValues, JSON_UNESCAPED_UNICODE);

        $classifications = Classification::all();
        $identifiers = Identifier::pluck('identifier', 'id');
        return view('products::edit', compact('productAttributesValues', 'category_id', 'categories', 'units', 'selects_branches', 'selects_units', 'name', 'description', 'tags', 'branches', 'brancheProducts', 'classifications', 'product', 'identifiers'));
    }


    /**
     * @param Request $request
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Product $product)
    {
        $this->validate($request, [
            'en.name' => 'required',
            'ar.name' => 'required',
            'preparation_duration' => 'required',
            'unit' => 'required',
            'unit_volume' => 'required',
            'category_id' => 'required',
            'price' => 'required',
            'discount_price' => 'required',
            'featured_image' => 'image',
        ]);


        $name = json_encode(['ar' => $request->ar['name'], 'en' => $request->en['name']], JSON_UNESCAPED_UNICODE);
        $description = json_encode(['ar' => $request->ar['description'], 'en' => $request->en['description']], JSON_UNESCAPED_UNICODE);
        $tags = json_encode(['ar' => $request->ar['tags'], 'en' => $request->en['tags']], JSON_UNESCAPED_UNICODE);

        $product->name = $name;
        $product->description = $description;
        $product->tags = $tags;

        $product->barcode = $request->barcode;
        $product->preparation_duration = $request->preparation_duration;
        $product->unit_id = $request->unit;
        $product->unit_volume = $request->unit_volume;
        $product->category_id = $request->category_id;
        if (isset($request->available)){
            $product->available = $request->available;
        }else{
            $product->available = 0;
        }
        $product->price = $request->price;
        $product->discount_price = $request->discount_price;
        $product->identifier_id = $request->identifier_id;
        $product->featured_image = ($request->hasFile('featured_image')) ? $request->featured_image->store('products') : $product->featured_image;
        $product->save();
        // $product->deleteTranslations();
      //  $product->translateAttributes($request->only('ar', 'en'));
        if ($request->filled('uploadedFiles')) {
            $files = json_decode($request->get('uploadedFiles'));
            ProductImage::whereIn('id', $files)->update(['product_id' => $product->id]);
        }
        $product->classifications()->sync($request->classifications);

        if ($request->attribute_product != null) {
            $data = [];

            foreach ($request->attribute_product as $attributeId => $translations) {
                if (!empty($attributeId) && !empty($translations) && !empty($translations['en']) && !empty($translations['ar'])) {
                    $data[$attributeId] = [
                        'value' => json_encode([
                            'en' => $translations['en'],
                            'ar' => $translations['ar'],
                        ], JSON_UNESCAPED_UNICODE)
                    ];
                }
            }

            $product->attributes()->detach();

            $product->attributes()->attach($data);
        }

        $datae = [];
        foreach ($request->branch_id as $k => $t) {
            $datae[$t] = ['stock_amount' => $request->stock_amount[$k] ,'price' => $request->pricee[$k] ,'discount_price' => $request->discount_pricee[$k]];
        }


        $product->branches()->detach();

        $product->branches()->attach($datae);

        //  $product->attributes()->attach($data);

        return redirect('admin/products')->with(['success' => 'product updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
       // $product->deleteTranslations();
        return redirect('admin/products')->with(['success' => 'product deleted successfully']);

    }

    public function uploadImages(Request $request)
    {
        $this->validate($request, [
            'images' => 'required',
            'images.*' => 'image',
        ]);
        $res = [];
        foreach ($request->images as $k => $image) {
            $img = new ProductImage();
            $img->image = $image->store('products');
            $img->product_id = null;
            $img->save();
            $res = $img->id;
        }
        return response()->json($res);
    }

    public function deleteImage(Request $request)
    {
        ProductImage::where('image', $request->key)->delete();
        return response()->json(['success' => 'true']);
    }

    public function datatable()
    {
        $locations = Product::query();
        return \DataTables::eloquent($locations)
            ->addColumn('name', function ($model) {
                $name = json_decode($model->name, true);
                return $name['en'];
            })->editColumn('options', function ($model) {
                return "<a href='" . url('admin/products/' . $model->id . '/edit') . "' class='btn btn-warning'>Edit</a>
                <a href='" . url('admin/products/' . $model->id . '/delete') . "' class='btn btn-danger'>Delete</a>";

            })
            ->make(true);
    }



    public function chechRecusive($father)
    {
        if ($father->parentRecursive === null) {
            return $this->list;

        } else {
            $this->list[] = $father->parentRecursive->id;
            $this->chechRecusive($father->parentRecursive);
        }
    }

    public function attributeCategory($parent_id)
    {

        $fathers = Category::with('parentRecursive')->where('id', $parent_id)->first();

        $this->chechRecusive($fathers);

        array_push($this->list, $parent_id);

        $attributesID = \DB::table('category_attributes')->whereIn('category_id', $this->list)->pluck('attribute_id');
        $attributes = Attribute::find($attributesID);
        return $attributes;
    }

    public function CheckFTP()
    {
        return view('products::FTP.check');
    }
}
