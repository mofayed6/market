<?php

namespace Modules\Products\Http\Controllers;

use Modules\Products\Models\Product;
use Modules\Categories\Models\Category;
use Modules\Attributes\Models\AttributeProduct;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Modules\Products\Models\ProductFavorite;
use Modules\User\Models\User;
use App\Http\Controllers\BaseController;
use Modules\Products\Models\BranchProduct;
use Modules\Attributes\Models\Attribute;

class ProductsSiteController extends BaseController
{
    public function index(Request $request, $locale, $categoryId)
    {
        $this->viewData['orderBy'] = 'orders_count';
        $this->viewData['orderDirection'] = 'desc';

        $orderBy = $request->query('order_by', 'orders_count-desc');
        $orderByParts = explode('-', $orderBy);

        if (in_array($orderByParts[0] ?? 0, ['orders_count', 'actual_price'])) {
            $this->viewData['orderBy'] = $orderByParts[0];
        }

        if (in_array($orderByParts[1] ?? 0, ['asc', 'desc'])) {
            $this->viewData['orderDirection'] = $orderByParts[1];
        }

        $this->viewData['attributesQuery'] = $request->query('attribute');

        $price_fromQuery = $request->query('price_from');
        $price_toQuery = $request->query('price_to');

        $categoryId = (int)$categoryId;
        $this->viewData['category'] = Category::findOrFail($categoryId);
        $attributesValuesQuery = AttributeProduct::whereIn('product_id', function ($query) {
            $query->select('id')
                ->from(with(new Product)->getTable())
                ->where('category_id', $this->viewData['category']->id);
        })
            ->whereIn('product_id', function ($subQuery) {
                $subQuery->select('product_id')
                    ->from(with(new BranchProduct)->getTable())
                    ->where('branch_id', getCurrentBranch()->id)
                    ->where('stock_amount', '>', 0);
            });

        if (!empty($price_fromQuery)) {
            $attributesValuesQuery->whereIn('product_id', function ($query) use ($price_fromQuery) {
                $query->select('id')
                    ->from(with(new Product)->getTable())
                    ->where('price', '>=', $price_fromQuery)
                    ->orWhere('discount_price', '>=', $price_fromQuery);
            });
        }

        if (!empty($price_toQuery)) {
            $attributesValuesQuery->whereIn('product_id', function ($query) use ($price_toQuery) {
                $query->select('id')
                    ->from(with(new Product)->getTable())
                    ->where('price', '<=', $price_toQuery)
                    ->orWhere('discount_price', '<=', $price_toQuery);
            });
        }

        $this->viewData['attributes_values'] = $attributesValuesQuery->get();
        $this->viewData['attributes'] = Attribute::whereIn(
            'id',
            $this->viewData['attributes_values']->pluck('attribute_id')->toArray()
        )->get();

        $baseProducts = Product::getProductsBaseQuery(
            $locale,
            [
                'categoryId' => $this->viewData['category']->id,
                'attributesQuery' => $this->viewData['attributesQuery'],
                'price_fromQuery' => $price_fromQuery,
                'price_toQuery' => $price_toQuery
            ]
        );

        $this->viewData['products'] = (clone $baseProducts)->orderBy($this->viewData['orderBy'], $this->viewData['orderDirection'])->paginate(40);
        $this->viewData['all_products_count'] = (clone $baseProducts)->count();
        $min_priceRow = Product::selectRaw('LEAST(min(discount_price), min(price)) AS min_price')->where('category_id', $this->viewData['category']->id)->first();
        $max_priceRow = Product::selectRaw('GREATEST(max(discount_price), max(price)) AS max_price')->where('category_id', $this->viewData['category']->id)->first();
        $this->viewData['min_price'] = !empty($min_priceRow) ? intval($min_priceRow->min_price) : null;
        $this->viewData['max_price'] = !empty($max_priceRow) ? intval($max_priceRow->max_price) : null;
        $this->viewData['old_price_from'] = !empty(intval($price_fromQuery)) ? intval($price_fromQuery) : $this->viewData['min_price'];
        $this->viewData['old_price_to'] = !empty(intval($price_toQuery)) ? intval($price_toQuery) : $this->viewData['max_price'];

        $this->viewData['pageTitle'] = $this->viewData['category']->translate('name', $locale);
        return view('site.products.products', $this->viewData);
    }


    public function search(Request $request, $locale)
    {
        $this->viewData['orderBy'] = 'orders_count';
        $this->viewData['orderDirection'] = 'desc';

        $keywords = $request->query('keywords');
        $keywordsArr = [sanitizeString($keywords)];
        if (mb_strpos($keywords, ' ') !== false) {
            $keywordsArr = [];
            $keywordsParts = explode(' ', $keywords);
            foreach ($keywordsParts as $keyword) {
                if (!empty($keyword)) {
                    $keywordsArr[] = sanitizeString($keyword);
                }
            }
        }

        if (empty($keywordsArr)) {
            redirect()->route('homepage.index');
        }

        $this->viewData['keywords'] = implode(' ', $keywordsArr);

        $orderBy = $request->query('order_by', 'orders_count-desc');
        $orderByParts = explode('-', $orderBy);

        if (in_array($orderByParts[0] ?? 0, ['orders_count', 'actual_price'])) {
            $this->viewData['orderBy'] = $orderByParts[0];
        }

        if (in_array($orderByParts[1] ?? 0, ['asc', 'desc'])) {
            $this->viewData['orderDirection'] = $orderByParts[1];
        }

        $this->viewData['attributesQuery'] = $request->query('attribute');

        $price_fromQuery = $request->query('price_from');
        $price_toQuery = $request->query('price_to');

        $attributesValuesQuery = AttributeProduct::whereIn('product_id', function ($subQuery) {
            $subQuery->select('product_id')
                ->from(with(new BranchProduct)->getTable())
                ->where('branch_id', getCurrentBranch()->id)
                ->where('stock_amount', '>', 0);
        });

        if (!empty($price_fromQuery)) {
            $attributesValuesQuery->whereIn('product_id', function ($query) use ($price_fromQuery) {
                $query->select('id')
                    ->from(with(new Product)->getTable())
                    ->where('price', '>=', $price_fromQuery)
                    ->orWhere('discount_price', '>=', $price_fromQuery);
            });
        }

        $attributesValuesQuery->whereIn('product_id', function ($query) use ($keywordsArr, $locale) {
            $query->select('id')->from(with(new Product)->getTable());
            $query->where('name', 'LIKE', "%" . $this->viewData['keywords'] . "%");
            foreach ($keywordsArr as $keyword) {
                $query->orWhere('name', 'LIKE', "%" . $keyword . "%");
            }
        });

        if (!empty($price_toQuery)) {
            $attributesValuesQuery->whereIn('product_id', function ($query) use ($price_toQuery) {
                $query->select('id')
                    ->from(with(new Product)->getTable())
                    ->where('price', '<=', $price_toQuery)
                    ->orWhere('discount_price', '<=', $price_toQuery);
            });
        }

        $this->viewData['attributes_values'] = $attributesValuesQuery->get();
        $this->viewData['attributes'] = Attribute::whereIn(
            'id',
            $this->viewData['attributes_values']->pluck('attribute_id')->toArray()
        )->get();

        $baseProducts = Product::getProductsBaseQuery(
            $locale,
            [
                'keywordsArr' => $keywordsArr,
                'attributesQuery' => $this->viewData['attributesQuery'],
                'price_fromQuery' => $price_fromQuery,
                'price_toQuery' => $price_toQuery
            ]
        );

        $this->viewData['products'] = (clone $baseProducts)->orderBy($this->viewData['orderBy'], $this->viewData['orderDirection'])->paginate(40);
        $this->viewData['all_products_count'] = (clone $baseProducts)->count();
        $this->viewData['min_price'] = null;
        $this->viewData['max_price'] = null;
        $this->viewData['old_price_from'] = !empty(intval($price_fromQuery)) ? intval($price_fromQuery) : $this->viewData['min_price'];
        $this->viewData['old_price_to'] = !empty(intval($price_toQuery)) ? intval($price_toQuery) : $this->viewData['max_price'];

        $this->viewData['pageTitle'] = "Search for " . $this->viewData['keywords'];
        return view('site.products.products', $this->viewData);
    }


    public function view(Request $request, $locale, $productId)
    {
        $productId = (int)$productId;
        $this->viewData['product'] = Product::with(['attributes', 'images', 'category'])->findOrFail($productId);
        $this->viewData['product']->incrementViewsCount();
        $this->viewData['attributes_values'] = AttributeProduct::where('product_id', $productId)->get();
        $this->viewData['grandParentCategories'] = Category::getParentsFlat($this->viewData['product']->category_id);
        $this->viewData['favorite'] = ProductFavorite::where(['product_id' => $productId, 'user_id' => auth()->id()])->first();

        $this->viewData['mostViewedProducts'] = Product::mostViewedInCategory($this->viewData['product']->category_id, 10);
        $this->viewData['trendingProducts'] = Product::trendingInCategory($this->viewData['product']->category_id, 10);

        return view('site.products.product_view', $this->viewData);
    }


    public function wish_list($locale)
    {
        $this->viewData['products'] = User::where('id', auth()->id())->with('favorites')->first()->favorites;

        return view('site.products.wish_list', $this->viewData);
    }

    public function add_wish_list($locale, $product_id)
    {
        $exist = ProductFavorite::where(['user_id' => auth()->id(), 'product_id' => $product_id])->first();
        if (empty($exist)) {
            ProductFavorite::create(['user_id' => auth()->id(), 'product_id' => $product_id]);
        }

        echo 1;
    }

    public function remove_wish_list($locale, $product_id)
    {
        $exist = ProductFavorite::where(['user_id' => auth()->id(), 'product_id' => $product_id])->first();
        if (!empty($exist)) {
            $exist->delete();
        }

        echo 1;
    }
}
