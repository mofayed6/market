<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->string('tags')->default('');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('unit_id')->default(0);
            $table->unsignedInteger('identifier_id')->nullable();
            $table->string('internal_id');
            $table->json('barcode')->nullable();
            $table->integer('unit_volume')->nullable();
            $table->integer('preparation_duration')->nullable();
            $table->decimal('price');
            $table->integer('views_count')->default(0);
            $table->integer('orders_count')->default(0);
            $table->decimal('discount_price')->nullable();
            $table->tinyInteger('available')->default(0);
            $table->string('featured_image', 255);
            $table->timestamps();
        });
        //set integers length
        Schema::table('products', function (Blueprint $table) {
            //foreign key constrains
            // $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            // $table->foreign('unit_id')->references('id')->on('units')->onDelete('cascade');
            // $table->foreign('identifier_id')->references('id')->on('identifiers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
