<?php

namespace Modules\Products\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Products\Models\ProductImage;
use Modules\Products\Models\Product;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Modules\Categories\Models\Category;
use Modules\Products\Models\BranchProduct;
use Modules\Branches\Models\Branch;
use Modules\Attributes\Models\Attribute;

class ProductsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Storage::persistentFake("products");

        $branches = Branch::all();
        $category_1 = Category::find(1);
        $attributes_1 = Attribute::whereIn('id', [1, 2, 3, 4, 5])->get();
        for ($i = 1; $i <= 5; $i++) {
            $product = Product::create([
                'name' => '{"ar":"المنتج' . $i . '","en":"Product' . $i . '"}',
                'description' => '{"ar":"وصف المنتج  ' . $i . '","en":"description' . $i . '"}',
                'barcode' => json_encode(['JAN' => "JAN {$i}", 'UPC' => "UPC {$i}"], JSON_UNESCAPED_UNICODE),
                'preparation_duration' => 4,
                'unit_id' => 1,
                'unit_volume' => 2,
                'category_id' => $category_1->id,
                'price' => 1000,
                'discount_price' => 900,
                'featured_image' => UploadedFile::fake()->image("picture_{$i}.jpg", 400, 400)->store('products', 'public'),
            ]);

//            $product->translateAttributes([
//                'en' => [
//                    'name' => $category_1->en['name'] . ": Product {$product->id}",
//                    'description' => "Product {$product->id} description",
//                ],
//                'ar' => [
//                    'name' => $category_1->ar['name'] . ": المنتج {$product->id}",
//                    'description' => "وصف المنتج {$product->id}",
//                ]
//            ]);

            for ($x = 1; $x <= 2; $x++) {
                ProductImage::create([
                    'image' => UploadedFile::fake()->image("picture_{$product->id}_{$x}.jpg", 400, 400)->store('products', 'public'),
                    'product_id' => $product->id,
                ]);
            }

            foreach ($attributes_1 as $attribute) {
                if ((in_array($product->id, [1, 2, 3]) && in_array($attribute->id, [1, 2, 3]))
                    || (in_array($product->id, [4, 5]) && in_array($attribute->id, [4, 5])))
                    $product->attributes()->sync([
                    $attribute->id => [
                        'value' => json_encode([
                            'en' => "AlfaMarket Brand " . $attribute->id,
                            'ar' => "تصنيف ألفا ماركت " . $attribute->id
                        ], JSON_UNESCAPED_UNICODE)
                    ]
                ]);
            }

            foreach ($branches as $branch) {
                BranchProduct::create([
                    'product_id' => $product->id,
                    'branch_id' => $branch->id,
                    'price' => 1000,
                    'discount_price' => 900,
                    'stock_amount' => ($branch->id * 10),
                ]);
            }
        }

        $category_2 = Category::find(2);
        for ($i = 6; $i <= 7; $i++) {
            $product = Product::create([
                'name' => '{"ar":"المنتج' . $i . '","en":"Product' . $i . '"}',
                'description' => '{"ar":"وصف المنتج  ' . $i . '","en":"description' . $i . '"}',

                'barcode' => json_encode(['SKU' => "SKU {$i}", 'ISBN' => "ISBN {$i}"], JSON_UNESCAPED_UNICODE),
                'preparation_duration' => 4,
                'unit_id' => 1,
                'unit_volume' => 2,
                'category_id' => $category_2->id,
                'price' => 1000,
                'discount_price' => 900,
                'featured_image' => UploadedFile::fake()->image("picture_{$i}.jpg", 400, 400)->store('products', 'public'),
            ]);

//            $product->translateAttributes([
//                'en' => ['name' => $category_2->en['name'] . ": Product {$product->id}"],
//                'ar' => ['name' => $category_2->ar['name'] . ": المنتج {$product->id}"]
//            ]);

            for ($x = 1; $x <= 2; $x++) {
                ProductImage::create([
                    'image' => UploadedFile::fake()->image("picture_{$product->id}_{$x}.jpg", 400, 400)->store('products', 'public'),
                    'product_id' => $product->id,
                ]);
            }

            foreach ($branches as $branch) {
                BranchProduct::create([
                    'product_id' => $product->id,
                    'branch_id' => $branch->id,
                    'stock_amount' => ($branch->id * 10),
                ]);
            }
        }

        $this->call(ProductFavoritesDatabaseSeeder::class);
    }
}
