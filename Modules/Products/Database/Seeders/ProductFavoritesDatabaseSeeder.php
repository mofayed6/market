<?php

namespace Modules\Products\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Products\Models\Product;
use Modules\User\Models\User;
use Modules\Products\Models\ProductFavorite;

class ProductFavoritesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $products = Product::limit(10)->get();
        $users = User::limit(5)->get();
        foreach ($users as $user) {
            foreach ($products as $product) {
                if ($product->id < $user->id || ($product->id % $user->id == 0)) {
                    ProductFavorite::create([
                        'product_id' => $product->id,
                        'user_id' => $user->id,
                    ]);
                }
            }
        }

        // $this->call("OthersTableSeeder");
    }
}
