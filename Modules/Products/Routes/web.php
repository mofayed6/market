<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::prefix('admin')->middleware('admin:admin')->group(function () {
    Route::group(['prefix' => 'products/images'], function () {
        Route::post('upload', 'ProductsController@uploadImages')->name('product.images.upload');
        Route::post('delete', 'ProductsController@deleteImage')->name('product.images.delete');

    });

    Route::get('products/datatable', 'ProductsController@datatable');
    Route::get('products/CheckFTP', 'ProductsController@CheckFTP');

    Route::get('products/{product}/delete', 'ProductsController@destroy');

    Route::get('products/attribute/{id}', 'ProductsController@attributeCategory');

    Route::resource('products', 'ProductsController');
});


Route::prefix('{locale}/products')->name('products.')->group(function () {
    Route::get('/wish_list/add/{product_id}', 'ProductsSiteController@add_wish_list')->middleware('auth')->name('add_wish_list');
    Route::get('/wish_list/remove/{product_id}', 'ProductsSiteController@remove_wish_list')->middleware('auth')->name('remove_wish_list');
    Route::get('/wish_list', 'ProductsSiteController@wish_list')->middleware('auth')->name('wish_list');
    Route::get('/view/{productId}', 'ProductsSiteController@view')->name('view');
    Route::get('/search', 'ProductsSiteController@search')->name('search');
    Route::get('/{categoryId}', 'ProductsSiteController@index')->name('index');
});

