@extends('admin.layouts.master')
@section('page-title','Edit Product')
@section('breadcrumb')
    <li class="breadcrumb-item active"><a href="{{url('admin/products')}}">Products</a></li>
    <li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')
    {{Form::model($product,['route'=>['products.update',$product],'method'=>'PATCH','files'=>true])}}

    <div class="row">
        <div class="col-md-9">
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-6">
                            <h2>Edit: {{ $name['en'] }} </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('admin.pratical.message')
                    <div class="table-responsive">
                        <ul class="nav nav-tabs">
                            @foreach(locales() as $k=>$local)
                                <li class="nav-item"><a class="nav-link {{($k==0)?'active show':''}}" data-toggle="tab" href="#{{ $local->code}}_tab">{{ $local->local_name}}</a></li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach(locales() as $k=>$local)
                                <div class="tab-pane {{($k==0)?'active show':''}}" id="{{ $local->code}}_tab">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Name {{ $local->code}} </label>
                                                {{Form::text($local->code.'[name]',$name[$local->code],['class'=>'form-control'])}}
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Description {{ $local->code}} </label>
                                                {{Form::textarea($local->code.'[description]',$description[$local->code],['class'=>'form-control','rows'=>3])}}
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Tags {{ $local->code}} </label>
                                                {{Form::textarea($local->code.'[tags]',$tags[$local->code],['class'=>'form-control','rows'=>3])}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="row">
{{--                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Barcode</label>
                                        {{Form::number('barcode',null,['class'=>'form-control'])}}
                                    </div>
                                </div> --}}
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Preparation Duration (minutes)</label>
                                        {{Form::number('preparation_duration',null,['class'=>'form-control'])}}
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Branch Product </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @foreach($brancheProducts as $pro)
                      <div class="row fieldGroup">

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Branch</label><br>
                                {{Form::select('branch_id[]',$selects_branches,$pro->branch_id,['class'=>'form-control disease product','placeholder'=>'choose Branch ..'])}}
                            </div>
                        </div>


                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Stock</label>
                                {{Form::number('stock_amount[]',$pro->stock_amount,['class'=>'form-control','required'=>'required'])}}
                            </div>
                        </div>


                          <div class="col-md-3">
                              <div class="form-group">
                                  <label> Price</label>
                                  {{Form::number('pricee[]',$pro->price,['class'=>'form-control','required'=>'required'])}}
                              </div>
                          </div>

                          <div class="col-md-2">
                              <div class="form-group">
                                  <label>Discount Price</label>
                                  {{Form::number('discount_pricee[]',$pro->discount_price,['class'=>'form-control','required'=>'required'])}}
                              </div>
                          </div>

                        <div class="form-group ">
                            <label></label>
                            <button type="button" class="btn btn-primary btn-labeled addMoreAttribute">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>

                            <button type="button" class="btn btn-primary btn-labeled remove">
                                <i class="fa fa-minus" aria-hidden="true"></i>
                            </button>
                        </div>

                    </div>
                    @endforeach
                </div>
            </div>

            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Images </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="form-group">
                        <label>Featured Image</label>
                        <div class="row">
                            <div class="col-md-3">
                                <img src="{{url('uploads/'.$product->featured_image)}}" alt="" class="img-thumbnail">
                            </div>
                        </div>
                        {{Form::file('featured_image',['class'=>'form-control'])}}
                    </div>
                    <div class="form-group">
                        <label>Images</label>
                        <div class="row">
                            {{Form::hidden('uploadedFiles')}}
                            <div class="col-md-12">
                                <div class="file-loading">
                                    <input id="input-pr" name="images[]" type="file" data-allowed-file-extensions='["png", "jpg","jpeg"]'  accept="image/*"  multiple>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Main Features</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="form-group">
                        <label>Unit</label>
                        {{Form::select('unit',$selects_units,null,['class'=>'form-control'])}}
                    </div>
                    <div class="form-group">
                        <label>Unit Volume</label>
                        {{Form::number('unit_volume',null,['class'=>'form-control'])}}
                    </div>
                    <div class="form-group">
                        <label>Identifier</label>
                        {{Form::select('identifier_id',$identifiers,null,['class'=>'form-control','placeholder'=>'select identifier ...'])}}
                    </div>
                    <div class="form-group">
                        <label>Category</label>
                        {{Form::select('category_id',$categories,$category_id,['class'=>'form-control','id'=>'category'])}}
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Attribute</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="form-group" id="att">
                    </div>
                    <div id="test"></div>
                </div>
            </div>

            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Pricing </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="form-group">
                        <label>Price</label>
                        {{Form::number('price',null,['class'=>'form-control','step'=>'0.01'])}}
                    </div>
                    <div class="form-group">
                        <label>Discount Price</label>
                        {{Form::number('discount_price',null,['class'=>'form-control','step'=>'0.01'])}}
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Classifications </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @foreach($classifications as $classification)
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="fancy-checkbox">
                                    {{Form::checkbox('classifications[]',$classification->id)}}
                                    <span>{{json_decode($classification->title,true)['en']}}</span>
                                </label>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Always Available </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="fancy-checkbox">
                                <input type="checkbox" name="available" value="1" @if($product->available == 1) checked @endif>
                                <span>Available</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Save </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-block"> Save </button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <button type="submit" class="btn btn-info btn-block"> Cancel </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{Form::close()}}

    <div class="row fieldGroupCopy" style="display: none;">

        <div class="col-md-3">
            <div class="form-group form-float">
                {{--<input type="text" class="form-control" placeholder="Username *" name="username" required>--}}
                <label>Branch</label><br>
                {{Form::select('branch_id[]',$selects_branches,null,['class'=>'form-control disease test','required'=>'required','placeholder'=>'choose Branch ..'])}}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group form-float">
                {{--<input type="password" class="form-control" placeholder="Password *" name="password" id="password" required>--}}
                <label>Stock Amount</label>
                {{Form::number('stock_amount[]',null,['class'=>'form-control','required'=>'required'])}}
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label> Price</label>
                {{Form::number('pricee[]',null,['class'=>'form-control','required'=>'required'])}}
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
                <label>Discount Price</label>
                {{Form::number('discount_pricee[]',null,['class'=>'form-control','required'=>'required'])}}
            </div>
        </div>


        <div class="form-group ">
            <label></label>
            <button type="button" class="btn btn-primary btn-labeled remove">
                <i class="fa fa-minus" aria-hidden="true"></i>
            </button>
        </div>

    </div>

@endsection
@section('styles')
    <link href="{{asset('/assets/vendor/bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">

@endsection


@section('scripts')
    <script src="{{asset('/assets/vendor/bootstrap-fileinput/js/plugins/piexif.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/assets/vendor/bootstrap-fileinput/js/plugins/sortable.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/assets/vendor/bootstrap-fileinput/js/plugins/purify.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/assets/vendor/bootstrap-fileinput/js/fileinput.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendor/select2/js/select2.full.min.js')}}"></script>

    <script>

        /*
            {
                123: {
                    'ar': 'fagagagsa',
                    'en': 'errgsvc'
                }
            }
        */
        var productAttributes = {!! $productAttributesValues !!};

function handleCategoryChange() {
    $( "#att" ).html("");
    $( "#test" ).html("");

    $.ajax({
        url: "{{ url('admin/products/attribute/') }}" + '/' +$("#category").val(),
        type: 'GET',
        success: function (result) {


            $.map( result, function( n ) {

//                $("#test").append("<input type='hidden' name='attribute_id[]' id='attribute_id"+ n.id +"'> ");
                var input = $( "#attribute_id" + n.id ).val(n.id);
                var name = JSON.parse(n.name);
                var valueEn = '';
                var valueAr = '';
                if (productAttributes[n.id]) {
                    valueTranslations = JSON.parse(productAttributes[n.id]);
                    if (valueTranslations.en) {
                        valueEn = valueTranslations.en;
                    }
                    
                    if (valueTranslations.ar) {
                        valueAr = valueTranslations.ar;
                    }
                }


                if(n.template === 'input_text'){
                    $( "#att" ).append( " <label>"+ name.en +"</label><input class='form-control' type='text' value='" + valueEn + "' name='attribute_product[" + n.id + "][en]'><br>" );
                    $( "#att" ).append( " <label>"+ name.ar +"</label><input class='form-control' type='text' value='" + valueAr + "' name='attribute_product[" + n.id + "][ar]'><br>" );
                }else{

                    var selectEn = "<label>"+ name.en +"</label><select class='form-control' name='attribute_product[" + n.id + "][en]'>";


                    $.each( n.options, function( e ,v) {
                    // var name_option = JSON.parse(v.name);
                        selectEn = selectEn + '<option ' + (valueEn == optionEn ? 'selected' : '') + '>' + v.optionEn + '</option>';

                    });

                    selectEn = selectEn + "</select>";

                    $( "#att" ).append(selectEn);

                    var selectAr = "<label>"+ name.ar +"</label><select class='form-control' name='attribute_product[" + n.id + "][ar]'>";

                    $.each( n.options, function( e ,v) {
                    // var name_option = JSON.parse(v.name);
                        selectAr = selectAr + '<option ' + (valueAr == optionAr ? 'selected' : '') + '>' + v.optionAr + '</option>';

                    });

                    selectAr = selectAr + "</select>";

                    $( "#att" ).append(selectAr);
                }

            });
        }
    });
}

        var urls =[
            @foreach($product->images as $image)
                "{{url('uploads/'.$image->image)}}",
            @endforeach
        ];

        $("#input-pr").fileinput({
            uploadUrl: "{{route('product.images.upload')}}",
            deleteUrl: "{{route('product.images.delete')}}",
            uploadAsync: false,
            maxFileCount: 10,
            overwriteInitial: false,
            initialPreview: urls,
            initialPreviewAsData: true, // allows you to set a raw markup
            initialPreviewFileType: 'image', // image is the default and can be overridden in config below
            initialPreviewDownloadUrl: "{{url('uploads/')}}/{key}", // includes the dynamic key tag to be replaced for each config
            initialPreviewConfig: [
                @foreach($product->images as $image)
                    {type: "image", caption: "Image-2.jpg", size: 817000,  key: "{{$image->image}}" },  // set as raw markup
                @endforeach
            ],
            uploadExtraData: {
                _token: "{{csrf_token()}}",
            },
            deleteExtraData: {
                _token: "{{csrf_token()}}"
            }
        }).on('filesorted', function(e, params) {
            console.log('File sorted params', params);
        }).on('fileuploaded', function(e, params) {
            console.log('File uploaded params', params);
        });



$(document).ready(function () {
    handleCategoryChange();
    $("#category").on('change', function() {
        //alert($(this).val());
        handleCategoryChange();
    });
});



    </script>


    <script>
        $(document).ready(function() {

            $('.disease').select2();


        });


        $(document).ready(function(){
            //group add limit
            var maxGroup = 20;
            $('.test').select2();
            //add more fields group
            $(".addMoreAttribute").click(function(){
                $('.test').select2("destroy");
                if($('body').find('.fieldGroup').length < maxGroup){
                    var fieldHTML = '<div class="row fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
                    $('body').find('.fieldGroup:last').after(fieldHTML);
                    $('.test').select2();

                    $("#product").change(function() {
                        alert($(this).val());

                    });



                    $('.datee').daterangepicker({
                        minDate: new Date(),
                        timePicker: true,
                        locale: {
                            format: 'Y-M-DD HH:mm:ss'
                        }
                    });



                }else{
                    alert('Maximum '+maxGroup+' groups are allowed.');
                }
            });

            //remove fields group
            $("body").on("click",".remove",function(){
                $(this).parents(".fieldGroup").remove();
            });




        });
    </script>
@endsection

