@extends('admin.layouts.master')
@section('page-title','Check FTP')
@section('breadcrumb')
    <li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')
    <div class="card">
        <div class="header">
            <div class="row">
                <div class="col-md-6">
                    <h2>Check FTP list</h2>
                </div>
                {{--<div class="col-md-6">--}}
                    {{--<a href="{{url('/admin/products/create/')}}" class="btn btn-primary pull-right"><i class="fa fa-plus-square"></i> <span>Add New</span></a>--}}
                {{--</div>--}}
            </div>
        </div>
        <div class="body">
            @include('admin.pratical.message')



            {{Form::open(['route'=>'products.store','files'=>true])}}

              <div class="row">
                <div class="col-md-12">
                    
                    <div class="card">
                        <div class="header">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>File </h2>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="form-group">
                                <label>URL</label>
                                {{Form::URL('link',null,['class'=>'form-control'])}}
                            </div>

                            <div class="form-group">
                                <label>Featured File</label>
                                {{Form::file('file',['class'=>'form-control'])}}
                            </div>

                        </div>
                    </div>

                </div>


                <div class="col-md-12">

                    <div class="card">
                        <div class="header">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>Save </h2>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success btn-block"> Save </button>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-block"> Cancel </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{Form::close()}}

            <div class="table-responsive">
                <table class="table table-hover m-b-0 c_list datatable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Price</th>
                        <th>Discount Price</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function() {
            $('.datatable').DataTable({
                processing: true,
                searching: true,
                serverSide: true,
                ajax: '{!! url('admin/products/datatable/') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'price', name: 'price' },
                    { data: 'discount_price', name: 'discount price' },
                    { data: 'options', name: 'options' },
                ],
            });
        });
    </script>
@stop
