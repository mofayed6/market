<?php

namespace Modules\Products\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ProductImage extends Model
{
    protected $fillable = [];

    public const PUBLIC_FILES_DIR = 'products';

    public function getPictureUrl()
    {
        return Storage::url(!empty($this->image) ? $this->image : self::PUBLIC_FILES_DIR . '/default.png');
    }
}
