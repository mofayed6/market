<?php

namespace Modules\Products\Models;

use App\Http\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;
use Modules\Branches\Models\Branch;

class BranchProduct extends Model
{
    protected $table = 'branches_product';

    public function branches()
    {
        return $this->hasOne(Branch::class,'id','branch_id');
    }


}
