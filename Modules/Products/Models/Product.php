<?php

namespace Modules\Products\Models;

use App\Http\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;
use Modules\Branches\Models\Branch;
use Modules\Classifications\Models\Classification;
use Modules\Attributes\Models\Attribute;
use Modules\DeliveryLocation\Models\DeliveryLocation;
use Modules\Products\Models\BranchProduct;
use Illuminate\Support\Facades\Storage;
use Modules\Categories\Models\Category;
use Modules\Attributes\Models\AttributeProduct;

class Product extends Model
{
    use TranslatableTrait;
    protected $fillable = [];
  //  protected $translatable = ['name', 'description', 'tags'];
   // protected $appends = ['name'];

    public const PUBLIC_FILES_DIR = 'products';

    public function getPictureUrl()
    {
        return Storage::url(!empty($this->featured_image) ? $this->featured_image : self::PUBLIC_FILES_DIR . '/default.png');
    }

    public function incrementViewsCount()
    {
        self::where('id', $this->id)->update(['views_count' => ++$this->views_count]);
    }

    public function incrementOrdersCount()
    {
        self::where('id', $this->id)->update(['orders_count' => ++$this->orders_count]);
    }

    public static function mostViewedInCategory($categoryId, $limit = 0, $offset = 0)
    {
        $query = self::where('category_id', $categoryId)->orderBy('views_count', 'DESC');
        if (!empty($limit)) {
            $query->limit($limit);
        }

        if (!empty($offset)) {
            $query->offset($offset);
        }

        return $query->get();
    }

    public static function trendingInCategory($categoryId, $limit = 0, $offset = 0)
    {
        $query = self::where('category_id', $categoryId)->orderBy('orders_count', 'DESC');
        if (!empty($limit)) {
            $query->limit($limit);
        }

        if (!empty($offset)) {
            $query->offset($offset);
        }

        return $query->get();
    }

    public static function latestInCategory($categoryId, $limit = 0, $offset = 0)
    {
        $query = self::where('category_id', $categoryId)->orderBy('created_at', 'DESC');
        if (!empty($limit)) {
            $query->limit($limit);
        }

        if (!empty($offset)) {
            $query->offset($offset);
        }

        return $query->get();
    }

    public static function mostViewed($limit = 0, $offset = 0)
    {
        $query = self::orderBy('views_count', 'DESC');
        if (!empty($limit)) {
            $query->limit($limit);
        }

        if (!empty($offset)) {
            $query->offset($offset);
        }

        return $query->get();
    }

    public static function trending($limit = 0, $offset = 0)
    {
        $query = self::orderBy('orders_count', 'DESC');
        if (!empty($limit)) {
            $query->limit($limit);
        }

        if (!empty($offset)) {
            $query->offset($offset);
        }

        return $query->get();
    }

    public static function latest($limit = 0, $offset = 0)
    {
        $query = self::orderBy('created_at', 'DESC');
        if (!empty($limit)) {
            $query->limit($limit);
        }

        if (!empty($offset)) {
            $query->offset($offset);
        }

        return $query->get();
    }


    public static function getProductsBaseQuery(string $locale, array $options)
    {
        $categoryId = $options['categoryId'] ?? null;
        $attributesQuery = $options['attributesQuery'] ?? null;
        $price_fromQuery = $options['price_fromQuery'] ?? null;
        $price_toQuery = $options['price_toQuery'] ?? null;
        $keywordsArr = $options['keywordsArr'] ?? null;

        $baseProducts = self::selectRaw('*, LEAST(discount_price, price) AS actual_price');

        if (!empty($categoryId)) {
            $baseProducts->where(['category_id' => $categoryId]);
        }

        /**
         * branch stock of products
         */
        $baseProducts->whereIn('id', function ($query) {
            $query->select('product_id')
                ->from(with(new BranchProduct)->getTable())
                ->where('branch_id', getCurrentBranch()->id)
                ->where('stock_amount', '>', 0);
        });

        if (!empty($keywordsArr)) {
            $baseProducts->whereIn('id', function ($query) use ($keywordsArr, $locale) {
                $query->select('id')->from(with(new Product)->getTable());
                $query->where('name', 'LIKE', "%" . implode(' ', $keywordsArr) . "%");
                foreach ($keywordsArr as $keyword) {
                    $query->orWhere('name', 'LIKE', "%" . $keyword . "%");
                }
            });
        }   

        if (!empty($attributesQuery)) {
            $baseProducts->whereIn('id', function ($query) use ($attributesQuery, $locale) {
                $query->select('product_id')
                    ->from(with(new AttributeProduct)->getTable())
                    ->whereIn('attribute_id', array_keys($attributesQuery))
                    // here handle value
                    ->whereIn('value', function ($subquery) use ($attributesQuery, $locale) {
                        $subquery->select('value')->from(with(new AttributeProduct)->getTable());
                        foreach ($attributesQuery as $attributeVals) {
                            foreach ($attributeVals as $attrVal) {
                                $subquery->orWhereRaw("JSON_CONTAINS(value, ?)", [json_encode([$locale => $attrVal], JSON_UNESCAPED_UNICODE)]);
                            }
                        }
                    });
            });
        }

        if (!empty($price_fromQuery)) {
            $baseProducts->whereIn('id', function ($query) use ($price_fromQuery) {
                $query->select('id')
                    ->from(with(new self)->getTable())
                    ->where('price', '>=', $price_fromQuery)
                    ->orWhere('discount_price', '>=', $price_fromQuery);
            });
        }

        if (!empty($price_toQuery)) {
            $baseProducts->whereIn('id', function ($query) use ($price_toQuery) {
                $query->select('id')
                    ->from(with(new self)->getTable())
                    ->where('price', '<=', $price_toQuery)
                    ->orWhere('discount_price', '<=', $price_toQuery);
            });
        }

        return $baseProducts;
    }

    public function classifications()
    {
        return $this->belongsToMany(Classification::class, 'product_classifications', 'product_id', 'classification_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class, 'product_id');
    }

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class, 'attributes_product', 'product_id', 'attribute_id')->withPivot('value');
    }

    public function branches()
    {
        return $this->belongsToMany(Branch::class, 'branches_product', 'product_id', 'branch_id')->withPivot('value');
    }

//    public function getNameAttribute($locale = 'en')
//    {
//        return $this->{$locale}['name'];
//    }

    public static function productBranch($product)
    {
        $branch = BranchProduct::where('product_id', $product)->get();

        return $branch;
    }


    public static function priceBranch($distract , $branch)
    {
            $price = DeliveryLocation::where([['district_id',$distract],['branch_id',$branch]])->first()->price;
            return $price;
    }

    /**
     * @return float Returns the discount_price if it exists,
     * else returns the price
     */
    public function getPrice()
    {
        if (!empty($this->discount_price)) {
            return $this->discount_price;
        }

        return $this->price;
    }
}
