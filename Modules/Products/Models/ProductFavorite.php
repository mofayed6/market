<?php

namespace Modules\Products\Models;

use App\Http\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;

class ProductFavorite extends Model
{
    protected $table = 'product_favorites';
    protected $fillable = ['product_id', 'user_id'];

    public function products()
    {
        return $this->hasMany(Product::class, 'id', 'product_id');
    }
}
