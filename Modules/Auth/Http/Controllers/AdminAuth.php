<?php

namespace Modules\Auth\Http\Controllers;

use App\Admin;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminAuth extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('guest', ['except' => 'logout']);
//    }

    public function login()
    {
        if(!auth()->guard('admin')->check())
            return view('auth::login');

        return redirect('admin/login');
    }

    public function dologin()
    {
        $remember = request('remember') ? true : false;
        if (!auth()->guard('admin')->attempt(['email'=>request('email'), 'password'=>request('password')],$remember)){
            session()->flash('error',trans('admin.inccorect_information_login'));
            return redirect('/admin/login');
        }else{
            return redirect('admin/dashboard');
        }
    }

    public function logout()
    {
        auth()->guard('admin')->logout();
        return redirect('/admin/login');

    }


}
