<?php

namespace Modules\Languages\Models;

use Illuminate\Database\Eloquent\Model;

class Local extends Model
{
    protected $fillable = ['local_name', 'code', 'direction'];
}