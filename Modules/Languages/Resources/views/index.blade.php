@extends('admin.layouts.master')

@section('content')
@section('menu', 'Settings')
@section('name','Language')
<div class="row clearfix">
    <div class="col-lg-12">
        <div class="card">
            <div class="header">
                <a class="btn btn-default" href="{{route('languages.create')}}"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Language</a>
            </div>
            <div class="body">
                <table id="mainTable" class="table table-hover">
                    <thead>

                    <tr>
                        <th>Name</th>
                        <th>Code</th>
                        <th>Directions</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($languages as $language)
                        <tr>
                            <td>{{$language->local_name}}</td>
                            <td>{{$language->code}}</td>
                            <td>{{$language->direction}}</td>
                            <td>
                                {!! BootForm::linkOfEdit('languages.edit', [$language->id], $language->local_name) !!}
                                {!! BootForm::linkOfDelete('languages.destroy', [$language->id], $language->local_name, 'link', true) !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
