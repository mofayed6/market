@extends('admin.layouts.master')

@section('content')
@section('menu', 'Settings')
@section('name','Language')
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <h2>Create Language</h2>
            </div>
            <div class="body">
                {!! BootForm::open('create', ['url'=>route('languages.store'),'novalidate','id'=>'basic-form']) !!}
                {!! BootForm::input('text', 'local_name', null, 'Name', $errors,['required','data-parsley-length'=>'[3,20]']) !!}
                {!! BootForm::input('text', 'code', null, 'Code', $errors,['required','data-parsley-maxlength'=>'2']) !!}
                {!! BootForm::select('direction', 'Direction', ['ltr'=>'ltr','rtl'=>'rtl'], null, $errors,['required']) !!}
                <br>
                {!! BootForm::submit() !!}
                {!! BootForm::close() !!}

            </div>
        </div>
    </div>
</div>
@stop


@section('scripts')
    <script src="{{ asset('assets/vendor/parsleyjs/js/parsley.min.js') }}"></script>
    <script>
        $(function() {
            // initialize after multiselect
            $('#basic-form').parsley();
        });
    </script>
@endsection