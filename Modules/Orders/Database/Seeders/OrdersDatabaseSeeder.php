<?php

namespace Modules\Orders\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Orders\Models\Order;
use Modules\Orders\Models\OrderStatus;
use Modules\Orders\Models\OrderProduct;
use Modules\User\Models\User;
use Modules\Branches\Models\Branch;
use Modules\Products\Models\Product;

class OrdersDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $branch = Branch::first();
        $users = User::with('addresses')->limit(5)->get();
        $products = Product::limit(3)->get();
        foreach ($users as $user) {
            $order = Order::create([
                'branch_id' => $branch->id,
                'user_id' => $user->id,
                'address_id' => $user->addresses[0]->id,
                'tax_percentage' => 15,
                'type' => 'Delivery',
                'progress' => 2,
                'total_cost' => 0,
                'payment' => 'cash_on_delivery'
            ]);

            OrderStatus::create([
                'order_id' => $order->id,
                'status' => 5,
            ]);

            $totalCost = 0;
            foreach ($products as $product) {
                OrderProduct::create([
                    'product_id' => $product->id,
                    'order_id' => $order->id,
                    'price' => $product->price,
                    'discount_price' => $product->discount_price,
                    'quantity' => $product->id * 2,
                ]);

                $totalCost += $product->getPrice() * $product->id * 2;
            }

            Order::where('id', $order->id)->update(['total_cost' => $totalCost]);
        }

        // $this->call("OthersTableSeeder");
    }
}
