<?php

namespace Modules\Orders\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Coupons\Models\Coupon;
use Modules\Products\Models\Product;
use Modules\User\Models\User;
use Session;
use Modules\DeliveryLocation\Models\UserAddress;
use Modules\DeliveryLocation\Models\DeliveryLocation;

class Order extends Model
{
    protected $fillable = ['branch_id', 'coupon_id', 'address_id', 'user_id', 'tax_percentage', 'total_cost', 'payment','type','dateDelivery', 'progress'];
    protected $hidden = ['priceAfterDiscount'];

    public const PAYMENT_CREDIT_ON_DELIVERY = 'credit_on_delivery';
    public const PAYMENT_CASH_ON_DELIVERY = 'cash_on_delivery';

    /**
     * @return float Returns the sum of cart products prices,
     * and their related taxes (later)
     */
    public function totalProductsCost()
    {
        $totalCost = 0;
        foreach ($this->orderProducts as $orderProduct) {
            $totalCost += $orderProduct->getSubTotalAttribute();
        }

        return $totalCost;
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_products', 'order_id', 'product_id');
    }

    public function orderProducts()
    {
        return $this->hasMany(OrderProduct::class, 'order_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function address()
    {
        return $this->belongsTo(UserAddress::class, 'address_id');
    }

    public function getPriceAfterDiscountAttribute()
    {
        return ($this->price - $this->discount_price);
    }

    public function status()
    {
        return $this->hasOne(OrderStatus::class, 'order_id', 'id');
    }

    public static function productBeforeDiscountTotal($id)
    {

        $products = OrderProduct::where('order_id', $id)->get();
        $productPrice = [];
        $Quantity =[];
        $tax = [];
        $productPriceDiscount = [];
        foreach ($products as $product) {
            $productPriceDiscount[] = $product->discount_price * $product->quantity;

            $productPrice[] = $product->price * $product->quantity;

        }

        $sumDiscpunt = array_sum($productPriceDiscount) ;
        $sumPrice = array_sum($productPrice);
        if ($sumDiscpunt < $sumPrice) {
            return $sumDiscpunt;
        } else {
            
            return $sumPrice;
        }


    }

    public static function totalPrice($id)
    {
        $subTotal = self::productBeforeDiscountTotal($id);
        
        $order = Order::find($id);
        $tax = $subTotal * $order->tax_percentage / 100;

        $user = User::where('id', $order->user_id)->first();
        $branchMony = DeliveryLocation::calcolateMony($order->branch_id, $user->id);

        $totalBeforCoupon = $subTotal + $tax + $branchMony;

        if ($order->coupon_id != null) {
            $cupon = Coupon::where('id', $order->coupon_id)->first();

            if ($cupon->amount_type === 'percentage') {
                $total = $totalBeforCoupon * $cupon->minimum_amount / 100;
            } else {
                $total = $totalBeforCoupon - $cupon->amount;
            }

            return $total;

        }

        return $totalBeforCoupon;

    }


    public static function productAfterDiscountTotal($id)
    {
        $products = OrderProduct::where('order_id', $id)->get();
        return $products->sum('price') - $products->sum('discount_price') + 17;
    }


    public static function productData($id)
    {
        $products = OrderProduct::where('order_id', $id)->with('product')->get()->toArray();
        return $products;
    }

    public static function CheckStatus($id)
    {
        $status = OrderStatus::where([['order_id', $id]])->first();
        if ($status) {
            return $status;
        }
        return '';
    }

    public static function CheckStatusDate($id)
    {
        $status = OrderStatus::where([['order_id', $id]])->first();
        if ($status) {
            $date = new Carbon($status->updated_at);
            $carbon = $date->toFormattedDateString();
            return $carbon;
        }
    }

    public static function lastOrderAdd()
    {
        $last = Order::latest('id')->first();
        return $last;
    }

    public function orderProduct($order_id)
    {
        $orderProduct = OrderProduct::where('order_id', $order_id)->pluck('order_id', 'product_id')->toArray();
        return $orderProduct;
    }

    public static function product($order_id)
    {
        $product = Product::whereIn('id', array_keys((new self)->orderProduct($order_id)))->get()->toArray();

        return $product;

    }

    public static function setSession()
    {
        $value = Order::count();
        $session = session('orderIdCount', $value);
       // dd($session);
        return $session;
    }

    public function getNameAttribute()
    {
        return $this->name;
    }
}
