<?php

namespace Modules\Orders\Models;

use App\Http\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    //use TranslatableTrait;
    protected $fillable = ['status', 'order_id'];
    protected $table = 'order_status';

    public static function status()
    {
        return [
            1 => 'processing',
            2 => 'delivering',
            3 => 'delivered',
            4 => 'canceled',
            5 => 'pending'
        ];
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    public static function statusName($statusId)
    {
        $statuses = self::status();
        if (!empty($statusId) && !empty($statuses[$statusId])) {
            return ucfirst($statuses[$statusId]);
        }

        return '';
    }
}
