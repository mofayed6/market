<?php

namespace Modules\Orders\Models;

use App\Http\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;
use Modules\Products\Models\Product;

class OrderProduct extends Model
{
    protected $fillable = ['product_id', 'order_id', 'price', 'discount_price', 'quantity'];
    protected $table = 'order_products';
  //  use TranslatableTrait;

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function getProductPrice()
    {
        if (!empty($this->discount_price)) {
            return $this->discount_price;
        }

        return $this->price;
    }

    public function getSubTotalAttribute()
    {
        return $this->getProductPrice() * $this->quantity;
    }
}
