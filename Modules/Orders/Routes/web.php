<?php


Route::middleware('admin:admin')->prefix('admin/orders')->group(function () {
//order
    Route::get('/datatable', 'OrdersController@datatable');
    Route::get('/create', 'OrdersController@create');
    Route::get('/{order}/edit', 'OrdersController@edit');
    Route::get('/{order}/show', 'OrdersController@shows');
    Route::patch('/{order}/update-progress', 'OrdersController@updateProgress');
    Route::patch('/{order}/update-value', 'OrdersController@updateValeProgress');
    Route::patch('/{order}/updatee', 'OrdersController@update')->name('orders.last.update');

    Route::get('/', 'OrdersController@index');
    Route::post('/orders', 'OrdersController@store')->name('orders.store');
    Route::get('/{id}/delete', 'OrdersController@destroy');
    Route::get('/count', 'OrdersController@countOrderDatabase');
//    Route::get('/change','OrdersController@ChangeDelevary');
    Route::get('/changeProduct/{id}', 'OrdersController@productPrice')->name('price');
    Route::get('/getBranch/{id}', 'OrdersController@getBranch');

//product order
    Route::get('/{parent}', 'ProductOrderController@indexOrder');
    Route::get('/create/{parent}', 'ProductOrderController@createProduct');
    Route::post('/{parent}', 'ProductOrderController@storeProductOrder')->name('orders.product.store');
    Route::get('/products/datatable/{order?}', 'ProductOrderController@datatableProduct');
    Route::get('/{id}/{proId}/order-product', 'ProductOrderController@editOrderProduct')->name('orders.product.edit');
    Route::patch('/{order}/update', 'ProductOrderController@updateOrderProduct')->name('orders.product.update');
    Route::get('/{id}/{proId}/delete', 'ProductOrderController@destroyOrderProduct');
});


Route::middleware('auth')->prefix('{locale}/')->name('user_order.')->group(function () {
    Route::get('show/{order_id}', 'UserOrderSiteController@show')->name('show');
    Route::get('checkout', 'UserOrderSiteController@create')->name('checkout');
    Route::post('checkout', 'UserOrderSiteController@store')->name('store');
    Route::get('all-orders/{all?}', 'UserOrderSiteController@allOrders')->middleware('auth')->name('all_orders');
});

Route::middleware('auth')->get('delivery-cost-lng-lat/{lat}/{lng}', 'UserOrderSiteController@getDeliveryCostByLngLat')->name('getDeliveryCostByLngLat');
Route::middleware('auth')->get('delivery-cost-address/{address_id}', 'UserOrderSiteController@getDeliveryCostByAddress')->name('getDeliveryCostByAddress');
