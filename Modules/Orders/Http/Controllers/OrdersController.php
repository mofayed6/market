<?php

namespace Modules\Orders\Http\Controllers;

use App\Mail\AcceptOrder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Mail;
use Modules\Branches\Models\Branch;
use Modules\Coupons\Models\Coupon;
use Modules\DeliveryLocation\Models\DeliveryLocation;
use Modules\Locations\Models\Location;
use Modules\Orders\Models\Order;
use Modules\Orders\Models\BranchProduct;
use Modules\Orders\Models\OrderProduct;
use Modules\Orders\Models\OrderStatus;
use Modules\Products\Models\Product;
use Modules\User\Models\User;
use Session;
use DB;
use Modules\DeliveryLocation\Models\UserAddress;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {


        $processing = OrderStatus::where('status', 1)->get()->count();
        $delivering = OrderStatus::where('status', 2)->get()->count();
        $delivered = OrderStatus::where('status', 3)->get()->count();
        $canceled = OrderStatus::where('status', 4)->get()->count();
        $pending = OrderStatus::where('status', 5)->get()->count();
        return view('orders::index', compact('processing', 'delivered', 'delivering', 'canceled', 'pending'));
    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $district  = Location::getDistrict();

        $branches = Branch::get();
        foreach ($branches as $branch) {

            $selected_types = json_decode($branch->name, true);

            $selects_branches[$branch->id] = $selected_types[app()->getLocale()];
        }



        $poducts = Product::pluck('name', 'id')->toArray();
        foreach ( $poducts as $key => $value){
            $selected_product[$key] = getAccordingLang($value , app()->getLocale());
        }


        $coupones = Coupon::all()->pluck('code', 'id');
        $users = User::get();
        $status = OrderStatus::status();
        return view('orders::create', compact('branchs', 'coupones','district','selected_product','selects_branches', 'users', 'poducts', 'status'));
    }


    public function getBranch($distract)
    {
        $dist = DeliveryLocation::where('district_id',$distract)->pluck('branch_id')->toArray();
        $branch = Branch::whereIn('id',$dist)->get();
        return $branch;

    }



    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {

     //   dd($request->all());

        $request->validate([
            'tax_percentage' => 'required',
            'branch_id' => 'required',
            'user_id' => 'required',
            'district_id' => 'required',
        ]);

        $order = new Order();
        $order->tax_percentage = $request->tax_percentage;
        $order->branch_id = $request->branch_id;
        $order->district_id = $request->district_id;
        $order->user_id = $request->user_id;
        $order->payment = $request->payment;
        $order->type = $request->type;
        $order->save();
        $order->status()->create(['status' => 5]);

        $data = [];
        foreach ($request->product_id as $k => $t) {
            $product = Product::where('id', $t)->first();
            $data[$t] = ['price' => (int)$product->price * $request->quantity[$k], 'discount_price' => (int)$product->discount_price * $request->quantity[$k], 'quantity' => $request->quantity[$k]];
        }

        $order->products()->sync($data);

        return redirect('admin/orders')->with(['success' => 'order created successfully']);
    }


    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Order $order)
    {
        $product = productOrder($order->id);

        $district  = Location::getDistrict();

        $branches = Branch::pluck('name', 'id')->toArray();
        foreach ( $branches as $key => $value){
            $selected_branches[$key] = getAccordingLang($value , app()->getLocale());
        }


        $poducts = Product::pluck('name', 'id')->toArray();
        foreach ( $poducts as $key => $value){
            $selected_product[$key] = getAccordingLang($value , app()->getLocale());
        }



        $coupones = Coupon::all()->pluck('code', 'id');
        $users = User::get();
        $status = OrderStatus::status();

        return view('orders::edit', compact('branchs', 'poducts','district','selected_product','selected_branches', 'coupones', 'users', 'order', 'status', 'product'));
    }


    public function shows($id)
    {
        $order = Order::with(['user'])->find($id);
        $orderss = Order::find($id);
       // $address = UserAddress::find($order->address_id);
        $address = Location::find($order->district_id);
        
        $user = User::find($order->user_id);
        $countOrder = Order::where('user_id', $order->user_id)->count();
        $orderUser = Order::where('user_id', $order->user_id)->orderBy('id', 'desc')->take(5)->get()->toArray();
        $productCount = OrderProduct::whereIn('order_id', $orderUser)->count();
        $orderProducts = Order::productData($id);
        $price = Order::productAfterDiscountTotal($id);
        $subTotal = Order::productBeforeDiscountTotal($id);
        $total = Order::totalPrice($id);

        return view('orders::show', compact('order', 'address','orderss', 'countOrder', 'total', 'productCount', 'orderUser', 'orderProducts', 'price', 'user', 'subTotal'));
    }


    public function updateProgress(Request $request, $order_id)
    {
        $order = Order::find($order_id);
        $order->progress = $request->progress;
        $order->update();

        if ($order->progress == 1) {
            $user = User::where('id', $order->user_id)->first();

            $data = [];

            $data['order'] = Order::with('user')->find($order->id);
            $data['user'] = User::with('addresses')->where('id', $order->user_id)->first();
            $data['countOrder'] = Order::where('user_id', $order->user_id)->count();
            $data['orderUser'] = Order::where('user_id', $order->user_id)->orderBy('id', 'desc')->take(5)->get()->toArray();
            $data['porducts'] = Order::productData($order->id);
            $data['price'] = Order::productAfterDiscountTotal($order->id);
            $data['status'] = OrderStatus::where('order_id', $order->id)->first();


            Mail::to($user->email)->send(new AcceptOrder($data));

            return redirect()->back()->with(['success' => 'order Complated successfully']);
        } else {
            return redirect()->back()->with(['success' => 'order refused successfully']);

        }
    }




    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tax_percentage' => 'required',
           // 'branch_id' => 'required',
            'user_id' => 'required',

        ]);
        $order = Order::find($id);

        $order->tax_percentage = $request->tax_percentage;
        if($request->branch_id == ''){
            $order->branch_id = $order->branch_id;
        } else{
            $order->branch_id = $request->branch_id;
        }
        $order->user_id = $request->user_id;
        $order->payment = $request->payment;
        $order->district_id = $request->district_id;
        //  $order->coupon_id       = $request->coupon_id ;
        $order->update();

        if ($request->status != null) {
            $order->status()->delete();
            $order->status()->create(['status' => $request->status]);
        }
        $time = [];

//        foreach ($request->date as $t=> $date){
//            $timee[$t] = explode(' - ',$date);
//            foreach ($timee[$t] as $b=>$n){
//                $time[$b] = $n;
//            }
//        }

        $data = [];
        foreach ($request->product_id as $k => $t) {
            $product = Product::where('id', $t)->first();
            $data[$t] = ['price' => (int)$product->price * $request->quantity[$k], 'discount_price' => (int)$product->discount_price * $request->quantity[$k], 'quantity' => $request->quantity[$k]];
        }


        $order->products()->detach();

        $order->products()->attach($data);

        return redirect('admin/orders')->with(['success' => 'order Updated successfully']);


    }


    /**
     * @param Order $order
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        $order->delete();
        // $order->deleteTranslations();
        return redirect('admin/orders')->with(['success' => 'product deleted successfully']);
    }


    public function productPrice($id)
    {
        $product = Product::find($id);
        return $product;
//            return $product->discount_price;
//
//            return $product->price;


    }





    public function datatable()
    {
        $orders = Order::with('products', 'status')->orderBy('created_at', 'Desc')->get();

        return \DataTables::of($orders)
            ->editColumn('branch_id', function ($model) {
                $branchs = Branch::all()->where('id', $model->branch_id)->pluck('name')->first();
                $name = json_decode($branchs,true);

                return $name['en'];

            })
            ->editColumn('status', function ($model) {
                $status = OrderStatus::where('order_id', $model->id)->first();
                if (!empty($status)) {
                    if ($status->status == 1) {
                        return '<span class="badge badge-success">Processing</span>';
                    } elseif ($status->status == 2) {
                        return '<span class="badge badge-warning">Delivering</span>';
                    } elseif ($status->status == 3) {
                        return '<span class="badge badge-danger">Delivered</span>';
                    } elseif ($status->status == 4) {
                        return '<span class="badge badge-default">Canceled</span>';
                    } elseif ($status->status == 5) {
                        return '<span class="badge badge-info">Pending</span>';
                    } else {
                        return '';
                    }
                }
                return 'null';
            })

            ->editColumn('user', function ($model) {
                $user = User::where('id', $model->user_id)->first();

                return $user->first_name . ' ' . $user->last_name;

            })

            ->editColumn('price', function ($model) {
                $price = Order::totalPrice($model->id);
                return $price;
            })

            ->editColumn('payment', function ($model) {

                switch ($model->payment) {
                    case Order::PAYMENT_CASH_ON_DELIVERY:
                        $retVal = '<span class="badge badge-danger">Cash on delivery</span>';
                        break;

                    case Order::PAYMENT_CREDIT_ON_DELIVERY:
                        $retVal = '<span class="badge badge-success">Credit card on delivery</span>';
                        break;
                    default:
                        $retVal = '';
                        break;
                }

                return $retVal;

            })
            ->editColumn('purchase', function ($model) {
                
                if ($model->type == 1) {
                    return '<span class="badge badge-success">Store</span>';
                }elseif($model->type == 0){
                    return '<span class="badge badge-info">Delivery</span>';
                }else{
                    return '';
                }

            })

            ->addColumn('options', function ($model) {
                return "<a href='" . url('admin/orders/' . $model->id . '/show') . "' class='btn btn-info'>Show</a>"
                    . "<a href='" . url('admin/orders/' . $model->id . '/edit') . "' class='btn btn-warning'>Edit</a>"
                    . "<a href='" . url('admin/orders/' . $model->id . '/delete') . "' class='btn btn-danger'>Delete</a>";

            })
            ->rawColumns(['payment', 'options'])
            ->make(true);
    }


    public function getCount(Request $request)
    {
        Order::setSession();
        return $value = Session::get('orderIdCount');
    }

    public function countOrderDatabase()
    {
        $value = Order::count();
        return $value;
    }



//   public function ChangeDelevary()
//   {
//       $orders = \Modules\Orders\Models\OrderStatus::where('status',5)->with('order')->get();
//         foreach ($orders as $key => $order) {
//                  $branchData = \Modules\Branches\Models\Branch::where('id',$order['order']->branch_id)->first();
//                  $delevary = \Modules\DeliveryLocation\Models\DeliveryLocation::where([['branch_id','!=',$order['order']->branch_id],['city_id',$branchData->city_id],['country_id',$branchData->country_id]])->get();
//                 if(count($delevary) > 0)
//                 {
//                    $other_branch = [];
//                     foreach ($delevary as $key => $value) {
//                        $other_branch = $value->branch_id;
//                     }
//
//                     $number_of_pending_roduct_in_main_branch  = DB::table('orders')
//                          ->join('order_status', 'orders.id', '=', 'order_status.order_id')
//                          ->select('orders.*', 'order_status.status')
//                          ->where('orders.branch_id',$order['order']->branch_id)
//                          ->where('order_status.status',5)
//                          ->count();
//                          $number_of_pending_roduct_in_other_branch  = DB::table('orders')
//                          ->join('order_status', 'orders.id', '=', 'order_status.order_id')
//                          ->select('orders.*', 'order_status.status')
//                          ->where('orders.branch_id',$other_branch)
//                          ->where('order_status.status',5)
//                          ->count();
//                          if(($number_of_pending_roduct_in_main_branch - $number_of_pending_roduct_in_other_branch) > 5)
//                            {
//                               $order = \Modules\Orders\Models\Order::where('id',$order['order']->id)->first();
//                               $order->branch_id = $other_branch;
//                               $order->update();
//                            }
//
//
//                 }
//
//       }
//   }


}
