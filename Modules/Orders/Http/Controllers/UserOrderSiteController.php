<?php

namespace Modules\Orders\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Cart\Models\CartProduct;
use Modules\Orders\Models\Order;
use Modules\Orders\Models\BranchProduct;
use Carbon\Carbon;
use Modules\Orders\Models\OrderStatus;
use Modules\Cart\Models\Cart;
use Modules\Orders\Models\OrderProduct;
use Modules\DeliveryLocation\Models\UserAddress;
use Modules\DeliveryLocation\Models\DeliveryLocation;
use Modules\Locations\Models\Location;
use Modules\DeliveryLocation\Models\PriceDelivary;
use App\Http\Controllers\BaseController;

class UserOrderSiteController extends BaseController
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function allOrders($locale)
    {
        $this->viewData['orders'] = Order::with(['orderProducts.product', 'status'])->where(['user_id' => auth()->id()])->get();

        return view('site.user_account.all_orders', $this->viewData);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create($locale)
    {

        $cartSession = session('cart');
        if($cartSession){
            $cart = new Cart();
            $cart->user_id = auth()->id();
            $cart->coupon_id = 0;
            $cart->save();


            foreach($cartSession as $item){
                $cartItem = new CartProduct();
                $cartItem->cart_id = $cart->id;
                $cartItem->product_id = $item['id'];
                $cartItem->quantity = $item['quantity'];
                $cartItem->save();
            }

        }


        $this->viewData['cart'] = Cart::with('cartProducts.product')->where('user_id', auth()->id())->firstOrFail();
        $this->viewData['user_addresses'] = UserAddress::with(['country', 'city', 'district'])->where('user_id', auth()->id())->get();

        $this->viewData['countries'] = Location::with('children.children')->where('parent_id', 0)->get();
        $this->viewData['defaultCountry'] = $this->viewData['countries']
            ->where('id', old('country_id', $this->viewData['countries']->first()->id))->first();
        $this->viewData['defaultCity'] = $this->viewData['defaultCountry']->children()
            ->where('id', old('city_id', $this->viewData['defaultCountry']->children()->first()->id))->first();
        $this->viewData['defaultDistrict'] = $this->viewData['defaultCity']->children()
            ->where('id', old('district_id', $this->viewData['defaultCity']->children()->first()->id))->first();

        $this->viewData['deliveryCost'] = 0;
        //if ($addressId = old('address_id') || old('address_type', 0) == 0) {
//            if (old('address_type', 0) == 0) {
//                $addressId = $this->viewData['user_addresses']->where('is_default', 1)->first()->id;
//            }
            $value = session('distract');

            $this->viewData['deliveryCost'] = DeliveryLocation::calcolateMony(getCurrentBranch()->id, $value);
       // }





        return view('site.checkout_process.checkout', $this->viewData);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request, $locale)
    {

        $request->validate([
            'address_type' => 'required',
            'payment' => 'required',
        ]);


        session()->forget('cart');

        $branch = getCurrentBranch();
        $cart = Cart::with('cartProducts.product')->where('user_id', auth()->id())->firstOrFail();

        if (!empty($request->address_type) && $request->address_type == 'saved_address') {
            $addressId = (int)$request->address_id;
        } else {
            $request->validate([
                'address' => 'required',
                'country_id' => 'required',
                'city_id' => 'required',
                'district_id' => 'required',
            ]);
            $newAddress = UserAddress::create([
                'user_id' => auth()->id(),
                'country_id' => $request->country_id,
                'city_id' => $request->city_id,
                'district_id' => $request->district_id,
                'address' => $request->address,
                'notes' => !empty($request->notes) ? $request->notes : '',
                'apartment_number' => !empty($request->apartment_number) ? $request->apartment_number : '',
                'building_number' => !empty($request->building_number) ? $request->building_number : '',
                'phone_number' => !empty($request->phone_number) ? $request->phone_number : '',
                'is_default' => 0,
            ]);
            $addressId = $newAddress->id;
        }
        $value = session('distract');

        $deliveryCost = DeliveryLocation::calcolateMony(getCurrentBranch()->id, $value);
        $order = Order::create([
            'branch_id' => getCurrentBranch()->id,
            'user_id' => auth()->id(),
            'progress' => 2, // 2 => Pending
            'address_id' => $addressId,
            'payment' => $request->payment,
            'type' => $request->type,
            'dateDelivery' => $request->date,
            'total_cost' => $deliveryCost + $cart->totalProductsCost()
        ]);

        OrderStatus::create([
            'order_id' => $order->id,
            'status' => 5, // 5 => Pending
        ]);

        foreach ($cart->cartProducts as $cartProduct) {
            OrderProduct::create([
                'product_id' => $cartProduct->product->id,
                'order_id' => $order->id,
                'price' => $cartProduct->product->price,
                'discount_price' => $cartProduct->product->discount_price,
                'quantity' => $cartProduct->quantity,
            ]);
        }

        $cart->delete();
        return redirect(route('user_order.success_page', [$locale, $order->id]));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($locale, $orderId)
    {
        $this->viewData['order'] = Order::with(['orderProducts.product', 'status'])->where(['id' => $orderId, 'user_id' => auth()->id()])->first();

        return view('site.checkout_process.success_page', $this->viewData);
    }

    /**
     * @return Response
     */
    public function getDeliveryCostByLngLat($lat, $lng)
    {
        $drivingDistance = (new DeliveryLocation)->GetDrivingDistance(getCurrentBranch()->latitude, getCurrentBranch()->longitude, $lat, $lng);
        $price = PriceDelivary::where('min', '<', $drivingDistance['distance'])
            ->where('max', '>', $drivingDistance['distance'])->first();

        if (empty($price)) {
            $price = PriceDelivary::first();
        }

        return $price->price;
    }

    /**
     * @return Response
     */
    public function getDeliveryCostByAddress($addressId)
    {
        return DeliveryLocation::calcolateMony(getCurrentBranch()->id, auth()->id(), $addressId);
    }
}
