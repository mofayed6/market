@extends('admin.layouts.master')
@section('page-title','Show details order')
@section('breadcrumb')
<li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')
@include('admin.pratical.message')
<h2 class="content-heading">

    {!! Form::model($order,['url'=>['admin/orders/'.$order->id.'/update-progress'],'method'=>'PATCH']) !!}
    <input type="hidden" name="progress" value="1">
    <button type="submit" class="btn btn-sm btn-secondary float-right">
        <i class="fa fa-check text-success mr-5"></i>Complete
    </button>
    {!! Form::close() !!}


    {!! Form::model($order,['url'=>['admin/orders/'.$order->id.'/update-progress'],'method'=>'PATCH']) !!}
    <input type="hidden" name="progress" value="0">
    <button type="submit" class="btn btn-sm btn-secondary float-right mr-5">
        <i class="fa fa-times text-danger mr-5"></i>Cancel
    </button>
    {!! Form::close() !!}

</h2>
<br>
<hr>
<div class="row clearfix">

    <div class="col-lg-4 col-md-12">
        <div class="card profile-header">
            <div class="body">
                <div class="profile-image"> <img src="{{url('assets/images/user/'.$order['user']['image'])}}" class="rounded-circle"
                        alt=""> </div>
                <div>
                    <h4 class="m-b-0"><strong>{{$order['user']['first_name']}}</strong> {{$order['user']['last_name']}}</h4>
                </div>
                <div class="row">
                    <div class="col-6">

                        <div class="body">
                            <i class="fa fa-shopping-bag fa-3x"></i>
                            <h5 class="m-b-0 number count-to" data-from="0" data-to="2365" data-speed="1000"
                                data-fresh-interval="700">{{$countOrder}} </h5>
                            <small>Orders</small>
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="body">
                            <i class="fa fa-shopping-basket fa-3x"></i>
                            <h5 class="m-b-0 number count-to" data-from="0" data-to="2365" data-speed="1000"
                                data-fresh-interval="700">{{$productCount}} </h5>
                            <small>Product</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <div class="col-lg-8 col-md-12">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="header">
                        <h2>Past order</h2>
                    </div>
                    <div class="body table-responsive">
                        <table class="table table-hover m-b-0">
                            <thead>
                                <tr>
                                    <th>OrderId</th>
                                    <th>Branch</th>
                                    <th>Status</th>
                                    <th>Submitted</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($orderUser as $order)
                                  <tr>

                                    <td>
                                        <h6><a href="{{url('admin/orders/'.$order['id'].'/show')}}">{{$order['id']}}</a></h6>
                                    </td>
                                    <td>

                                        <h6 class="margin-0">{{json_decode(\Modules\Branches\Models\Branch::where('id',$order['branch_id'])->first()->name,true)['en']}}</h6>

                                    </td>
                                    <td>


                                        <h6 class="m-b-0">
                                            @if(\Modules\Orders\Models\Order::CheckStatus($order['id'])->status == 5)
                                            <span class="badge badge-success">Pending</span>
                                            @elseif(\Modules\Orders\Models\Order::CheckStatus($order['id'])->status ==
                                            4)
                                            <span class="badge badge-default">Canceled</span>
                                            @elseif(\Modules\Orders\Models\Order::CheckStatus($order['id'])->status ==
                                            3)
                                            <span class="badge badge-danger">Delivered</span>
                                            @elseif(\Modules\Orders\Models\Order::CheckStatus($order['id'])->status ==
                                            2)
                                            <span class="badge badge-warning">Delivering</span>
                                            @elseif(\Modules\Orders\Models\Order::CheckStatus($order['id'])->status ==
                                            1)
                                            <span class="badge badge-success">Processing</span>
                                            @endif

                                        </h6>

                                    </td>
                                    <td>
                                        <h6>{{$order['created_at']}}</h6>

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<div class="row clearfix">

    {{--{{dd($user)}}--}}
    <div class="col-lg-12 col-md-12">
        <div class="card profile-header">
            <div class="header">
                <h2>  Billing Address </h2>
            </div>

            <div class="body">
                <small class="text-muted">Address: </small>
                <p>{{json_decode($address->name,true)['en']}}</p>
                {{--<div>--}}
                    {{--<iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAoacCf8RDzMIaHcI5Ywh9zQ-kZt1-V_fc&q={{$address->lat}},{{$address->lng}}"--}}
                        {{--width="100%" height="150" frameborder="0" style="border:0" allowfullscreen=""></iframe>--}}
                {{--</div>--}}
                <hr>
                <small class="text-muted">Email address: </small>
                <p>{{$user['email']}}</p>
                <hr>
                <small class="text-muted">Mobile: </small>
                <p>{{$user['mobile']}}</p>
                <hr>
                <small class="text-muted">Apartment Number: </small>
                <p>{{$address->apartment_number}}</p>



            </div>


        </div>


    </div>
</div>






<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card profile-header">
            <div class="header">


                <h2> order product</h2>
            </div>
            <div class="body">
                
                <small class="text-muted">Time Pick Up: </small>
                <p>{{$orderss->dateDelivery}}</p>
                <div class="body table-responsive">
                    <table class="table table-hover m-b-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Product</th>
                                <th>Barcode</th>
                                <th>QTY</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orderProducts as $orderProduct)
                            <tr>
                                <td>
                                    <h6>{{$orderProduct['id']}}</h6>
                                </td>
                                <td>

                                    <h6 class="margin-0">
                                        {{json_decode($orderProduct['product']['name'],true)['en']}}
                                    </h6>

                                </td>
                                <td>

                                    <h6 class="margin-0">
                                        <pre>{!! str_replace('\"', '"', json_encode($orderProduct['product']['barcode'], JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT)) !!}</pre>
                                    </h6>

                                </td>
                                <td>
                                    <h6 class="m-b-0">
                                        {{$orderProduct['quantity']}}
                                    </h6>

                                </td>
                                <td>
                                    @if($orderProduct['discount_price'] == null)
                                    <h6>{{$orderProduct['price']}} EG</h6>
                                    @else
                                    <h6>{{$orderProduct['discount_price']}} EG</h6>
                                    @endif
                                </td>
                            </tr>


                            @endforeach

                                <tr>
                                    <td colspan="3" class="text-right font-w600">Total Paid:</td>
                                    <td class="text-right">{{$total}} EG</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                            <tr>
                                <td colspan="3" class="text-right font-w600">SubTotal Paid:</td>
                                <td class="text-right">{{$subTotal}} EG</td>
                            </tr>

                            <tr>
                                <td colspan="3" class="text-right font-w600">Total Paid:</td>
                                <td class="text-right">{{$price}} EG</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        {{--</div>--}}


    {{--</div>--}}
{{--</div>--}}






@endsection
