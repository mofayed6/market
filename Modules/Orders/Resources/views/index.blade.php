@extends('admin.layouts.master')
@section('page-title','Orders')
@section('breadcrumb')
<li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')
<div class="row clearfix">

    <div class="col-lg-2 col-md-4">
        <div class="card overflowhidden">
            <div class="body">
                <h3>{{$processing}} <i class="icon-basket-loaded float-right"></i></h3>
                <span>Processing </span>
            </div>
            <div class="progress progress-xs progress-transparent custom-color-blue m-b-0">
                <div class="progress-bar" data-transitiongoal="{{$processing}}"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-2 col-md-4">
        <div class="card overflowhidden">
            <div class="body">
                <h3>{{$delivered}} <i class="fa fa-eye float-right"></i></h3>
                <span>Delivered </span>
            </div>
            <div class="progress progress-xs progress-transparent custom-color-purple m-b-0">
                <div class="progress-bar" data-transitiongoal="{{$delivered}}"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-2 col-md-4">
        <div class="card overflowhidden">
            <div class="body">
                <h3>{{$canceled}} <i class="fa fa-dollar float-right"></i></h3>
                <span>Canceled </span>
            </div>
            <div class="progress progress-xs progress-transparent custom-color-yellow m-b-0">
                <div class="progress-bar" data-transitiongoal="{{$canceled}}"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-4">
        <div class="card overflowhidden">
            <div class="body">
                <h3>{{$pending}} <i class="fa fa-university float-right"></i></h3>
                <span>Pending </span>
            </div>
            <div class="progress progress-xs progress-transparent custom-color-green m-b-0">
                <div class="progress-bar" data-transitiongoal="{{$pending}}"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-4">
        <div class="card overflowhidden">
            <div class="body">
                <h3>{{$delivering}} <i class="fa fa-credit-card float-right"></i></h3>
                <span>Delivering </span>
            </div>
            <div class="progress progress-xs progress-transparent custom-color-pink m-b-0">
                <div class="progress-bar" data-transitiongoal="{{$delivering}}"></div>
            </div>
        </div>
    </div>

    
</div>
<div class="card">
    <div class="header">
        <div class="row">
            <div class="col-md-6">
                <h2>Order list</h2>
            </div>
            <div class="col-md-6">
                <a href="{{url('/admin/orders/create/')}}" class="btn btn-primary pull-right"><i class="fa fa-plus-square"></i>
                    <span>Add New</span></a>
            </div>
        </div>
    </div>
    <div class="body">
        <p id="table-filter">
            Filter By Status:
            <select>
                <option value="">All</option>
                <option>Pending</option>
                <option>Processing</option>
                <option>Delivering</option>
                <option>Delivered</option>
                <option>Canceled</option>

            </select>

            Filter By Type:
            <select id="filter">
                <option value="">All</option>
                <option >Store</option>
                <option >Delivering</option>

            </select>
        </p>
        @include('admin.pratical.message')
        <div class="table-responsive">
            <table class="table table-hover m-b-0 c_list datatable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Branch</th>
                        <th>Status</th>
                        <th>User</th>
                        <th>Price</th>
                        <th>Payment</th>
                        <th>Purchase Type</th>
                        <th>Submitted</th>
                        <th>Options</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection



@section('scripts')
<script>
    $(function () {
        table = $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            orderCellsTop: true,
            oLanguage: {
                sInfo: "Showing_START_to_END_of_TOTAL_items."
            },
            order: [
                [0, "desc"]
            ],
            ajax: '{!! url("admin/orders/datatable/") !!}',
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'branch_id',
                    name: 'branch_id'
                },
                {
                    data: 'status',
                    name: 'status'
                },
                {
                    data: 'user',
                    name: 'user'
                },
                {
                    data: 'price',
                    name: 'price'
                },
                {
                    data: 'payment',
                    name: 'payment'
                },
                {
                    data: 'purchase',
                    name: 'purchase '
                },
                {
                    data: 'created_at',
                    name: 'Submitted'
                },
                {
                    data: 'options',
                    name: 'options'
                },
            ],
            dom: 'lr<"table-filter-container">tip',
            initComplete: function (settings) {
                
                var api = new $.fn.dataTable.Api(settings);
                $('.table-filter-container', api.table().container()).append(
                    $('#table-filter').detach().show()
                );

                $('#table-filter select').on('change', function () {
                    table.search(this.value).draw();
                });
            }
        });

        $('#table-filter select').on('change', function () {
            table.search(this.value).draw();
        });

        var lastCount = '{!! Session::get("orderIdCount") !!}';

        setInterval(function () {
            $.ajax({
                url: "{!! url('admin/orders/count/') !!}",
                success: function (result) {
                    if (result >= lastCount) {
                        table.ajax.url("{!! url('admin/orders/datatable/') !!}").load();
                    }
                }
            });
        }, 30000);
    });

</script>
@stop
