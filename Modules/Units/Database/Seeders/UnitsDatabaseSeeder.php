<?php

namespace Modules\Units\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Units\Models\Unit;

class UnitsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        for ($i = 1; $i <= 5; ++$i) {
            $unit = Unit::create([
                'id' => $i,
                 'name'=>'{"ar":"الوحدة'.$i.'","en":"Unit'.$i.'"}',

            ]);
//            $unit->translateAttributes([
//                'en' => [
//                    'name' => "Unit {$i}",
//                ],
//                'ar' => [
//                    'name' => "الوحدة {$i}",
//                ],
//            ]);
        }

        // $this->call("OthersTableSeeder");
    }
}
