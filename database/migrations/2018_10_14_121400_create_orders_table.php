<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('coupon_id')->default(0);
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('address_id');
            $table->unsignedInteger('district_id');
            $table->integer('tax_percentage')->default(0);
            $table->tinyInteger('progress')->default(2); // [0 => Rejected, 1 => Accepted, 2 => Pending]
            $table->tinyInteger('type')->default(0); // [0 => Delivery, 1 => Store]
            $table->dateTime('dateDeliveryFrom')->nullable(); // [0 => Delivery, 1 => Store]
            $table->dateTime('dateDeliveryTo')->nullable(); // [0 => Delivery, 1 => Store]
            $table->string('payment'); // ['cash_on_delivery','credit_on_delivery']
            $table->decimal('total_cost');
            $table->timestamps();
        });

        Schema::table('orders', function (Blueprint $table) {
            //foreign key constrains
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
//            $table->foreign('district_id')->references('id')->on('locations')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
