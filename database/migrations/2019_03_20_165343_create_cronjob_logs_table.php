<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCronjobLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cronjobLogs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->string('errors');
            $table->dateTime('run_job_time');
            $table->unsignedBigInteger('count_result');
            $table->unsignedBigInteger('count_error');
            $table->unsignedBigInteger('count_success');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cronjobLogs');
    }
}
