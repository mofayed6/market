<?php

use Illuminate\Database\Seeder;
use Modules\Admins\Database\Seeders\AdminsDatabaseSeeder;
use Modules\Attributes\Database\Seeders\AttributesDatabaseSeeder;
use Modules\Auth\Database\Seeders\AuthDatabaseSeeder;
use Modules\Branches\Database\Seeders\BranchesDatabaseSeeder;
use Modules\Cart\Database\Seeders\CartDatabaseSeeder;
use Modules\Categories\Database\Seeders\CategoriesDatabaseSeeder;
use Modules\Classifications\Database\Seeders\ClassificationsDatabaseSeeder;
use Modules\Coupons\Database\Seeders\CouponsDatabaseSeeder;
use Modules\DeliveryLocation\Database\Seeders\DeliveryLocationDatabaseSeeder;
use Modules\HomePages\Database\Seeders\HomePagesDatabaseSeeder;
use Modules\Identifiers\Database\Seeders\IdentifiersDatabaseSeeder;
use Modules\Languages\Database\Seeders\LanguagesDatabaseSeeder;
use Modules\Locations\Database\Seeders\LocationsDatabaseSeeder;
use Modules\Orders\Database\Seeders\OrdersDatabaseSeeder;
use Modules\Products\Database\Seeders\ProductsDatabaseSeeder;
use Modules\Rules\Database\Seeders\RulesDatabaseSeeder;
use Modules\Units\Database\Seeders\UnitsDatabaseSeeder;
use Modules\User\Database\Seeders\UserDatabaseSeeder;
use Modules\DeliveryLocation\Database\Seeders\UserAddressDatabaseSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
            LanguagesDatabaseSeeder::class,
            LocationsDatabaseSeeder::class,
            RulesDatabaseSeeder::class,
            AdminsDatabaseSeeder::class,
            UserDatabaseSeeder::class,
            BranchesDatabaseSeeder::class,
            CategoriesDatabaseSeeder::class,
            AttributesDatabaseSeeder::class,
            AuthDatabaseSeeder::class,
            ClassificationsDatabaseSeeder::class,
            CouponsDatabaseSeeder::class,
            DeliveryLocationDatabaseSeeder::class,
            UserAddressDatabaseSeeder::class,
            HomePagesDatabaseSeeder::class,
            IdentifiersDatabaseSeeder::class,
            UnitsDatabaseSeeder::class,
            ProductsDatabaseSeeder::class,
            CartDatabaseSeeder::class,
            OrdersDatabaseSeeder::class,
        ]);
    }
}
