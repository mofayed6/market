<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::group(['middleware' => 'guest:web'], function () {
    Route::get('login', 'Auth\AuthController@login')->name('login');
    Route::post('login', 'Auth\AuthController@doLogin');
    Route::get('register', 'Auth\AuthController@register');
    Route::post('register', 'Auth\AuthController@doRegister');
});

Route::group(['middleware' => 'auth:web'], function () {
    Route::get('logout', 'Auth\AuthController@logout');
});

Route::group(['prefix' => 'email/verify'], function () {
    Route::get('{email}', 'Auth\VerifyController@verifyPage');
    Route::post('{email}', 'Auth\VerifyController@manualVerify');

    Route::post('{email}/resend', 'Auth\VerifyController@resendVerifyToken');
    Route::get('{email}/{token}', 'Auth\VerifyController@verify');
});



Route::get('test', function() {
    session()->put('progress', '5%');
    return session()->get('progress');
});

// Route::get('/', 'HomeController@index');

Route::get('password/reset-email', 'Auth\AuthController@showLinkRequestForm')->name('password.reset.email');
Route::post('password/email', 'Auth\AuthController@sendResetLinkEmail')->name('password.emailUser');

Route::get('email/reset/{token}', 'Auth\AuthController@resendRememberToken');

Route::post('update/DataUser', 'Auth\AuthController@updateDataUser')->name('updateDataUser');

Route::get('{locale}/pages/{pageSlug}', 'PagesSiteController@show')->name('pages');

Route::prefix('admin')->middleware('admin:admin')->group(function () {
    Route::get('pages/{locale}/{pageSlug}', 'PagesAdminController@edit')->name('admin.pages.edit');
    Route::post('pages/{locale}/{pageSlug}', 'PagesAdminController@update')->name('admin.pages.update');
});
