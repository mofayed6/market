<?php return array(
  'title_en' => 'Alfamarket',
  'title_ar' => 'سوق ألفا',
  'description_en' => 'Alfamarket is an e-commerce platform',
  'description_ar' => 'سوق ألفا هو منصة تجارة إلكترونية',
  'phone' => '+201010101010',
  'email' => 'info@alfamarket.com',
  'email_password' => '8Mk88Mgj',
  'facebook_url' => 'https://facebook.com/alfamarket',
  'twitter_url' => 'https://twitter.com/alfamarket',
  'instagram_url' => 'https://instagram.com/alfamarket',
  'youtube_url' => 'https://youtube.com/alfamarket',
);
