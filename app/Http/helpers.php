<?php

/**
 * Sets the current branch
 *
 * @return void
 */


function setCurrentBranch(\Modules\Branches\Models\Branch $branch = null)
{
    if (empty($branch)) {
        $branch = \Modules\Branches\Models\Branch::first();

    }
    \session(['branch' => $branch->toArray()]);

}


function setCart($item_id,$quantity){


    $cart = \session('cart');
    $product = \Modules\Products\Models\Product::find($item_id);
    // if cart is empty then this the first product
    if($product->discount_price != null){
            $price = $product->discount_price;
    }else{
        $price = $product->price;
    }
    if(!$cart) {
        $cart = [
            $item_id => [
                "id" => $product->id,
                "name" => $product->name,
                "image" => $product->featured_image,
                "quantity" => $quantity,
                "price" => $price  ,
            ]
        ];
        \session(['cart' => $cart]);
    }


    // if item not exist in cart then add to cart with quantity = 1

            $cart[$item_id] = [
                "id" => $product->id,
                "name" => $product->name,
                "image" => $product->featured_image,
                "quantity" => $quantity,
                "price" => $price  ,
            ];

    \session(['cart' => $cart]);
//    session()->push()

}


function getPictureUrl($image)
{
    return \Illuminate\Support\Facades\Storage::url(!empty($image) ? $image : '/default.png');
}

function getAccordingLang($text,$lang = null){

    if ($lang == null){
        $lang = app()->getLocale();
    }

    $json_decoded = json_decode($text);

    try{
        return $json_decoded->$lang;
    }catch (Exception $e){
        return false;
    }
}


/**
 * Gets the current branch
 *
 * @return \Modules\Branches\Models\Branch
 */
function getCurrentBranch()
{
    /** @todo cache the current branch and invalidate/renew it on change */
    if (session('branch', null) === null) {
        $branch = \Modules\Branches\Models\Branch::first();
        \session(['branch' => $branch->toArray()]);
    } else {
        $branch = \Modules\Branches\Models\Branch::find(\session('branch.id'));
    }

    return $branch;
}

/**
 * Sets the app locale based on the locale route parameter
 *
 * @return void
 */
function handleLocale()
{
    $locale = \Illuminate\Support\Facades\Route::current()->parameter('locale');

    if (empty($locale) || !in_array($locale, ['en', 'ar'])) {
        $currentLocale = app()->getLocale();
        if (!empty($currentLocale)) {
            app()->setLocale(app()->getLocale());
        } else {
            app()->setLocale('en');
        }

    } else {
        app()->setLocale($locale);
    }
}

/**
 * Generate a url to the same page where it is called
 * with the destination locale
 * @param $toLocale language shortcut: en, ar
 *
 * @return string the url to the same page where it is called
 * with the destination locale
 */
function changeLocaleUrl($toLocale)
{
    $requestPathInfo = request()->getPathInfo();
    if (mb_strpos($requestPathInfo, '/' . $toLocale) !== 0) {
        $requestPathInfo = '/' . $toLocale . mb_substr($requestPathInfo, 3);
    }

    $queryString = request()->getQueryString();
    if (!empty($queryString)) {
        $queryString = '/?' . $queryString;
    }

    return url($requestPathInfo . $queryString);
}

/**
 * @param string|array $uris
 * Returns 'active' if the current page is one of the $uris
 * else returns an empty string
 *
 * @return string
 */
function checkActivePage($uris)
{
    if (!empty($uris)) {
        if (!is_array($uris)) {
            $uris = [$uris];
        }

        foreach ($uris as $uri) {
            if (request()->is($uri)) {
                return 'active';
            }
        }
    }

    return '';
}

/**
 * @param mixed $firstValue
 * @param mixed $secondValue
 * Returns 'selected' if $firstValue equals/matches $secondValue
 * else returns an empty string
 *
 * @return string
 */
function setSelected($firstValue, $secondValue)
{
    if ("{$firstValue}" === "{$secondValue}") {
        return 'selected';
    }

    return '';
}

/**
 * @param mixed $firstValue
 * @param mixed $secondValue
 * Returns 'checked' if $firstValue equals/matches $secondValue
 * else returns an empty string
 *
 * @return string
 */
function setChecked($firstValue, $secondValue)
{
    if ("{$firstValue}" === "{$secondValue}") {
        return 'checked';
    }

    return '';
}

/**
 * @param array $dataArray
 * @param array $targetedKeys
 * Perform htmlentities() on $targetedKeys of a $dataArray
 *
 * @return string
 */
function sanitizeString($str = '')
{
    if (!empty($str)) {
        $str = htmlentities($str, ENT_QUOTES, 'UTF-8');
    }

    return $str;
}

/**
 * @param array $dataArray
 * @param array $targetedKeys
 * Perform htmlentities() on $targetedKeys of a $dataArray
 *
 * @return array
 */
function sanitizeArray(array $dataArray, array $targetedKeys = [])
{
    if (!empty($dataArray)) {
        if (empty($targetedKeys)) {
            $targetedKeys = array_keys($dataArray);
        }

        if (!is_array($targetedKeys)) {
            $targetedKeys = [$targetedKeys];
        }

        foreach ($targetedKeys as $key) {
            if (!empty($dataArray[$key])) {
                $dataArray[$key] = sanitizeString($dataArray[$key]);
            }
        }
    }

    return $dataArray;
}

/**
 * @param array $dataArray
 * @param array $targetedKeys
 * Perform nl2br() on $targetedKeys of a $dataArray
 * and leave other elements of $dataArray untouched
 *
 * @return array
 */
function nl2brArray(array $dataArray, array $targetedKeys = [])
{
    if (!empty($dataArray)) {
        if (empty($targetedKeys)) {
            $targetedKeys = array_keys($dataArray);
        }

        if (!is_array($targetedKeys)) {
            $targetedKeys = [$targetedKeys];
        }

        foreach ($targetedKeys as $key) {
            if (!empty($dataArray[$key])) {
                $dataArray[$key] = nl2br($dataArray[$key]);
            }
        }
    }

    return $dataArray;
}

function nlbr2entity($str = '')
{
    return !empty($str) ? str_replace(['<br>', '<br />', "\r\n", "\n", "\r"], '&#13;&#10;', $str) : '';
}

function nl2br_custom($str = '')
{
    return !empty($str) ? str_replace(['<br>', '&#13;&#10;', "\r\n", "\n", "\r"], '<br />', $str) : '';
}

function br2nl($str = '')
{
    return !empty($str) ? str_replace(['<br>', '<br />'], "\n", $str) : '';
}

function encrypt_string($string, $key = 'this+is+not+the+way+you+realize+what+is+important')
{
    return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key))));
}

function decrypt_string($encrypted_string, $key = 'this+is+not+the+way+you+realize+what+is+important')
{
    return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted_string), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
}

function get_count_str($num, $single, $double, $more)
{
    if (($num == 1) || ($num > 10)) $str = $num . ' ' . $single;
    elseif ($num == 2) $str = $double;
    else $str = $num . ' ' . $more;

    return $str;
}

function get_random_str($length)
{
    return substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'), 0, $length);
}

function utf8_prep($string)
{
    //reject overly long 2 byte sequences, as well as characters above U+10000 and replace with --
    $string = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]' .
        '|[\x00-\x7F][\x80-\xBF]+' .
        '|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*' .
        '|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})' .
        '|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S', ' ', $string);
    //reject overly long 3 byte sequences and UTF-16 surrogates and replace with --
    $string = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]' . '|\xED[\xA0-\xBF][\x80-\xBF]/S', '--', $string);
    return $string;
}

function search_str($needle = null, $haystack = null)
{
    return (!empty($needle) && !empty($haystack) && strpos($haystack, $needle) !== false) ? true : false;
}

function short_str($string, $max_length)
{
    return strip_tags(mb_substr($string, 0, $max_length, 'utf-8'));
}

function generateUpToDateMimeArray()
{
    $url = 'http://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types';
    $s = array();
    foreach (@explode("\n", @file_get_contents($url)) as $x)
        if (isset($x[0]) && $x[0] !== '#' && preg_match_all('#([^\s]+)#', $x, $out) && isset($out[1]) && ($c = count($out[1])) > 1)
        for ($i = 1; $i < $c; $i++)
        $s[] = '&nbsp;&nbsp;&nbsp;\'' . $out[1][$i] . '\' => \'' . $out[1][0] . '\'';
    return @sort($s) ? '$mime_types = array(<br />' . implode($s, ',<br />') . '<br />);' : false;
}



