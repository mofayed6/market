<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->segment(1) === 'admin') {
            if(auth()->guard('admin')->check()){
                return redirect('admin/dashboard');
            }else{
                return redirect('admin/login');

            }
        }
        return $next($request);

    }

}
