<?php

function increment_string($str, $separator = '-', $first = 1)
{
    preg_match('/(.+)' . $separator . '([0-9]+)$/', $str, $match);
    return isset($match[2]) ? $match[1] . $separator . ($match[2] + 1) : $str . $separator . $first;
}

function make_slug($string = null, $lettersCount = 100, $separator = "-")
{
    if (is_null($string)) {
        return "";
    }
    $string = trim($string);
    $string = mb_strtolower($string, "UTF-8");
    $string = preg_replace("/[^a-z0-9_\s-ءاأإآؤئبتثجحخدذرزسشصضطظعغفقكلمنهويةى]/u", "", $string);
    $string = preg_replace("/[\s-]+/", " ", $string);
    $string = preg_replace("/[\s_]/", $separator, $string);
    return str_limit($string, $lettersCount, '');
}

function locales()
{
    return \Cache::rememberForever('system_locales',function (){
        return \Modules\Languages\Models\Local::select('id','local_name','code','direction')->get();
    });
}

function adminHasPermissions($permission)
{
    if(auth('admin')->check())
        return auth('admin')->user()->rule->permissions->contains('permission',$permission);
    return false;
}


function productOrder($order)
{
   $product =  \Modules\Orders\Models\OrderProduct::where('order_id',$order)->get();

   return $product;
}


// function calcolateMony($branch , $user)
// {



//         $branchs = \Modules\DeliveryLocation\Models\DeliveryLocation::CalcolateDistance($branch , $user);

//         $branchprice = \Modules\DeliveryLocation\Models\DeliveryLocation::where('branch_id', $branch)->first();
//         //return $branchprice->id;
//         $Price = \Modules\DeliveryLocation\Models\PriceDelivary::where('delivery_location_id',$branchprice->id)->first();

//         if($branchs['distance']  > $Price->min && $branchs['distance']  < $Price->max){
//             return $Price->price;
//         }



// }
