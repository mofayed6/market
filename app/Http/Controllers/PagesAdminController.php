<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PagesAdminController extends Controller
{
    public const PAGES = [
        'about' => 'About',
        'privacy' => 'Privacy',
        'return_policy' => 'Return Policy',
        'terms' => 'Terms'
    ];

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($locale, $pageSlug)
    {
        abort_if(
            !isset(self::PAGES[$pageSlug]) || !in_array($locale, ['en', 'ar']),
            404
        );

        $this->viewData['locale'] = $locale;
        $this->viewData['page_slug'] = $pageSlug;
        $this->viewData['page_title'] = self::PAGES[$pageSlug];
        $this->viewData['page_content'] = file_get_contents(
            resource_path("static-pages/{$locale}/{$pageSlug}.php")
        );

        return view('admin.static_page', $this->viewData);
    }


    /**
     * @param Request $request
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $locale, $pageSlug)
    {
        abort_if(
            !isset(self::PAGES[$pageSlug]) || !in_array($locale, ['en', 'ar']),
            404
        );
        $this->viewData['page_content'] = file_put_contents(
            resource_path("static-pages/{$locale}/{$pageSlug}.php"),
            $request->content
        );

        sleep(1);
        return redirect()->route('admin.pages.edit', [$locale, $pageSlug])->with(['success' => 'Page updated successfully']);
    }

}
