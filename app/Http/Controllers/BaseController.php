<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\Branches\Models\Branch;
use Modules\Categories\Models\Category;
use Modules\DeliveryLocation\Models\DeliveryLocation;
use Modules\Locations\Models\Location;

class BaseController extends Controller
{
    public function __construct()
    {
        $this->viewData = [];
        handleLocale();
        view()->share('locale', app()->getLocale());
        $headerDistricts  = Location::getDistrictSite();
        $headerBranches = Branch::all();
        view()->share('headerBranches', $headerBranches);
        view()->share('headerDistricts', $headerDistricts);
        view()->share('currentBranch', getCurrentBranch());

        $headerCategories = Category::with('children')->where(['parent_id' => 0])->get();
        view()->share('headerCategories', $headerCategories);
    }


 
}
