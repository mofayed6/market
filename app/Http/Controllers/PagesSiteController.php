<?php

namespace App\Http\Controllers;

class PagesSiteController extends BaseController
{
    public const PAGES = [
        'about' => 'About',
        'privacy' => 'Privacy',
        'return_policy' => 'Return Policy',
        'terms' => 'Terms'
    ];

    public function show($locale, $pageSlug)
    {
        abort_if(
            !isset(self::PAGES[$pageSlug]),
            404
        );

        $this->viewData['page_title'] = self::PAGES[$pageSlug];
        $this->viewData['page_content'] = file_get_contents(resource_path("static-pages/{$locale}/{$pageSlug}.php"));

        return view('site.static_page', $this->viewData);
    }
}
