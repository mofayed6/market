<?php

namespace App\Http\Traits;

use Illuminate\Database\Eloquent\Model;
use Modules\Languages\Models\Local;
use \Modules\Languages\Models\Translation;
use \LaravelLocalization;

trait TranslatableTrait
{
    public function translate($property, $locale = 'en')
    {
        if (is_array($this->{$property})) {
            $translation = $this->{$property};
        } else {
            $translation = json_decode($this->{$property}, true);
        }

        if (!empty($translation[$locale])) {
            return $translation[$locale];
        }

        return '';
    }

    /**
     * $translations = [
     *      'en' => [
     *          'column' => 'translationString'
     *      ]
     * ]
     */
    public function translateAttributes($translations = [])
    {
        $newValues = [];
        foreach ($translations as $locale => $translation) {
            foreach ($translation as $column => $translationString) {
                if (empty($newValues[$column])) {
                    $newValues[$column] = json_decode($this->{$column}, true);
                }

                $newValues[$column][$locale] = $translationString;
            }
        }

        foreach ($newValues as $column => $value) {
            $this->{$column} = json_encode($value, JSON_UNESCAPED_UNICODE);
        }

        $this->save();
    }
}
