<?php

namespace App\Services;

class CartService
{
    protected $dbStorage = false;

    public function getItems()
    {
    }
    public function addItem($item)
    {
    }
    public function updateItem($item_id, $data)
    {
    }
    public function removeItem($item_id)
    {
    }
    public function clearItems()
    {
    }
    public function totalPrice()
    {
    }
    public function subTotalPrice($item_id)
    {
    }
}
