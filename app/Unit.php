<?php

namespace App;

use App\Http\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    use TranslatableTrait;
    protected $translatable = ['name','description'];
    protected $fillable=['id'];
}
