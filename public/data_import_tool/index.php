<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On');
ini_set('display_startup_errors', 'On');
use DIT\App\App;

require_once __DIR__ . '/../../data-import-tool/vendor/autoload.php';

$files = [
    'xls' => __DIR__ . '/../../data-import-tool/dummy_data/itemsen.xls'
];
App::run($files);
