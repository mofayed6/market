<p><a href="https://www.google.com.eg/maps/place/Alfa+Market/@29.961378,31.2461405,18z/data=!4m12!1m6!3m5!1s0x145847c0170ade2b:0xfa6a938dc813cf8b!2sConstitutional+Court!8m2!3d29.9687751!4d31.2396997!3m4!1s0x145847ee956b07bb:0xae59dd11485ca2bd!8m2!3d29.9616313!4d31.2471624?hl=en" target="_blank"><strong>Maadi</strong></a>:7 A&nbsp; Corniche El_Neel - Maadi - next to the Sofitel Hotel -&nbsp;<br />
Tel :25256400/ 25265047/ 01006664861/01099949119 / 01221112706</p>

<p><a href="https://www.google.com.eg/maps/place/%D8%A7%D9%84%D9%81%D8%A7+%D9%85%D8%A7%D8%B1%D9%83%D8%AA%E2%80%AD/@30.0641677,31.218651,17z/data=!4m8!1m2!2m1!1z2KjYsdisINin2YUg2YPZhNir2YjZhQ!3m4!1s0x145840e1ee224683:0x4c398631021c176!8m2!3d30.0631822!4d31.2177627?hl=ar" target="_blank"><strong>Zamalek&nbsp;</strong></a>:3 Almalek Al_Afdal St,&nbsp; Zamalek -&nbsp;</p>

<p>Tel :27370805/27370802/01147924649/ 01207605713/ 01022739061&nbsp;</p>

<p><a href="https://www.google.com.eg/maps/place/%D8%A3%D9%84%D9%81%D8%A7+%D9%85%D8%A7%D8%B1%D9%83%D8%AA%E2%80%AD/@30.1074752,31.3406575,17z/data=!3m1!4b1!4m5!3m4!1s0x145815e90ad1b6ed:0xd0ae849d14a5b044!8m2!3d30.1074706!4d31.3384688?hl=ar" target="_blank"><strong>Al_Hegaz&nbsp;</strong></a>:13 Montazah St. Heliplolis Square<br />
Tel :26375555/ 20670409/ 01099949664/ 01221112705</p>

<p><a href="https://www.google.com.eg/maps/place/%D8%A7%D9%84%D9%81%D8%A7%E2%80%AD/@30.0410707,31.2204344,17z/data=!3m1!4b1!4m5!3m4!1s0x1458412a6c5027a9:0x2e3c8661419fff05!8m2!3d30.0410661!4d31.2182457?hl=ar" target="_blank"><strong>Dokki&nbsp;</strong></a>:&nbsp;7 El_Sad El_Aali St.</p>

<p>Tel:33370832/ 33382280 / 01061750535 / 01145541516 / 01210033240&nbsp;</p>

<p><a href="https://www.google.com.eg/maps/place/%D8%A7%D9%84%D9%81%D8%A7+%D9%85%D8%A7%D8%B1%D9%83%D8%AA%E2%80%AD/@30.0891349,31.3147313,17z/data=!3m1!4b1!4m5!3m4!1s0x145840e1ee224683:0x75ab2b6c4ef84bff!8m2!3d30.0891303!4d31.3125426?hl=ar" target="_blank"><strong>Roxy&nbsp;</strong></a>:7 Eltahawy St, Elzahaby&nbsp;Square - Elkhalifa Elmamoun</p>

<p>Tel:&nbsp;24182075 / 24182065 / 01099949144 / 01111936573 / 01280754718</p>

<p><a href="https://www.google.com.eg/maps/place/Alfa+Market+Mohandessin/@30.0483533,31.2107946,15z/data=!4m5!3m4!1s0x1458413ab09e18c9:0xe28d8f77df0fce3f!8m2!3d30.0527673!4d31.2046822?hl=ar"><strong>Al_Mohandeseen&nbsp;</strong></a>:15 Al_Aanab Str, intersection Abdel Hamid Lotfy branching from Batal Ahmed Abdel Aziz</p>

<p>Tel : 37496951 / 37496954 / 01025132089 / 01147876132 / 01204295409</p>