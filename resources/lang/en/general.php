<?php

return [
    'home' => 'Home',
    'egp' => 'EGP',
    'primary' => 'Primary',
    'no_items_here' => 'No items Here!',
    'show_more' => 'Show More',
    'show' => 'Show',
    'save' => 'Save',
    'edit' => 'Edit',
    'update' => 'Update',
];
