<?php

return [
    'products_found' => 'Products found',
    'save_for_later' => 'Save for later',
    'remove_from_wish_list' => 'Remove from wish list',
    'delete' => 'Delete',
    'latest_products' => 'Latest Products',
    'trending_products' => 'Trending Products',
    'most_viewed_products' => 'Trending Products',
    'in_this_category' => 'in this category',
    'off' => 'OFF',
    'description' => 'Description',
    'specification' => 'Specification',
    'popularity' => 'Popularity',
    'price_low_to_high' => 'Price: Low to High',
    'price_high_to_low' => 'Price: High to Low',
    'sort_by' => 'Sort By',
];
