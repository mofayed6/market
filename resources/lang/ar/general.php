<?php

return [
    'home' => 'الرئيسية',
    'egp' => 'ج.م',
    'primary' => 'الإفتراضي',
    'no_items_here' => 'لم يتم العثور على منتجات!',
    'show_more' => 'عرض المزيد',
    'show' => 'عرض',
    'save' => 'حفظ',
    'edit' => 'تعديل',
    'update' => 'تحديث',
];
