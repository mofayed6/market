<?php

return [
    'products_found' => 'منتجات موجودة',
    'save_for_later' => 'إضافة للمفضلة',
    'remove_from_wish_list' => 'إزالة من المفضلة',
    'delete' => 'إزالة',
    'latest_products' => 'أحدث المنتجات',
    'trending_products' => 'أشهر المنتجات',
    'most_viewed_products' => 'أشهر المنتجات',
    'in_this_category' => 'بهذا التصنيف',
    'off' => 'خصم',
    'description' => 'الوصف',
    'specification' => 'المواصفات',
    'popularity' => 'الشهرة',
    'price_low_to_high' => 'السعر: الأقل إلى الأعلى',
    'price_high_to_low' => 'السعر: الأعلى إلى الأقل',
    'sort_by' => 'الترتيب بحسب',
];
