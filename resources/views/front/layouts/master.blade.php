
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="robots" content="index, follow">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="description" content="The initial Workflow of Ahmed Shaarawy">
    <title>Alfa Market</title>
    <link rel="shortcut icon" href="{{Url('front/')}}/images/favicon.png" type="image/x-icon">
    <link rel="apple-touch-icon" href="{{Url('front/')}}/images/touch-icon-iphone.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{Url('front/')}}/images/touch-icon-ipad.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="{{Url('front/')}}/images/touch-icon-iphone-retina.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="{{Url('front/')}}/images/touch-icon-ipad-retina.png" />
    <link rel="mask-icon" href="{{Url('front/')}}/images/safari-pinned-tab.svg" color="#ff2857" />


    <link rel="stylesheet" href="{{Url('front/')}}/css/app.css">

@yield('style')

    <link href="https://fonts.googleapis.com/css?family=Cairo:300,400,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 10]>
    <![endif]-->
</head>

<body>
<div class="globalWrapper">
    <aside class="side_nav">
        <nav>
            <ul>
                <li>
                    <a href="#">All Categories</a>
                </li>
                <li class="dropdown slide">
                    <a href="#">Mobiles & Tablets</a>
                    <ul class="dropdown_menu">
                        <li><a href="#">list item</a></li>
                        <li><a href="#">list item</a></li>
                        <li><a href="#">list item</a></li>
                        <li><a href="#">list item</a></li>
                        <li><a href="#">list item</a></li>
                    </ul>
                </li>
                <li class="dropdown slide">
                    <a href="#">Electronics</a>
                    <ul class="dropdown_menu">
                        <li><a href="#">list item</a></li>
                        <li><a href="#">list item</a></li>
                        <li><a href="#">list item</a></li>
                        <li><a href="#">list item</a></li>
                        <li><a href="#">list item</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Home</a>
                </li>
                <li>
                    <a href="#">Appliances</a>
                </li>
                <li>
                    <a href="#">Health & Beauty</a>
                </li>
                <li>
                    <a href="#">Moms & Babies</a>
                </li>
                <li>
                    <a href="#">Lifestyle</a>
                </li>

            </ul>
        </nav>
    </aside>

    <header id="header">
        <div class="container">
            <div class="logo">
                <button type="button" class="nav_toggle">
                    <img class="black" src="{{Url('front/')}}/images/menu.svg" alt="">
                    <img class="white" src="{{Url('front/')}}/images/menu-white.svg" alt="">
                </button>
                <a href="index.html">
                    <img src="{{Url('front/')}}/images/logo_negative.svg" alt="">
                </a>
            </div>
            <div class="search">
                <div class="dropdown">
                    <a href="#"><img src="{{Url('front/')}}/images/search.svg" alt=""></a>
                    <div class="dropdown_menu">
                        <form>
                            <input type="text" placeholder="Looking for something?">
                            <button type="button"><img src="{{Url('front/')}}/images/search.svg" alt=""></button>
                        </form>
                    </div>
                </div>
                <input type="text" placeholder="Looking for something?">
                <button type="button"><img src="{{Url('front/')}}/images/search.svg" alt=""></button>
            </div>
            <div class="right">
                <div class="dropdown">
                    <a href="#">
                        <span>EN</span> <span class="caret"><img src="{{Url('front/')}}/images/caret.svg" alt=""></span>
                    </a>
                    <ul class="dropdown_menu">
                        <li><a href="#">EN</a></li>
                        <li><a href="#" class="ar">AR</a></li>
                    </ul>
                </div>
                <div class="user">
                    @if(!auth()->check())
                    <div class="">
                        <a href="{{url('login')}}">
                            Login
                        </a>
                    </div>
                    @else
                    <div class="dropdown">
                        <a href="#">
                            <span><span class="text">{{auth()->user()->first_name}}</span><span class="caret"><img src="{{Url('front/')}}/images/user.svg" alt=""></span></span>
                        </a>
                        <ul class="dropdown_menu">
                            <li><a href="#">My Orders</a></li>
                            <li><a href="#">My Addresses</a></li>
                            <li><a href="#">Wish Lists</a></li>
                            <li><a href="#">Buy it Again</a></li>
                            <li><a href="#">Account Settings</a></li>
                            <li><a href="#">Account Summary</a></li>
                            <li><a href="{{url('logout')}}">Logout</a></li>
                        </ul>
                    </div>
                    @endif
                </div>
                <div class="store">
                    <div class="dropdown">
                        <a href="#">
                            <span><span class="text">Zamalek branch</span> <span class="shop"><img src="{{Url('front/')}}/images/store.svg" alt=""></span></span>
                        </a>
                        <ul class="dropdown_menu">
                            <li><a href="#">Cairo branch</a></li>
                            <li><a href="#">Cairo branch</a></li>
                            <li><a href="#">Cairo branch</a></li>
                            <li><a href="#">Cairo branch</a></li>
                            <li><a href="#">Cairo branch</a></li>
                            <li><a href="#">Cairo branch</a></li>
                            <li><a href="#">Cairo branch</a></li>
                        </ul>
                    </div>
                </div>
                <!-- <div>
                    <a href="#">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.063 20">
                            <g id="Icon" transform="translate(-76)">
                                <g id="Group_2" data-name="Group 2" transform="translate(76)">
                                    <g id="Group_1" data-name="Group 1">
                                        <path id="Path_15" data-name="Path 15" d="M83.031,0A7.033,7.033,0,0,0,77.05,10.729l5.581,8.994a.586.586,0,0,0,.5.277h0a.586.586,0,0,0,.5-.285l5.439-9.082A7.033,7.033,0,0,0,83.031,0Zm5.034,10.032L83.12,18.289l-5.074-8.177a5.863,5.863,0,1,1,10.019-.079Z"
                                         transform="translate(-76)" />
                                    </g>
                                </g>
                                <g id="Group_4" data-name="Group 4" transform="translate(79.516 3.516)">
                                    <g id="Group_3" data-name="Group 3">
                                        <path id="Path_16" data-name="Path 16" d="M169.516,90a3.516,3.516,0,1,0,3.516,3.516A3.52,3.52,0,0,0,169.516,90Zm0,5.867a2.352,2.352,0,1,1,2.348-2.352A2.353,2.353,0,0,1,169.516,95.867Z"
                                         transform="translate(-166 -90)" />
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </a>
                </div> -->
                <div>
                    <a href="#"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                            <g id="Icon" transform="translate(-17.2 0)">
                                <path id="Path_14" data-name="Path 14" d="M22.9,13.169H34.759A2.365,2.365,0,0,0,37.2,10.9V6.214a.049.049,0,0,0,0-.025c0-.008,0-.021,0-.029s0-.016,0-.025a.247.247,0,0,1-.009-.029c0-.008,0-.016-.009-.025s0-.016-.009-.025a.193.193,0,0,1-.013-.029.089.089,0,0,0-.013-.021l-.013-.025c0-.008-.009-.012-.013-.021s-.013-.016-.018-.025a.073.073,0,0,0-.018-.021c0-.008-.013-.012-.018-.021s-.013-.012-.018-.021-.013-.012-.018-.016L37,5.84c-.009,0-.013-.012-.022-.016s-.018-.012-.027-.016l-.022-.012a.093.093,0,0,1-.027-.016l-.027-.012-.027-.012-.027-.012c-.009,0-.018,0-.027-.008a.087.087,0,0,0-.031-.008.2.2,0,0,1-.022,0,.1.1,0,0,0-.035,0s-.009,0-.018,0L21.657,3.785V1.836a.235.235,0,0,0,0-.058.029.029,0,0,0,0-.016c0-.012,0-.025,0-.037s0-.021-.009-.033,0-.012,0-.021l-.013-.037s0-.012,0-.016a.12.12,0,0,0-.018-.033s0-.012-.009-.016a.086.086,0,0,0-.018-.025c0-.008-.009-.012-.013-.021s-.009-.012-.013-.021-.013-.016-.018-.025l-.013-.012-.027-.025-.013-.012a.189.189,0,0,0-.031-.025s-.013-.008-.018-.012-.018-.012-.027-.021-.027-.016-.035-.021-.009,0-.013-.008l-.058-.025L18.03.044a.614.614,0,0,0-.783.3.541.541,0,0,0,.318.728L20.459,2.2V14.361a2.336,2.336,0,0,0,2.128,2.248,2.124,2.124,0,0,0-.345,1.151,2.417,2.417,0,0,0,4.821,0,2.081,2.081,0,0,0-.332-1.13H32.1a2.092,2.092,0,0,0-.332,1.13,2.417,2.417,0,0,0,4.821,0,2.336,2.336,0,0,0-2.411-2.24H22.9a1.207,1.207,0,0,1-1.247-1.159v-1.5A2.614,2.614,0,0,0,22.9,13.169Zm2.972,4.587a1.22,1.22,0,1,1-1.216-1.13A1.178,1.178,0,0,1,25.873,17.756Zm9.527,0a1.22,1.22,0,1,1-1.216-1.13A1.178,1.178,0,0,1,35.4,17.756Zm-.641-5.7H22.9A1.207,1.207,0,0,1,21.653,10.9v-6L36.006,6.74V10.9A1.208,1.208,0,0,1,34.759,12.059Z"
                                      transform="translate(0 0)" />
                            </g>
                        </svg>
                    </a>
                    <span class="cart_number">1</span>
                </div>
            </div>
        </div>
        <!-- container -->
    </header>
    <!-- Header -->

    <nav class="main_nav">
        <div class="container">
            <ul>
                <li>
                    <button type="button" class="nav_toggle">
                        <img src="{{Url('front/')}}/images/menu.svg" alt="">
                    </button>
                </li>
                <li class="dropdown onhover">
                    <a href="#"><span>Mobiles & Tablets</span></a>
                    <div class="category-dropdown">
                        <div class="left">
                            <h5>Categories</h5>
                            <ul>
                                <li><a href="#!">Mobiles</a></li>
                                <li><a href="#!">Laptops & IT Accessories</a></li>
                                <li><a href="#!">Video Games</a></li>
                                <li><a href="#!">Televisions</a></li>
                                <li><a href="#!">Home Appliances</a></li>
                                <li><a href="#!">Speakers</a></li>
                                <li><a href="#!">Headphones & Earphones</a></li>
                                <li><a href="#!">Audio & Video</a></li>
                                <li><a href="#!">Wearable Devices</a></li>
                                <li><a href="#!">Camera, Photo & Video</a></li>
                                <li><a href="#!">Tablets</a></li>
                            </ul>
                        </div>

                        <div class="middle">
                            <h5>Top Brands</h5>
                            <ul>
                                <li><a href=":#!"><img src="{{Url('front/')}}/images/brands/apple.jpg" alt="Brand Logo"></a></li>
                                <li><a href=":#!"><img src="{{Url('front/')}}/images/brands/samsung.jpg" alt="Brand Logo"></a></li>
                                <li><a href=":#!"><img src="{{Url('front/')}}/images/brands/sony.jpg" alt="Brand Logo"></a></li>
                                <li><a href=":#!"><img src="{{Url('front/')}}/images/brands/lenovo.jpg" alt="Brand Logo"></a></li>
                                <li><a href=":#!"><img src="{{Url('front/')}}/images/brands/hp.jpg" alt="Brand Logo"></a></li>
                                <li><a href=":#!"><img src="{{Url('front/')}}/images/brands/kenwood.jpg" alt="Brand Logo"></a></li>
                                <li><a href=":#!"><img src="{{Url('front/')}}/images/brands/canon.jpg" alt="Brand Logo"></a></li>
                                <li><a href=":#!"><img src="{{Url('front/')}}/images/brands/microsoft.jpg" alt="Brand Logo"></a></li>
                                <li><a href=":#!"><img src="{{Url('front/')}}/images/brands/philips.jpg" alt="Brand Logo"></a></li>
                            </ul>
                        </div>

                        <div class="right">
                            <ul>
                                <li><a href="#"><img src="{{Url('front/')}}/images/deal-image-1.png" alt="Deal Image"></a></li>
                                <li><a href="#"><img src="{{Url('front/')}}/images/deal-image-2.png" alt="Deal Image"></a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="dropdown onhover">
                    <a href="#"><span>Electronics</span></a>
                    <div class="category-dropdown">
                        <div class="left">
                            <h5>Categories</h5>
                            <ul>
                                <li><a href="#!">Mobiles</a></li>
                                <li><a href="#!">Laptops & IT Accessories</a></li>
                                <li><a href="#!">Video Games</a></li>
                                <li><a href="#!">Televisions</a></li>
                                <li><a href="#!">Home Appliances</a></li>
                                <li><a href="#!">Speakers</a></li>
                                <li><a href="#!">Headphones & Earphones</a></li>
                                <li><a href="#!">Audio & Video</a></li>
                                <li><a href="#!">Wearable Devices</a></li>
                                <li><a href="#!">Camera, Photo & Video</a></li>
                                <li><a href="#!">Tablets</a></li>
                            </ul>
                        </div>

                        <div class="middle">
                            <h5>Top Brands</h5>
                            <ul>
                                <li><a href=":#!"><img src="{{Url('front/')}}/images/brands/apple.jpg" alt="Brand Logo"></a></li>
                                <li><a href=":#!"><img src="{{Url('front/')}}/images/brands/samsung.jpg" alt="Brand Logo"></a></li>
                                <li><a href=":#!"><img src="{{Url('front/')}}/images/brands/sony.jpg" alt="Brand Logo"></a></li>
                                <li><a href=":#!"><img src="{{Url('front/')}}/images/brands/lenovo.jpg" alt="Brand Logo"></a></li>
                                <li><a href=":#!"><img src="{{Url('front/')}}/images/brands/hp.jpg" alt="Brand Logo"></a></li>
                                <li><a href=":#!"><img src="{{Url('front/')}}/images/brands/kenwood.jpg" alt="Brand Logo"></a></li>
                                <li><a href=":#!"><img src="{{Url('front/')}}/images/brands/canon.jpg" alt="Brand Logo"></a></li>
                                <li><a href=":#!"><img src="{{Url('front/')}}/images/brands/microsoft.jpg" alt="Brand Logo"></a></li>
                                <li><a href=":#!"><img src="{{Url('front/')}}/images/brands/philips.jpg" alt="Brand Logo"></a></li>
                            </ul>
                        </div>

                        <div class="right">
                            <ul>
                                <li><a href="#"><img src="{{Url('front/')}}/images/deal-image-3.png" alt="Deal Image"></a></li>
                                <li><a href="#"><img src="{{Url('front/')}}/images/deal-image-4.png" alt="Deal Image"></a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <a href="#"><span>Home</span></a>
                </li>
                <li>
                    <a href="#"><span>Appliances</span></a>
                </li>
                <li>
                    <a href="#"><span>Health & Beauty</span></a>
                </li>
                <li>
                    <a href="#"><span>Moms & Babies</span></a>
                </li>
                <li>
                    <a href="#"><span>Lifestylep</span></a>
                </li>

            </ul>
        </div>
    </nav>
    <!-- Nav -->
<!-- <div class="top">
        <div class="container">
            <div>
                <a href="#"><img src="{{Url('front/')}}/images/banner_img.jpg" alt="" class=""></a>
            </div>
            <div>
                <a href="#"><img src="{{Url('front/')}}/images/banner_img.jpg" alt="" class=""></a>
            </div>
            <div>
                <a href="#"><img src="{{Url('front/')}}/images/banner_img.jpg" alt="" class=""></a>
            </div>
        </div>
    </div> -->
    <!-- Top -->

    @yield('content')
    <!-- Trending Products  -->
    <div id="footer">
    <!-- <div class="need_help">
            <div class="container">
                <p>Need help? Call our amazing support team at 01006666057</p>
                <ul class="social_media">
                    <li>
                        <a href="#">
                            <img src="{{Url('front/')}}/images/facebook.svg" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="{{Url('front/')}}/images/twitter.svg" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="{{Url('front/')}}/images/instagram.svg" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="{{Url('front/')}}/images/youtube.svg" alt="">
                        </a>
                    </li>
                </ul>
            </div>
        </div> -->
        <footer>
            <div class="container">
                <div class="columns">
                    <div class="column">
                        <h3>About Alfa Market</h3>
                        <ul>
                            <li>
                                <a href="#">About Us</a>
                            </li>
                            <li>
                                <a href="#">Contact Us</a>
                            </li>
                            <li>
                                <a href="#">Legal Us</a>
                            </li>
                            <li>
                                <a href="#">Corporate Responsibility</a>
                            </li>
                            <li>
                                <a href="#">Privacy and Cookies</a>
                            </li>
                        </ul>
                    </div>
                    <div class="column">
                        <h3>About Alfa Market</h3>
                        <ul>
                            <li>
                                <a href="#">About Us</a>
                            </li>
                            <li>
                                <a href="#">Contact Us</a>
                            </li>
                            <li>
                                <a href="#">Legal Us</a>
                            </li>
                            <li>
                                <a href="#">Corporate Responsibility</a>
                            </li>
                            <li>
                                <a href="#">Privacy and Cookies</a>
                            </li>
                        </ul>
                    </div>
                    <div class="column">
                        <h3>About Alfa Market</h3>
                        <ul>
                            <li>
                                <a href="#">About Us</a>
                            </li>
                            <li>
                                <a href="#">Contact Us</a>
                            </li>
                            <li>
                                <a href="#">Legal Us</a>
                            </li>
                            <li>
                                <a href="#">Corporate Responsibility</a>
                            </li>
                            <li>
                                <a href="#">Privacy and Cookies</a>
                            </li>
                        </ul>
                    </div>
                    <div class="column">
                        <h3>About Alfa Market</h3>
                        <ul>
                            <li>
                                <a href="#">About Us</a>
                            </li>
                            <li>
                                <a href="#">Contact Us</a>
                            </li>
                            <li>
                                <a href="#">Legal Us</a>
                            </li>
                            <li>
                                <a href="#">Corporate Responsibility</a>
                            </li>
                            <li>
                                <a href="#">Privacy and Cookies</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="bottom_section">
                    <div class="language">
                        <div class="dropdown">
                            <a href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16">
                                    <g id="earth-globe" transform="translate(0)">
                                        <path id="Path_10" data-name="Path 10" class="cls-1" d="M24.656,13.344A7.994,7.994,0,1,0,27,19,7.941,7.941,0,0,0,24.656,13.344Zm1.62,5.3h-3.1a13.891,13.891,0,0,0-.506-3.478,7.934,7.934,0,0,0,1.649-1.136A7.265,7.265,0,0,1,26.275,18.641Zm-6.916.718h3.1a13.131,13.131,0,0,1-.454,3.2,8.076,8.076,0,0,0-2.645-.575ZM23.8,13.527a7.275,7.275,0,0,1-1.358.944,8.341,8.341,0,0,0-.421-.98,5.756,5.756,0,0,0-1.051-1.5A7.252,7.252,0,0,1,23.8,13.527Zm-4.441-1.7a3.385,3.385,0,0,1,2.021,1.979,8.161,8.161,0,0,1,.411.97,7.328,7.328,0,0,1-2.433.545Zm2.648,3.638a13.133,13.133,0,0,1,.447,3.177h-3.1v-2.6a8.08,8.08,0,0,0,2.648-.578Zm-3.367,3.174h-3.1a13.133,13.133,0,0,1,.447-3.177,7.931,7.931,0,0,0,2.648.575v2.6Zm0-6.811v3.494a7.31,7.31,0,0,1-2.433-.545,8.161,8.161,0,0,1,.411-.97,3.385,3.385,0,0,1,2.021-1.979Zm-1.61.16a5.811,5.811,0,0,0-1.051,1.5,9.605,9.605,0,0,0-.421.98,7.383,7.383,0,0,1-1.358-.944A7.252,7.252,0,0,1,17.034,11.989Zm-3.347,2.038a8.061,8.061,0,0,0,1.649,1.136,13.764,13.764,0,0,0-.506,3.478h-3.1A7.265,7.265,0,0,1,13.687,14.027Zm-1.959,5.332h3.1a13.873,13.873,0,0,0,.513,3.5,7.862,7.862,0,0,0-1.642,1.13A7.254,7.254,0,0,1,11.728,19.359Zm2.488,5.127a7.3,7.3,0,0,1,1.352-.937,8.849,8.849,0,0,0,.415.96,5.833,5.833,0,0,0,1.051,1.505A7.288,7.288,0,0,1,14.216,24.486Zm4.428,1.685a3.385,3.385,0,0,1-2.021-1.979,8.393,8.393,0,0,1-.4-.95,7.207,7.207,0,0,1,2.426-.542v3.471ZM16,22.556a13.131,13.131,0,0,1-.454-3.2h3.1v2.622A7.913,7.913,0,0,0,16,22.556Zm3.363,3.615V22.7a7.287,7.287,0,0,1,2.426.542,8.393,8.393,0,0,1-.4.95A3.385,3.385,0,0,1,19.362,26.171Zm1.61-.16a5.776,5.776,0,0,0,1.051-1.505,8.849,8.849,0,0,0,.415-.96,7.3,7.3,0,0,1,1.352.937A7.2,7.2,0,0,1,20.972,26.011Zm3.337-2.024a7.988,7.988,0,0,0-1.642-1.13,13.9,13.9,0,0,0,.513-3.5h3.1a7.288,7.288,0,0,1-1.972,4.627Z" transform="translate(-11 -11)"/>
                                    </g>
                                </svg>

                                <span>English</span> <span class="caret"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3.5 2">
                                <g id="caret-arrow-up" transform="translate(3.5 2) rotate(180)">
                                  <path id="Path_11" data-name="Path 11" class="cls-1" d="M3.435,1.622,1.9.066a.212.212,0,0,0-.308,0L.065,1.622a.22.22,0,0,0,0,.312A.209.209,0,0,0,.219,2H3.281a.209.209,0,0,0,.154-.066.22.22,0,0,0,0-.312Z"/>
                                </g>
                              </svg>
                              </span>
                            </a>
                            <ul class="dropdown_menu top">
                                <li><a href="#">English</a></li>
                                <li><a href="#" class="ar">عربي</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="social_media">
                        <span>Follow Us</span>
                        <ul>
                            <li>
                                <a href="#">
                                    <img src="{{Url('front/')}}/images/facebook.svg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="{{Url('front/')}}/images/twitter.svg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="{{Url('front/')}}/images/instagram.svg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="{{Url('front/')}}/images/youtube.svg" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <div class="copyright">
            <div class="container">
                <p>© 2018 Alfa Market. All Rights Reserved.</p>
            </div>
        </div>
    </div>
    <!-- Footer -->
</div>
<!-- globalWrapper -->
<script src="{{Url('front/')}}/js/jquery.js" type="text/javascript"></script>
<script src="{{Url('front/')}}/js/script.js" type="text/javascript"></script>
<script>
    function validate() {
        $("#form").validate({
            ignore: '',
            messages: {
                textfield: {
                    required: "Required"
                },
                emailfield: {
                    required: "Required"
                },
                phonefield: {
                    required: "Required"
                },
                selectfield: {
                    required: "Required"
                },
                textareafield: {
                    required: "Required"
                },
                radio_block: {
                    required: "Required"
                },
                test: {
                    required: "Required"
                }
            },
            rules: {
                emailfield: {
                    email: true
                },
                phonefield: {
                    number: true
                }
            }
        });
    }
    validate();

    function addToCart($productId) {
        $.ajax({
            method: "POST",
            url: "{{url('cart')}}",
            data: {
                _token: "{{csrf_token()}}",
                product_id: $productId
            }
        }).done(function (count) {
            $('.cart_number').text(count);
        })
    }

    $( "#all" ).click(function() {
        $.ajax({
            method: "GET",
            url: "{!! url('/all-orders/all') !!}",
        }).done(

            function(data)

            {
                console.log('fffff');
               //$( ".ord" ).empty();
                $('.ord').html(data.html);

            }

        );
    });


    $( ".more" ).click(function() {
        $.ajax({
            method: "GET",
            url: "{!! url('/all-orders/more') !!}",
        }).done(

            function(data)

            {
                console.log('fffff');
                //$( ".ord" ).empty();
                $('.ord').html(data.html);

            }

        );
    });

    $( "#last" ).click(function() {
        $.ajax({
            method: "GET",
            url: "{!! url('/all-orders/last') !!}",
        }).done(

            function(data)

            {
                console.log('fffff');
                //$( ".ord" ).empty();
                $('.ord').html(data.html);

            }

        );
    });

    $(document).ready(function () {

        //First Request on load
        $.ajax({
            method: "GET",
            url: "{{url('cart')}}",
            contentType : "json",
            dataType : "json",
        }).done(function (count) {
            $('.cart_number').text(count);
        });
        //Request every 3 seconds
        setInterval( function () {
            $.ajax({
                method: "GET",
                url: "{{url('cart')}}",
                contentType : "json",
                dataType : "json",
            }).done(function (count) {
                $('.cart_number').text(count);
            })
        }, 3000 );
    })
</script>

@yield('script')

</body>

</html>
