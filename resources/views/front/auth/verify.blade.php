@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif
                    {{Form::open(['action'=>['\App\Http\Controllers\Auth\VerifyController@manualVerify',request()->email]])}}
                        <div class="form-group">
                            <label for="">Activation Code</label>
                            {{Form::text('token',null,['class'=>'form-control'])}}
                            <small class="text-danger">{{$errors->first('email')}}</small>
                        </div>
                        <button type="submit" class="btn btn-success">{{ __('Activate') }}</button>
                    {{Form::close()}}
                    <hr>
                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }},
                    {{Form::open(['action'=>['\App\Http\Controllers\Auth\VerifyController@resendVerifyToken',request()->email]])}}
                        <button type="submit" class="btn btn-primary">{{ __('click here') }}</button>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
