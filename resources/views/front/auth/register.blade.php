
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="robots" content="index, follow">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="description" content="The initial Workflow of Ahmed Shaarawy">
    <title>Alfa Group</title>
    <!--<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link rel="apple-touch-icon" href="images/touch-icon-iphone.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="images/touch-icon-ipad.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="images/touch-icon-iphone-retina.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="images/touch-icon-ipad-retina.png" />
    <link rel="mask-icon" href="images/safari-pinned-tab.svg" color="#ff2857" />-->
    <link rel="stylesheet" href="{{Url('front/')}}/css/app.css">

    <link href="https://fonts.googleapis.com/css?family=Cairo:300,400,600" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 10]>
    <![endif]-->
</head>

<body>
<div class="globalWrapper">
    <div class="welcome">
        <div class="logo">
            <a href="index.html">
                <img src="{{Url('front/')}}/images/logo.svg" alt="">
            </a>
        </div>
        {{Form::open()}}
        <div class="form_box">
            <div class="title">
                <h1>Sign Up</h1>
                <a href="{{url('login')}}" class="colored">Log In</a>
            </div>
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
            @endif

            <!-- <div class="sign_in_with">
                <a href="#"><img src="https://cf2.s3.souqcdn.com/public/style/img/en/facebook-login-button-new.png" alt=""></a>
                <div class="or"><span>Or</span></div>

            </div> -->
            <div class="label_top">
                <div class="mutli_field">
                    <div class="custom_field">
                        <label for="">First Name</label>
                        {{Form::text('first_name',null,['class'=>'custom_input'])}}
                        <small class="text-danger">{{$errors->first('first_name')}}</small>
                    </div>
                    <div class="custom_field">
                        <label for="">Last Name</label>
                        {{Form::text('last_name',null,['class'=>'custom_input'])}}
                        <small class="text-danger">{{$errors->first('last_name')}}</small>
                    </div>
                </div>
                <div class="custom_field">
                    <label for="">Phone Number</label>
                    {{Form::text('phone_number',null,['class'=>'custom_input'])}}
                    <small class="text-danger">{{$errors->first('phone_number')}}</small>

                </div>
                <div class="custom_field">
                    <label for="">Email</label>
                    {{Form::email('email',null,['class'=>'custom_input'])}}
                    <small class="text-danger">{{$errors->first('email')}}</small>
                </div>
                <div class="radios">
                    <div class="radio inline m_t_0">
                        {{Form::radio('gender','male',null,['id'=>'1'])}}
                        <label for="1">Male</label>
                    </div>
                    <div class="radio inline m_t_0">
                        {{Form::radio('gender','female',null,['id'=>'2'])}}
                        <label for="2">Female</label>
                    </div>
                    <div class="row">
                        <small class="text-danger">{{$errors->first('gender')}}</small>
                    </div>
                </div>

                <div class="custom_field">
                    <label for="">Password</label>
                    {{Form::password('password',['class'=>'custom_input'])}}
                    <small class="text-danger">{{$errors->first('password')}}</small>
                </div>
                <button type="submit" class="btn btn-primary btn-block m_t_20 shadow_hover">Sign Up</button>
            </div>
            <p class="m_t_10">By clicking the 'Sign Up' button, you confirm that you accept our <a href="#" class="colored">Terms of use</a> and <a href="#" class="colored">Privacy Policy</a> .</p>

            <div class="to_signup m_t_30">
                Have an account? <a href="{{url('login')}}" class="colored">Log In</a>
            </div>
        </div>
        {{Form::close()}}
        <div class="footer">
            <div class="container">
                <p><strong>© 2018 Alfa Market</strong></p>
                <ul class="list_inline colored">
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Media Center</a></li>
                    <li><a href="#">Careers</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Terms and Conditions</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
</body>
