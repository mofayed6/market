@extends('front.layouts.master') 
@section('content')

<div class="main_content">
    <div class="container">
        <div class="breadcrumb">
            <ul class="breadcrumb">
                <li>
                    <a href="#">Alfa.com</a>
                </li>
                <li>
                    <a href="#">My Account</a>
                </li>
                <li>
                    <span>Order No. #{{ $order->id }}</span>
                </li>
            </ul>
        </div>
        <div class="title">
            <h4 class="colored m_t_0">Order Info</h4>
        </div>
        <section class="inner_section white">
            <p><span class="space_r">Order <strong>#{{ $order->id }}</strong></span><span>Order Placed: {{ $date }}</span></p>
            <div class="history_info">
                <div>
                    <h5>Payment Method</h5>
                    <p>Credit Card</p>
                </div>
                <div>
                    <h5>Receipient</h5>
                    <p>{{auth()->user()->first_name .' '.auth()->user()->last_name }}</p>
                </div>
                <div>
                    <h5>Order Summary</h5>
                    <p>Subtotal (2 Items): {{$beforeDiscount}} {{ __('general.egp', [], $locale) }}</p>
                    <p><strong>Grand Total: {{$afterDiscount}} {{ __('general.egp', [], $locale) }}</strong></p>
                </div>
            </div>
        </section>
        <section class="inner_section white">
            <div class="shipment_details">
                <div>
                    <p><strong>Shipment 1 of 1</strong></p>
                    <span class="colored">
                         {{\Modules\Orders\Models\OrderStatus::statusName($status->status)}}
                        </span>
                </div>
                <div>
                    <p>Shipped by: Alfa.com</p>
                    <p>AWB: <span class="colored">183791873912</span></p>
                </div>
                <button type="button" class="btn btn-basic" disabled>Track Shipment</button>
            </div>
            @foreach($products as $product)

            <div class="shipment_items">
                <div>
                    <div class="image">
                        <img src="images/box_1.jpg" alt="">
                    </div>
                    <div class="details">
                        <div class="title">
                            <h6><a href="#" class="colored">{{$product['products'][\App::getLocale()]['name']}}</a></h6>
                            <span class="price"><strong>{{$product['price']}} {{ __('general.egp', [], $locale) }}</strong></span>
                        </div>
                        <!-- <p>Sold By: <a href="#" class="colored">Alfa</a></p> -->
                        <p>Quantity: {{$product['quantity']}}</p>
                        <!-- <div class="rating">
                                <p>Rate this product</p>
                                <div class="stars">
                                    <span><i class="mIcon"></i></span>
                                    <span><i class="mIcon"></i></span>
                                    <span><i class="mIcon"></i></span>
                                    <span><i class="mIcon"></i></span>
                                    <span><i class="mIcon"></i></span>
                                </div>
                            </div> -->
                    </div>
                </div>
            </div>
            @endforeach


            <div class="shipment_price">
                <dl>
                    <dt>Item(s) Subtotal:</dt>
                    <dd>{{$beforeDiscount}} {{ __('general.egp', [], $locale) }}</dd>
                    <dt>Shipping Fees:</dt>
                    <dd>17 {{ __('general.egp', [], $locale) }}</dd>
                    <dt>Amount to be paid:</dt>
                    <dd><strong>{{$afterDiscount}} {{ __('general.egp', [], $locale) }}</strong></dd>
                </dl>
            </div>
        </section>
    </div>
</div>

@stop