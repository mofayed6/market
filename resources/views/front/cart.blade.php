@extends('front.layouts.master')
@section('content')
<div class="main_content">
    <div class="container">
        <div class="main_with_sidebar right">
            <aside>
                <div class="title flex vcenter m_b_10">
                    <h4 class="no_margin">Total <strong>{{$total}}</strong> <small class="small">{{ __('general.egp', [], $locale) }}</small></h4>
                </div>
                <button type="button" {{ count($items) == 0 ? "disabled":"" }} class="btn btn-success btn-block shadow_hover">Proceed to Checkout</button>
            </aside>
            <main>
                <div class="title flex vcenter m_b_10">
                    <h4 class="no_margin">Shopping Cart (<span>{{count($items)}}</span>)</h4>
                </div>
                    @if(count($items)>0)
                        @foreach($items as $item)
                            <section class="inner_section white">
                                {{Form::open(['action'=>['\Modules\Cart\Http\Controllers\CartController@update',$item],'method'=>'patch'])}}
                                <div class="shipment_items">
                                    <div>
                                        <div class="image">
                                            <img src="{{asset('uploads/'.$item->product->featured_image)}}" alt="">
                                        </div>
                                        <div class="details">
                                            <div class="title">
                                                <h6><a href="#">{{$item->product->name}}</a></h6>
                                                <div class="quantity custom_field">
                                                    <span class="space_r">QTY</span>
                                                    {{Form::selectRange('quantity',1,20,$item->quantity,['class'=>'custom_input small','onChange'=>'$(this).parent().parent().parent().parent().parent().parent().submit()'])}}
                                                </div>
                                            </div>
                                            <p><strong class="colored">{{$item->price}} <small class="small">{{ __('general.egp', [], $locale) }}</small></strong></p>
                                            <dl class="m_t_20">
                                                <dt>Size:</dt>
                                                <dd>XXL</dd>
                                                <dt>Color:</dt>
                                                <dd>Yellow</dd>
                                            </dl>
                                        </div>
                                    </div>
                                </div>

                                <div class="line_separator m_t_20 m_b_20"></div>
                                <div>
                                    <button type="submit" name="action" value="delete" class="delete" style="display: none">{{ __('product.delete', [], $locale) }}</button>
                                    <button type="submit" name="action" value="save" class="save"  style="display: none">Save</button>
                                    <a href="#" class="colored" onclick="$(this).parent().find('.save').click();">{{ __('product.save_for_later', [], $locale) }}</a> | <a href="#" class="colored" onclick="$(this).parent().find('.delete').click();">{{ __('product.delete', [], $locale) }}</a>
                                </div>
                                {{Form::close()}}
                            </section>
                        @endforeach
                    @else
                        <strong>{{ __('general.no_items_here', [], $locale) }}</strong>
                    @endif
                        {{-- end loop --}}
            </main>
        </div>
    </div>
</div>
@stop
