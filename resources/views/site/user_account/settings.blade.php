@extends('site.layouts.main')

@section('page_content')
<div class="container">
    <div class="main_with_sidebar">

        @include('site.user_account.sidebar')

        <main>
            <div class="title">
                <h4 class="m_t_0">{{ __("user.account_settings", [], $locale) }}</h4>
            </div>

            @include('site.partials.success_failure_messages')

            <form action="{{ route('user_account.update', [$locale]) }}" method="POST">
                @csrf

                <section class="inner_section white m_t_0">
                    <div class="mutli_field m_b_20">
                        <div class="label_top">
                            <div class="custom_field">
                                <label for="first_name">{{ __("user.first_name", [], $locale) }}</label>
                                <input required type="text" class="custom_input" name="first_name" id="first_name"
                                    value="{{ $user->first_name }}">
                            </div>
                        </div>
                        <div class="label_top">
                            <div class="custom_field">
                                <label for="last_name">{{ __("user.last_name", [], $locale) }}</label>
                                <input required type="text" class="custom_input" name="last_name" id="last_name" value="{{ $user->last_name }}">
                            </div>
                        </div>
                    </div>
                    <div class="mutli_field m_b_20">
                        <div class="label_top">
                            <div class="custom_field">
                                <label for="mobile">{{ __("user.mobile", [], $locale) }}</label>
                                <input required type="text" class="custom_input" name="mobile" id="mobile" value="{{ $user->mobile }}">
                            </div>
                        </div>
                        <div class="label_top">
                            <div class="custom_field">
                                <label for="email">{{ __("user.email", [], $locale) }}</label>
                                <input required type="text" class="custom_input" name="email" id="email" value="{{ $user->email }}">
                            </div>
                        </div>
                    </div>
                    <div class="mutli_field m_b_20">
                        <div class="label_top">
                            <div class="custom_field">
                                <label for="gender">{{ __("user.gender", [], $locale) }}</label>
                                <select required class="custom_input" name="gender" id="gender">
                                    <option {{ setSelected($user->gender, 'male') }} value="male">{{ __("user.male", [], $locale) }}</option>
                                    <option {{ setSelected($user->gender, 'female') }} value="female">{{ __("user.female", [], $locale) }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="label_top">
                            <div class="custom_field">
                                <label for="date_of_birth">{{ __("user.date_of_birth", [], $locale) }}</label>
                                <input required type="text" class="custom_input" name="date_of_birth" id="date_of_birth"
                                    value="{{ $user->date_of_birth }}">
                            </div>
                        </div>
                    </div>
                    <div class="mutli_field m_b_20">
                        <div class="label_top">
                            <div class="custom_field">
                                <label for="password">{{ __("user.password", [], $locale) }}</label>
                                <input type="password" class="custom_input" name="password" id="password" autocomplete="off">
                            </div>
                        </div>
                        <div class="label_top">
                            <div class="custom_field">
                                <label for="password_confirmation">{{ __("user.password_confirmation", [], $locale) }}</label>
                                <input type="password" class="custom_input" name="password_confirmation" id="password_confirmation"
                                    autocomplete="off">
                            </div>
                        </div>
                    </div>
                </section>
                <button type="submit" class="btn btn-basic go_success regular small">{{ __("general.save", [], $locale) }}</button>
            </form>
        </main>
    </div>
</div>
@endsection
