@extends('site.layouts.main')

@section('page_content')
<div class="container">
    <div class="main_with_sidebar">

        @include('site.user_account.sidebar')

        <main>
            <div class="title flex vcenter m_b_10">
                <h4 class="no_margin">{{ __("order.track_your_orders", [], $locale) }}</h4>
                <!--
            <div class="button_group right">
                <button type="button" class="btn btn-basic icon_left"><i class="mIcon"></i>All</button>
                <button type="button" class="btn btn-basic icon_left"><i class="mIcon"></i>Past 6 Months</button>
            </div>
            -->
            </div>

            @foreach ($orders as $order)
            <section class="inner_section white">
                <div class="quick_info">
                    <dl>
                        <dt>{{ __("order.order_placed_on", [], $locale) }}:</dt>
                        <dd>{{ date('Y-m-d (h:i a)', strtotime($order->created_at)) }}</dd>
                        <dt>{{ __("order.order_id", [], $locale) }}:</dt>
                        <dd>#{{ $order->id }}</dd>
                    </dl>
                    <dl>
                        {{-- <dt>Recipient:</dt>
                        <dd>{{ auth()->id() }}</dd> --}}
                        <dt>{{ __("order.payment_method", [], $locale) }}:</dt>
                        <dd>{{ __("order.{$order->payment}", [], $locale) }}</dd>
                        <dt>{{ __("order.total", [], $locale) }}:</dt>
                        <dd>{{ number_format($order->total_cost) }} <span class="small">{{ __('general.egp', [], $locale) }}</span></dd>
                    </dl>
                </div>
                <div class="line_separator m_t_20 m_b_20"></div>
                <div class="shipment_items">
                    <div>
                        <div class="image">
                            <img src="{{ url('assets/site/images/box_1.jpg') }}" alt="">
                        </div>
                        <div class="details">
                            <div class="title">
                                <h6>
                                    <a href="{{ route('products.view', [$locale, $order->products()->first()->id]) }}"
                                        class="colored">
                                        {{ $order->products()->first()->translate('name', $locale) }}
                                    </a>
                                </h6>
                            </div>
                            <!--
                                <p class="m_t_10">Return window closed on <strong>22 March 2018, item cannot be returned</strong></p>
                            -->
                        </div>
                    </div>
                </div>
            </section>
            <a href="{{ route('user_order.show', [$locale, $order->id]) }}" class="btn btn-basic btn-block">{{ __("general.show", [], $locale) }}</a>
            @endforeach
        </main>
    </div>
</div>
@endsection
