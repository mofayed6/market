@extends('site.layouts.main')

@section('page_content')
<div class="container">
    <div class="main_with_sidebar">

        @include('site.user_account.sidebar')

        <main>
            <div class="title flex vcenter m_b_10">
                <h4 class="m_t_0">{{ __("user.shipping_addresses", [], $locale) }}</h4>
                <a onclick="addAddress()" href="#add-new-address" class="btn btn-basic right go_success regular small">
                    {{ __("user.create_new_address", [], $locale) }}
                </a>
            </div>
            <section class="inner_section white m_t_0">
                <div class="addresses clearfix">
                    <ul>
                        @foreach ($user_addresses as $user_address)
                        <li>
                            <div class="radio">
                                <input onclick="changePrimaryAddress(this.value)" value="{{ $user_address->id }}" id="address-{{ $user_address->id }}"
                                    type="radio" class="user-address-item" name="address_id"
                                    {{ setChecked(old('address_id', $user_address->is_default ? $user_address->id : 0), $user_address->id) }}>
                                <label for="address-{{ $user_address->id }}">
                                    <strong>{{ $user_address->country->translate('name', $locale) . ' - ' .
                                        $user_address->city->translate('name', $locale) . ' - ' .
                                        $user_address->district->translate('name', $locale) }}</strong>
                                    <span class="small space_l grey">
                                        <span class="user_address-primary" id="user_address-primary-{{ $user_address->id }}">{{
                                            $user_address->is_default ? '(' . __('general.primary', [], $locale) . ')' : ''}}</span>
                                    </span>
                                </label>
                            </div>
                            <p class="grey">
                                {{ $user_address->address }}<br>
                                {{ $user_address->apartment_number }}<br>
                                {{ $user_address->building_number }}<br>
                                {{ $user_address->phone_number }}
                            </p>
                            <span id="edit-address-object-{{ $user_address->id }}" style="display: none;">
                                {!! json_encode($user_address, JSON_UNESCAPED_UNICODE) !!}
                            </span>
                            <p><a href="#edit-address" onclick="editAddress({{ $user_address->id }})" class="colored">{{ __("general.edit", [], $locale) }}</a></p>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </section>

            <section id="edit-address" class="inner_section white m_t_0" style="display: none;">
                <div>
                    <h5 class="m_t_0">{{ __("user.shipping_addresses", [], $locale) }}</h5>
                    <div class="mutli_field m_b_20">
                        <div class="label_top">
                            <div class="custom_field">
                                <label for="edit-input-country_id">{{ __("user.country", [], $locale) }}</label>
                                <select class="custom_input" name="country_id" id="edit-input-country_id" class="country_id"
                                    onchange="countryChange(this.value, 'edit-input-city_id', 'edit-input-district_id')"
                                    required>
                                    @foreach ($countries as $country)
                                    <option {{ setSelected($country->id, $defaultCountry->id) }} value="{{ $country->id }}">{{
                                        $country->translate('name', $locale) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="label_top">
                            <div class="custom_field">
                                <label for="edit-input-city_id">{{ __("user.city", [], $locale) }}</label>
                                <select class="custom_input" name="city_id" id="edit-input-city_id" class="city_id"
                                    onchange="cityChange(this.value, 'edit-input-district_id')" required>
                                    @foreach ($defaultCountry->children as $city)
                                    <option {{ setSelected($city->id, $defaultCity->id) }} value="{{ $city->id }}">{{
                                        $city->translate('name', $locale) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="label_top">
                            <div class="custom_field">
                                <label for="edit-input-district_id">{{ __("user.district", [], $locale) }}</label>
                                <select class="custom_input" name="district_id" id="edit-input-district_id" class="district_id"
                                    required>
                                    @foreach ($defaultCity->children as $district)
                                    <option {{ setSelected($district->id, $defaultDistrict->id) }} value="{{ $district->id }}">{{
                                        $district->translate('name', $locale) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="label_top">
                        <div class="custom_field">
                            <label for="edit-input-address">{{ __("user.address", [], $locale) }}</label>
                            <input type="text" class="custom_input" name="address" id="edit-input-address" required>
                        </div>
                    </div>
                    <div class="mutli_field m_b_20 m_t_20">
                        <div class="label_top">
                            <div class="custom_field">
                                <label for="edit-input-building_number">{{ __("user.building_number", [], $locale) }}</label>
                                <input type="text" class="custom_input" name="building_number" id="edit-input-building_number">
                            </div>
                        </div>
                        <div class="label_top">
                            <div class="custom_field">
                                <label for="edit-input-apartment_number">{{ __("user.apartment_number", [], $locale) }}</label>
                                <input type="text" class="custom_input" name="apartment_number" id="edit-input-apartment_number">
                            </div>
                        </div>
                    </div>
                    <div class="label_top">
                        <div class="custom_field">
                            <label for="edit-input-phone_number">{{ __("user.phone_number", [], $locale) }}</label>
                            <input type="text" class="custom_input" name="phone_number" id="edit-input-phone_number">
                        </div>
                    </div>

                    <h4>{{ __("user.map_pin_location", [], $locale) }}</h4>

                    <div id="map" style="width:100%; height:400px;" class="m_t_30"></div>
                    <input type="hidden" name="lat" class="lat" id="edit-input-lat">
                    <input type="hidden" name="lng" class="lng" id="edit-input-lng">
                    <input type="hidden" name="address_id" id="edit-input-id">

                    <button onclick="updateAddress()" type="button" class="btn btn-basic m_t_30">{{ __("general.update", [], $locale) }}</button>
                </div>
            </section>

            <section id="add-new-address" class="inner_section white m_t_0" style="display: none;">
                <div>
                    <h5 class="m_t_0">{{ __("user.shipping_addresses", [], $locale) }}</h5>
                    <div class="mutli_field m_b_20">
                        <div class="label_top">
                            <div class="custom_field">
                                <label for="add-country_id">{{ __("user.country", [], $locale) }}</label>
                                <select class="custom_input" name="country_id" id="add-country_id" class="country_id"
                                    onchange="countryChange(this.value, 'add-city_id', 'add-district_id')" required>
                                    @foreach ($countries as $country)
                                    <option {{ setSelected($country->id, $defaultCountry->id) }} value="{{ $country->id }}">{{
                                        $country->translate('name', $locale) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="label_top">
                            <div class="custom_field">
                                <label for="add-city_id">{{ __("user.city", [], $locale) }}</label>
                                <select class="custom_input" name="city_id" id="add-city_id" class="city_id" onchange="cityChange(this.value, 'add-district_id')"
                                    required>
                                    @foreach ($defaultCountry->children as $city)
                                    <option {{ setSelected($city->id, $defaultCity->id) }} value="{{ $city->id }}">{{
                                        $city->translate('name', $locale) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="label_top">
                            <div class="custom_field">
                                <label for="add-district_id">{{ __("user.district", [], $locale) }}</label>
                                <select class="custom_input" name="district_id" id="add-district_id" class="district_id"
                                    required>
                                    @foreach ($defaultCity->children as $district)
                                    <option {{ setSelected($district->id, $defaultDistrict->id) }} value="{{ $district->id }}">{{
                                        $district->translate('name', $locale) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="label_top">
                        <div class="custom_field">
                            <label for="add-address">{{ __("user.address", [], $locale) }}</label>
                            <input type="text" class="custom_input" name="address" id="add-address" required>
                        </div>
                    </div>
                    <div class="mutli_field m_b_20 m_t_20">
                        <div class="label_top">
                            <div class="custom_field">
                                <label for="add-building_number">{{ __("user.building_number", [], $locale) }}</label>
                                <input type="text" class="custom_input" name="building_number" id="add-building_number">
                            </div>
                        </div>
                        <div class="label_top">
                            <div class="custom_field">
                                <label for="add-apartment_number">{{ __("user.apartment_number", [], $locale) }}</label>
                                <input type="text" class="custom_input" name="apartment_number" id="add-apartment_number">
                            </div>
                        </div>
                    </div>
                    <div class="label_top">
                        <div class="custom_field">
                            <label for="add-phone_number">{{ __("user.phone_number", [], $locale) }}</label>
                            <input type="text" class="custom_input" name="phone_number" id="add-phone_number">
                        </div>
                    </div>

                    <h4>{{ __("user.map_pin_location", [], $locale) }}</h4>

                    <div id="map" style="width:100%; height:400px;" class="m_t_30"></div>
                    <input type="hidden" name="lat" class="lat">
                    <input type="hidden" name="lng" class="lng">

                    <button onclick="storeAddress()" type="button" class="btn btn-basic m_t_30">{{ __("general.update", [], $locale) }}</button>
                </div>
            </section>

        </main>
    </div>
</div>
@endsection

@section('page_scripts')
<script>
    var primaryTranslation = "{{ __('general.primary', [], $locale) }}";
    function changePrimaryAddress(addressId) {
        $.ajax({
            method: "GET",
            url: baseUrl + '/' + baseLocale + '/shipping-addresses/change-primary-address/' + addressId,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (result) {
                if (result && result == 1) {
                    $('.user_address-primary').text('');
                    $('#user_address-primary-' + addressId).text('(' + primaryTranslation + ')');
                }
            }
        });
    }

    function addAddress() {
        $('#edit-address').hide();
        $('#add-new-address').show();
    }

    function editAddress(addressId) {
        address = JSON.parse($('#edit-address-object-' + addressId).text());
        $('#add-new-address').hide();
        $('#edit-address').show();
        $('#edit-input-id').val(address.id);
        $('#edit-input-address').val(address.address);
        $('#edit-input-country_id').val(address.country_id);

        countryChange(address.country_id, 'edit-input-city_id', 'edit-input-district_id');

        $('#edit-input-city_id').val(address.city_id);

        cityChange(address.city_id, 'edit-input-district_id');

        $('#edit-input-district_id').val(address.district_id);
        $('#edit-input-building_number').val(address.building_number);
        $('#edit-input-apartment_number').val(address.apartment_number);
        $('#edit-input-phone_number').val(address.phone_number);
        $('#edit-input-lat').val(address.lat);
        $('#edit-input-lng').val(address.lng);
    }

    function storeAddress() {
        address_value = $('#add-address').val();
        country_id_value = $('#add-country_id').val();
        city_id_value = $('#add-city_id').val();
        district_id_value = $('#add-district_id').val();
        building_number_value = $('#add-building_number').val();
        apartment_number_value = $('#add-apartment_number').val();
        phone_number_value = $('#add-phone_number').val();
        lat_value = $('#add-lat').val();
        lng_value = $('#add-lng').val();

        $.ajax({
            method: "POST",
            url: baseUrl + '/' + baseLocale + '/shipping-addresses/address-store',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                address: address_value,
                country_id: country_id_value,
                city_id: city_id_value,
                district_id: district_id_value,
                building_number: building_number_value,
                apartment_number: apartment_number_value,
                phone_number: phone_number_value,
                lat: lat_value,
                lng: lng_value
            },
            success: function (result) {
                reload_current_page();
            }
        });
    }

    function updateAddress() {
        address_id_value = $('#edit-input-id').val();
        address_value = $('#edit-input-address').val();
        country_id_value = $('#edit-input-country_id').val();
        city_id_value = $('#edit-input-city_id').val();
        district_id_value = $('#edit-input-district_id').val();
        building_number_value = $('#edit-input-building_number').val();
        apartment_number_value = $('#edit-input-apartment_number').val();
        phone_number_value = $('#edit-input-phone_number').val();
        lat_value = $('#edit-input-lat').val();
        lng_value = $('#edit-input-lng').val();

        $.ajax({
            method: "POST",
            url: baseUrl + '/' + baseLocale + '/shipping-addresses/address-update',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                address_id: address_id_value,
                address: address_value,
                country_id: country_id_value,
                city_id: city_id_value,
                district_id: district_id_value,
                building_number: building_number_value,
                apartment_number: apartment_number_value,
                phone_number: phone_number_value,
                lat: lat_value,
                lng: lng_value
            },
            success: function (result) {
                reload_current_page();
            }
        });
    }

</script>
<script type="text/javascript">
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: -33.8688,
                lng: 151.2195
            },
            zoom: 13
        });

        var input = document.getElementById('pac-input');

        var autocomplete = new google.maps.places.Autocomplete(
            input, {
                placeIdOnly: true
            });
        autocomplete.bindTo('bounds', map);

        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var geocoder = new google.maps.Geocoder;
        var marker = new google.maps.Marker({
            map: map
        });
        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });

        autocomplete.addListener('place_changed', function () {
            infowindow.close();
            var place = autocomplete.getPlace();

            if (!place.place_id) {
                return;
            }
            geocoder.geocode({
                'placeId': place.place_id
            }, function (results, status) {
                //console.log(results);    
                if (status !== 'OK') {
                    window.alert('Geocoder failed due to: ' + status);
                    return;
                }
                map.setZoom(11);
                console.log(results[0].geometry.location.lat());
                console.log(results[0].geometry.location.lng());
                map.setCenter(results[0].geometry.location);
                // Set the position of the marker using the place ID and location.
                marker.setPlace({
                    placeId: place.place_id,
                    location: results[0].geometry.location
                });
                marker.setVisible(true);

                $('#address').val(results[0].formatted_address);
                $('.lat').val(results[0].geometry.location.lat());
                $('.lng').val(results[0].geometry.location.lng());

                infowindowContent.children['place-name'].textContent = place.name;
                infowindowContent.children['place-id'].textContent = place.place_id;
                infowindowContent.children['place-address'].textContent =
                    results[0].formatted_address;
                infowindow.open(map, marker);
            });
        });
    }

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAoacCf8RDzMIaHcI5Ywh9zQ-kZt1-V_fc&libraries=places&callback=initMap"
    async defer></script>
@endsection
