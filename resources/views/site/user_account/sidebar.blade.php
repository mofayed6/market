<aside>
    <div class="account_settings">
        <div class="user">
            <div class="icon">
                <i class="mIcon"></i>
            </div>
            <div class="details">
                <h5>{{ auth()->user()->getFullName() }}</h5>
                <p>{{ auth()->user()->email }}</p>
            </div>
        </div>
        <nav>
            <ul>
                <li><a href="{{ route('user_order.all_orders', [$locale]) }}"><i class="mIcon"></i>{{ __("user.my_orders", [], $locale) }}</a></li>
                <li><a href="{{ route('user_addresses.show', [$locale]) }}"><i class="mIcon"></i>{{ __("user.shipping_addresses", [], $locale) }}</a></li>
                <li><a href="{{ route('products.wish_list', [$locale]) }}"><i class="mIcon"></i>{{ __("user.wish_list", [], $locale) }}</a></li>
                <li><a href="{{ route('user_account.view', [$locale]) }}"><i class="mIcon"></i>{{ __("user.account_settings", [], $locale) }}</a></li>
            </ul>
        </nav>
    </div>
</aside>
