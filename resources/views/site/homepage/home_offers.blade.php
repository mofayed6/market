<div class="home_offers block">
    <div class="container">
        <div class="offers">

        @foreach ($homePageImages = $homePageComponent_promotionboxwithtext->images->shuffle()->take(3) as $image)
            <div class="{{ $homePageImages->first()->id === $image->id ? 'v2' : '' }}">
                <a href="{{ $image->link }}">
                    <img src="{{ $image->getPictureUrl() }}" alt="" class="">
                    <p>{{ $image->text }}</p>
                </a>
            </div>
        @endforeach
        </div>
    </div>
</div>
