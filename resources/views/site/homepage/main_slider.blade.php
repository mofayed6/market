<div class="banner">
    <div class="container">
        <div class="slider_container">
            <div class="slider">
                @foreach ($homePageComponent_slider->images->shuffle()->take(4) as $image)
                    <a href="{{ $image->link }}">
                        <img src="{{ $image->getPictureUrl() }}" alt="" class="">
                    </a>
                @endforeach
            </div>
        </div>
        <div class="ads">
            <div class="ad1">
                <a href="{{ $homePageComponent_slidertop->images->shuffle()->first()->link }}">
                    <img src="{{ $homePageComponent_slidertop->images->shuffle()->first()->getPictureUrl() }}" alt="" class="">
                </a>
            </div>
            <div class="ad2">
                <a href="{{ $homePageComponent_sliderbottom->images->shuffle()->first()->link }}">
                    <img src="{{ $homePageComponent_sliderbottom->images->shuffle()->first()->getPictureUrl() }}" alt="" class="">
                </a>
            </div>
        </div>
    </div>
</div>