<div class="trending_products block m_t_30">
    <div class="container">
        <h2>{{ __('product.trending_products', [], $locale) }}</h2>
        <div class="products carousel">
            @foreach ($trendingProducts as $trendingProduct)
            <div>
                <!--
                        <div class="badge"><span>33</span></div>
                        -->
                <figure>
                    <a href="{{ route('products.view', [$locale, $trendingProduct->id]) }}">
                        <img src="{{ $trendingProduct->getPictureUrl() }}" alt="">
                    </a>
                </figure>
                <h3>
                    <a href="{{ route('products.view', [$locale, $trendingProduct->id]) }}">
                        {{ $trendingProduct->translate('name', $locale) }}
                    </a>
                </h3>
                <div class="price">
                    @if (!empty($trendingProduct->discount_price))
                        <p class="before">
                            <span>{{ number_format($trendingProduct->price) }} {{ __('general.egp', [], $locale) }}</span> 
                            <span>
                                {{ number_format((($trendingProduct->price - $trendingProduct->discount_price) / $trendingProduct->price) * 100) }}% {{ __('product.off', [], $locale) }}
                            </span>
                        </p>
                        <p class="after">
                            <span>{{ number_format($trendingProduct->discount_price) }}</span> 
                            {{ __('general.egp', [], $locale) }}
                            <img 
                                onclick="addToCart({{ $trendingProduct->id }})" 
                                class="add-to-cart-img" 
                                src="{{ url('assets/site/images/add-to-cart.svg') }}"
                                alt="{{ __('order.add_to_cart', [], $locale) }}"
                            >
                        </p>
                    @else
                    <p class="before"><span style="padding: 0px;"></span></p>
                        <p class="after">
                            <span>{{ number_format($trendingProduct->price) }}</span> 
                            {{ __('general.egp', [], $locale) }}
                            <img 
                                onclick="addToCart({{ $trendingProduct->id }})" 
                                class="add-to-cart-img" 
                                src="{{ url('assets/site/images/add-to-cart.svg') }}"
                                alt="{{ __('order.add_to_cart', [], $locale) }}"
                            >
                        </p>
                    @endif
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
