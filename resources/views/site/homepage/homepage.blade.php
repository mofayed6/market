@extends('site.layouts.main') 

@section('page_content')
<div class="container">
    @include('site.homepage.main_slider')

    {{-- @include('site.homepage.user_promos') --}}

    @include('site.homepage.full_width_ads')

    @include('site.homepage.home_offers')

    @include('site.homepage.latest_products')

    @include('site.homepage.trending_products')
</div>
@endsection