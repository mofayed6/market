<div class="big-promo" style="margin-top: 10px;">
    <div class="container">
        <a href="{{ $homePageComponent_fullbanner->images->shuffle()->first()->link }}">
            <img src="{{ $homePageComponent_fullbanner->images->shuffle()->first()->getPictureUrl() }}" alt="" class="">
        </a>
    </div>
</div>

<div class="long">
    <div class="container">
        <a href="{{ $homePageComponent_fullpagetrip->images->shuffle()->first()->link }}">
            <img src="{{ $homePageComponent_fullpagetrip->images->shuffle()->first()->getPictureUrl() }}" alt="" class="">
        </a>
    </div>
</div>
<!-- Full Width Ad -->