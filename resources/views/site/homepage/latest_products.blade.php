<div class="latest_products block m_t_30">
    <div class="container">
        <h2>{{ __('product.latest_products', [], $locale) }}</h2>
        <div class="products carousel">
            @foreach ($latestProducts as $latestProduct)
            <div>
                <!--
                        <div class="badge"><span>33</span></div>
                        -->
                <figure>
                    <a href="{{ route('products.view', [$locale, $latestProduct->id]) }}">
                        <img src="{{ $latestProduct->getPictureUrl() }}" alt="">
                    </a>
                </figure>
                <h3>
                    <a href="{{ route('products.view', [$locale, $latestProduct->id]) }}">
                        {{ $latestProduct->translate('name', $locale) }}
                    </a>
                </h3>
                <div class="price">
                    @if (!empty($latestProduct->discount_price))
                        <p class="before"><span>{{ number_format($latestProduct->price) }} {{ __('general.egp', [], $locale) }}</span> <span>{{
                                number_format((($latestProduct->price - $latestProduct->discount_price) /
                                $latestProduct->price) * 100) }}% {{ __('product.off', [], $locale) }}</span></p>
                        <p class="after"><span>{{ number_format($latestProduct->discount_price) }}</span> {{ __('general.egp', [], $locale) }}
                            <img onclick="addToCart({{ $latestProduct->id }})" class="add-to-cart-img" src="{{ url('assets/site/images/add-to-cart.svg') }}"
                                alt="{{ __('order.add_to_cart', [], $locale) }}">
                        </p>
                    @else
                        <p class="before"><span style="padding: 0px;"></span></p>
                        <p class="after">
                            <span>{{ number_format($latestProduct->price) }}</span> 
                            {{ __('general.egp', [], $locale) }}
                            <img 
                                onclick="addToCart({{ $latestProduct->id }})" 
                                class="add-to-cart-img" 
                                src="{{ url('assets/site/images/add-to-cart.svg') }}"
                                alt="{{ __('order.add_to_cart', [], $locale) }}"
                            >
                        </p>
                    @endif
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<!-- Latest Products  -->
