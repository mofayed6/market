<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    @yield('head_prepend')

    @include('site.partials.header_meta_tags')

    <title>{{ config('site_info.title_' . app()->getLocale()) . (!empty($page_title) ? " - {$page_title}" : '') }}</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base-url" content="{{ url('') }}">
    <meta name="base-locale" content="{{ app()->getLocale() }}">

    @include('site.partials.styles')

    @yield('page_styles')
    @yield('head_append')
</head>

<body>
    @yield('body_prepend')

    <div class="globalWrapper">
        @include('site.partials.sidebar')
        @include('site.partials.header')
        @include('site.partials.navbar')

        <div class="main_content">
            @yield('page_content')
        </div>

        @include('site.partials.footer')
    </div>

    @include('site.partials.modals')
    @include('site.partials.scripts')
    @include('site.partials.custom_scripts')

    @yield('page_scripts')
    @yield('body_append')



</body>

</html>