@extends('site.layouts.main')
@section('page-title', $page_title)
@section('page_content')
<div class="container">
    {!! $page_content !!}
</div>
@endsection
