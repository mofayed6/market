<meta charset="UTF-8">

<meta name="robots" content="index, follow">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!-- @todo dynamically adding the value of page description -->
<meta name="description" content="">

<link rel="shortcut icon" href="{{ url('assets/site/images/favicon.png') }}" type="image/x-icon">
<link rel="apple-touch-icon" href="{{ url('assets/site/images/touch-icon-iphone.png') }}" />
<link rel="apple-touch-icon" sizes="76x76" href="{{ url('assets/site/images/touch-icon-ipad.png') }}" />
<link rel="apple-touch-icon" sizes="120x120" href="{{ url('assets/site/images/touch-icon-iphone-retina.png') }}" />
<link rel="apple-touch-icon" sizes="152x152" href="{{ url('assets/site/images/touch-icon-ipad-retina.png') }}" />
<link rel="mask-icon" href="{{ url('assets/site/images/safari-pinned-tab.svg') }}" color="#ff2857" />
