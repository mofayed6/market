<script>
    var baseUrl = $('meta[name="base-url"]:first').attr('content');
    var baseLocale = $('meta[name="base-locale"]:first').attr('content');

    function submitForm(formId) {
        $('#' + formId).submit();
    }

    function printPage(className = 'printable-area') {
        var originalContents = document.body.innerHTML;
        var printContents = '';
        var printableElements = document.getElementsByClassName(className);

        for (var i = 0; i < printableElements.length; ++i) {
            printContents += printableElements[i].innerHTML;
        }

        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

    function urlExists(url) {
        var http = new XMLHttpRequest();
        http.open('HEAD', url, false);
        http.send();
        return http.status != 404;
    }

    function brTOnl(string_to_replace) {
        return string_to_replace.replace(/&nbsp;/g, ' ').replace(/\n<br.*?>/g, '\n').replace(/<br.*?>\n/g, '\n').replace(
            /<br.*?>/g, '\n');
    }

    function nlTObr(string_to_replace) {
        return string_to_replace.replace(/ /g, '&nbsp;').replace(/\n/g, '<br>');
    }

    function number_format(number, decimals, dec_point, thousands_sep) {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                    .toFixed(prec);
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '')
            .length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }

    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    function show_messages(identifier, errors) {
        $(identifier).html('');
        $(identifier).html(errors);
    }

    function redir_global(page) {
        window.location.assign(page);
    }

    function reload_current_page() {
        location.reload();
    }

    function rawurlencode(str) {
        str = (str + '').toString();

        return encodeURIComponent(str)
            .replace(/!/g, '%21')
            .replace(/'/g, '%27')
            .replace(/\(/g, '%28')
            .replace(/\)/g, '%29')
            .replace(/\*/g, '%2A');
    }


    function getAbsolutePath() {
        var loc = window.location;
        var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
        return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
    }

    function getQueryStringValue(key) {
        return decodeURIComponent(
            window.location.search.replace(
                new RegExp(
                    "^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$",
                    "i"
                ),
                "$1"
            )
        );
    }

    function changeOrAddQueryParameter(requiredParam, paramValue) {
        var current_requiredParam_value = getQueryStringValue(requiredParam);
        if (current_requiredParam_value) {
            return window.location.href.replace(requiredParam + '=' + current_requiredParam_value, requiredParam + '=' +
                paramValue);

        } else if (window.location.href.indexOf("?") != -1) {
            return window.location.href + '&' + requiredParam + '=' + paramValue;

        } else {
            return window.location.href + '?' + requiredParam + '=' + paramValue;
        }
    }

    /**
     * @param mixed $firstValue
     * @param mixed $secondValue
     * Returns 'selected' if $firstValue equals/matches $secondValue
     * else returns an empty string
     *
     * @return string
     */
    function setSelected(firstValue, secondValue) {
        if (firstValue === secondValue) {
            return 'selected';
        }

        return '';
    }

    /**
     * @param mixed firstValue
     * @param mixed secondValue
     * Returns 'checked' if firstValue equals/matches secondValue
     * else returns an empty string
     *
     * @return string
     */
    function setChecked(firstValue, secondValue) {
        if (firstValue === secondValue) {
            return 'checked';
        }

        return '';
    }

</script>


<script>
    $(document).ready(function () {
        //First Request on load
        $.ajax({
            method: "GET",
            url: baseUrl + '/' + baseLocale + '/cart',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            contentType: "json",
            dataType: "json",
        }).done(function (count) {
            $('.cart_number').text(count);
        });
    })

</script>

<script>
    function addToCart(productId) {
        $.ajax({
            method: "POST",
            url: baseUrl + '/' + baseLocale + '/cart',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                product_id: productId
            }
        }).done(function (count) {
            $('.cart_number').text(count);
        });
    }

    function changeBranch(branchId) {
        console.log(branchId);
        // send the request to the branch change end-point
        $.ajax({
            url: baseUrl + '/' + baseLocale + '/branches/change-branch/' + branchId,
            type: 'GET',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (result) {
                if (result == 1) {
                    $('#changeBranchModal').show();
                    $('#change').click(function() {
                         reload_current_page();
                    });
                    $('#close').click(function() {
                        $('#changeBranchModal').hide();
                    });

                }
            }
        });
    }

    
    function countryChange(countryId, targetCitiesDivId, targetDistrictsDivId) {
        $.ajax({
            url: baseUrl + '/' + baseLocale + '/get-cities/' + countryId,
            type: 'GET',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (result) {
                if (result) {
                    cities = JSON.parse(result);
                    var selectOptions = '';
                    $(cities).each(function (index, city) {
                        selectOptions += '<option value="' + city.id + '" ' + setSelected(city.id,
                            cities[0].id) + '>' + city.name + '</option>';
                    });
                    $('#' + targetCitiesDivId).html(selectOptions);
                    cityChange(cities[0].id, targetDistrictsDivId);
                }
            }
        });
    }

    function cityChange(cityId, targetDivId) {
        $.ajax({
            url: baseUrl + '/' + baseLocale + '/get-districts/' + cityId,
            type: 'GET',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (result) {
                if (result) {
                    districts = JSON.parse(result);
                    var selectOptions = '';
                    $(districts).each(function (index, district) {
                        selectOptions += '<option value="' + district.id + '" ' + setSelected(
                            district.id, districts[0].id) + '>' + district.name + '</option>';
                    });
                    $('#' + targetDivId).html(selectOptions);
                }
            }
        });
    }

    function productFavorite(productId) {
        $.ajax({
            method: "GET",
            url: baseUrl + '/' + baseLocale + '/products/wish_list/add/' + productId,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }).done(function (result) {
            if (result == 1) {
                $('#product-favorite-' + productId).hide();
                $('#product-unfavorite-' + productId).show();
            }
        });
    }

    function productUnfavorite(productId) {
        $.ajax({
            method: "GET",
            url: baseUrl + '/' + baseLocale + '/products/wish_list/remove/' + productId,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }).done(function (result) {
            if (result == 1) {
                $('#product-favorite-' + productId).show();
                $('#product-unfavorite-' + productId).hide();
            }
        });
    }


</script>


