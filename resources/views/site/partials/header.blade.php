<header id="header">
    <div class="container">
        <div class="logo">
            <button type="button" class="nav_toggle">
                <!-- <img class="black" src="{{ url('assets/site/images/menu.svg') }}" alt=""> -->
                <img class="white" src="{{ url('assets/site/images/menu-white.svg') }}" alt="">
            </button>
            <a href="{{ url('/' . app()->getLocale()) }}">
                <img src="{{ url('assets/site/images/logo_negative.svg') }}" alt="">
            </a>
        </div>
        <div class="search">
            <div class="dropdown">
                <a href="#"><img src="{{ url('assets/site/images/search.svg') }}" alt=""></a>
                <div class="dropdown_menu">
                    <form method="GET" action="{{ route('products.search', [$locale]) }}">
                        <input required name="keywords" type="text" placeholder="Looking for something?">
                        <button type="button"><img src="{{ url('assets/site/images/search.svg') }}" alt=""></button>
                    </form>
                </div>
            </div>
            <form method="GET" action="{{ route('products.search', [$locale]) }}">
                <input required name="keywords" type="text" placeholder="Looking for something?">
                <button type="button"><img src="{{ url('assets/site/images/search.svg') }}" alt=""></button>
            </form>
        </div>
        <div class="right">
            <div class="dropdown">
                <a href="#">
                    <span>{{ app()->getLocale() == 'ar' ? 'عربي' : ucfirst(app()->getLocale()) }}</span> <span class="caret"><img
                            src="{{ url('assets/site/images/caret.svg') }}" alt=""></span>
                </a>
                <ul class="dropdown_menu">
                    <li><a href="{{ changeLocaleUrl('en') }}">EN</a></li>
                    <li><a href="{{ changeLocaleUrl('ar') }}" class="ar">عربي</a></li>
                </ul>
                <!--
                <ul class="dropdown_menu">
                    <li><a href="#">EN</a></li>
                    <li><a href="#" class="ar">AR</a></li>
                </ul>
                -->
            </div>
            <div class="user">
                @if (auth()->check())
                <div class="dropdown">
                    <a href="#">
                        <span><span class="text">{{ auth()->user()->first_name }}</span><span class="caret"><img src="{{ url('assets/site/images/user.svg') }}"
                                    alt=""></span></span>
                    </a>
                    <ul class="dropdown_menu">
                        <li><a href="{{ route('user_order.all_orders', [$locale]) }}">{{ __("user.my_orders", [], $locale) }}</a></li>
                        <li><a href="{{ route('user_addresses.show', [$locale]) }}">{{ __("user.shipping_addresses", [], $locale) }}</a></li>
                        <li><a href="{{ route('products.wish_list', [$locale]) }}">{{ __("user.wish_list", [], $locale) }}</a></li>
                        <li><a href="{{ route('user_account.view', [$locale]) }}">{{ __("user.account_settings", [], $locale) }}</a></li>
                        <li><a href="{{ url('logout') }}">{{ __("user.logout", [], $locale) }}</a></li>
                    </ul>
                </div>
                @else
                <a href="{{ url('login') }}" class="" style="color: #fff;">{{ __("user.login", [], $locale) }}</a>
                <!--
                        <a href="{{ url('register') }}" class="btn btn-basic" style="padding: 10px; font-size: 14px;">Register</a>
                    -->
                @endif
            </div>
            <div class="store">
                <div class="dropdown">
                    <a href="#">
                        <span>
                            <span class="text">{{ getCurrentBranch()->translate('name', $locale) }}</span>
                            <span class="shop">
                                <img src="{{ url('assets/site/images/store.svg') }}" alt="">
                            </span>
                        </span>
                    </a>
                    <ul class="dropdown_menu" id="btnTest" >
                        @foreach ($headerBranches as $headerBranch)
                        <li>
                            <a  {!! $headerBranch->id == getCurrentBranch()->id ? 'disabled' : ('onclick="changeBranch('
                                . $headerBranch->id . ')" style="cursor: pointer;"') !!}
                                >
                                {{ $headerBranch->translate('name', $locale) }}
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>

            @if (true)
            <div>
                <a href="{{ route('cart.index', [$locale]) }}">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <g id="Icon" transform="translate(-17.2 0)">
                            <path id="Path_14" data-name="Path 14" d="M22.9,13.169H34.759A2.365,2.365,0,0,0,37.2,10.9V6.214a.049.049,0,0,0,0-.025c0-.008,0-.021,0-.029s0-.016,0-.025a.247.247,0,0,1-.009-.029c0-.008,0-.016-.009-.025s0-.016-.009-.025a.193.193,0,0,1-.013-.029.089.089,0,0,0-.013-.021l-.013-.025c0-.008-.009-.012-.013-.021s-.013-.016-.018-.025a.073.073,0,0,0-.018-.021c0-.008-.013-.012-.018-.021s-.013-.012-.018-.021-.013-.012-.018-.016L37,5.84c-.009,0-.013-.012-.022-.016s-.018-.012-.027-.016l-.022-.012a.093.093,0,0,1-.027-.016l-.027-.012-.027-.012-.027-.012c-.009,0-.018,0-.027-.008a.087.087,0,0,0-.031-.008.2.2,0,0,1-.022,0,.1.1,0,0,0-.035,0s-.009,0-.018,0L21.657,3.785V1.836a.235.235,0,0,0,0-.058.029.029,0,0,0,0-.016c0-.012,0-.025,0-.037s0-.021-.009-.033,0-.012,0-.021l-.013-.037s0-.012,0-.016a.12.12,0,0,0-.018-.033s0-.012-.009-.016a.086.086,0,0,0-.018-.025c0-.008-.009-.012-.013-.021s-.009-.012-.013-.021-.013-.016-.018-.025l-.013-.012-.027-.025-.013-.012a.189.189,0,0,0-.031-.025s-.013-.008-.018-.012-.018-.012-.027-.021-.027-.016-.035-.021-.009,0-.013-.008l-.058-.025L18.03.044a.614.614,0,0,0-.783.3.541.541,0,0,0,.318.728L20.459,2.2V14.361a2.336,2.336,0,0,0,2.128,2.248,2.124,2.124,0,0,0-.345,1.151,2.417,2.417,0,0,0,4.821,0,2.081,2.081,0,0,0-.332-1.13H32.1a2.092,2.092,0,0,0-.332,1.13,2.417,2.417,0,0,0,4.821,0,2.336,2.336,0,0,0-2.411-2.24H22.9a1.207,1.207,0,0,1-1.247-1.159v-1.5A2.614,2.614,0,0,0,22.9,13.169Zm2.972,4.587a1.22,1.22,0,1,1-1.216-1.13A1.178,1.178,0,0,1,25.873,17.756Zm9.527,0a1.22,1.22,0,1,1-1.216-1.13A1.178,1.178,0,0,1,35.4,17.756Zm-.641-5.7H22.9A1.207,1.207,0,0,1,21.653,10.9v-6L36.006,6.74V10.9A1.208,1.208,0,0,1,34.759,12.059Z"
                                transform="translate(0 0)" />
                        </g>
                    </svg>
                </a>
                <span class="cart_number">0</span>
            </div>
            @endif
        </div>
    </div>

    <!-- Modal -->


    <!-- container -->
</header>



