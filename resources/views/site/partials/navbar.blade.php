<nav class="main_nav">
    <div class="container">
        <ul>
            <li>
                <button type="button" class="nav_toggle">
                    <img src="{{ url('assets/site/images/menu.svg') }}" alt="">
                </button>
            </li>

            @foreach ($headerCategories as $headerCategory)
            <li class="dropdown onhover">
                <a href="{{ route('products.index', [$locale, $headerCategory->id]) }}"><span>{{ $headerCategory->translate('name', $locale) }}</span></a>
                @if (count($headerCategory->children))
                <div class="category-dropdown">
                    <div class="left">
                        <h5>Sub-Categories</h5>
                        <ul>
                            @foreach ($headerCategory->children as $subCat)
                                @if (!empty($subCat->children) && count($subCat->children))
                                    <li><a href="{{ route('products.index', [$locale, $subCat->id]) }}">{{ $subCat->translate('name', $locale) }}</a>
                                        <ul style="padding-left: 25px; padding-right: 25px;">
                                            @foreach ($subCat->children as $grandSubCat)
                                                <li>
                                                    <a href="{{ route('products.index', [$locale, $grandSubCat->id]) }}">
                                                        {{ $grandSubCat->translate('name', $locale) }}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @else
                                    <li><a href="{{ route('products.index', [$locale, $subCat->id]) }}">{{ $subCat->translate('name', $locale) }}</a></li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endif
            </li>
            @endforeach

        </ul>
    </div>
</nav>