<input type="hidden" id="firstVisit" name="firstVisit" value="1" hidden>

<!-- The Branch Modal -->
<div id="branchModal" class="modal">
    @if(app()->getLocale() == "en")
    <div id="modal-en" class="modal-content">
        <span class="close">&times;</span>
        <h4>Welcome to Alfa Market</h4>
        <p>Please select the branch that is closest to your area.</p>
        <div class="form_inline m_t_20">
            <label class="custom_field" for="district_id-en">District</label>
            <div class="custom_field">
                <select class="custom_input test" id="district_id-en" onchange="onDistarctChange('en')">
                    <option selected value="">Choose Distract</option>
                    @foreach ($headerDistricts as $headerDistrict)
                      <option value="{{ $headerDistrict['id'] }}">{{ json_decode($headerDistrict['name'],true)['en'] }}</option>
                    @endforeach
                </select>
            </div>

            <label class="custom_field" for="branch_id-en">Branch</label>
            <div class="custom_field" >
                <select class="custom_input" id="branch_id-en">

                </select>
            </div>
            <label class="custom_field" for="language-en">Language</label>
            <div class="custom_field">
                <select class="custom_input" id="language-en" onchange="changeLanguage(this.value)">
                    <option selected value="en">English</option>
                    <option value="ar">العربية</option>
                </select>
            </div>
            <button onclick="bootstrapSite('en')" type="submit" class="btn btn-success btn-block m_t_20">Start
                Shopping!</button>
        </div>
    </div>
    @else
    <div id="modal-ar" class="modal-content" >
        <span class="close">&times;</span>
        <h4>مرحباً بكم في ألفا ماركت</h4>
        <p>من فضلك قم بإختيار الفرع الأقرب لك.</p>
        <div class="form_inline m_t_20">

            <label class="custom_field" for="district_id-en">اقرب منطقة</label>
            <div class="custom_field">
                <select class="custom_input test" id="district_id-ar" onchange="onDistarctChange('ar')">
                    <option selected value=""> اختر المنطقة</option>
                    @foreach ($headerDistricts as $headerDistrict)
                        <option value="{{ $headerDistrict['id'] }}">{{ json_decode($headerDistrict['name'],true)['ar'] }}</option>
                    @endforeach
                </select>
            </div>

            <label class="custom_field" for="branch_id-ar">الفرع</label>
            <div class="custom_field">
                <select class="custom_input" id="branch_id-ar">
      
                </select>
            </div>
            <label class="custom_field" for="language-ar">اللغة</label>
            <div class="custom_field">
                <select class="custom_input" id="language-ar" onchange="changeLanguage(this.value)">
					<option selected value="ar">العربية</option>
                    <option value="en">English</option>
                </select>
            </div>
            <button onclick="bootstrapSite('ar')" type="submit" class="btn btn-success btn-block m_t_20">إبدأ التسوق!</button>
        </div>
    </div>
    @endif
</div>

<input id="change-locale-url-en" type="hidden" value="{{ changeLocaleUrl('en') }}">
<input id="change-locale-url-ar" type="hidden" value="{{ changeLocaleUrl('ar') }}">


<div id="changeBranchModal" class="modal">
     @if(app()->getLocale() == "en")

        <div id="modal-en" class="modal-content">
        <span class="close">&times;</span>
        <h4>Alfa Market</h4>
            
        <div class="form_inline m_t_20">
            <p>Do you really change branch ? (you cart is empty)</p>

            <button id="change" type="submit" class="btn btn-success btn-block m_t_20">Change</button>
            <button type="button" id="close" class="btn btn-danger btn-block m_t_20" data-dismiss="modal">Close</button>
        </div>



    </div>
    @else
       <div id="modal-ar" class="modal-content" >
        <span class="close">&times;</span>
        <h4> ألفا ماركت</h4>
        <p>هل تريد تغيير الفرع  ؟(عربة التسوق الخاصة بك ستكون فارغة)</p>
        <div class="form_inline m_t_20">
            <button id="change" type="submit" class="btn btn-success btn-block m_t_20">تغيير</button>
            <button type="button" class="btn btn-danger btn-block m_t_20" data-dismiss="modal">اغلاق</button>

        </div>
    </div>
    @endif
</div>



<script>
    function checkFirstVisit() {
        if (!localStorage.noFirstVisit) {
            localStorage.noFirstVisit = 1;
        } else {
            document.getElementById('firstVisit').value = 0;
        }
    }

    function changeLanguage(locale) {
        $('#modal-ar').hide();
        $('#modal-en').hide();
        $('#modal-' + locale).show();
		$('#language-en').val(locale);
		$('#language-ar').val(locale);
    }

    function bootstrapSite(locale) {
        branchId = $('#branch_id-' + locale).val();
        $.ajax({
            url: baseUrl + '/' + locale + '/branches/change-branch/' + branchId,
            type: 'GET',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (result) {
                if (result == 1) {
                    redir_global($('#change-locale-url-' + locale).val());
                }
            }
        });
    }




    function onDistarctChange(locale){

            distractId = $('#district_id-' + locale).val();
            $.ajax({
                url: baseUrl + '/' + locale + '/branches/get-branch/' + distractId,
                type: 'GET',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    $('#branch_id-' + locale).empty();
                    $.each(result, function (key, value) {
                        var myObj = JSON.parse(value.name);
                        if (locale == 'en')
                        {
                            $('#branch_id-' + locale).append($('<option>', {
                                value: value.id,
                                text: myObj.en,
                            }));
                        }else{
                            $('#branch_id-' + locale).append($('<option>', {
                                value: value.id,
                                text: myObj.ar,
                            }));
                        }

                    });

                }
            });
    }


    checkFirstVisit();

</script>
