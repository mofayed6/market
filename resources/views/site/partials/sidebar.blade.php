<aside class="side_nav">
    <nav>
        <ul>
            @foreach ($headerCategories as $headerCategory)
            <li class="{{ count($headerCategory->children) ? 'dropdown slide' : '' }}">
                <a href="{{ count($headerCategory->children) ? route('products.index', [$locale, $headerCategory->id]) : '#' }}">{{
                    $headerCategory->translate('name', $locale) }}</a>
                @if (count($headerCategory->children))
                <ul class="dropdown_menu">
                    @foreach ($headerCategory->children as $subCat)
                    <li><a href="{{ route('products.index', [$locale, $subCat->id]) }}">{{ $subCat->translate('name', $locale) }}</a></li>
                    @endforeach
                </ul>
                @endif
            </li>
            @endforeach
        </ul>
    </nav>
</aside>
