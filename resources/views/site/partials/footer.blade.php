<div id="footer">
    <footer>
        <div class="container">
            <div class="columns">
                <div class="column">
                    <h3>About Alfa Market</h3>
                    <ul>
                        <li>
                            <a href="{{ route('pages', [$locale, 'about']) }}">About Us</a>
                        </li>
                        <li>
                            <a href="mailto:{{ config('site_info.email') }}">Contact Us</a>
                        </li>
                        <li>
                            <a href="{{ route('pages', [$locale, 'return_policy']) }}">Return Policy</a>
                        </li>
                        <li>
                            <a href="{{ route('pages', [$locale, 'terms']) }}">Terms & Conditions</a>
                        </li>
                        <li>
                            <a href="{{ route('pages', [$locale, 'privacy']) }}">Privacy Policy</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="bottom_section">
                <div class="language">
                    <div class="dropdown">
                        <a href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16">
                                <g id="earth-globe" transform="translate(0)">
                                    <path id="Path_10" data-name="Path 10" class="cls-1" d="M24.656,13.344A7.994,7.994,0,1,0,27,19,7.941,7.941,0,0,0,24.656,13.344Zm1.62,5.3h-3.1a13.891,13.891,0,0,0-.506-3.478,7.934,7.934,0,0,0,1.649-1.136A7.265,7.265,0,0,1,26.275,18.641Zm-6.916.718h3.1a13.131,13.131,0,0,1-.454,3.2,8.076,8.076,0,0,0-2.645-.575ZM23.8,13.527a7.275,7.275,0,0,1-1.358.944,8.341,8.341,0,0,0-.421-.98,5.756,5.756,0,0,0-1.051-1.5A7.252,7.252,0,0,1,23.8,13.527Zm-4.441-1.7a3.385,3.385,0,0,1,2.021,1.979,8.161,8.161,0,0,1,.411.97,7.328,7.328,0,0,1-2.433.545Zm2.648,3.638a13.133,13.133,0,0,1,.447,3.177h-3.1v-2.6a8.08,8.08,0,0,0,2.648-.578Zm-3.367,3.174h-3.1a13.133,13.133,0,0,1,.447-3.177,7.931,7.931,0,0,0,2.648.575v2.6Zm0-6.811v3.494a7.31,7.31,0,0,1-2.433-.545,8.161,8.161,0,0,1,.411-.97,3.385,3.385,0,0,1,2.021-1.979Zm-1.61.16a5.811,5.811,0,0,0-1.051,1.5,9.605,9.605,0,0,0-.421.98,7.383,7.383,0,0,1-1.358-.944A7.252,7.252,0,0,1,17.034,11.989Zm-3.347,2.038a8.061,8.061,0,0,0,1.649,1.136,13.764,13.764,0,0,0-.506,3.478h-3.1A7.265,7.265,0,0,1,13.687,14.027Zm-1.959,5.332h3.1a13.873,13.873,0,0,0,.513,3.5,7.862,7.862,0,0,0-1.642,1.13A7.254,7.254,0,0,1,11.728,19.359Zm2.488,5.127a7.3,7.3,0,0,1,1.352-.937,8.849,8.849,0,0,0,.415.96,5.833,5.833,0,0,0,1.051,1.505A7.288,7.288,0,0,1,14.216,24.486Zm4.428,1.685a3.385,3.385,0,0,1-2.021-1.979,8.393,8.393,0,0,1-.4-.95,7.207,7.207,0,0,1,2.426-.542v3.471ZM16,22.556a13.131,13.131,0,0,1-.454-3.2h3.1v2.622A7.913,7.913,0,0,0,16,22.556Zm3.363,3.615V22.7a7.287,7.287,0,0,1,2.426.542,8.393,8.393,0,0,1-.4.95A3.385,3.385,0,0,1,19.362,26.171Zm1.61-.16a5.776,5.776,0,0,0,1.051-1.505,8.849,8.849,0,0,0,.415-.96,7.3,7.3,0,0,1,1.352.937A7.2,7.2,0,0,1,20.972,26.011Zm3.337-2.024a7.988,7.988,0,0,0-1.642-1.13,13.9,13.9,0,0,0,.513-3.5h3.1a7.288,7.288,0,0,1-1.972,4.627Z"
                                        transform="translate(-11 -11)" />
                                </g>
                            </svg>

                            <span>{{ app()->getLocale() == 'ar' ? 'عربي' : ucfirst(app()->getLocale()) }}</span> 
                            <span class="caret">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3.5 2">
                                    <g id="caret-arrow-up" transform="translate(3.5 2) rotate(180)">
                                        <path id="Path_11" data-name="Path 11" class="cls-1" d="M3.435,1.622,1.9.066a.212.212,0,0,0-.308,0L.065,1.622a.22.22,0,0,0,0,.312A.209.209,0,0,0,.219,2H3.281a.209.209,0,0,0,.154-.066.22.22,0,0,0,0-.312Z" />
                                    </g>
                                </svg>
                            </span>
                        </a>
                        <ul class="dropdown_menu top">
                            <li><a href="{{ changeLocaleUrl('en') }}">EN</a></li>
                            <li><a href="{{ changeLocaleUrl('ar') }}" class="ar">عربي</a></li>
                        </ul>
                    </div>
                </div>
                <div class="social_media">
                    <span>Follow Us</span>
                    <ul>
                        <li>
                            <a href="{{ config('site_info.facebook_url') }}">
                                <img src="{{ url('assets/site/images/facebook.svg') }}" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="{{ config('site_info.twitter_url') }}">
                                <img src="{{ url('assets/site/images/twitter.svg') }}" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="{{ config('site_info.instagram_url') }}">
                                <img src="{{ url('assets/site/images/instagram.svg') }}" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="{{ config('site_info.youtube_url') }}">
                                <img src="{{ url('assets/site/images/youtube.svg') }}" alt="">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <div class="copyright">
        <div class="container">
            <p>© {{ date('Y') }} {{ config('site_info.title_' . app()->getLocale()) }}. All Rights Reserved.</p>
        </div>
    </div>
</div>