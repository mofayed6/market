@extends('site.layouts.main')

@section('page_content')
<div class="container">
    <div class="main_with_sidebar right">
        <aside>
            <div class="title flex vcenter m_b_10">
                <h4 class="no_margin">{{ __('order.shopping_cart', [], $locale) }}</h4>
                <a href="{{ route('cart.index', [$locale]) }}" class="colored right">{{ __('order.back_to_cart', [], $locale) }}</a>

                @include('site.partials.success_failure_messages')

            </div>
            <div class="block">
                <div class="cart">
                    <div class="items">
                        <ul>
                            @foreach ($cart->cartProducts as $cartProduct)
                            <li>
                                <div class="img">
                                    <img src="{{ $cartProduct->product->getPictureUrl() }}" alt="">
                                </div>
                                <div class="details">
                                    <h6>{{ $cartProduct->product->translate('name', $locale) }}</h6>
                                    <strong class="colored">{{ number_format($cartProduct->product->getPrice()) }} {{ __('general.egp', [], $locale) }}</strong>
                                    <p class="small grey">{{ __('order.qty', [], $locale) }}: {{ $cartProduct->quantity }}</p>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="price">
                        <dl>
                            <dt>{{ __('order.item_s', [], $locale) }}:</dt>
                            <dd>
                                <span>{{ number_format($cart->totalProductsCost()) }}</span>
                                {{ __('general.egp', [], $locale) }}
                            </dd>
                            
                            <dt>+ {{ __('order.shipping', [], $locale) }}:</dt>
                            <dd>
                                <span class="order-delivery-cost">
                                    {{ number_format($deliveryCost) }}
                                </span> {{ __('general.egp', [], $locale) }}
                            </dd>
                        </dl>
                    </div>
                    <div class="total">
                        <dl>
                            <dt><strong>{{ __('order.grand_total', [], $locale) }}</strong></dt>
                            <dd>
                                <strong>
                                    <span class="order-grand-final-total">{{ number_format($deliveryCost + $cart->totalProductsCost()) }}</span> 
                                    {{ __('general.egp', [], $locale) }}
                                    </strong>
                                </dd>
                        </dl>
                        <!-- <p class="small grey">Order total includes any applicable VAT</p> -->
                    </div>
                </div>
            </div>
            <button onclick="submitForm('checkout-form')" type="button" class="btn btn-success btn-block shadow_hover m_t_20">
                {{ __('order.place_order', [], $locale) }}
            </button>
        </aside>
        <main>
            <form method="POST" id="checkout-form" action="{{ route('user_order.store', [$locale]) }}">
                @csrf

                <div class="radio m_t_0">
                    <input onchange="addressTypeChange(this)" type="radio" name="address_type" value="saved_address"
                        class="address-type-input" id="choose_saved_address" {!! setChecked('saved_address',
                        old('address_type', 'saved_address' )) !!}>
                    <label for="choose_saved_address">{{ __('order.ship_to_my_saved_delivery_address', [], $locale) }}</label>
                </div>
                <div class="radio">
                    <input onchange="addressTypeChange(this)" type="radio" name="address_type" value="new_address"
                        class="address-type-input" id="choose_add_address" {!! setChecked('new_address',
                        old('address_type', 'saved_address' )) !!}>
                    <label for="choose_add_address">{{ __('order.add_new_address', [], $locale) }}</label>
                </div>
                <div class="title flex vcenter m_b_10">
                    <h4 class="no_margin">{{ __('order.delivery_options', [], $locale) }}</h4>
                </div>
                <section id="saved_addresses" class="inner_section white m_t_0" {!! old('address_type', 'saved_address'
                    ) !='saved_address' ? 'style="display: none;"' : '' !!}>
                    <div class="addresses clearfix">
                        <ul>
                            @foreach ($user_addresses as $user_address)
                            <li>
                                <div class="radio">
                                    <input onclick="addDeliveryCostByAddress(this.value)" value="{{ $user_address->id }}"
                                        id="address-{{ $user_address->id }}" type="radio" class="user-address-item"
                                        name="address_id"
                                        {{ setChecked(old('address_id', $user_addresses->where('is_default', 1)->first()->id), $user_address->id) }}>
                                    <label for="address-{{ $user_address->id }}">
                                        <strong>{{ $user_address->country->translate('name', $locale) . ' - ' .
                                            $user_address->city->translate('name', $locale) . ' - ' .
                                            $user_address->district->translate('name', $locale) }}</strong>
                                        <span class="small space_l grey">
                                            {{ $user_address->is_default ? '(' . __('general.primary', [], $locale) . ')' : ''}}
                                        </span>
                                    </label>
                                </div>
                                <p class="grey">
                                    {{ $user_address->address }}<br>
                                    {{ $user_address->apartment_number }}<br>
                                    {{ $user_address->building_number }}<br>
                                    {{ $user_address->phone_number }}
                                </p>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </section>

                <section id="add_address" class="inner_section white" {!! old('address_type', 'saved_address' ) !='new_address'
                    ? 'style="display: none;"' : '' !!}>
                    <div>
                        <h5 class="m_t_0">{{ __("user.shipping_addresses", [], $locale) }}</h5>
                        <div class="mutli_field m_b_20">
                            <div class="label_top">
                                <div class="custom_field">
                                    <label for="country_id">{{ __("user.country", [], $locale) }}</label>
                                    <select class="custom_input" name="country_id" id="country_id" onchange="countryChange(this.value, 'city_id', 'district_id')">
                                        @foreach ($countries as $country)
                                        <option {{ setSelected($country->id, $defaultCountry->id) }} value="{{ $country->id }}">{{
                                            $country->translate('name', $locale) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="label_top">
                                <div class="custom_field">
                                    <label for="city">{{ __("user.city", [], $locale) }}</label>
                                    <select class="custom_input" name="city_id" id="city_id" onchange="cityChange(this.value, 'district_id')">
                                        @foreach ($defaultCountry->children as $city)
                                        <option {{ setSelected($city->id, $defaultCity->id) }} value="{{ $city->id }}">{{
                                            $city->translate('name', $locale) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="label_top">
                                <div class="custom_field">
                                    <label for="district_id">{{ __("user.district", [], $locale) }}</label>
                                    <select class="custom_input" name="district_id" id="district_id">
                                        @foreach ($defaultCity->children as $district)
                                        <option {{ setSelected($district->id, $defaultDistrict->id) }} value="{{ $district->id }}">{{
                                            $district->translate('name', $locale) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="label_top">
                            <div class="custom_field">
                                <label for="address">{{ __("user.address", [], $locale) }}</label>
                                <input type="text" class="custom_input" name="address" id="address">
                            </div>
                        </div>
                        <div class="mutli_field m_b_20 m_t_20">
                            <div class="label_top">
                                <div class="custom_field">
                                    <label for="building_number">{{ __("user.building_number", [], $locale) }}</label>
                                    <input type="text" class="custom_input" name="building_number" id="building_number">
                                </div>
                            </div>
                            <div class="label_top">
                                <div class="custom_field">
                                    <label for="apartment_number">{{ __("user.apartment_number", [], $locale) }}</label>
                                    <input type="text" class="custom_input" name="apartment_number" id="apartment_number">
                                </div>
                            </div>
                        </div>
                        <div class="label_top">
                            <div class="custom_field">
                                <label for="phone_number">{{ __("user.phone_number", [], $locale) }}</label>
                                <input type="text" class="custom_input" name="phone_number" id="phone_number">
                            </div>
                        </div>

                        <h4>{{ __("user.map_pin_location", [], $locale) }}</h4>
                        
                        <div id="map" style="width:100%; height:400px;" class="m_t_30"></div>
                        <input type="hidden" name="lat" id="lat">
                        <input type="hidden" name="lng" id="lng">
                    </div>
                </section>
                <div class="title flex vcenter m_b_10">
                    <h4 class="no_margin">{{ __('order.payment_method', [], $locale) }}</h4>
                </div>
                <section class="inner_section white">
                    <p class="hint grey_background">
                        {{ __('order.how_would_you_like_to_pay', [], $locale) }}
                        <strong>
                            <span class="order-grand-final-total">{{ number_format($deliveryCost +
                                $cart->totalProductsCost()) }}</span>
                            {{ __('general.egp', [], $locale) }}
                        </strong>
                    </p>
                    <div class="radio">
                        <input type="radio" name="payment" value="cash_on_delivery" id="cash_on_delivery"
                            {{ setChecked(old('payment', 'cash_on_delivery'), 'cash_on_delivery') }}>
                        <label for="cash_on_delivery"><strong>{{ __('order.cash_on_delivery', [], $locale) }}</strong></label>
                    </div>
                    <div class="radio">
                        <input type="radio" name="payment" value="credit_on_delivery" id="credit_on_delivery"
                            {{ setChecked(old('payment', 'cash_on_delivery'), 'credit_on_delivery') }}>
                        <label for="credit_on_delivery"><strong>{{ __('order.credit_on_delivery', [], $locale) }}</strong></label>
                    </div>
                </section>

                <div class="title flex vcenter m_b_8">
                    <h4 class="no_margin">{{ __('order.Purchase_type', [], $locale) }}</h4>
                </div>
                <section class="inner_section ">
                    <div class="radio">
                        <input type="radio" name="type" value="1" id="store" {{ setChecked(old('type', '1'), '1') }}>
                        <label for="store"><strong>{{ __('order.store', [], $locale) }}</strong></label>
                    </div>
                    <div class="radio">
                        <input type="radio" name="type" value="0" id="delivery"
                            {{ setChecked(old('type', '0'), '0') }}>
                        <label for="delivery"><strong>{{ __('order.delivery', [], $locale) }}</strong></label>
                    </div>

                </section>

                <section class="inner_section " id="dateTime" style="display: none">
                    <div class="title flex vcenter m_b_8">
                        <h4 class="no_margin">{{ __('order.Date-Recived', [], $locale) }}</h4>
                    </div>
                    <div class='input-group '>
 
                        <label>start time</label>
                        <input id="scrollDefaultExample"  name="dateDeliveryFrom" type="text" class="time ui-timepicker-input form-control" >

                        
                        <label>end time</label>
                        <input id="scrollDefault" name="dateDeliveryTo" type="text" class="time ui-timepicker-input form-control" >


                        

                    </div>

                </section>


            </form>
        </main>
    </div>
</div>
<input id="total-products-cost" type="hidden" value="{{ $cart->totalProductsCost() }}">

@endsection

@section('page_scripts')
@if (old('address_id'))
<script>
    window.onload = function () {
        adrsId = {{ old('address_id') }};
        addDeliveryCostByAddress(adrsId);
    };

</script>
@endif
<script>

    $(document).ready(function () {

        $('#scrollDefaultExample').timepicker({
            timeFormat: 'h:i a',
            interval: 60,
            minTime: new Date(),
            maxTime: '00:00am',
            startTime: new Date(),
            dynamic: false,
            dropdown: true,
            scrollbar: true,
            pick12HourFormat: false ,
        });


        $("#scrollDefaultExample").on('change', function(){

            $('#scrollDefault').val('');

            var time = $(this).val();
            var getTime = time.split(":"); //split time by colon
            var hours = parseInt(getTime[0])+1; //add one hours
             //set new time
            var newTime = hours+":"+getTime[1];
           //set time picker
                $('#scrollDefault').timepicker({
                    timeFormat: 'h:i a',
                    interval: 60,
                    minTime: newTime,
                    maxTime: '00:00am',

                });

        });







            $('input:radio[name="type"]').change(function () {
            if ($(this).is(':checked') && $(this).val() == '1') {

                $('#dateTime').show();

               //  $('input[name="daterange"]').daterangepicker({
               //      timePicker: true,
               //      timePicker24Hour: true,
               // //s     timePickerIncrement: 30,
               //      locale: {
               //          format: 'hh:mm'
               //      }
               //  });

            } else {
                $('#dateTime').hide();

            }
        });

    });


    function addressTypeChange(chosenInput) {
        $('.address-type-input').prop('checked', false);
        $(chosenInput).prop('checked', true);
        if (chosenInput.value == 'saved_address') {
            $('#add_address').hide();
            $('#saved_addresses').show();
        } else if (chosenInput.value == 'new_address') {
            $('#saved_addresses').hide();
            $('#add_address').show();
        }
    }

    function addDeliveryCostByLatLng(lat, lng) {
        $.ajax({
            url: baseUrl + '/delivery-cost-lng-lat/' + lat + '/' + lng,
            type: 'GET',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (deliveryCost) {
                if (deliveryCost) {
                    deliveryCost = parseFloat(deliveryCost);
                    totalCost = number_format(parseFloat($('#total-products-cost').val()) + deliveryCost, 2)
                        .replace(/\.00$/, '');
                    $('.order-delivery-cost').text(number_format(deliveryCost).replace(/\.00$/, ''));
                    $('.order-grand-final-total').text(totalCost);
                }
            }
        });
    }

    function addDeliveryCostByAddress(adressId) {
        $.ajax({
            url: baseUrl + '/delivery-cost-address/' + adressId,
            type: 'GET',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (deliveryCost) {
                if (deliveryCost) {
                    deliveryCost = parseFloat(deliveryCost);
                    totalCost = number_format(parseFloat($('#total-products-cost').val()) + deliveryCost, 2)
                        .replace(/\.00$/, '');
                    $('.order-delivery-cost').text(number_format(deliveryCost).replace(/\.00$/, ''));
                    $('.order-grand-final-total').text(totalCost);
                }
            }
        });
    }

</script>

<script type="text/javascript">
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: -33.8688,
                lng: 151.2195
            },
            zoom: 13
        });

        var input = document.getElementById('pac-input');

        var autocomplete = new google.maps.places.Autocomplete(
            input, {
                placeIdOnly: true
            });
        autocomplete.bindTo('bounds', map);

        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var geocoder = new google.maps.Geocoder;
        var marker = new google.maps.Marker({
            map: map
        });
        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });

        autocomplete.addListener('place_changed', function () {
            infowindow.close();
            var place = autocomplete.getPlace();

            if (!place.place_id) {
                return;
            }
            geocoder.geocode({
                'placeId': place.place_id
            }, function (results, status) {
                //console.log(results);    
                if (status !== 'OK') {
                    window.alert('Geocoder failed due to: ' + status);
                    return;
                }
                map.setZoom(11);
                console.log(results[0].geometry.location.lat());
                console.log(results[0].geometry.location.lng());
                map.setCenter(results[0].geometry.location);
                // Set the position of the marker using the place ID and location.
                marker.setPlace({
                    placeId: place.place_id,
                    location: results[0].geometry.location
                });
                marker.setVisible(true);

                $('#address').val(results[0].formatted_address);
                $('#lat').val(results[0].geometry.location.lat());
                $('#lng').val(results[0].geometry.location.lng());

                addDeliveryCostByLatLng($('#lat').val(), $('#lng').val());

                infowindowContent.children['place-name'].textContent = place.name;
                infowindowContent.children['place-id'].textContent = place.place_id;
                infowindowContent.children['place-address'].textContent =
                    results[0].formatted_address;
                infowindow.open(map, marker);
            });
        });
    }

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAoacCf8RDzMIaHcI5Ywh9zQ-kZt1-V_fc&libraries=places&callback=initMap"
    async defer></script>

@endsection
