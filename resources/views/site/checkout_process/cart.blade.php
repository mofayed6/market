@extends('site.layouts.main') 

@section('page_content')
<div class="container">
    <div class="main_with_sidebar right">
    <aside>
        <div class="title flex vcenter m_b_10">
            <h4 class="no_margin">{{ __('order.total', [], $locale) }} <strong>{{$total}}</strong> <small class="small">{{ __('general.egp', [], $locale) }}</small></h4>
        </div>
        <a href="{{ route('user_order.checkout', [$locale]) }}" {{ count($items) == 0 ? "disabled":"" }} class="btn btn-success btn-block shadow_hover">{{ __('order.proceed_to_checkout', [], $locale) }}</a>
    </aside>
    <main>
        <div class="title flex vcenter m_b_10">
            <h4 class="no_margin">{{ __('order.shopping_cart', [], $locale) }} (<span>{{count($items)}}</span>)</h4>
        </div>
        @if(count($items)>0)

            @foreach($items as $item)
                <section class="inner_section white">
                    {{Form::open(['action'=>['\Modules\Cart\Http\Controllers\CartController@update',$locale, $item['id']],'method'=>'patch'])}}
                    <div class="shipment_items">
                        <div>
                            <div class="image">
                                @if (Auth::check() && isset($item->product))
                                <img src="{{$item->product->getPictureUrl()}}" alt="">
                                  @else
                                    <img src="{{getPictureUrl($item['image'])}}" alt="">
                                @endif
                            </div>
                            <div class="details">
                                <div class="title">
                                    @if (Auth::check() && isset($item->product))
                                        <h6><a href="#">{{$item->product->translate('name', $locale)}}</a></h6>
                                    @else
                                        {{--<h6><a href="#">{{$item['name']->translate('name', $locale)}}</a></h6>--}}
                                        <h6><a href="#">{{json_decode($item['name'],true)[$locale]}}</a></h6>
                                    @endif
                                    <div class="quantity custom_field">
                                        <span class="space_r">{{ __('order.qty', [], $locale) }}</span>
                                        @if (Auth::check() && isset($item->quantity))
                                            {{Form::selectRange('quantity',1,20,$item->quantity,['class'=>'custom_input small','onChange'=>'$(this).parent().parent().parent().parent().parent().parent().submit()'])}}
                                        @else
                                            {{Form::selectRange('quantity',1,20,$item['quantity'],['class'=>'custom_input small','onChange'=>'$(this).parent().parent().parent().parent().parent().parent().submit()'])}}
                                        @endif
                                    </div>
                                </div>
                                @if (Auth::check() && isset($item->price))
                                    <p><strong class="colored">{{$item->price}} <small class="small">{{ __('general.egp', [], $locale) }}</small></strong></p>
                                @else
                                    <p><strong class="colored">{{$item['price']}} <small class="small">{{ __('general.egp', [], $locale) }}</small></strong></p>
                                @endif
                                 <dl class="m_t_20">
                                    <dt>Size:</dt>
                                    <dd>XXL</dd>
                                    <dt>Color:</dt>
                                    <dd>Yellow</dd>
                                </dl>
                            </div>
                        </div>
                    </div>

                    <div class="line_separator m_t_20 m_b_20"></div>
                    <div>
                        <button type="submit" name="action" value="delete" class="delete" style="display: none">{{ __('product.delete', [], $locale) }}</button>
                        <button type="submit" name="action" value="save" class="save"  style="display: none">{{ __('product.save_for_later', [], $locale) }}</button>

                        <a href="#" class="colored" onclick="$(this).parent().find('.save').click();">{{ __('product.save_for_later', [], $locale) }}</a>
                        |
                        <a href="#" class="colored" onclick="$(this).parent().find('.delete').click();">{{ __('product.delete', [], $locale) }}</a>
                    </div>
                    {{Form::close()}}
                </section>
            @endforeach
        @else
            <strong>{{ __('general.no_items_here', [], $locale) }}</strong>
        @endif
    </main>
</div>
</div>
@endsection
