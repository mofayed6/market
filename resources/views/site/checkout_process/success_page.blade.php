
@extends('site.layouts.main')

@section('page_content')
<div class="container">
    <div class="breadcrumb">
        <ul class="breadcrumb">
            <li>
                <a href="{{ route('homepage.index', [$locale]) }}">{{ __("general.home", [], $locale) }}</a>
            </li>
            <li>
                <a href="{{ route('user_order.all_orders', [$locale]) }}">{{ __("user.my_orders", [], $locale) }}</a>
            </li>
            <li>
                <span>{{ __("order.order_id", [], $locale) }} #{{ $order->id }}</span>
            </li>
        </ul>
    </div>
    <div class="title">
        <h4 class="colored m_t_0">{{ __("order.order_details", [], $locale) }}</h4>
    </div>
    <section class="inner_section white">
        <p>
            <span class="space_r">{{ __("order.order", [], $locale) }} <strong>#{{ $order->id }}</strong></span>
            <span>{{ __("order.order_placed_on", [], $locale) }}: {{ date('Y-m-d (h:i a)', strtotime($order->created_at)) }}</span>
        </p>
        <div class="history_info">
            <div>
                <h5>{{ __("order.payment_method", [], $locale) }}</h5>
                <p>{{ __("order.{$order->payment}", [], $locale) }}</p>
            </div>
            {{-- <div>
                <h5>Receipient</h5>
                <p>{{ auth()->user()->getFullName() }}</p>
            </div> --}}
            <div>
                <h5>{{ __("order.order_summary", [], $locale) }}</h5>
                <p><strong>{{ __("order.item_s", [], $locale) }}: {{ number_format($order->totalProductsCost()) }} {{ __('general.egp', [], $locale) }}</strong></p>
                <p><strong>{{ __("order.grand_total", [], $locale) }}: {{ number_format($order->total_cost) }} {{ __('general.egp', [], $locale) }}</strong></p>
            </div>
        </div>
    </section>
    <section class="inner_section white">
        <div class="shipment_details">
            <div>
                <span class="colored">{{ \Modules\Orders\Models\OrderStatus::statusName($order->status->id) }}</span>
            </div>
        </div>
        <div class="shipment_items">

            @foreach ($order->orderProducts as $orderProduct)
            <div>
                <div class="image">
                    <img src="{{ $orderProduct->product->getPictureUrl() }}" alt="">
                </div>
                <div class="details">
                    <div class="title">
                        <h6>
                            <a href="{{ route('products.view', [$locale, $orderProduct->product_id]) }}" class="colored">
                                {{ $orderProduct->product->translate('name', $locale) }}
                            </a>
                        </h6>
                        <span class="price"><strong>{{ number_format($orderProduct->getSubTotalAttribute()) }} {{ __('general.egp', [], $locale) }}</strong></span>
                    </div>
                    <p>{{ __("order.qty", [], $locale) }}: {{ number_format($orderProduct->quantity) }}</p>
                </div>
            </div>
            @endforeach

        </div>
        <div class="shipment_price">
            <dl>
                <dt>{{ __("order.item_s", [], $locale) }}:</dt>
                <dd>{{ number_format($order->totalProductsCost()) }} {{ __('general.egp', [], $locale) }}</dd>
                <dt>{{ __("order.shipping", [], $locale) }}:</dt>
                <dd>{{ number_format($order->total_cost - $order->totalProductsCost()) }} {{ __('general.egp', [], $locale) }}</dd>
                <dt>{{ __("order.grand_total", [], $locale) }}:</dt>
                <dd><strong>{{ number_format($order->total_cost) }} {{ __('general.egp', [], $locale) }}</strong></dd>
            </dl>
        </div>
    </section>
</div>
@endsection
