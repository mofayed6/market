<aside class="aside-filters">
    @if (count($products))

    <button type="button" class="btn icon_circle toggle_filter"><i class="mIcon"></i></button>
    <div class="filter_results">
        <a href="#" class="close"><i class="mIcon"></i></a>

        <form method="GET" action="?">
        @if (!empty($keywords))
            <input type="hidden" name="keywords" value="{{ $keywords }}">
        @endif
        @foreach ($attributes as $attribute)
            <div class="block">
                <h4>{{ $attribute->translate('name', $locale) }}</h4>
                <!-- <input type="text" class="custom_input" placeholder="Search Brands"> -->
                <div class="choices">

                    @foreach ($attributes_values->where('attribute_id', $attribute->id)->unique('value') as $attributeValue)
                        <div class="checkbox">
                            <input type="checkbox" id="Samsung" name="attribute[{{ $attribute->id }}][]" value="{{ $attributeValue->translate('value', $locale) }}" {{ (!empty($attributesQuery[$attribute->id]) && in_array($attributeValue->translate('value', $locale), $attributesQuery[$attribute->id])) ? 'checked' : '' }}>
                            <label for="Samsung">{{ $attributeValue->translate('value', $locale) }}</label>
                            <div class="ch-count">({{ $attributes_values->where('attribute_id', $attribute->id)->count() }})</div>
                        </div>
                    @endforeach

                </div>
                <!--
                    @todo
                <a class="see_filter" href="#">See all</a>
                -->
            </div>
        @endforeach


        <div class="block Price">
            <h4>Price</h4>
            <div class="range-slider label_top">
                <div class="mutli_field">
                    <div class="custom_field">
                        <!-- <label>From ({{ __('general.egp', [], $locale) }})</label> -->
                    <input type="text" class="js-input-from custom_input" name="price_from" />
                    </div>
                    <div class="custom_field">
                        <!-- <label>To ({{ __('general.egp', [], $locale) }})</label> -->
                        <input type="text" class="js-input-to custom_input" name="price_to" />
                    </div>
                </div>
                <input type="text" class="js-range-slider" data-from="{{ !empty($old_price_from) ? $old_price_from : $min_price }}" data-to="{{ !empty($old_price_to) ? $old_price_to : $max_price }}" data-min="{{ $min_price }}" data-max="{{ $max_price }}">
            </div>
            <div class="flex vcenter m_t_20">
                <button type="reset" class="btn btn-primary small regular space_r_10">Clear</button>
                <button type="submit" class="btn btn-primary  regular small">Apply</button>
            </div>
        </div>

        </form>
    </div>
    @endif

</aside>