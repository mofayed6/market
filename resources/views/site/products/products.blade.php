@extends('site.layouts.main')
@section('page_content')
<div class="container">
    <div class="main_with_sidebar">
        @include('site.products.sidebar')

        <main>
            <div class="filters">
                <h2>{{ $pageTitle }} <small>({{ $all_products_count }} {{ __('product.products_found', [], $locale) }})</small></h2>
                @if (count($products))
                <div class="sorting">
                    <span>{{ __('product.sort_by', [], $locale) }}</span>
                    <select onchange="changeProductsOrderBy(this.value)">
                        <option {{ setSelected("{$orderBy}-{$orderDirection}", 'orders_count-desc') }} value="orders_count-desc">{{ __('product.popularity', [], $locale) }}</option>
                        <option {{ setSelected("{$orderBy}-{$orderDirection}", 'actual_price-asc') }} value="actual_price-asc">{{ __('product.price_low_to_high', [], $locale) }}</option>
                        <option {{ setSelected("{$orderBy}-{$orderDirection}", 'actual_price-desc') }} value="actual_price-desc">{{ __('product.price_high_to_low', [], $locale) }}</option>
                    </select>
                </div>
                @endif
            </div>
            @if (!count($products))
            <div class="alert alert-warning" role="alert">
                {{ __('general.no_items_here', [], $locale) }}
            </div>
            @endif
            <div class="products">
                @foreach ($products as $product)
                <div>
                    <!--
                        <div class="badge blue"><span>o</span></div>
                        <div class="badge"><span>g</span></div>
                        -->
                    <figure>
                        <a href="{{ route('products.view', [$locale, $product->id]) }}">
                            <img src="{{ $product->getPictureUrl() }}" alt="">
                        </a>
                    </figure>
                    <h3>
                        <a href="{{ route('products.view', [$locale, $product->id]) }}">
                            {{ $product->translate('name', $locale) }}
                        </a>
                    </h3>
                    <div class="price">
                        @if (!empty($product->discount_price))
                            <p class="before"><span>{{ number_format($product->price) }} {{ __('general.egp', [], $locale) }}</span> <span>{{
                                    number_format((($product->price - $product->discount_price) / $product->price) * 100)
                                    }}% {{ __('product.off', [], $locale) }}</span></p>
                            <p class="after"><span>{{ number_format($product->discount_price) }}</span> {{ __('general.egp', [], $locale) }}
                            <img onclick="addToCart({{ $product->id }})" class="add-to-cart-img" src="{{ url('assets/site/images/add-to-cart.svg') }}"
                                        alt="{{ __('order.add_to_cart', [], $locale) }}" /></p>
                        @else
                        <p class="before"><span style="padding: 0px;"></span></p>
                            <p class="after">
                                <span>{{ number_format($product->price) }}</span> 
                                {{ __('general.egp', [], $locale) }}
                                <img 
                                    onclick="addToCart({{ $product->id }})" 
                                    class="add-to-cart-img" 
                                    src="{{ url('assets/site/images/add-to-cart.svg') }}"
                                    alt="{{ __('order.add_to_cart', [], $locale) }}"
                                >
                            </p>
                        @endif
                    </div>
                </div>
                @endforeach


            </div>

            <!-- @todo -->
            {{ $products->links() }}
        </main>
    </div>
</div>
@endsection


@section('page_scripts')
<script>
    function changeProductsOrderBy(orderBy) {
        redir_global(changeOrAddQueryParameter('order_by', orderBy));
    }

</script>
@endsection
