@extends('site.layouts.main')

@section('page_content')
<div class="container">
    <div class="breadcrumb">
        <ul class="breadcrumb">
            @foreach ($grandParentCategories as $grandParentCategoryId => $grandParentCategoryName)
            <li>
                <a href="{{ route('products.index', [$locale, $grandParentCategoryId]) }}">
                {{ json_decode($grandParentCategoryName, true)[$locale] }}</a>
            </li>
            @endforeach
            <li>
                <span>{{ $product->category->translate('name', $locale) }}</span>
            </li>
        </ul>
    </div>
    <section class="product_details inner_section white">
        <div class="left">
            <div class="slider_for">

                <div>
                    <a href="{{ $product->getPictureUrl() }}" class="gallery-item">
                        <img src="{{ $product->getPictureUrl() }}" alt="">
                    </a>
                </div>

                @if($product->images)
                    @foreach ($product->images as $image)
                    <div>
                        <a href="{{ $image->getPictureUrl() }}" class="gallery-item">
                            <img src="{{ $image->getPictureUrl() }}" alt="">
                        </a>
                    </div>
                    @endforeach
                @endif

            </div>
            <div class="slider_nav">

                <img width="60" height="60" src="{{ $product->getPictureUrl() }}" alt="">

                @if($product->images)
                    @foreach ($product->images as $image)
                        <img width="60" height="60" src="{{ $image->getPictureUrl() }}" alt="">
                    @endforeach
                @endif

            </div>
        </div>
        <div class="center">
            <h3 class="m_t_0">{{ $product->translate('name', $locale) }}</h3>
            <!--
        <div class="size options">
            <p>Size</p>
            <a href="#">32”</a>
            <a href="#">43”</a>
            <a href="#">58”</a>
        </div>
        <div class="color options">
            <p>Color</p>
            <a href="#">32”</a>
            <a href="#">43”</a>
            <a href="#">58”</a>
        </div>
    -->
            <div class="other_details">
                <div class="price">
                    @if (!empty($product->discount_price))
                        <p class="after">{{ __('general.egp', [], $locale) }} {{ number_format($product->price) }}</p>
                        <p class="before"><span class="price_value">{{ __('general.egp', [], $locale) }} {{ number_format($product->discount_price) }}</span>
                            <span class="discount_value">
                                -{{ number_format((($product->price - $product->discount_price) / $product->price) * 100) }}%
                            </span>
                        </p>
                    @else
                        <p class="after">{{ __('general.egp', [], $locale) }} {{ number_format($product->price) }}</p>
                    @endif
                </div>
                <div class="actions">
                    <button onclick="addToCart({{ $product->id }})" type="button" class="add">{{ __('order.add_to_cart', [], $locale) }}</button>
                    @if (auth()->check())
                        @if (!empty($favorite))
                            <button style="display: none;" type="button" onclick="productFavorite({{ $product->id }})" id="product-favorite-{{ $product->id }}" class="product-favorite save">
                                <i class="mIcon"></i> {{ __('product.save_for_later', [], $locale) }}
                            </button>
                            <button type="button" onclick="productUnfavorite({{ $product->id }})" id="product-unfavorite-{{ $product->id }}" class="unsave">
                                <i class="mIcon"></i> {{ __('product.remove_from_wish_list', [], $locale) }}
                            </button>
                        @else
                            <button type="button" onclick="productFavorite({{ $product->id }})" id="product-favorite-{{ $product->id }}" class="save">
                                <i class="mIcon"></i> {{ __('product.save_for_later', [], $locale) }}
                            </button>
                            <button style="display: none;" type="button" onclick="productUnfavorite({{ $product->id }})" id="product-unfavorite-{{ $product->id }}" class="unsave">
                                <i class="mIcon"></i> {{ __('product.remove_from_wish_list', [], $locale) }}
                            </button>
                        @endif
                    @endif
                </div>
                <!-- <div class="key_features">
                <h3>Key Features</h3>
                <ul>
                    <li>- Screen Size: 32 inch</li>
                    <li>- Panel Backlight: LED</li>
                    <li>- Resolution: HD 1366 x 768</li>
                    <li>- Connectivity: 2 x HDMI, 1 x USB</li>
                    <li>- Watch movies from your USB</li>
                </ul>
            </div> -->
            </div>
            <!-- <div class="share">
            <span>Share</span>
            <div class="social">
                <a href="#"><img src="{{ url('assets/site/images/facebook_share.svg') }}" alt=""></a>
                <a href="#"><img src="{{ url('assets/site/images/twitter_share.svg') }}" alt=""></a>
                <a href="#"><img src="{{ url('assets/site/images/google_share.svg') }}" alt=""></a>
            </div>
        </div> -->
        </div>
        <div class="right">
            <div class="pay_features">
                <div>
                    <img src="{{ url('assets/site/images/notes.svg') }}" alt="">
                    <p>{{ __('order.cash_on_delivery', [], $locale) }}</p>
                </div>
                <div>
                    <img src="{{ url('assets/site/images/shopping-store.svg') }}" alt="">
                    <p>{{ __('order.credit_on_delivery', [], $locale) }}</p>
                </div>
            </div>
        </div>
    </section>

{{-- @include('site.products.bought_together') --}}
    @include('site.products.most_viewed_products')

    <section class="inner_section white">
        <h4 class="m_t_0">{{ __('product.specification', [], $locale) }}</h4>
        <dl>
            @foreach ($product->attributes as $attribute)
                @if (in_array($attribute->id, $attributes_values->pluck('attribute_id')->toArray()))
                    <dt>{{ $attribute->translate('name', $locale) }}</dt>
                    @foreach ($attributes_values->where('attribute_id', $attribute->id)->unique('value') as $attributeValue)
                       <dd>{{ $attributeValue->translate('value', $locale) }}</dd>
                    @endforeach
                @endif
            @endforeach
        </dl>
        <!--
        @todo
    <a href="#" class="red_link small">Read More</a>
    -->
    </section>
    <section class="inner_section white">
        <h4 class="m_t_0">{{ __('product.description', [], $locale) }}</h4>
        <div class="show_more_content">
            <p class="">{!! $product->translate('description', $locale) !!}</p>
            <!--
            @todo
        <a href="#" class="colored show_more">Show More</a>
        -->
        </div>
    </section>

    @include('site.products.trending_products')
</div>
@endsection
