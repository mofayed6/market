<section class="inner_section no_padding">
    <h4>{{ __('product.most_viewed_products', [], $locale) }} {{ __('product.in_this_category', [], $locale) }}</h4>
    <div class="products carousel">

        @foreach ($mostViewedProducts as $mostViewedProduct)
        <div>
            <!--
                    <div class="badge"><span>33</span></div>
                    -->
            <figure>
                <a href="{{ route('products.view', [$locale, $mostViewedProduct->id]) }}">
                    <img src="{{ $mostViewedProduct->getPictureUrl() }}" alt="">
                </a>
            </figure>
            <h3>
                <a href="{{ route('products.view', [$locale, $mostViewedProduct->id]) }}">
                    {{ $mostViewedProduct->translate('name', $locale) }}
                </a>
            </h3>
            <div class="price">
                @if (!empty($mostViewedProduct->discount_price))
                    <p class="before"><span>{{ number_format($mostViewedProduct->price) }} {{ __('general.egp', [], $locale) }}</span> <span>{{
                            number_format((($mostViewedProduct->price - $mostViewedProduct->discount_price) /
                            $mostViewedProduct->price) * 100) }}% {{ __('product.off', [], $locale) }}</span></p>
                    <p class="after"><span>{{ number_format($mostViewedProduct->discount_price) }}</span> {{ __('general.egp', [], $locale) }}
                        <img onclick="addToCart({{ $mostViewedProduct->id }})" class="add-to-cart-img" src="{{ url('assets/site/images/add-to-cart.svg') }}"
                            alt="{{ __('order.add_to_cart', [], $locale) }}">
                    </p>
                @else
                <p class="before"><span style="padding: 0px;"></span></p>
                    <p class="after">
                        <span>{{ number_format($mostViewedProduct->price) }}</span> 
                        {{ __('general.egp', [], $locale) }}
                        <img 
                            onclick="addToCart({{ $mostViewedProduct->id }})" 
                            class="add-to-cart-img" 
                            src="{{ url('assets/site/images/add-to-cart.svg') }}"
                            alt="{{ __('order.add_to_cart', [], $locale) }}"
                        >
                    </p>
                @endif
            </div>
        </div>
        @endforeach

    </div>
</section>
