@extends('site.layouts.main') 

@section('page_content')
<div class="container">
<div class="main_with_sidebar">

    @include('site.user_account.sidebar')

    <main>
        {{-- <div class="title flex vcenter m_b_10">
            <h4 class="no_margin">Mobile Wishlist</h4>
            <div class="button_group right">
                <button type="button" class="btn btn-edit btn-icon"><i class="mIcon"></i></button>
                <button type="button" class="btn btn-primary btn-icon"><i class="mIcon"></i></button>
            </div>
        </div> --}}

        <section class="inner_section white">
            <div class="shipment_items">

                @foreach ($products as $product)
                    <div>
                        <div class="image">
                            <img src="{{ $product->getPictureUrl() }}" alt="">
                        </div>
                        <div class="details">
                            <div class="title">
                                <h6>
                                    <a href="{{ route('products.view', [$locale, $product->id]) }}" class="colored">
                                        {{ $product->translate('name', $locale) }}
                                    </a>
                                </h6>
                            </div>
                            <p>{{ short_str($product->translate('description', $locale), 200) }}</p>
                        </div>
                    </div>
                @endforeach

            </div>
        </section>
    </main>
</div>
</div>
@endsection