@extends('site.layouts.main') 

@section('page_content')
<div class="container">
    <div class="breadcrumb">
        <ul class="breadcrumb">
            <li>
                <a href="#">Help Center</a>
            </li>
            <li>
                <a href="#">Help With My Orders</a>
            </li>
            <li>
                <span>Cancellation</span>
            </li>
        </ul>
        <div class="form_inline small">
            <div class="custom_field">
                <input type="text" class="custom_input styled no_icon" placeholder="Need quick help? Search here">
                <button type="button" class="btn btn-basic">Find</button>
            </div>
        </div>
    </div>
    <div class="main_with_sidebar m_t_40 right">
        <aside>
            <div class="block">
                <h6 class="block_title border">FREQUENTLY ASKED QUESTIONS</h6>
                <ul class="list">
                    <li><a href="#">Where is My Order? Where is My Order? Where is My Order?</a></li>
                    <li><a href="#">Returning a Product</a></li>
                    <li><a href="#">Placing Order on Souq. ...</a></li>
                    <li><a href="#">I Got a Counterfeit It ...</a></li>
                </ul>
                <a href="#" class="more">
                    < View all Questions</a>
            </div>
        </aside>
        <main>
            <article>
                <h4 class="article_title">Cancelling an Order</h4>
                <p>You can cancel your order/item only before it enters the shipping phase. The option to cancel an order/item
                    will cease to appear when the shipment has entered the shipping phase.<br>
                    <br> To cancel an item/order:<br> 1. Log in to your Souq.com account using your registered email address
                    and password.<br> 2. Click on your name displayed on the top right-hand corner of the page.<br> 3. Click
                    on <strong>My Orders</strong>.<br> 4. Locate the item you wish to cancel.<br> 5. Click on <strong>I No Longer Want This Item</strong>                    against the item you wish to cancel.<br>
                </p>
                <br>
                <br>
                <a href="#" class="btn btn-basic">Cancel an Item</a>
                <br>
                <br>
                <br>
                <p>
                    6. Select the desired reason for cancellation of the order/item from drop-down menu.
                    <br> 7. Click on <strong>Complete Cancellation</strong> to complete the cancellation request.
                    <br><br> Note: For purchases made with a credit card, the amount will be refunded to the same card within
                    5 to 7 working days after successful cancellation of the order.
                </p>
            </article>
        </main>
    </div>
</div>
@endsection