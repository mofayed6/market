@extends('site.layouts.main') 
@section('page_content')
<div class="page_header with_image white">
    <div class="container">
        <div class="details">
            <h1>Return Policy</h1>
            <p>Guaranteeing a satisfactory shopping experience</p>
            <ul>
                <li><a href="#">Make a Return</a></li>
                <li><a href="#">FAQs</a></li>
                <li><a href="#">Warranty</a></li>
            </ul>
        </div>
        <img src="{{ url('assets/site/images/big.jpg') }}" alt="">
    </div>
</div>
<div class="container">
    <div class="policy_features">
        <h4 class="normal">Enjoy shopping on Alfa with these great return policy features:</h4>
        <div class="side_boxes white m_t_20 m_b_40">


            <div>
                <figure>
                    <img src="https://cms.souqcdn.com/spring/cms/en/global/return/img1.png" alt="" height="80" width="80">
                </figure>
                <h5 class="normal m_t_10 m_b_10">15 Days Returns</h5>
                <p>You have 15 days to make a refund request after your order has been delivered.</p>
            </div>


        </div>
        <h4 class="normal">How does Return Policy Work?</h4>
        <div class="side_boxes line_through m_t_20">


            <div>
                <figure>
                    <img src="https://cms.souqcdn.com/spring/cms/en/global/return/box1.png" alt="" height="80" width="80">
                </figure>
                <h5 class="normal m_t_20 m_b_20">15 Days Returns</h5>
                <p>You have 15 days to make a refund request after your order has been delivered.</p>
            </div>


        </div>
        <div class="line_separator m_t_60 m_b_60"></div>
        <h3 class="normal">Frequently Asked Questions (FAQs)</h3>
        <div class="faq no_margin full_width">
            <div class="container">
                <div class="questions">


                    <div class="no_border">
                        <h5><a href="#">Where is My Order?</a></h5>
                        <p>Your order will be delivered by the expected date specified on your order. The expected delivery
                            date is the date on or before which you can reasonably expect the item to arrive at your preferred
                            shipping address. Read More...</p>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection