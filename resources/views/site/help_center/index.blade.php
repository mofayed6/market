@extends('site.layouts.main') 
@section('page_content')
<div class="page_header">
    <div class="container">
        <h1>Hi Ahmed! How Can We Help?</h1>
        <div class="form_inline">
            <div class="custom_field">
                <input type="text" class="custom_input styled no_icon" placeholder="Need quick help? Search here">
                <button type="button" class="btn btn-basic">Find</button>
            </div>
        </div>
    </div>
</div>
<div class="help_categories">
    <div class="container">
        <div class="categories">
            <div>
                <a href="#">
                    <figure>
                        <img src="{{ url('assets/site/images/orders.svg') }}" alt="">
                    </figure>
                    <h5>Help With My Orders</h5>
                    <p>Orders delivery, tracking, payment and more</p>
                </a>
            </div>
            <div>
                <a href="#">
                    <figure>
                        <img src="{{ url('assets/site/images/refund.svg') }}" alt="">
                    </figure>
                    <h5>Returns And Refund</h5>
                    <p>Know how to return an item and get your money back</p>
                </a>
            </div>
            <div>
                <a href="#">
                    <figure>
                        <img src="{{ url('assets/site/images/account.svg') }}" alt="">
                    </figure>
                    <h5>My Account</h5>
                    <p>All account related help, account settings and addresses</p>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="faq">
    <div class="container">
        <h4>FREQUENTLY ASKED QUESTIONS</h4>
        <div class="questions">

            <div>
                <h5><a href="#">Where is My Order?</a></h5>
                <p>Your order will be delivered by the expected date specified on your order. The expected delivery date is
                    the date on or before which you can reasonably expect the item to arrive at your preferred shipping address.
                    Read More...</p>
            </div>

        </div>
    </div>
</div>
@endsection