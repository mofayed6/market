@extends('site.layouts.main') 
@section('page_content')
<div class="container">
    <h1 class="normal">How to Buy</h1>
    <p>Buying on souq.com is really easy. Navigation through categories, searching for specific products and product selection
        are simple. Where multi payment options are supported for all of your needs. Get started by following some simple
        steps: (<strong><a href="#" class="img_toggle">Hide/Show Images Below?</a></strong>)</p>
    <p class="m_t_10">
        <a href="#1" class="colored">[1] Search and explore products</a><br>
        <a href="#2" class="colored">[2] Learn about the product, and Add to cart</a><br>
        <a href="#3" class="colored">[3] Review your shopping cart, and Sign in to checkout</a><br>
        <a href="#4" class="colored">[4] Place your order</a><br>
        <a href="#5" class="colored">[5] Congrats! Track your order until it's delivered to your door</a><br>
    </p>
    <article>
        <h3><span class="title_number">1</span>Search and explore products</h3>
        <p>
            We have a wide range of fabulous products to choose from.
        </p>
        <ul class="list">
            <li class="m_b_20"><strong>Tip 1:</strong> If you're looking for a specific product, use the keyword search box located at the top
                of the site. Simply <strong>type</strong> what you are looking for, and prepare to be amazed!

                <a href="https://cms.souqcdn.com/spring/cms/en/global/buying/01.png" class="gallery-item"><img src="https://cms.souqcdn.com/spring/cms/en/global/buying/01.png" class="m_t_10 block"></a></li>
            <li class="m_b_20"><strong>Tip 2:</strong> If you want to explore a category of products, use the "Shop All Categories" menu, and
                <strong>navigate</strong> through your favorite categories where we'll feature the best products in each.
                <a href="https://cms.souqcdn.com/spring/cms/en/global/buying/02.png" class="gallery-item"><img id="hs" src="https://cms.souqcdn.com/spring/cms/en/global/buying/02.png" class="m_t_10 block"></a></li>
            <li class="m_b_20"><strong>Tip 3:</strong> Or just <strong>explore</strong> through our home page and prepare to be surprised by
                the latest and best deals in your city.
            </li>

            <li class="m_b_20"><strong>Tip 4:</strong> While exploring and searching, check out the the key info we display under each product.
                You may also use our various sorting methods, and the filters on left to narrow down the results to fit your
                needs - as per product features and price. <strong>Click on the product you want</strong> to learn more about
                and get ready to buy.
                <a href="https://cms.souqcdn.com/spring/cms/en/global/buying/02.png" class="gallery-item"><img id="hs" src="https://cms.souqcdn.com/spring/cms/en/global/buying/03.png" class="m_t_10 block"></a></li>
        </ul>
    </article>
    <h3><span class="title_number">2</span>Learn about the product, and Add to cart</h3>
    <p>Check out the product details, images, specs, videos, and customer reviews. We feature the best seller so <strong>click "Add to cart"</strong>        if the price and seller rating are convenient for you, or explore the "more offers" box for more buying options and
        further sellers and product conditions for all needs.</p>
    <a href="https://cms.souqcdn.com/spring/cms/en/global/buying/05.png" class="gallery-item"><img id="hs" src="https://cms.souqcdn.com/spring/cms/en/global/buying/05.png" class="m_t_10 block"></a>
</div>
@endsection