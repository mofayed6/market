@component('mail::message')

Welcome to Alfa Market,
Please Click the button below to activate your account

@component('mail::button', ['url' => url('email/reset/'.$token->remember_token)])
Reset Paasword
@endcomponent

Or Enter this code Manually : <b><code>{{$token->remember_token}}</code></b>

Thanks,<br>
Alfa Market
@endcomponent
