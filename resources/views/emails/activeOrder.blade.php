<!DOCTYPE html>
<html lang="ar">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="robots" content="index/follow">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="HandheldFriendly" content="true">

    <title>Email Template</title>
</head>

<body style="direction: rtl;font-family: 'Cairo', sans-serif; padding: 15px; margin: 15px; border: 1px solid #eaeaea;">

<link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">


<header style="padding: 20px 0 40px">
    <img src="logo.png" style="display: block; margin: auto; height: 100px">
</header>



<section style="padding: 20px; background: #f4f4f4;">
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane in active" id="details" aria-expanded="true">
            <div class="row clearfix">

                <div class="col-md-6 col-sm-6 text-left">
                    <p class="m-b-0"><strong>Order Date: </strong> {{$order['order']['created_at']}}</p>
                    <p class="m-b-0"><strong>Order Status: </strong> <span class="badge badge-warning m-b-0">
                @if($order['status']['status'] != null)
                    @if($order['status']['status'] == 1)
                        <span class="badge badge-success">Processing</span>
                    @elseif($order['status']['status'] == 2)
                        <span class="badge badge-warning">Delivering</span>
                    @elseif($order['status']['status'] == 3)
                        <span class="badge badge-danger">Delivered</span>
                    @elseif($order['status']['status'] == 4)
                        <span class="badge badge-default">Canceled</span>
                    @elseif($order['status']['status'] == 5)
                        <span class="badge badge-info">Pending</span>
                    @else
                        <span></span>
                    @endif

                 @endif

                        </span></p>
                    <p><strong>Order ID: </strong> #{{$order['order']['id']}}</p>
                </div>
            </div>
            <div class="row clearfix">2
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th>barcode</th>
                                <th class="hidden-sm-down">quantity</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($order['porducts'] as $porduct)
                            <tr>
                                <td>{{$porduct['order_id']}}</td>
                                <td>{{$porduct['product']['name']}}</td>
                                <td class="hidden-sm-down"><pre>{!! str_replace('\"', '"', json_encode($porduct['product']['barcode'], JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT)) !!}</pre></td>
                                <td>{{$porduct['quantity']}}</td>
                                <td class="hidden-sm-down">
                                    @if($porduct['discount_price'] == null)
                                        <h6>{{$porduct['price']}} EG</h6>
                                    @else
                                        <h6>{{$porduct['discount_price']}} EG</h6>
                                    @endif</td>

                            </tr>
                             @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row clearfix">
                <div class="col-md-6 text-right">


                    <h3 class="m-b-0 m-t-10">{{$order['price']}}</h3>
                </div>


        </div>

    </div>
</section>

<footer style="text-align: center; border-top: 1px solid #eaeaea; padding: 30px 0 15px">
    <p style="margin: 0">جميع الحقوق محفوظة لموقع <a href="#"> </a></p>

    <ul style="list-style: none; padding: 0; margin: 25px 0 0">
        <li style="display: inline-block; vertical-align: middle; padding: 0 4px">
            <a href="#" style="display: block">
                <img src="https://facebookbrand.com/wp-content/themes/fb-branding/prj-fb-branding/assets/images/fb-art.png" style="height: 30px">
            </a>
        </li>
        <li style="display: inline-block; vertical-align: middle; padding: 0 4px">
            <a href="#" style="display: block">
                <img src="https://upload.wikimedia.org/wikipedia/de/thumb/9/9f/Twitter_bird_logo_2012.svg/1259px-Twitter_bird_logo_2012.svg.png" style="height: 30px">
            </a>
        </li>
        <li style="display: inline-block; vertical-align: middle; padding: 0 4px">
            <a href="#" style="display: block">
                <img src="https://botw-pd.s3.amazonaws.com/styles/logo-thumbnail/s3/012012/google_plus-01.png?itok=4R35nkQu" style="height: 30px">
            </a>
        </li>
    </ul>
</footer>

</body>
</html>













