<?php
namespace DIT\App\EntityProcessors;

use DIT\App\FileProcessors\Alfa1FileProcessor;
use DIT\App\EntityProcessors\Traits\HasWebImage;

class ProductEntityProcessor extends AlfaEntityProcessor
{
    use HasWebImage;
    public const INTERNAL_ID_COLUMN = 'E';

    public const FILES_DIR = __DIR__ . '/../../../storage/app/public/products/';

    public static $requiredColumns = [
        'E' => 'Model',
        'B' => 'Language',
        'M' => 'Product Name',
        'P' => 'Description',
        'R' => 'Price',
        'Y' => 'Image(Main image)',
    ];

    public static $dbColumns = [
        'E' => 'internal_id',
        'R' => 'price',
    ];

    public static $updateDbColumns = ['price'];

    /**
     * Data processing
     * 
     * @return array
     */
    public static function dataProcessing(array $returnedData, array $rowData, array $allRows) : array
    {
        $primaryKey = self::INTERNAL_ID_COLUMN;
        $barcode = self::processBarcode($rowData);
        if (!empty($barcode)) {
            $returnedData['barcode'] = json_encode($barcode, JSON_UNESCAPED_UNICODE);
        }

        $returnedData['name'] = self::getTranslation('M', $primaryKey, $rowData[$primaryKey], $allRows);
        $returnedData['description'] = self::getTranslation('P', $primaryKey, $rowData[$primaryKey], $allRows);
        $returnedData['featured_image'] = 'products/' . self::saveImageFromWeb($rowData['Y'], self::FILES_DIR);

        return $returnedData;
    }

    public static function getInternalId(array $rowData) : string
    {
        return trim($rowData[self::INTERNAL_ID_COLUMN]);
    }

    private static function processBarcode(array $rowData) : array
    {
        $barcode = [];
        $barcodeElements = [
            'F' => 'SKU', 'G' => 'UPC', 'H' => 'EAN', 'I' => 'JAN', 'J' => 'ISBN', 'K' => 'MPN'
        ];

        foreach ($barcodeElements as $columnKey => $columnName) {
            if (!empty($rowData[$columnKey])) {
                $barcode[$columnName] = $rowData[$columnKey];
            }
        }

        return $barcode;
    }
}
