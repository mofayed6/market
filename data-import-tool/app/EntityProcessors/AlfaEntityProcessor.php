<?php
namespace DIT\App\EntityProcessors;

use DataImportTool\EntityProcessor;
use DIT\App\Helper;
use DataImportTool\Exceptions\EntityProcessorException;
use DIT\App\FileProcessors\Alfa1FileProcessor;

class AlfaEntityProcessor extends EntityProcessor
{
    public static function getTranslation($propertyColumnKey, $idColumnKey, $idValue, $allRows)
    {
        $property = [];
        $whereConditions = [$idColumnKey => $idValue];
        $property['en'] = Alfa1FileProcessor::getColumnTranslation($allRows, 'en', $propertyColumnKey, $whereConditions);
        $property['ar'] = Alfa1FileProcessor::getColumnTranslation($allRows, 'ar', $propertyColumnKey, $whereConditions);
        if (empty($property['ar'])) {
            $property['ar'] = $property['en'];
        }

        return json_encode($property, JSON_UNESCAPED_UNICODE);
    }

}
