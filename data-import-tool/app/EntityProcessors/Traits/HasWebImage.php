<?php

namespace DIT\App\EntityProcessors\Traits;

use DataImportTool\Exceptions\EntityProcessorException;
use DIT\App\Helper;


trait HasWebImage
{
    /**
     * grab the image, save it, return the filename (without the path)
     * @throws \DataImportTool\Exceptions\EntityProcessorException
     * @return string $filename
     */
    public static function saveImageFromWeb(string $imageUrl, string $destinationDirectory) : string
    {
        if (!is_dir($destinationDirectory)) {
            throw new EntityProcessorException("Destination directory does not exist!" . PHP_EOL . "Directory: {$destinationDirectory}");
        }

        if (mb_substr($destinationDirectory, -1, 1) != '/') {
            $destinationDirectory .= '/';
        }

        $imageExt = Helper::cutStringUsingLast('.', $imageUrl, 'right', false);
        if (empty($imageExt)) {
            $imageExt = 'jpg';
        }

        do {
            $filename = Helper::getRandomStr(30) . '.' . $imageExt;
        } while (file_exists($filename));

        $destinationFilePath = $destinationDirectory . $filename;
        Helper::downloadFile($imageUrl, $destinationFilePath);

        usleep(500);
        if (!is_file($destinationFilePath)) {
            throw new EntityProcessorException("Getting image failed!" . PHP_EOL . "Url: {$imageUrl}");
        }

        return $filename;
    }
}