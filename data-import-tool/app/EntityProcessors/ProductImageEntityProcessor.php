<?php
namespace DIT\App\EntityProcessors;

use DIT\App\EntityProcessors\Traits\HasWebImage;
use DataImportTool\Exceptions\EntityProcessorException;
use DIT\App\Helper;


class ProductImageEntityProcessor extends AlfaEntityProcessor
{
    use HasWebImage;
    public const FILES_DIR = __DIR__ . '/../../../storage/app/public/products/';

    public static $requiredColumns = [
        'AQ' => '(image1;image2;image3)',
    ];

    /**
     * Data processing
     * 
     * @return array
     */
    public static function dataProcessing(array $returnedData, array $rowData, array $allRows) : array
    {
        $imagesUrls = self::extractListFromString($rowData['AQ']);

        foreach ($imagesUrls as $url) {
            try {
                $localPath = self::saveImageFromWeb($url, self::FILES_DIR);
            } catch (EntityProcessorException $e) {
                continue;
            }

            $returnedData[] = [
                'sheet_url' => $url,
                'image' => $localPath,
            ];
        }

        return $returnedData;
    }


    public static function extractListFromString(string $string) : array
    {
        $list = [$string];
        if (mb_strpos($string, ';') !== false) {
            $list = explode(';', $string);
        }

        $list = Helper::trimArray($list);
        return $list;
    }
}
