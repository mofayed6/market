<?php
namespace DIT\App\EntityProcessors;

use DIT\App\FileProcessors\Alfa1FileProcessor;
use DIT\App\Helper;

class AttributeEntityProcessor extends AlfaEntityProcessor
{
    public static $requiredColumns = [
        'BD' => 'Attributes',
    ];

    public static $dbColumns = [];

    /**
     * Data processing
     * 
     * @return array returns attributes "parent first, then its child, then its grandchild..." without the parent_id column
     */
    public static function dataProcessing(array $returnedData, array $rowData, array $allRows) : array
    {
        $attributesNamesEn = self::getNamesList('en', $rowData, $allRows);
        $attributesNamesAr = self::getNamesList('ar', $rowData, $allRows);
        $attributesValuesEn = self::getValuesList('en', $rowData, $allRows);
        $attributesValuesAr = self::getValuesList('ar', $rowData, $allRows);

        for ($i = 0; $i < count($attributesNamesEn); ++$i) {
            $returnedData[] = [
                'attribute' => [
                    'name' => json_encode([
                        'ar' => $attributesNamesAr[$i], 'en' => $attributesNamesEn[$i]
                    ], JSON_UNESCAPED_UNICODE),
                ],
                'value' => json_encode([
                    'ar' => $attributesValuesAr[$i], 'en' => $attributesValuesEn[$i]
                ], JSON_UNESCAPED_UNICODE),
            ];
        }

        return $returnedData;
    }

    public static function getNamesList(string $locale, array $rowData, array $allRows) : array
    {
        $primaryKey = Alfa1FileProcessor::PRIMARY_COLUMN_KEY;

        $attributesBothLangs = json_decode(self::getTranslation('BD', $primaryKey, $rowData[$primaryKey], $allRows), true);

        return self::extractListFromString($attributesBothLangs[$locale], 0);
    }

    public static function getValuesList(string $locale, array $rowData, array $allRows) : array
    {
        $primaryKey = Alfa1FileProcessor::PRIMARY_COLUMN_KEY;

        $attributesBothLangs = json_decode(self::getTranslation('BD', $primaryKey, $rowData[$primaryKey], $allRows), true);

        return self::extractListFromString($attributesBothLangs[$locale], 1);
    }

    public static function extractListFromString(string $string, int $elementToReturn) : array
    {
        $list = [];
        $attributesPairs = [$string];

        if (mb_strpos($string, ';') !== false) {
            $attributesPairs = explode(';', $string);
        }

        $attributesPairs = Helper::trimArray($attributesPairs);

        foreach ($attributesPairs as $attributePair) {
            $attrName = $attributePair;
            if (mb_strpos($attributePair, ':') !== false) {
                $attributePairParts = explode(':', $attributePair);
                $list[] = $attributePairParts[$elementToReturn];
            }
        }

        return $list;
    }
}
