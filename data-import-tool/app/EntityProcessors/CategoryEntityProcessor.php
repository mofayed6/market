<?php
namespace DIT\App\EntityProcessors;

use DIT\App\FileProcessors\Alfa1FileProcessor;
use DIT\App\Helper;

class CategoryEntityProcessor extends AlfaEntityProcessor
{
    public const INTERNAL_ID_COLUMN = 'AK';

    public static $requiredColumns = [
        'AK' => 'Categories id',
        'AL' => 'Categories (category>subcategory;)',
    ];

    public static $dbColumns = [];

    /**
     * Data processing
     * 
     * @return array returns categories "parent first, then its child, then its grandchild..." without the parent_id column
     */
    public static function dataProcessing(array $returnedData, array $rowData, array $allRows) : array
    {
        $primaryKey = Alfa1FileProcessor::PRIMARY_COLUMN_KEY;

        $categoriesNamesBothLangs = json_decode(
            self::getTranslation('AL', $primaryKey, $rowData[$primaryKey], $allRows),
            true
        );

        $categoriesIds = self::extractListFromString($rowData[self::INTERNAL_ID_COLUMN]);
        $categoriesNamesEn = self::extractListFromString($categoriesNamesBothLangs['en']);
        $categoriesNamesAr = self::extractListFromString($categoriesNamesBothLangs['ar']);

        for ($i = 0; $i < count($categoriesIds); ++$i) {
            $returnedData[] = [
                'name' => json_encode(['ar' => $categoriesNamesAr[$i], 'en' => $categoriesNamesEn[$i]], JSON_UNESCAPED_UNICODE),
                'slug' => Helper::slugify($categoriesNamesEn[$i]),
                'description' => json_encode(['ar' => $categoriesNamesAr[$i], 'en' => $categoriesNamesEn[$i]], JSON_UNESCAPED_UNICODE),
                'internal_id' => $categoriesIds[$i]
            ];
        }


        return $returnedData;
    }

    public static function getInternalId(array $rowData) : string
    {
        $columnValue = $rowData[self::INTERNAL_ID_COLUMN];
        if (mb_strpos($columnValue, ';') !== false) {
            $columnParts = explode(';', $columnValue);
            $columnValue = $columnParts[0];
        }

        if (mb_strpos($columnValue, '>') !== false) {
            $columnParts = explode('>', $columnValue);
            $columnValue = $columnParts[count($columnParts) - 1];
        }

        $internalId = trim($columnValue);

        return $internalId;
    }

    /**
     * returns the parent first, then its child, then its grandchild ...
     */
    public static function extractListFromString(string $string) : array
    {
        if (mb_strpos($string, ';') !== false) {
            $columnParts = explode(';', $string);
            $string = $columnParts[0];
        }

        $list = [$string];

        if (mb_strpos($string, '>') !== false) {
            $list = explode('>', $string);
        }

        $list = Helper::trimArray($list);

        return $list;
    }
}
