<?php
namespace DIT\App\EntityProcessors;

use DIT\App\Helper;


class BranchProductEntityProcessor extends AlfaEntityProcessor
{
    public static $requiredColumns = [
        'D' => 'Stores id',
        'S' => 'Quantity',
    ];

    public static $dbColumns = [];
    public static $updateDbColumns = ['stock_amount'];

    /**
     * Data processing
     * 
     * @return array
     */
    public static function dataProcessing(array $returnedData, array $rowData, array $allRows) : array
    {
        $branchesIds = BranchEntityProcessor::getInternalIds($rowData);
        $stockAmounts = self::extractListFromString($rowData['S']);

        for ($i = 0; $i < count($branchesIds); ++$i) {
            $returnedData[] = [
                'stock_amount' => $stockAmounts[$i],
                'internal_id' => $branchesIds[$i],
            ];
        }

        return $returnedData;
    }

    /**
     * @return int
     */
    public static function getDefaultStockAmount(array $rowData) : int
    {
        $string = $rowData['S'];
        $list = [$string];
        if (mb_strpos($string, ';') !== false) {
            $list = explode(';', $string);
        }

        $list = Helper::trimArray($list);

        return intval($list[0]);
    }

    public static function extractListFromString(string $string) : array
    {
        $list = [$string];
        if (mb_strpos($string, ';') !== false) {
            $list = explode(';', $string);
        }

        // to remove "default"
        unset($list[0]);

        $list = Helper::trimArray($list);

        return array_values($list);
    }
}
