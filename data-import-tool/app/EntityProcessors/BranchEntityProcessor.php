<?php
namespace DIT\App\EntityProcessors;

use DIT\App\FileProcessors\Alfa1FileProcessor;
use DIT\App\Helper;


class BranchEntityProcessor extends AlfaEntityProcessor
{
    public const INTERNAL_ID_COLUMN = 'D';

    public static $requiredColumns = [
        'C' => 'Stores',
        'D' => 'Stores id',
    ];

    public static $dbColumns = [];

    /**
     * Data processing
     * 
     * @return array returns branches
     */
    public static function dataProcessing(array $returnedData, array $rowData, array $allRows) : array
    {
        $primaryKey = Alfa1FileProcessor::PRIMARY_COLUMN_KEY;

        $branchesNamesBothLangs = self::getTranslation('C', $primaryKey, $rowData[$primaryKey], $allRows);

        $branchesIds = self::getInternalIds($rowData);
        $branchesNamesEn = self::extractListFromString($branchesNamesBothLangs['en']);
        $branchesNamesAr = self::extractListFromString($branchesNamesBothLangs['ar']);

        for ($i = 0; $i < count($branchesIds); ++$i) {
            $returnedData[] = [
                'name' => json_encode(['ar' => $branchesNamesAr[$i], 'en' => $branchesNamesEn[$i]], JSON_UNESCAPED_UNICODE),
                'internal_id' => $branchesIds[$i]
            ];
        }

        return $returnedData;
    }

    public static function getInternalIds(array $rowData) : array
    {
        $columnValue = $rowData[self::INTERNAL_ID_COLUMN];
        $internalIds = [$columnValue];

        if (mb_strpos($columnValue, ';') !== false) {
            $internalIds = explode(';', $columnValue);
        }

        // to remove "default"
        unset($internalIds[0]);

        $internalIds = Helper::trimArray($internalIds);

        return array_values($internalIds);
    }

    public static function getTranslation($propertyColumnKey, $idColumnKey, $idValue, $allRows) : array
    {
        $branchesNamesBothLangs = json_decode(
            parent::getTranslation($propertyColumnKey, $idColumnKey, $idValue, $allRows),
            true
        );

        return $branchesNamesBothLangs;
    }

    public static function extractListFromString(string $string) : array
    {
        $list = [$string];
        if (mb_strpos($string, ';') !== false) {
            $list = explode(';', $string);
        }

        // to remove "default"
        unset($list[0]);

        $list = Helper::trimArray($list);

        return array_values($list);
    }
}
