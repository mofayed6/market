<?php
namespace DIT\App\Handlers;

use PDO;
use DIT\App\Traits\Loggable;
use Tightenco\Collect\Support\Collection;
use DIT\App\DatabaseModels\Category;
use DIT\App\FileProcessors\Alfa1FileProcessor;
use DIT\App\EntityProcessors\CategoryEntityProcessor;
use DataImportTool\Exceptions\DataImportToolException;

class CategoryHandler
{
    use Loggable;

    /**
     * @return \Tightenco\Collect\Support\Collection all categories in the sheet
     */
    public static function handleCategories(PDO $dbConnection, array $allRows) : Collection
    {
        $allCategories = [];
        $allCategoriesInternalIds = [];
        $oldCategoriesCollection = Collection::make(Category::get($dbConnection));

        if (!empty($allRows)) {
            foreach ($allRows as $index => $row) {
                if (Alfa1FileProcessor::getRowLanguage($row) != 'en') {
                    continue;
                }

                $categoryParentId = null;

                try {
                    CategoryEntityProcessor::checkRequired($row, $index + 1);

                    $categoriesList = CategoryEntityProcessor::prepareForDb($row, $allRows);
                    if (!empty($categoriesList)) {
                        foreach ($categoriesList as $cat) {
                            if ($categoryParentId !== null) {
                                $cat['parent_id'] = $categoryParentId;
                                $categoryParentId = null;
                            }

                            $catExist = $oldCategoriesCollection->where('internal_id', '=', $cat['internal_id'])->first();

                            if (!empty($catExist)) {
                                $categoryParentId = $catExist['id'];
                            } else {
                                Category::create($dbConnection, [$cat]);
                                $category = Category::get($dbConnection, "internal_id = '{$cat["internal_id"]}'")[0];
                                $oldCategoriesCollection->push($category);

                                $categoryParentId = $category['id'];
                            }

                            $allCategoriesInternalIds[] = $cat['internal_id'];
                        }
                    }

                } catch (DataImportToolException $e) {
                    self::log($e->getMessage());
                }
            }
        }

        return !empty($allCategoriesInternalIds) ? Category::getCollectionFromDb($dbConnection, $allCategoriesInternalIds) : Collection::make([]);
    }
}
