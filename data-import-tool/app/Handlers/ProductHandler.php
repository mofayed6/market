<?php
namespace DIT\App\Handlers;

use PDO;
use DIT\App\Traits\Loggable;
use Tightenco\Collect\Support\Collection;
use DIT\App\FileProcessors\Alfa1FileProcessor;
use DataImportTool\Exceptions\EntityProcessorException;
use DIT\App\DatabaseModels\Product;
use DIT\App\EntityProcessors\BranchProductEntityProcessor;
use DIT\App\DatabaseModels\Branch;
use DIT\App\DatabaseModels\BranchProduct;
use DIT\App\EntityProcessors\ProductEntityProcessor;
use DIT\App\DatabaseModels\Category;
use DIT\App\EntityProcessors\CategoryEntityProcessor;
use DataImportTool\Exceptions\DatabaseModelException;
use DIT\App\DatabaseModels\ProductImage;
use DIT\App\EntityProcessors\ProductImageEntityProcessor;

class ProductHandler
{
    use Loggable;

    /**
     * @return \Tightenco\Collect\Support\Collection only newly created products
     */
    public static function handleProducts(
        PDO $dbConnection,
        array $allRows,
        Collection $categoriesCollection,
        Collection $branchesCollection
    ) : Collection {
        $allProducts = [];
        if (!empty($allRows)) {
            foreach ($allRows as $index => $row) {
                if (Alfa1FileProcessor::getRowLanguage($row) != 'en') {
                    continue;
                }

                try {
                    $productData = self::prepareProduct($index, $row, $allRows, $categoriesCollection);
                    $allProducts[] = $productData;
                } catch (EntityProcessorException $e) {
                    self::log($e->getMessage());
                }
            }
        }

        $toBeCreatedProducts = self::getToBeCreatedProducts($dbConnection, $allProducts);
        if (!empty($toBeCreatedProducts)) {
            self::createProducts($dbConnection, $toBeCreatedProducts);
        }

        $toBeUpdatedProducts = self::getToBeUpdatedProducts($dbConnection, $allProducts);
        if (!empty($toBeUpdatedProducts)) {
            self::updateProducts($dbConnection, $allRows, $toBeUpdatedProducts, $branchesCollection);
        }

        return !empty($toBeCreatedProducts) ? Product::getCollectionFromDb($dbConnection, array_column($toBeCreatedProducts, 'internal_id')) : Collection::make([]);
    }

    public static function handleBranchesProducts(PDO $dbConnection, array $allRows, Collection $productsCollection, Collection $branchesCollection)
    {
        $toBeCreatedBranchesProducts = [];
        try {
            foreach ($allRows as $row) {
                if (Alfa1FileProcessor::getRowLanguage($row) != 'en') {
                    continue;
                }

                $definedBranchesProductsDbIds = [];

                $branchesWithStocks = BranchProductEntityProcessor::prepareForDb($row, $allRows);
                foreach ($branchesWithStocks as $branchWithStock) {
                    $branchProductData = [
                        'stock_amount' => $branchWithStock['stock_amount'],
                        'branch_id' => Branch::getDbIdByInternalId($branchesCollection, $branchWithStock['internal_id']),
                        'product_id' => Product::getDbIdByInternalId($productsCollection, ProductEntityProcessor::getInternalId($row)),
                    ];

                    $branchProductExist = BranchProduct::get($dbConnection, "branch_id = {$branchProductData['branch_id']} AND product_id = {$branchProductData['product_id']}");

                    if (!empty($branchProductExist)) {
                        $definedBranchesProductsDbIds[] = $branchProductExist[0]['id'];
                        BranchProduct::update($dbConnection, $branchProductData, $branchProductExist[0]['id']);
                    } else {
                        $toBeCreatedBranchesProducts[] = $branchProductData;
                    }
                }

                self::handleBranchesProductDefaultValue(
                    $dbConnection,
                    BranchProductEntityProcessor::getDefaultStockAmount($row),
                    $branchProductData['product_id'],
                    $definedBranchesProductsDbIds
                );
            }
        } catch (EntityProcessorException $e) {
            self::log($e->getMessage());
        }

        if (!empty($toBeCreatedBranchesProducts)) {
            try {
                BranchProduct::create($dbConnection, $toBeCreatedBranchesProducts);
            } catch (DatabaseModelException $e) {
                self::log($e->getMessage());
            }
        }
    }

    public static function handleProductsImages(PDO $dbConnection, array $allRows, Collection $productsCollection)
    {
        $allProductsImages = [];
        if (!empty($allRows)) {
            foreach ($allRows as $index => $row) {
                if (Alfa1FileProcessor::getRowLanguage($row) != 'en') {
                    continue;
                }

                try {
                    ProductImageEntityProcessor::checkRequired($row, $index + 1);
                    $productImagesData = ProductImageEntityProcessor::prepareForDb($row, $allRows);
                    foreach ($productImagesData as $imageData) {
                        $imageData['product_id'] = Product::getDbIdByInternalId(
                            $productsCollection,
                            ProductEntityProcessor::getInternalId($row)
                        );

                        $imgExist = ProductImage::get($dbConnection, "sheet_url = '{$imageData['sheet_url']}' AND product_id = {$imageData['product_id']}");
                        if (empty($imgExist)) {
                            $allProductsImages[] = $imageData;
                        }
                    }
                } catch (EntityProcessorException $e) {
                    self::log($e->getMessage());
                }
            }
        }

        try {
            ProductImage::create($dbConnection, $allProductsImages);
        } catch (DatabaseModelException $e) {
            self::log($e->getMessage());
        }
    }

    private static function createProducts(PDO $dbConnection, array $toBeCreatedProducts) : void
    {
        try {
            Product::create($dbConnection, $toBeCreatedProducts);
        } catch (DatabaseModelException $e) {
            self::log($e->getMessage());
        }
    }

    private static function updateProducts(
        PDO $dbConnection,
        array $allRows,
        array $toBeUpdatedProducts,
        Collection $branchesCollection
    ) : void {
        $toBeUpdatedProductsCollection = Product::getCollectionFromDb($dbConnection, array_column($toBeUpdatedProducts, 'internal_id'));
        $toBeUpdatedproductsInternalIds = [];

        foreach ($toBeUpdatedProducts as $product) {
            $toBeUpdatedproductsInternalIds[] = $product['internal_id'];

            foreach (ProductEntityProcessor::$updateDbColumns as $column) {
                $productData[$column] = $product[$column];
            }

            Product::update($dbConnection, $productData, Product::getDbIdByInternalId($toBeUpdatedProductsCollection, $product['internal_id']));
        }

        $toBeUpdatedProductsRows = Collection::make($allRows)->whereNotIn(
            'internal_id',
            $toBeUpdatedproductsInternalIds
        )->toArray();

        self::handleBranchesProducts($dbConnection, $toBeUpdatedProductsRows, $toBeUpdatedProductsCollection, $branchesCollection);
    }

    private static function prepareProduct(int $rowIndex, array $row, array $allRows, Collection $categoriesCollection) : array
    {
        ProductEntityProcessor::checkRequired($row, $rowIndex + 1);

        $productData = ProductEntityProcessor::prepareForDb($row, $allRows);
        $productData['category_id'] = Category::getDbIdByInternalId(
            $categoriesCollection,
            CategoryEntityProcessor::getInternalId($row)
        );

        return $productData;
    }

    private static function getToBeCreatedProducts(PDO $dbConnection, array $products) : array
    {
        $productsCollection = Collection::make($products);

        $oldProductsInternalIds = Product::getCollectionFromDb($dbConnection, array_column($products, 'internal_id'))
            ->pluck('internal_id')->toArray();

        return $productsCollection->whereNotIn(
            'internal_id',
            $oldProductsInternalIds
        )->toArray();
    }

    private static function getToBeUpdatedProducts(PDO $dbConnection, array $products) : array
    {
        $productsCollection = Collection::make($products);

        $oldProductsInternalIds = Product::getCollectionFromDb($dbConnection, array_column($products, 'internal_id'))
            ->pluck('internal_id')->toArray();

        return $productsCollection->whereIn(
            'internal_id',
            $oldProductsInternalIds
        )->toArray();
    }

    private static function handleBranchesProductDefaultValue(PDO $dbConnection, int $defaultStockAmount, int $productDbId, array $definedBranchesProductsDbIds) : void
    {
        $whereNotInIds = !empty($definedBranchesProductsDbIds) ? " AND id NOT IN(" . implode(',', $definedBranchesProductsDbIds) . ")" : '';
        BranchProduct::updateWhere(
            $dbConnection,
            ['stock_amount' => $defaultStockAmount],
            "product_id = {$productDbId} " . $whereNotInIds
        );
    }
}
