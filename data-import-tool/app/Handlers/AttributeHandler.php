<?php
namespace DIT\App\Handlers;

use PDO;
use DIT\App\Traits\Loggable;
use Tightenco\Collect\Support\Collection;
use DIT\App\DatabaseModels\Attribute;
use DIT\App\FileProcessors\Alfa1FileProcessor;
use DIT\App\EntityProcessors\AttributeEntityProcessor;
use DataImportTool\Exceptions\DatabaseModelException;
use DataImportTool\Exceptions\DataImportToolException;
use DIT\App\DatabaseModels\AttributeCategory;
use DIT\App\DatabaseModels\Category;
use DIT\App\EntityProcessors\CategoryEntityProcessor;
use DIT\App\DatabaseModels\Product;
use DIT\App\EntityProcessors\ProductEntityProcessor;
use DIT\App\DatabaseModels\AttributeProduct;

class AttributeHandler
{
    use Loggable;

    public static function handleAttributes(PDO $dbConnection, array $allRows, Collection $categoriesCollection, Collection $productsCollection) : void
    {
        if (!empty($allRows)) {
            foreach ($allRows as $index => $row) {
                if (Alfa1FileProcessor::getRowLanguage($row) != 'en') {
                    continue;
                }

                try {
                    AttributeEntityProcessor::checkRequired($row, $index + 1);

                    $attributeList = AttributeEntityProcessor::prepareForDb($row, $allRows);
                    if (!empty($attributeList)) {
                        $attributesProducts = [];
                        $productDbId = Product::getDbIdByInternalId($productsCollection, ProductEntityProcessor::getInternalId($row));

                        foreach ($attributeList as $attributeData) {
                            $attrName = json_decode($attributeData['attribute']['name'], true);
                            $attribute = Attribute::getByEnglishName($dbConnection, $attrName['en']);

                            if (empty($attribute)) {
                                Attribute::create($dbConnection, [$attributeData['attribute']]);
                                $attribute = Attribute::getByEnglishName($dbConnection, $attrName['en']);
                            }

                            if (empty($attribute)) {
                                throw new DatabaseModelException(
                                    "Attribute was not inserted in the database!"
                                        . PHP__EOL
                                        . "Attribute: {$attrName}"
                                );
                            }

                            $attributeDbId = $attribute['id'];

                            self::handleAttributeCategory($dbConnection, $row, $attributeDbId, $categoriesCollection);

                            $attributesProducts[] = [
                                'product_id' => $productDbId,
                                'attribute_id' => $attributeDbId,
                                'value' => $attributeData['value'],
                            ];
                        }

                        self::handleAttributesProduct($dbConnection, $attributesProducts);
                    }

                } catch (DataImportToolException $e) {
                    self::log($e->getMessage());
                }
            }
        }
    }

    private static function handleAttributeCategory(PDO $dbConnection, array $row, int $attributeDbId, Collection $categoriesCollection) : void
    {
        try {
            $attributeCategoryData = [
                'category_id' => Category::getDbIdByInternalId($categoriesCollection, CategoryEntityProcessor::getInternalId($row)),
                'attribute_id' => $attributeDbId,
            ];

            $attributeCategoryExist = AttributeCategory::get($dbConnection, "category_id = {$attributeCategoryData['category_id']} AND attribute_id = {$attributeCategoryData['attribute_id']}");

            if (empty($attributeCategoryExist)) {
                AttributeCategory::create($dbConnection, [$attributeCategoryData]);
            }
        } catch (DataImportToolException $e) {
            self::log($e->getMessage());
        }
    }

    private static function handleAttributesProduct(PDO $dbConnection, array $attributesProducts) : void
    {
        if (!empty($attributesProducts)) {
            foreach ($attributesProducts as $attributeProductData) {
                try {
                    $attributeProductExist = AttributeProduct::get($dbConnection, "product_id = {$attributeProductData['product_id']} AND attribute_id = {$attributeProductData['attribute_id']}");

                    if (empty($attributeProductExist)) {
                        AttributeProduct::create($dbConnection, [$attributeProductData]);
                    }
                } catch (DataImportToolException $e) {
                    self::log($e->getMessage());
                }
            }
        }
    }
}
