<?php
namespace DIT\App\Handlers;

use PDO;
use DIT\App\Traits\Loggable;
use Tightenco\Collect\Support\Collection;
use DIT\App\FileProcessors\Alfa1FileProcessor;
use DIT\App\EntityProcessors\BranchEntityProcessor;
use DataImportTool\Exceptions\EntityProcessorException;
use DIT\App\DatabaseModels\Branch;
use DataImportTool\Exceptions\DatabaseModelException;

class BranchHandler
{
    use Loggable;

    public static function handleBranches(PDO $dbConnection, array $allRows) : Collection
    {
        $allBranches = [];
        if (!empty($allRows)) {
            foreach ($allRows as $index => $row) {
                if (Alfa1FileProcessor::getRowLanguage($row) != 'en') {
                    continue;
                }

                try {
                    BranchEntityProcessor::checkRequired($row, $index + 1);
                    $branchesData = BranchEntityProcessor::prepareForDb($row, $allRows);

                    $allBranches += $branchesData;
                } catch (EntityProcessorException $e) {
                    self::log($e->getMessage());
                }
            }
        }

        if (!empty($allBranches)) {
            $toBeCreatedBranches = self::getToBeCreatedBranches($dbConnection, $allBranches);
        }

        if (!empty($toBeCreatedBranches)) {
            try {
                Branch::create($dbConnection, $toBeCreatedBranches);
            } catch (DatabaseModelException $e) {
                self::log($e->getMessage());
            }
        }

        return !empty($allBranches) ? Branch::getCollectionFromDb($dbConnection, array_column($allBranches, 'internal_id')) : Collection::make([]);
    }

    private static function getToBeCreatedBranches(PDO $dbConnection, array $branches) : array
    {
        $branchesCollection = Collection::make($branches);

        $oldBranchesInternalIds = Branch::getCollectionFromDb($dbConnection, array_column($branches, 'internal_id'))
            ->pluck('internal_id')->toArray();

        return $branchesCollection->whereNotIn(
            'internal_id',
            $oldBranchesInternalIds
        )->toArray();
    }

}
