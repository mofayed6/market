<?php
namespace DIT\App\DatabaseModels;

use DIT\App\DatabaseModels\Traits\HasInternalId;
use DataImportTool\DatabaseModel;

class Product extends DatabaseModel
{
    use HasInternalId;
    public static $table = 'products';
    public static $modelName = 'Product';
    public static $timestamped = true;
}
