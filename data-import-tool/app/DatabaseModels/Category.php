<?php
namespace DIT\App\DatabaseModels;

use DIT\App\DatabaseModels\Traits\HasInternalId;
use DataImportTool\DatabaseModel;

class Category extends DatabaseModel
{
    use HasInternalId;
    public static $table = 'categories';
    public static $modelName = 'Category';
    public static $timestamped = true;
}
