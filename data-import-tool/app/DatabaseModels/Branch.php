<?php
namespace DIT\App\DatabaseModels;

use DIT\App\DatabaseModels\Traits\HasInternalId;
use DataImportTool\DatabaseModel;

class Branch extends DatabaseModel
{
    use HasInternalId;
    public static $table = 'branches';
    public static $modelName = 'Branch';
    public static $timestamped = true;
}
