<?php
namespace DIT\App\DatabaseModels;

use DataImportTool\DatabaseModel;

class BranchProduct extends DatabaseModel
{
    public static $table = 'branches_product';
    public static $modelName = 'Product stock';
    public static $timestamped = true;
}
