<?php
namespace DIT\App\DatabaseModels;

use PDO;
use DataImportTool\DatabaseModel;

class Attribute extends DatabaseModel
{
    public static $table = 'attributes';
    public static $modelName = 'Attribute';
    public static $timestamped = true;

    public static function getByEnglishName(PDO $dbConnection, string $englishName) : array
    {
        $result = self::get($dbConnection, "JSON_CONTAINS(name, '{\"en\": \"$englishName\"}')");

        return !empty($result[0]) ? $result[0] : [];
    }
}
