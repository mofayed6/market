<?php
namespace DIT\App\DatabaseModels;

use DataImportTool\DatabaseModel;

class AttributeProduct extends DatabaseModel
{
    public static $table = 'attributes_product';
    public static $modelName = 'Product attribute';
    public static $timestamped = true;
}
