<?php
namespace DIT\App\DatabaseModels;

use DataImportTool\DatabaseModel;

class ProductImage extends DatabaseModel
{
    public static $table = 'product_images';
    public static $modelName = 'Product image';
    public static $timestamped = true;
}
