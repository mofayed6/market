<?php
namespace DIT\App\DatabaseModels\Traits;

use PDO;
use DataImportTool\Exceptions\EntityProcessorException;
use Tightenco\Collect\Support\Collection;


trait HasInternalId
{
    public static function getDbIdByInternalId(Collection $collection, string $internalId) : int
    {
        $filteredRows = $collection->where('internal_id', '=', $internalId);
        if (!empty($filteredRows) && $filteredRows->count()) {
            $row = $filteredRows->first();

            if (!empty($row)) {
                return $row['id'];
            }
        }

        throw new EntityProcessorException(static::$modelName . " does not exist in the database!: Internal id: {$internalId}");
    }

    public static function getCollectionFromDb(PDO $dbConnection, array $internalIds) : Collection
    {
        $internalIdsPlaceholders = implode(',', array_fill(0, count($internalIds), '?'));
        $whereClause = "internal_id IN ({$internalIdsPlaceholders})";
        $rows = self::get($dbConnection, $whereClause, $internalIds);

        return Collection::make($rows);
    }
}