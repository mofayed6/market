<?php
namespace DIT\App\DatabaseModels;

use DataImportTool\DatabaseModel;

class AttributeCategory extends DatabaseModel
{
    public static $table = 'category_attributes';
    public static $modelName = 'Category attribute';
    public static $timestamped = true;
}
