<?php
namespace DIT\App;

use PDO;
use Throwable;
use DataImportTool\Importer;
use DIT\App\FileProcessors\Alfa1FileProcessor;
use DataImportTool\Exceptions\DataImportToolException;
use DataImportTool\DatabaseConnection;
use DIT\App\Traits\Loggable;
use DIT\App\Handlers\ProductHandler;
use DIT\App\Handlers\CategoryHandler;
use DIT\App\Handlers\BranchHandler;
use DIT\App\Handlers\AttributeHandler;
use DataImportTool\Exceptions\DatabaseConnectionException;


class App
{
    use Loggable;

    public static function run(array $files) : void
    {
        try {
            $dbConnection = DatabaseConnection::getConnection(__DIR__ . '/db_config.php');

            foreach ($files as $fileType => $filePath) {
                try {
                    $extractedData = Importer::extractDataFromExcel($filePath, $fileType);
                    Alfa1FileProcessor::check($extractedData, $filePath);

                    $allRows = Alfa1FileProcessor::getRows($extractedData);

                    $branchesCollection = BranchHandler::handleBranches($dbConnection, $allRows);
                    $categoriesCollection = CategoryHandler::handleCategories($dbConnection, $allRows);

                    if (!empty($categoriesCollection) && $categoriesCollection->count()) {
                        $newProductsCollection = ProductHandler::handleProducts($dbConnection, $allRows, $categoriesCollection, $branchesCollection);
                    }

                    if (!empty($newProductsCollection) && $newProductsCollection->count()) {
                        ProductHandler::handleProductsImages($dbConnection, $allRows, $newProductsCollection);

                        if (!empty($branchesCollection) && $branchesCollection->count()) {
                            ProductHandler::handleBranchesProducts($dbConnection, $allRows, $newProductsCollection, $branchesCollection);
                        }

                        AttributeHandler::handleAttributes($dbConnection, $allRows, $categoriesCollection, $newProductsCollection);
                    }

                } catch (DataImportToolException $e) {
                    self::log($e->getMessage());
                }
            }

            DatabaseConnection::closeConnection($dbConnection);

        } catch (DatabaseConnectionException $e) {
            self::log($e->getMessage());
        } catch (Throwable $e) {
            self::log($e->getMessage());
        }
    }
}
