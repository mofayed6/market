<?php
namespace DIT\App;

class Helper
{
    public static function cutStringUsingLast(string $character, string $string, string $side, bool $keepCharacter = true) : string
    {
        $offset = ($keepCharacter ? 1 : 0);
        $wholeLength = strlen($string);
        $rightLength = (strlen(strrchr($string, $character)) - 1);
        $leftLength = ($wholeLength - $rightLength - 1);
        switch ($side) {
            case 'left':
                $piece = substr($string, 0, ($leftLength + $offset));
                break;
            case 'right':
                $start = (0 - ($rightLength + $offset));
                $piece = substr($string, $start);
                break;
            default:
                $piece = '';
                break;
        }

        return $piece;
    }

    public static function downloadFile(string $fileUrl, string $saveTo) : void
    {
        $in = fopen($fileUrl, "rb");
        $out = fopen($saveTo, "wb");

        while ($chunk = fread($in, 8192)) {
            fwrite($out, $chunk, 8192);
        }

        fclose($in);
        fclose($out);
    }

    public static function getRandomStr(int $length = 0) : string
    {
        return substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'), 0, $length);
    }

    public static function slugify(string $text) : string
    {
      // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    
      // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    
      // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
    
      // trim
        $text = trim($text, '-');
    
      // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);
    
      // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    public static function trimArray(array $array) : array
    {
        $returnedArray = [];
        foreach ($array as $key => $value) {
            if (!empty($value)) {
                $value = trim($value);
            }

            if (!empty($value) || $value === 0 || $value === '0') {
                $returnedArray[$key] = $value;
            }
        }

        return $returnedArray;
    }

}
