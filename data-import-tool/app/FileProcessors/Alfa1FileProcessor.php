<?php
namespace DIT\App\FileProcessors;

use DataImportTool\FileProcessor;

class Alfa1FileProcessor extends FileProcessor
{

    public const PRIMARY_COLUMN_KEY = 'E';

    public function __construct()
    {
        self::$requiredColumns = [
            'E' => 'Model',
            'B' => 'Language',
            'M' => 'Product Name',
            'P' => 'Description',
            'R' => 'Price',
        ];
    }

    public static function getColumnTranslation(array $allRows, string $locale, string $columnKey, array $whereConditions) : string
    {
        $localeColumnKey = 'B';
        $allRowsCollection = self::getRowsCollection($allRows);
        $filteredRows = $allRowsCollection->where($localeColumnKey, '=', $locale);

        foreach ($whereConditions as $column => $value) {
            $filteredRows = $filteredRows->where($column, '=', $value);
        }

        $row = $filteredRows->first();
        if (!empty($row)) {
            return $row[$columnKey];
        }

        return '';
    }

    public static function getRowLanguage(array $row) : string
    {
        $localeColumnKey = 'B';

        return $row[$localeColumnKey];
    }

}
