<?php
namespace DIT\App\Traits;

trait Loggable
{
    private static function log($logContent)
    {
        echo '<pre>' . var_export($logContent, true) . '</pre>';
    }
}
