<?php
namespace DataImportTool;

use PDO;
use PDOException;
use DataImportTool\Exceptions\DatabaseConnectionException;

class DatabaseConnection
{
    /**
     * @throws \DatabaseConnectionException
     */
    public static function getConnection(string $configPath) : PDO
    {
        if (!is_readable($configPath)) {
            throw new DatabaseConnectionException("Unable to read Database configuration file! File: {$configPath}");
        }

        $config = require $configPath;
        $requiredConfiguration = ['host', 'db', 'user', 'pass', 'charset'];
        foreach ($requiredConfiguration as $key) {
            if (empty($config[$key])) {
                throw new DatabaseConnectionException(
                    "Malformed database configuration file!" . PHP_EOL . "The file must return an array with these keys " . implode(',', $requiredConfiguration) . "." . PHP_EOL . " File: {$configPath}"
                );
            }
        }

        $dsn = "mysql:host={$config['host']};dbname={$config['db']};charset={$config['charset']}";
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];

        try {
            $connection = new PDO($dsn, $config['user'], $config['pass'], $options);
        } catch (PDOException $e) {
            throw new DatabaseConnectionException($e->getMessage(), (int)$e->getCode());
        }

        return $connection;
    }

    public static function closeConnection(PDO $connection) : void
    {
        // By default, PHP pass objects by reference
        $connection = null;
    }
}
