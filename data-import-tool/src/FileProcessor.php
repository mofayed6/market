<?php
namespace DataImportTool;

use Tightenco\Collect\Support\Collection;
use DataImportTool\Exceptions\FileProcessorException;


abstract class FileProcessor
{
    public static $requiredColumns = [];

    /**
     * In the constructor, Add the values of the static array: self::$requiredColumns
     * if there are any
     */
    abstract public function __construct();

    /**
     * Check required column
     * 
     * @throws \DataImportTool\Exceptions\FileProcessorException
     * 
     * @return bool true
     */
    public static function check(array $extractedData, string $filePath) : void
    {
        static::checkRequiredColumns($extractedData, $filePath);
        static::checkHasRows($extractedData, $filePath);
    }

    /**
     * @throws \DataImportTool\Exceptions\FileProcessorException
     */
    public static function checkRequiredColumns(array $extractedData, string $filePath) : void
    {
        $columnsKeys = static::getColumnsKeys($extractedData);

        $missingRequiredColumns = [];
        foreach (static::$requiredColumns as $requiredColumnKey => $requiredColumnName) {
            if (!in_array($requiredColumnKey, $columnsKeys)) {
                $missingRequiredColumns[] = $requiredColumnName;
            }
        }

        if (!empty($missingRequiredColumns)) {
            throw new FileProcessorException(
                "Missing required columns "
                    . implode(',', $missingRequiredColumns
                    . PHP_EOL
                    . "File: {$filePath}")
            );
        }
    }

    /**
     * @throws \DataImportTool\Exceptions\FileProcessorException
     */
    public static function checkHasRows(array $extractedData, string $filePath) : void
    {
        $allRows = static::getRows($extractedData);
        if (empty($allRows)) {
            throw new FileProcessorException("Empty sheet file!" . PHP_EOL . "File: {$filePath}");
        }
    }

    public static function getColumnsKeys(array $extractedSheetData) : array
    {
        return array_keys($extractedSheetData[1]);
    }

    public static function getColumnsNames(array $extractedSheetData) : array
    {
        return array_values($extractedSheetData[1]);
    }

    public static function getColumnsNamesWithKeys(array $extractedSheetData) : array
    {
        return $extractedSheetData[1];
    }

    public static function getRows(array $extractedSheetData) : array
    {
        unset($extractedSheetData[1]);
        return array_values($extractedSheetData);
    }

    public static function getRowsCollection(array $rows) : Collection
    {
        return Collection::make($rows);
    }

    public static function getRowsWithColumnNames(array $extractedSheetData) : array
    {
        $columns = static::getColumnsKeys($extractedSheetData);
        $rows = static::getRows($extractedSheetData);
        foreach ($rows as $key => $row) {
            foreach ($row as $k => $v) {
                unset($row[$k]);
                $row[$columns[$k]] = $v;
            }
            $rows[$key] = $row;
        }

        return $rows;
    }

}
