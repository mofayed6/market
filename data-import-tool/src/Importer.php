<?php
namespace DataImportTool;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use DataImportTool\Exceptions\DataImportToolException;

class Importer
{
    const COMPATIBLE_DATATYPES = ['xls', 'xlsx', 'xml', 'ods', 'slk', 'gnumeric', 'csv'];

    /**
     * @throws \DataImportTool\Exceptions\DataImportToolException
     */
    public static function extractDataFromExcel(string $inputFilename, string $inputFileType) : array
    {
        if (!in_array($inputFileType, self::COMPATIBLE_DATATYPES)) {
            throw new DataImportToolException('File type is not supported.');
        }

        try {
            /**  Create a new Reader of the type defined in $inputFileType  **/
            $reader = IOFactory::createReader(ucfirst($inputFileType));
        } catch (\Exception $e) {
            throw new DataImportToolException('File type is not supported.');
        }

        /**  Load $inputFilename to a Spreadsheet Object  **/
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($inputFilename);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        return $sheetData;
    }
}
