<?php
namespace DataImportTool;

use PDO;
use DataImportTool\Exceptions\DatabaseModelException;

class DatabaseModel
{
    public static $table = '';
    public static $modelName = '';
    public static $timestamped = false;

    /**
     * @throws \DataImportTool\Exceptions\DatabaseModelException
     */
    public static function create(PDO $dbConnection, array $rows) : void
    {
        $failed = [];
        foreach ($rows as $row) {
            if (static::$timestamped) {
                $row = static::addTimestampColumns($row);
            }

            $columns = '';
            foreach (array_keys($row) as $column) {
                if (!empty($columns)) {
                    $columns .= ',';
                }
                $columns .= '`' . $column . '`';
            }

            $placeholders = ':' . implode(', :', array_keys($row));
            $sql = "INSERT INTO " . static::$table . " ({$columns}) VALUES ({$placeholders})";
            try {
                $stmt = $dbConnection->prepare($sql);
                $stmt->execute($row);
            } catch (\PDOException $e) {
                echo $sql . PHP_EOL;
                $failed[] = $row;
            }
        }

        if (!empty($failed)) {
            throw new DatabaseModelException(json_encode($failed, JSON_UNESCAPED_UNICODE));
        }
    }

    public static function update(PDO $dbConnection, array $data, int $id) : void
    {
        $setValues = '';
        $parameters = [];

        foreach ($data as $column => $value) {
            if (!empty($setValues)) {
                $setValues .= ', ';
            }

            $setValues .= "`{$column}` = :{$column}";
            $parameters[] = $value;
        }

        $sql = "UPDATE " . static::$table . " SET {$setValues} WHERE id = {$id}";
        $stmt = $dbConnection->prepare($sql);
        $stmt->execute($parameters);
    }

    public static function updateWhere(PDO $dbConnection, array $data, string $whereClause = '', array $parameters = []) : void
    {
        $setValues = '';
        $parameters = [];

        foreach ($data as $column => $value) {
            if (!empty($setValues)) {
                $setValues .= ', ';
            }

            $setValues .= "`{$column}` = :{$column}";
            $parameters[] = $value;
        }

        try {
            if (empty($whereClause)) {
                $sql = "UPDATE " . static::$table . " SET {$setValues} ";
                $stmt = $dbConnection->prepare($sql);
                $stmt->execute();
            } else {
                $sql = "UPDATE " . static::$table . " SET {$setValues} WHERE {$whereClause}";
                $stmt = $dbConnection->prepare($sql);
                $stmt->execute($parameters);
            }
        } catch (\PDOException $e) {
            echo $sql . PHP_EOL;
        }
    }

    public static function get(PDO $dbConnection, string $whereClause = '', array $parameters = []) : array
    {
        try {
            if (empty($whereClause)) {
                $sql = "SELECT * FROM " . static::$table;
                $stmt = $dbConnection->prepare($sql);
                $stmt->execute();
            } else {
                $sql = "SELECT * FROM " . static::$table . " WHERE {$whereClause}";
                $stmt = $dbConnection->prepare($sql);
                $stmt->execute($parameters);
            }

            return $stmt->fetchAll();
        } catch (\PDOException $e) {
            echo $sql . PHP_EOL;
        }
    }

    /**
     * Add Timestamp Columns
     * 
     * @return array
     */
    public static function addTimestampColumns(array $row)
    {
        $row['created_at'] = $row['updated_at'] = date('Y-m-d H:i:s');

        return $row;
    }
}
