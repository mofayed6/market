<?php
namespace DataImportTool;

class EntityProcessor
{
    public static $requiredColumns = [];
    public static $dbColumns = [];

    /**
     * Check required data
     * 
     * @throws \DataImportTool\Exceptions\EntityProcessorException
     * 
     * @return bool true
     */
    public static function checkRequired(array $rowData, int $rowIndex) : bool
    {
        $missingRequiredDataColumns = [];
        foreach (static::$requiredColumns as $requiredColumnKey => $requiredColumnName) {
            if (empty($rowData[$requiredColumnKey])) {
                $missingRequiredDataColumns[] = $requiredColumnName;
            }
        }

        if (!empty($missingRequiredDataColumns)) {
            throw new EntityProcessorException(json_encode([
                'row_index' => $rowIndex,
                'missing_required_columns' => $missingRequiredDataColumns
            ], JSON_UNESCAPED_UNICODE));
        }

        return true;
    }

    /**
     * Get data
     * 
     * @return array
     */
    public static function prepareForDb(array $rowData, array $allRows) : array
    {
        $returnedData = [];

        foreach (static::$dbColumns as $sheetColumnKey => $dbColumnName) {
            $returnedData[$dbColumnName] = $rowData[$sheetColumnKey];
        }

        $returnedDataProcessed = static::dataProcessing($returnedData, $rowData, $allRows);

        return $returnedDataProcessed;
    }

    /**
     * Data pre processing
     * 
     * @return array
     */
    public static function dataProcessing(array $returnedData, array $rowData, array $allRows) : array
    {
        return $returnedData;
    }

}
